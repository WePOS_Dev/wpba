<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(empty($page_active)){
	$page_active = 'beranda';
}

?>
<!DOCTYPE html>
<!--[if IE 9]><html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-focus"> <!--<![endif]-->
    <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title><?php echo $program_name.' v'.$program_version; ?></title>
    <meta name="keywords" content="<?php echo $program_name.' v'.$program_version; ?>" />
    <meta name="description" content="<?php echo $program_name.' v'.$program_version; ?>">
    <meta name="author" content="<?php echo $program_author; ?>">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
	
	<!-- Icons -->
	<!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
	<link rel="shortcut icon" href="<?php echo ASSETS_URL; ?>img/favicons/favicon.png">

	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-192x192.png" sizes="192x192">

	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-180x180.png">
	<!-- END Icons -->
	
	<!-- Stylesheets -->
	<!-- Web fonts -->
	<link rel="stylesheet" href="<?php echo ASSETS_URL; ?>fonts/fonts.css">
	
	
	<!-- Custom CSS Libraries End -->
	<link href="<?php echo APP_URL; ?>libs/bootstrap-validator/css/bootstrapValidator.min.css" rel="stylesheet" type="text/css" />                
    <link href="<?php echo APP_URL; ?>libs/isotope/isotope_animation.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo APP_URL; ?>libs/datatables/jquery.dataTables.min.css">
	
	<?php
	if(!empty($add_css_page)){
		echo $add_css_page;
	}
	?>
	
	<link href="<?php echo APP_URL; ?>libs/bootstrap-fileinput-krajee/css/fileinput.min.css" rel="stylesheet" type="text/css" />
	
	<!-- OneUI CSS framework -->
	<link rel="stylesheet" id="css-main" href="<?php echo ASSETS_URL; ?>css/oneui.css">
	<link rel="stylesheet" id="css-main" href="<?php echo ASSETS_URL; ?>css/themes/city.min.css">
	
    <link href="<?php echo ASSETS_URL; ?>css/custom.css" rel="stylesheet" type="text/css" />
		
	<script>
		var appUrl 		= "<?php echo site_url(); ?>";
		var programName	= "<?php echo config_item('program_name'); ?>";
		var copyright	= "<?php echo config_item('copyright'); ?>";
		var wepos_srv	= "<?php echo config_item('wepos_srv'); ?>";
	</script>
	
	
</head>
    <body>
	<!-- Page Container -->
	<!--
		Available Classes:

		'sidebar-l'                  Left Sidebar and right Side Overlay
		'sidebar-r'                  Right Sidebar and left Side Overlay
		'sidebar-mini'               Mini hoverable Sidebar (> 991px)
		'sidebar-o'                  Visible Sidebar by default (> 991px)
		'sidebar-o-xs'               Visible Sidebar by default (< 992px)

		'side-overlay-hover'         Hoverable Side Overlay (> 991px)
		'side-overlay-o'             Visible Side Overlay by default (> 991px)

		'side-scroll'                Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (> 991px)

		'header-navbar-fixed'        Enables fixed header
	-->
	<div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
		

		<!-- Sidebar -->
		<nav id="sidebar">
			<!-- Sidebar Scroll Container -->
			<div id="sidebar-scroll">
				<!-- Sidebar Content -->
				<!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
				<div class="sidebar-content">
					<!-- Side Header -->
					<div class="side-header side-content bg-white-op">
						<!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
						<button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
							<i class="fa fa-times"></i>
						</button>
						<a class="text-white" href="<?php echo BASE_URL; ?>">
							<small class="font-w600 sidebar-mini-hide"><?php echo $program_name_short.' v'.$program_version; ?></small>
						</a>
					</div>
					<!-- END Side Header -->

					<!-- Side Content -->
					<div class="side-content">
						<ul class="nav-main">
							<li>
								<a href="<?php echo BASE_URL; ?>"><i class="si si-home"></i><span class="sidebar-mini-hide" style="font-size:12px;">Beranda</span></a>
							</li>
							<?php
							if($login_data['role_id'] == 1){
								?>
								<li>
									<a href="<?php echo BASE_URL.'master_data/user'; ?>" <?php if($page_active == 'user'){ echo 'class="active"'; } ?>><i class="fa fa-user"></i><span class="sidebar-mini-hide" style="font-size:12px;">User / Pengguna</span></a>
								</li>
								<?php
							}
							
							if($login_data['role_id'] == 1 OR $login_data['role_id'] == 2){
								?>
								<li class="separator">
									<a class="text-warning" href="javascript:void(0);">KANIM</a>
								</li>
								<li>
									<a href="<?php echo BASE_URL.'kanim/permohonan'; ?>" class="text-warning"><i class="fa fa-book text-warning"></i><span class="sidebar-mini-hide text-warning" style="font-size:12px;">Pendaftaran Permohonan</span></a>
								</li>
								<li>
									<a href="<?php echo BASE_URL.'kanim/list_data'; ?>"><i class="fa fa-database text-warning"></i><span class="sidebar-mini-hide text-warning" style="font-size:12px;">Daftar Permohonan</span></a>
								</li>
								<li>
									<a href="<?php echo BASE_URL.'kanim/pemberitahuan'; ?>"><i class="fa fa-database text-warning"></i><span class="sidebar-mini-hide text-warning" style="font-size:12px;">Daftar Pemberitahuan</span></a>
								</li>
								<li>
									<a href="<?php echo BASE_URL.'kanim/laporan_WBPA'; ?>"><i class="si si-notebook text-warning"></i><span class="sidebar-mini-hide text-warning" style="font-size:12px;">Laporan WBPA</span></a>
								</li>
								<li>
									<a href="<?php echo BASE_URL.'kanim/laporan_Tindakan'; ?>"><i class="si si-notebook text-warning"></i><span class="sidebar-mini-hide text-warning" style="font-size:12px;">Laporan Tindakan</span></a>
								</li>
								<li>
									<a href="<?php echo BASE_URL.'kanim/laporan_Pemberitahuan'; ?>"><i class="si si-bar-chart text-warning"></i><span class="sidebar-mini-hide text-warning" style="font-size:12px;">Statistik Pemberitahuan</span></a>
								</li>
								<li>
									<a href="<?php echo BASE_URL.'kanim/statistik_Tindakan'; ?>"><i class="si si-bar-chart text-warning"></i><span class="sidebar-mini-hide text-warning" style="font-size:12px;">Statistik Tindakan</span></a>
								</li>
								<?php
							}
							
							if($login_data['role_id'] == 1 OR $login_data['role_id'] == 3){
								?>
								<li class="separator">
									<a class="text-warning" href="javascript:void(0);">LAPAS</a>
								</li>
								<li>
									<a href="<?php echo BASE_URL.'lapas/pemberitahuan'; ?>"><i class="fa fa-book text-warning"></i><span class="sidebar-mini-hide text-warning" style="font-size:12px;">Form Pemberitahuan</span></a>
								</li>
								<li>
									<a href="<?php echo BASE_URL.'lapas/list_data'; ?>"><i class="fa fa-database text-warning"></i><span class="sidebar-mini-hide text-warning" style="font-size:12px;">Daftar Pemberitahuan</span></a>
								</li>
								<li>
									<a href="<?php echo BASE_URL.'lapas/laporan'; ?>"><i class="si si-notebook text-warning"></i><span class="sidebar-mini-hide text-warning" style="font-size:12px;">Laporan</span></a>
								</li>
								<?php
							}
							
							if($login_data['role_id'] == 1 OR $login_data['role_id'] == 4){
								?>
								<li class="separator">
									<a class="text-warning" href="javascript:void(0);">RUTAN</a>
								</li>
								<li>
									<a href="<?php echo BASE_URL.'rutan/pemberitahuan'; ?>"><i class="fa fa-book text-warning"></i><span class="sidebar-mini-hide text-warning" style="font-size:12px;">Form Pemberitahuan</span></a>
								</li>
								<li>
									<a href="<?php echo BASE_URL.'rutan/list_data'; ?>"><i class="fa fa-database text-warning"></i><span class="sidebar-mini-hide text-warning" style="font-size:12px;">Daftar Pemberitahuan</span></a>
								</li>
								<li>
									<a href="<?php echo BASE_URL.'rutan/laporan'; ?>"><i class="si si-notebook text-warning"></i><span class="sidebar-mini-hide text-warning" style="font-size:12px;">Laporan</span></a>
								</li>
								</li>
								<?php
							}
							?>
						</ul>
					</div>
					
					<!-- END Side Content -->
				</div>
				<!-- Sidebar Content -->
			</div>
			<!-- END Sidebar Scroll Container -->
		</nav>
		<!-- END Sidebar -->

		<!-- Header -->
		<header id="header-navbar" class="content-mini content-mini-full">
			<!-- Header Navigation Right -->
			<ul class="nav-header pull-right">
				<li>
					<div class="btn-group">
						<button class="btn btn-lg btn-default dropdown-toggle" data-toggle="dropdown" type="button">
							<i class="si si-user pull-left"></i> <span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right">
							<li class="dropdown-header">Profile</li>
							<li>
								<a tabindex="-1" href="<?php echo BASE_URL.'backend/change_profile'; ?>">
									<i class="si si-user pull-right"></i>Ubah Profil
								</a>
							</li>
							<li>
								<a tabindex="-1" href="<?php echo BASE_URL.'backend/change_password'; ?>">
									<i class="si si-settings pull-right"></i>Ubah Password
								</a>
							</li>
							<li class="divider"></li>
							<li>
								<a tabindex="-1" href="#" id="app-logout">
									<i class="si si-logout pull-right"></i>Log out
								</a>
							</li>
						</ul>
					</div>
				</li>
			</ul>
			<!-- END Header Navigation Right -->

			<!-- Header Navigation Left -->
			<ul class="nav-header pull-left">				
				<li class="hidden-xs hidden-sm">
					<!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
					<button class="btn btn-lg btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
						<i class="fa fa-ellipsis-v"></i>
					</button>
				</li>
				<li class="hidden-md hidden-lg">
					<!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
					<button class="btn btn-lg btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
					<i class="fa fa-navicon"></i>
					</button>
				</li>
			</ul>
			<!-- END Header Navigation Left -->
		</header>
		<!-- END Header -->

		
		
		