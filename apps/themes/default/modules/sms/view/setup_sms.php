<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once THEME_PATH."modules/header.php";

$module_config = array(
	//'primaryKey' => '',
	'name' => 'setup_sms',
	'text' => 'Setup INI SMS'
);
?>		
	
	<!-- Main Container -->
	<main id="main-container">
	
		
		<!-- Page Content -->
		<div class="content">
			<div class="row">
				<div class="col-lg-12">
					<!-- Latest Sales Widget -->
					<div class="block block-themed block-rounded" id="setup_sms-area">
						<div class="block-header bg-primary-dark">
							<h3 class="block-title" id="setup_sms-title">Setup ISI SMS</h3>
						</div>
							
						<?php
						if(empty($setup_sms)){
							$setup_sms['no_mobile_test'] = '';
							$setup_sms['isi_sms'] = '';
						}
						?>
						<div class="block-content">
							<form class="js-validation-bootstrap form-horizontal" id="addEditForm_<?php echo $module_config['name']; ?>" method="post">
								<div class="form-group"">
									<label class="col-md-2 control-label" for="no_mobile_test">No HP - Tester <span class="text-danger">*</span></label>
									<div class="col-md-6">
										<input class="form-control" type="text" id="no_mobile_test" name="no_mobile_test" placeholder="+6281XXX atau 081XXX" value="<?php echo $setup_sms['no_mobile_test']; ?>">
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-2 control-label" for="isi_sms">Isi SMS <span class="text-danger">*</span><br/></label>
									<div class="col-md-6">
										<textarea class="form-control" id="isi_sms" name="isi_sms" rows="3" placeholder="Setup Isi SMS disini..."><?php echo $setup_sms['isi_sms']; ?></textarea>
									</div>
								</div>
								
								<div class="form-group">
									<div id="messageAddEditForm_<?php echo $module_config['name']; ?>" class="msgInfo"></div>
								</div>
								<button type="submit" class="hidden">Submit</button>
											
								
								
							</form>
							<div class="form-horizontal">
								<div class="form-group">
									
									<div class="col-md-2">
										&nbsp;
									</div>
									<div class="col-md-2">
										<button class="btn btn-block btn-primary" id="test_addEditForm_<?php echo $module_config['name']; ?>" type="reset">Test SMS</button>
									</div>
									<div class="col-md-2">
										<button type="button" class="btn btn-block btn-success" id="save_addEditForm_<?php echo $module_config['name']; ?>">Save Setup</button>
									</div>
								</div>
								
								<div class="form-group">
									
									<div class="col-md-6">
										Keterangan:<br/>
										* <b>{nama}</b> akan menampilkan data nama<br/>
										* <b>{kitas}</b> akan menampilkan data no kitas<br/>
										* <b>{expired}</b> akan menampilkan data masa berlaku<br/>
									</div>
								</div>
							</div>
						</div>
						
					</div>
					<!-- END Latest Sales Widget -->
				</div>
				
				
			</div>
			
			
		</div>
		<!-- END Page Content -->
		
	</main>
	<!-- END Main Container -->
	
<?php
include_once THEME_PATH."modules/footer.php";
?>
