/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */

var BaseTableDatatables = function() {
    // Init full DataTable, for more examples you can check out https://www.datatables.net/
    var initDataTableFull = function() {
        jQuery('#table-list-data').dataTable({
            //columnDefs: [ { orderable: false, targets: [ 4 ] } ],
            pageLength: 10,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]]
        });
    };

    // DataTables Bootstrap integration
    var bsDataTables = function() {
        var $DataTable = jQuery.fn.dataTable;

        // Set the defaults for DataTables init
        jQuery.extend( true, $DataTable.defaults, {
            dom:
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            renderer: 'bootstrap',
            oLanguage: {
                sLengthMenu: "_MENU_",
                sInfo: "Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>",
                oPaginate: {
                    sPrevious: '<i class="fa fa-angle-left"></i>',
                    sNext: '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        // Default class modification
        jQuery.extend($DataTable.ext.classes, {
            sWrapper: "dataTables_wrapper form-inline dt-bootstrap",
            sFilterInput: "form-control",
            sLengthSelect: "form-control"
        });

        // Bootstrap paging button renderer
        $DataTable.ext.renderer.pageButton.bootstrap = function (settings, host, idx, buttons, page, pages) {
            var api     = new $DataTable.Api(settings);
            var classes = settings.oClasses;
            var lang    = settings.oLanguage.oPaginate;
            var btnDisplay, btnClass;

            var attach = function (container, buttons) {
                var i, ien, node, button;
                var clickHandler = function (e) {
                    e.preventDefault();
                    if (!jQuery(e.currentTarget).hasClass('disabled')) {
                        api.page(e.data.action).draw(false);
                    }
                };

                for (i = 0, ien = buttons.length; i < ien; i++) {
                    button = buttons[i];

                    if (jQuery.isArray(button)) {
                        attach(container, button);
                    }
                    else {
                        btnDisplay = '';
                        btnClass = '';

                        switch (button) {
                            case 'ellipsis':
                                btnDisplay = '&hellip;';
                                btnClass = 'disabled';
                                break;

                            case 'first':
                                btnDisplay = lang.sFirst;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'previous':
                                btnDisplay = lang.sPrevious;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'next':
                                btnDisplay = lang.sNext;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            case 'last':
                                btnDisplay = lang.sLast;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            default:
                                btnDisplay = button + 1;
                                btnClass = page === button ?
                                        'active' : '';
                                break;
                        }

                        if (btnDisplay) {
                            node = jQuery('<li>', {
                                'class': classes.sPageButton + ' ' + btnClass,
                                'aria-controls': settings.sTableId,
                                'tabindex': settings.iTabIndex,
                                'id': idx === 0 && typeof button === 'string' ?
                                        settings.sTableId + '_' + button :
                                        null
                            })
                            .append(jQuery('<a>', {
                                    'href': '#'
                                })
                                .html(btnDisplay)
                            )
                            .appendTo(container);

                            settings.oApi._fnBindAction(
                                node, {action: button}, clickHandler
                            );
                        }
                    }
                }
            };

            attach(
                jQuery(host).empty().html('<ul class="pagination"/>').children('ul'),
                buttons
            );
        };

        // TableTools Bootstrap compatibility - Required TableTools 2.1+
        if ($DataTable.TableTools) {
            // Set the classes that TableTools uses to something suitable for Bootstrap
            jQuery.extend(true, $DataTable.TableTools.classes, {
                "container": "DTTT btn-group",
                "buttons": {
                    "normal": "btn btn-default",
                    "disabled": "disabled"
                },
                "collection": {
                    "container": "DTTT_dropdown dropdown-menu",
                    "buttons": {
                        "normal": "",
                        "disabled": "disabled"
                    }
                },
                "print": {
                    "info": "DTTT_print_info"
                },
                "select": {
                    "row": "active"
                }
            });

            // Have the collection use a bootstrap compatible drop down
            jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
                "collection": {
                    "container": "ul",
                    "button": "li",
                    "liner": "a"
                }
            });
        }
    };

    return {
        init: function() {
            // Init Datatables
            bsDataTables();
            //initDataTableSimple();
            //initDataTableFull();
        }
    };
}();



var tanggal_dari = jQuery('#tanggal_dari').val();
var tanggal_sampai = jQuery('#tanggal_sampai').val();
var keyword_list_data = jQuery('#keyword_list_data').val();
var keyword_list_data_display = jQuery('#list_data_keyword_display').val();
var result_excel = 0;
var result_print = 0;
var dtTableAbsensi;

function refresh_list_data(load_init){
	
	var getID = '#list_data_area';
	App.blocks(getID, 'state_loading');
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();

	// Request
	var data = {
		tanggal_dari: tanggal_dari,
		tanggal_sampai: tanggal_sampai,
		keyword: keyword_list_data,
		tipe: 'proses,tertunda',
		xtime: sendTimer
	};
	
	var getTarget = appUrl +'sms/sms_tertunda/load_list_data';

	//ajax-load
	$.ajax({
		url: getTarget,
		//dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata){
				
				/*
				if(load_init == true){
					BaseTableDatatables.init(); 
					$('#table-list-data-area').html(rdata);
					dtTableAbsensi = jQuery('#table-list-data').dataTable({
						info: false,
						ordering: false,
						searching: false,
						paging: false,
						scrollY: 400
					});
				}else{
					
					dtTableAbsensi.fnDestroy(true);
					$('#table-list-data-area').html(rdata);
					dtTableAbsensi = jQuery('#table-list-data').dataTable({
						info: false,
						ordering: false,
						searching: false,
						paging: false,
						scrollY: 400
					});
					
				}
				*/
				
				if(load_init == true){
					BaseTableDatatables.init(); 
					$('#table-list-data-area').html(rdata);
					dtTableAbsensi = jQuery('#table-list-data').dataTable({
						//columnDefs: [ { orderable: false, targets: [ 4 ] } ],
						ordering: false,
						searching: false,
						pageLength: 10,
						lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
						oLanguage: {
							sLengthMenu: ""
						},
						dom:
							"<'row'<'col-sm-12'tr>>" +
							"<'row'<'col-sm-6'i><'col-sm-6'p>>"
					});
				}else{
					
					dtTableAbsensi.fnDestroy(true);
					$('#table-list-data-area').html(rdata);
					dtTableAbsensi = jQuery('#table-list-data').dataTable({
						//columnDefs: [ { orderable: false, targets: [ 4 ] } ],
						ordering: false,
						searching: false,
						pageLength: 10,
						lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
						oLanguage: {
							sLengthMenu: ""
						},
						dom:
							"<'row'<'col-sm-12'tr>>" +
							"<'row'<'col-sm-6'i><'col-sm-6'p>>"
					});
				}
				
			}else{
				$('#table-list-data-area').html('');
			}
			
			
			App.blocks(getID, 'state_normal');
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			App.blocks(getID, 'state_normal');
		}
	});
}

function print_excel(xval){
	
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();
	
	var getTarget = appUrl +'sms/sms_tertunda/load_list_data?xtime='+sendTimer;
	
	if(!xval){
		result_print = 1;
		result_excel = 0;
		
	}else{
		result_print = 0;
		result_excel = 1;
		
	}
	
	getTarget += '&tanggal_dari='+tanggal_dari;
	getTarget += '&tanggal_sampai='+tanggal_sampai;
	getTarget += '&keyword='+keyword_list_data;
	getTarget += '&tipe=proses,tertunda';
	getTarget += '&result_excel='+result_excel;
	getTarget += '&result_print='+result_print;
	
	$('#print_excel_iframe').attr('src', getTarget);
	
	
}


function resend_sms(getId){
	var sms_nama = $('#sms_nama_'+getId).val();
	var sms_kitas = $('#sms_kitas_'+getId).val();
	var sms_expired = $('#sms_expired_'+getId).val();
	var sms_no_tujuan = $('#sms_no_tujuan_'+getId).val();

	var r = confirm("Kirim Ulang SMS ke "+sms_no_tujuan+",\n untuk wna dgn data sbb, \n Nama: "+sms_nama+",\n Kitas: "+sms_kitas+" ?");
	if (r == true) {
		var dtPost = {
			sms_nama : sms_nama,
			sms_kitas : sms_kitas,
			sms_expired : sms_expired,
			sms_no_tujuan : sms_no_tujuan,
			sms_id : getId
		};
		
		var d = new Date();
		
		$.ajax({
			url: appUrl+'sms/sms_tertunda/resend_sms?_dc='+d.getTime(),
			dataType: 'json',
			type: 'POST',
			data: dtPost,
			success: function(data, textStatus, XMLHttpRequest)
			{
				refresh_list_data(false);
								
			},
			error: function(XMLHttpRequest, textStatus, errorThrown)
			{	
				alert('kirim ulang sms terkirim gagal!');
			}

		});
	} else {
		//return false;
	}
	
}

function delete_sms(getId){
	
	var sms_nama = $('#sms_nama_'+getId).val();
	var sms_kitas = $('#sms_kitas_'+getId).val();
	var sms_expired = $('#sms_expired_'+getId).val();
	var sms_no_tujuan = $('#sms_no_tujuan_'+getId).val();

	var r = confirm("Hapus SMS ke "+sms_no_tujuan+",\n untuk wna dgn data sbb, \n Nama: "+sms_nama+",\n Kitas: "+sms_kitas+" ?");
	if (r == true) {
		var dtPost = {
			sms_nama : sms_nama,
			sms_kitas : sms_kitas,
			sms_expired : sms_expired,
			sms_no_tujuan : sms_no_tujuan,
			sms_id : getId
		};
		
		var d = new Date();
		
		$.ajax({
			url: appUrl+'sms/sms_tertunda/delete_sms?_dc='+d.getTime(),
			dataType: 'json',
			type: 'POST',
			data: dtPost,
			success: function(data, textStatus, XMLHttpRequest)
			{
				refresh_list_data(false);
								
			},
			error: function(XMLHttpRequest, textStatus, errorThrown)
			{	
				alert('delete sms terkirim gagal!');
			}

		});
	} else {
		//return false;
	}
}

// Initialize when page loads
jQuery(function(){ 
	
	 
	$('.js-datepicker').add('.input-daterange').datepicker({
		weekStart: 1,
		autoclose: true,
		todayHighlight: true
	});
		
	$('.js-select2').select2();	
	
	$('#do_filter_list_data').click(function(){
		
		tanggal_dari = $('#tanggal_dari').val();
		tanggal_sampai = $('#tanggal_sampai').val();
		keyword_list_data = $('#keyword_list_data').val();
		keyword_list_data_display = $( "#keyword_list_data option:selected" ).text();
		
		if(tanggal_dari == '' || tanggal_sampai == ''){
			alert('Tanggal harus dipilih!');
			return false;
		}
		if(keyword_list_data == ''){
			//alert('Kata Kunci harus dipilih!');
			//return false;
		}
		
		//list-data
		$('#list_data_tanggal_dari_display').html(tanggal_dari);
		$('#list_data_tanggal_sampai_display').html(tanggal_sampai);
		$('#list_data_keyword_display').html(keyword_list_data_display);
		
		$('#close-modal-filter').trigger('click');
		refresh_list_data(false);
		
	});
	
	$('#refresh_list_data').click(function(){
		refresh_list_data(false);
	});
	
	refresh_list_data(true);
	
});