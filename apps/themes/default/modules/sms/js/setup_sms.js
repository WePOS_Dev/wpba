$(function() {

	$('.js-select2').select2();	
	
	/*just edit module_config */
	var module_config = {
		name: 'setup_sms',
		text: 'Setup ISI SMS',
		text_test: 'Test SMS',
		save_url: appUrl+'sms/save_setup',
		test_sms_url: appUrl+'sms/send_sms'
	};
	
	//FORM-VALIDATOR - Custom if need it: its related to form
	$('#addEditForm_'+module_config.name).bootstrapValidator({
        message: 'Data yang diinputkan tidak valid',
        fields: {
            no_mobile_test: {
                validators: {
                    notEmpty: {
                        message: 'No HP Tester tidak boleh kosong'
                    }
                }
            },
            isi_sms: {
                validators: {
                    notEmpty: {
                        message: 'Isi SMS tidak boleh kosong'
                    }
                }
            }
        }
    });
	
	$('#reset_addEditForm_'+module_config.name).click(function(){
		
	});
	
	$('#save_addEditForm_'+module_config.name).click(function(){
		$('#addEditForm_'+module_config.name).submit();
	});
	
	
	//ADD / EDIT - SAVE
	$('#addEditForm_'+module_config.name).on('submit', function(e) {
		
		e.preventDefault;
		
		//check has-error
		var is_has_error = false;
		$('#addEditForm_'+module_config.name+' .form-group').each(function(){			
			if($(this).hasClass('has-error')){
				is_has_error = true;
			}
		});
		
		if(is_has_error){
			return false;
		}
		
		
				
		showMessageInfo('#messageAddEditForm_'+module_config.name, 'alert-info', 'fa-spinner', 'Menyimpan Data...');
							
		var submitBt = $('#save_addEditForm_'+module_config.name);
		
		$.ajax({
			url: module_config.save_url,
			dataType: 'json',
			type: 'POST',
			data: $(this).serialize(),
			success: function(data, textStatus, XMLHttpRequest)
			{
				if(data.success == false){
					showMessageInfo('#messageAddEditForm_'+module_config.name, 'alert-danger', 'fa-warning', data.info);
					
				}else{
					
					$('#no_mobile_test').val(data.no_mobile_test);
					$('#isi_sms').val(data.isi_sms);
					
					$('#close_addEditForm_'+module_config.name).trigger('click');
										
					showMessageInfo('#messageAddEditForm_'+module_config.name, 'alert-success', 'fa-save', data.info);
					setTimeout(function(){					
						hideMessageInfo('#messageAddEditForm_'+module_config.name);
					}, 5000);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown)
			{		
				showMessageInfo('#messageAddEditForm_'+module_config.name, 'alert-danger', 'fa-warning', module_config.text+' Gagal!');	
			}

		});
					
	});	
	
	//Tester
	$('#test_addEditForm_'+module_config.name).click(function(){
		
		// Start timer
		var sendTimer = new Date().getTime();
		
		// Request
		var dataSend = {
			isi_sms: $('#isi_sms').val(),
			no_mobile_test: $('#no_mobile_test').val(),
			xtime: sendTimer
		};
		
		
		showMessageInfo('#messageAddEditForm_'+module_config.name, 'alert-info', 'fa-spinner', 'Test Kirim SMS...');
		
		$.ajax({
			url: module_config.test_sms_url,
			dataType: 'json',
			type: 'POST',
			data: dataSend,
			success: function(data, textStatus, XMLHttpRequest)
			{
				if(data.success == false){
					showMessageInfo('#messageAddEditForm_'+module_config.name, 'alert-danger', 'fa-warning', data.info);
					
				}else{
					
					$('#no_mobile_test').val(dataSend.no_mobile_test);
					$('#isi_sms').val(dataSend.isi_sms);
					
					$('#close_addEditForm_'+module_config.name).trigger('click');
										
					showMessageInfo('#messageAddEditForm_'+module_config.name, 'alert-success', 'fa-save', data.info);
					setTimeout(function(){					
						hideMessageInfo('#messageAddEditForm_'+module_config.name);
					}, 5000);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown)
			{		
				showMessageInfo('#messageAddEditForm_'+module_config.name, 'alert-danger', 'fa-warning', module_config.text_test+' Gagal!');	
			}

		});
		
	});
});