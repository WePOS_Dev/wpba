<?php

header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-type:   application/x-msexcel; charset=utf-8");
header("Content-Disposition: attachment; filename=".$file_name.".xls"); 
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);


echo '
<style>
table {
	font-family: Arial,sans-serif;
	border-spacing: 0;
	border-collapse: collapse;
	table-layout: fixed;
}
tr{ height: 19.5pt; }
td,th {
	border: 0.5pt solid windowtext;
	font-family: Arial,sans-serif;
	font-size: 8pt;
	font-weight: normal;
	text-align: left;
	vertical-align: middle;
	white-space: normal;
	padding:1px;
}
th{
	background: #f2f2f2;
	font-size: 9pt;
	font-weight: 700;
	text-align: center;
}
.text-left{text-align: left;}
.text-center{text-align: center;}

.dateFormat {
    mso-number-format:"mm\/dd\/yyyy"
}
.numFormat {
  mso-number-format:General;
}
.textFormat{
  mso-number-format:"\@";/*force text*/
}
</style>
';

//hidden-xs 
$dt_replace = array(
	'hidden-xs '	=> ''
);

$html_display = strtr($html_display, $dt_replace);

echo $html_display;
?>	