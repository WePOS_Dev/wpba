<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once THEME_PATH."modules/header.php";

?>		
	
	<!-- Main Container -->
	<main id="main-container">
		
		
		<div class="content">
			
			<!-- Dynamic Table Full -->
			<div class="block block-themed block-rounded" id="permohonan_area">
				<div class="block-header bg-amethyst-dark">
					<h3 class="block-title">KANIM / Detail Permohonan</h3>
				</div>
				
				<div class="block-content bg-gray-lighter">
					
					<?php
					if(!empty($is_error)){
						?>
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<h3 class="font-w300 push-15">Ada Kesalahan!</h3>
							<p><?php echo $is_error; ?></p>
						</div>
						<?php
					}
					?>
					
					
					<div class="block block-bordered">
						<ul class="nav nav-tabs nav-tabs-alt nav-justified" data-toggle="tabs">
							<li class="active">
								<a href="#btabs-static-justified-identitas"><i class="fa fa-user"></i> Identitas</a>
							</li>
							<li>
								<a href="#btabs-static-justified-sponsor"><i class="fa fa-building-o"></i> Sponsor</a>
							</li>
							<li>
								<a href="#btabs-static-justified-pendaratan"><i class="fa fa-flag-o"></i> Pendaratan</a>
							</li>
							<li>
								<a href="#btabs-static-justified-izin"><i class="fa fa-group"></i> Izin Tinggal</a>
							</li>
							<li>
								<a href="#btabs-static-justified-tindakan"><i class="fa fa-gavel"></i> Tindakan</a>
							</li>
							<li>
								<a href="#btabs-static-justified-kelengkapan"><i class="fa fa-folder-open-o"></i> Dokumen</a>
							</li>
						</ul>
						<div class="block-content tab-content">
							<div class="tab-pane active" id="btabs-static-justified-identitas">
								<h4 class="font-w300 push-15">Permohonan dan Identitas</h4>
								
								<form class="js-validation-bootstrap form-horizontal" id="form-data-permohonan-kanim" action="#" method="post" onsubmit="return false;">
						
									<div class="row items-push">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-4 control-label" for="no_permohonan">No. Permohonan</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="no_permohonan" name="no_permohonan" value="<?php echo $data_edit['no_permohonan']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="tanggal_permohonan">Tanggal Permohonan</label>
												<div class="col-md-4">
													<input class="js-datepicker form-control" type="text" id="tanggal_permohonan" name="tanggal_permohonan" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $data_edit['tanggal_permohonan']; ?>">
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-md-4 control-label" for="niora">Produk</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="produk" name="produk" readonly="readonly" value="DEPORTASI">
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-md-4 control-label" for="niora">Niora</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="niora" name="niora" value="<?php echo $data_edit['niora']; ?>">
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-md-4 control-label" for="alamat">Catatan</label>
												<div class="col-md-8">
													<textarea class="form-control" id="catatan" name="catatan" rows="2"><?php echo $data_edit['catatan']; ?></textarea>
												</div>
											</div>
											
											
											<div class="form-group">
												<label class="col-md-4 control-label" for="no_paspor">No. Paspor</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="no_paspor" name="no_paspor" value="<?php echo $data_edit['no_paspor']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="paspor_diberikan_tanggal">Tanggal diberikan</label>
												<div class="col-md-4">
													<input class="js-datepicker form-control" type="text" id="paspor_diberikan_tanggal" name="paspor_diberikan_tanggal" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $data_edit['paspor_diberikan_tanggal']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="paspor_diberikan_di">Tempat diberikan</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="paspor_diberikan_di" name="paspor_diberikan_di" value="<?php echo $data_edit['paspor_diberikan_di']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="paspor_berlaku_sampai">Berlaku s/d</label>
												<div class="col-md-4">
													<input class="js-datepicker form-control" type="text" id="paspor_berlaku_sampai" name="paspor_berlaku_sampai" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $data_edit['paspor_berlaku_sampai']; ?>">
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-md-4 control-label" for="kantor_imigrasi">Kantor Imigrasi</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="kantor_imigrasi" name="kantor_imigrasi" value="<?php echo $data_edit['kantor_imigrasi']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="kantor_wilayah">Kantor Wilayah</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="kantor_wilayah" name="kantor_wilayah" value="<?php echo $data_edit['kantor_wilayah']; ?>">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-4 control-label" for="nama_wna">Nama</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="nama_wna" name="nama_wna" value="<?php echo $data_edit['nama_wna']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="tempat_lahir">Tempat Lahir</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="tempat_lahir" name="tempat_lahir" value="<?php echo $data_edit['tempat_lahir']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="tanggal_lahir">Tanggal Lahir</label>
												<div class="col-md-8">
													<input class="js-datepicker form-control" type="text" id="tanggal_lahir" name="tanggal_lahir" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $data_edit['tanggal_lahir']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="kebangsaan">Kebangsaan</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="kebangsaan" name="kebangsaan" value="<?php echo $data_edit['kebangsaan']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="jenis_kelamin">Jenis Kelamin (L/P)</label>
												<div class="col-md-6">
													<select class="js-select2 form-control" id="jenis_kelamin"  id="jenis_kelamin">
														<option value="">Pilih Jenis Kelamin</option>
														<?php
														$selected_L = '';
														$selected_P = '';
														if($data_edit['jenis_kelamin'] == 'L'){
															$selected_L = ' selected="selected"';
														}
														if($data_edit['jenis_kelamin'] == 'P'){
															$selected_P = ' selected="selected"';
														}
														echo '<option value="L"'.$selected_L.'>Laki-laki</option>';
														echo '<option value="P"'.$selected_P.'>Perempuan</option>';
														?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="status_sipil">Status Sipil</label>
												<div class="col-md-6">
													<select class="js-select2 form-control" id="status_sipil"  id="status_sipil">
														<option value="">Pilih Status</option>
														<?php
														$selected_L = '';
														$selected_P = '';
														if($data_edit['status_sipil'] == 'KAWIN'){
															$selected_L = ' selected="selected"';
														}
														if($data_edit['status_sipil'] == 'BELUM KAWIN'){
															$selected_P = ' selected="selected"';
														}
														echo '<option value="KAWIN"'.$selected_L.'>KAWIN</option>';
														echo '<option value="BELUM KAWIN"'.$selected_P.'>BELUM KAWIN</option>';
														?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="pekerjaan">Pekerjaan</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="pekerjaan" name="pekerjaan" value="<?php echo $data_edit['pekerjaan']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="alamat">Alamat</label>
												<div class="col-md-8">
													<textarea class="form-control" id="alamat" name="alamat" rows="2"><?php echo $data_edit['alamat']; ?></textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="kota">Kota</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="kota" name="kota" value="<?php echo $data_edit['kota']; ?>">
												</div>
											</div>
										</div>
									</div>
									
									
									<input type="hidden" id="is_edit" name="is_edit" value="<?php echo $data_edit['is_edit']; ?>">
									<input type="hidden" id="permohonan_id" name="permohonan_id" value="<?php echo $data_edit['permohonan_id']; ?>">
									<input type="hidden" id="id_bebas" name="id_bebas" value="<?php echo $data_edit['id_bebas']; ?>">
									<input type="hidden" id="tipe_instansi" name="tipe_instansi" value="<?php echo $data_edit['tipe_instansi']; ?>">
									<div class="form-group">
										<div class="col-md-2 col-md-offset-2" style="margin-top:10px;">
											<button class="btn btn-block btn-success" id="do_add_permohonan" type="submit"><i class="fa fa-save pull-right"></i> Simpan</button>
										</div>
									</div>
									
								</form>
								
								
							</div>
							<div class="tab-pane" id="btabs-static-justified-sponsor">
								<h4 class="font-w300 push-15">Informasi Sponsor</h4>
								<form class="js-validation-sponsor form-horizontal" id="form-data-sponsor-kanim" action="#" method="post" onsubmit="return false;">
						
									<div class="row items-push">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-4 control-label" for="jenis_sponsor">Jenis Sponsor</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="jenis_sponsor" name="jenis_sponsor" value="<?php echo $data_sponsor['jenis_sponsor']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="status_perusahaan">Status Perusahaan</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="status_perusahaan" name="status_perusahaan" value="<?php echo $data_sponsor['status_perusahaan']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="sektor_kegiatan">Sektor Kegiatan</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="sektor_kegiatan" name="sektor_kegiatan" value="<?php echo $data_sponsor['sektor_kegiatan']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="nama_sponsor">Nama Sponsor</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="nama_sponsor" name="nama_sponsor" value="<?php echo $data_sponsor['nama_sponsor']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="nama_pimpinan">Nama Pimpinan</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="nama_pimpinan" name="nama_pimpinan" value="<?php echo $data_sponsor['nama_pimpinan']; ?>">
												</div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-4 control-label" for="alamat_sponsor">Alamat</label>
												<div class="col-md-8">
													<textarea class="form-control" id="alamat_sponsor" name="alamat_sponsor" rows="2"><?php echo $data_sponsor['alamat_sponsor']; ?></textarea>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="kota_sponsor">Kota</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="kota_sponsor" name="kota_sponsor" value="<?php echo $data_sponsor['kota_sponsor']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="kodepos_sponsor">Kodepos</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="kodepos_sponsor" name="kodepos_sponsor" value="<?php echo $data_sponsor['kodepos_sponsor']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="telp_sponsor">No.Telp</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="telp_sponsor" name="telp_sponsor" value="<?php echo $data_sponsor['telp_sponsor']; ?>">
												</div>
											</div>
										</div>
									</div>
									
									<input type="hidden" id="permohonan_id_sponsor" name="permohonan_id_sponsor" value="<?php echo $data_sponsor['permohonan_id']; ?>">
									<input type="hidden" id="sponsor_id" name="sponsor_id" value="<?php echo $data_sponsor['sponsor_id']; ?>">
									<div class="form-group">
										<div class="col-md-2 col-md-offset-2" style="margin-top:10px;">
											<button class="btn btn-block btn-success" id="do_add_sponsor" type="submit"><i class="fa fa-save pull-right"></i> Simpan</button>
										</div>
									</div>
									
								</form>

							</div>
							<div class="tab-pane" id="btabs-static-justified-pendaratan">
								<h4 class="font-w300 push-15">Informasi Pendaratan</h4>
								<form class="js-validation-pendaratan form-horizontal" id="form-data-pendaratan-kanim" action="#" method="post" onsubmit="return false;">
						
									<div class="row items-push">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-4 control-label" for="no_visa">No Visa</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="no_visa" name="no_visa" value="<?php echo $data_pendaratan['no_visa']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="tempat_pengeluaran">Tempat Pengeluaran</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="tempat_pengeluaran" name="tempat_pengeluaran" value="<?php echo $data_pendaratan['tempat_pengeluaran']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="tanggal_pengeluaran">Tanggal Pengeluaran</label>
												<div class="col-md-8">
													<input class="js-datepicker form-control" type="text" id="tanggal_pengeluaran" name="tanggal_pengeluaran" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $data_pendaratan['tanggal_pengeluaran']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="berlaku_sd">Berlaku s/d</label>
												<div class="col-md-8">
													<input class="js-datepicker form-control" type="text" id="berlaku_sd" name="berlaku_sd" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $data_pendaratan['berlaku_sd']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="tempat_masuk">Tempat Masuk</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="tempat_masuk" name="tempat_masuk" value="<?php echo $data_pendaratan['tempat_masuk']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="tanggal_masuk">Tanggal Masuk</label>
												<div class="col-md-8">
													<input class="js-datepicker form-control" type="text" id="tanggal_masuk" name="tanggal_masuk" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $data_pendaratan['tanggal_masuk']; ?>">
												</div>
											</div>
											
										</div>
										<div class="col-md-6">
											&nbsp;
										</div>
									</div>
									
									<input type="hidden" id="permohonan_id_pendaratan" name="permohonan_id_pendaratan" value="<?php echo $data_pendaratan['permohonan_id']; ?>">
									<input type="hidden" id="pendaratan_id" name="pendaratan_id" value="<?php echo $data_pendaratan['pendaratan_id']; ?>">
									<div class="form-group">
										<div class="col-md-2 col-md-offset-2" style="margin-top:10px;">
											<button class="btn btn-block btn-success" id="do_add_pendaratan" type="submit"><i class="fa fa-save pull-right"></i> Simpan</button>
										</div>
									</div>
									
								</form>
							</div>
							<div class="tab-pane" id="btabs-static-justified-izin">
								<h4 class="font-w300 push-15">Data Keimigrasian Izin Tinggal</h4>
								
								<form class="js-validation-izin form-horizontal" id="form-data-izin-kanim" action="#" method="post" onsubmit="return false;">
						
									<div class="row items-push">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-4 control-label" for="no_register">No Register</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="no_register" name="no_register" value="<?php echo $data_izin['no_register']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="izin_tinggal_ke">Izin Tinggal Ke</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="izin_tinggal_ke" name="izin_tinggal_ke" value="<?php echo $data_izin['izin_tinggal_ke']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="izin_tanggal_pengeluaran">Tanggal Pengeluaran</label>
												<div class="col-md-8">
													<input class="js-datepicker form-control" type="text" id="izin_tanggal_pengeluaran" name="izin_tanggal_pengeluaran" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $data_izin['tanggal_pengeluaran']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="izin_berlaku_sd">Berlaku s/d</label>
												<div class="col-md-8">
													<input class="js-datepicker form-control" type="text" id="izin_berlaku_sd" name="izin_berlaku_sd" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $data_izin['berlaku_sd']; ?>">
												</div>
											</div>
											
										</div>
										<div class="col-md-6">
											&nbsp;
										</div>
									</div>
									
									<input type="hidden" id="permohonan_id_izin" name="permohonan_id_izin" value="<?php echo $data_izin['permohonan_id']; ?>">
									<input type="hidden" id="izin_id" name="izin_id" value="<?php echo $data_izin['izin_id']; ?>">
									<div class="form-group">
										<div class="col-md-2 col-md-offset-2" style="margin-top:10px;">
											<button class="btn btn-block btn-success" id="do_add_izin" type="submit"><i class="fa fa-save pull-right"></i> Simpan</button>
										</div>
									</div>
									
								</form>
								
							</div>
							<div class="tab-pane" id="btabs-static-justified-tindakan">
								<h4 class="font-w300 push-15">Tindakan Administrasi Keimigrasian</h4>
								<form class="js-validation-tindakan form-horizontal" id="form-data-tindakan-kanim" action="#" method="post" onsubmit="return false;">
						
									<div class="row items-push">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-4 control-label" for="pelanggaran_pasal">Ketentuan Pasal yang dilanggar</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="pelanggaran_pasal" name="pelanggaran_pasal" value="<?php echo $data_tindakan['pelanggaran_pasal']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="sumber_kasus">Sumber Kasus</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="sumber_kasus" name="sumber_kasus" value="<?php echo $data_tindakan['sumber_kasus']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" style="text-align:left">Tindakan Administrasi Keimigrasian - Pasal 75 Ayat 2:</label>
												<div class="col-md-8">
													<div class="checkbox">
														<label for="example-checkbox1">
															<input id="tindakan_a" name="tindakan_a" value="1" type="checkbox" <?php if($data_tindakan['tindakan_a'] == 1){ echo 'checked="checked"'; } ?>> a. pencantuman dalam daftar Pencegahan atau Penagkalan;
														</label>
													</div>
													<div class="checkbox">
														<label for="example-checkbox2">
															<input id="tindakan_b" name="tindakan_b" value="1" type="checkbox" <?php if($data_tindakan['tindakan_b'] == 1){ echo 'checked="checked"'; } ?>> b. pembatasan, perubahan, atau pembatalan Izin Tinggal;
														</label>
													</div>
													<div class="checkbox">
														<label for="example-checkbox2">
															<input id="tindakan_c" name="tindakan_c" value="1" type="checkbox" <?php if($data_tindakan['tindakan_c'] == 1){ echo 'checked="checked"'; } ?>> c. larangan untuk berada si satu atau beberapa tempat tertentu di Wilayah Indonesia;
														</label>
													</div>
													<div class="checkbox">
														<label for="example-checkbox2">
															<input id="tindakan_d" name="tindakan_d" value="1" type="checkbox" <?php if($data_tindakan['tindakan_d'] == 1){ echo 'checked="checked"'; } ?>> d. keharusan untuk bertempat tinggal disuatu tempat tertentu di Wilayah Indonesia;
														</label>
													</div>
													<div class="checkbox">
														<label for="example-checkbox2">
															<input id="tindakan_e" name="tindakan_e" value="1" type="checkbox" <?php if($data_tindakan['tindakan_e'] == 1){ echo 'checked="checked"'; } ?>> e. pengenaan biaya beban; dan/atau
														</label>
													</div>
													<div class="checkbox">
														<label for="example-checkbox2">
															<input id="tindakan_f" name="tindakan_f" value="1" type="checkbox" <?php if($data_tindakan['tindakan_f'] == 1){ echo 'checked="checked"'; } ?>> f. Deportasi dari Wilayah Indonesia
														</label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="tanggal_deportasi">Tanggal Deportasi</label>
												<div class="col-md-8">
													<input class="js-datepicker form-control" type="text" id="tanggal_deportasi" name="tanggal_deportasi" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $data_tindakan['tanggal_deportasi']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="sudah_dideportasi">Sudah di Deportasi</label>
												<div class="col-md-8">
													<input id="sudah_dideportasi" name="sudah_dideportasi" value="1" type="checkbox" <?php if($data_tindakan['sudah_dideportasi'] == 1){ echo 'checked="checked"'; } ?>>
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-4 control-label" for="pengajuan_keberatan">Pengajuan Keberatan</label>
												<div class="col-md-8">
													<input class="form-control" type="text" id="pengajuan_keberatan" name="pengajuan_keberatan" value="<?php echo $data_tindakan['pengajuan_keberatan']; ?>">
												</div>
											</div>
											
										</div>
										<div class="col-md-6">
											&nbsp;
										</div>
									</div>
									
									<input type="hidden" id="permohonan_id_tindakan" name="permohonan_id_tindakan" value="<?php echo $data_tindakan['permohonan_id']; ?>">
									<input type="hidden" id="tindakan_id" name="tindakan_id" value="<?php echo $data_tindakan['tindakan_id']; ?>">
									<div class="form-group">
										<div class="col-md-2 col-md-offset-2" style="margin-top:10px;">
											<button class="btn btn-block btn-success" id="do_add_tindakan" type="submit"><i class="fa fa-save pull-right"></i> Simpan</button>
										</div>
									</div>
									
								</form>
							</div>
							<div class="tab-pane" id="btabs-static-justified-kelengkapan">
								<h4 class="font-w300 push-15">Kelengkapan Dokumen</h4>
								
						
									<div class="row items-push">
										<div class="col-md-6">
											<form class="form-horizontal" id="form-data-dokumen-kanim" action="#" method="post" onsubmit="return false;">
											<div class="form-group">
												<label class="col-md-6 control-label" for="laporan_kejadian">Laporan Kejadian</label>
												<div class="col-md-6">
													<input class="form-control" type="text" id="laporan_kejadian" name="laporan_kejadian" value="<?php echo $data_dokumen['laporan_kejadian']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-6 control-label" for="berita_acara_instansi_lain">Berita Acara Instansi Lain</label>
												<div class="col-md-6">
													<input class="form-control" type="text" id="berita_acara_instansi_lain" name="berita_acara_instansi_lain" value="<?php echo $data_dokumen['berita_acara_instansi_lain']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-6 control-label" for="berita_acara_pemeriksaan">Berita Acara Pemeriksaan</label>
												<div class="col-md-6">
													<input class="form-control" type="text" id="berita_acara_pemeriksaan" name="berita_acara_pemeriksaan" value="<?php echo $data_dokumen['berita_acara_pemeriksaan']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-6 control-label" for="berita_acara_pendapat">Berita Acara Pendapat</label>
												<div class="col-md-6">
													<input class="form-control" type="text" id="berita_acara_pendapat" name="berita_acara_pendapat" value="<?php echo $data_dokumen['berita_acara_pendapat']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-6 control-label" for="keputusan_kakanim">Keputusan Kepala Kantor Imigrasi</label>
												<div class="col-md-6">
													<input class="form-control" type="text" id="keputusan_kakanim" name="keputusan_kakanim" value="<?php echo $data_dokumen['keputusan_kakanim']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-6 control-label" for="surat_perintah_pendetensian">Surat Perintah Pendetensian</label>
												<div class="col-md-6">
													<input class="form-control" type="text" id="surat_perintah_pendetensian" name="surat_perintah_pendetensian" value="<?php echo $data_dokumen['surat_perintah_pendetensian']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-6 control-label" for="surat_perintah_pengeluaran_pendetensian">Surat Perintah Pengeluaran Pendetensian</label>
												<div class="col-md-6">
													<input class="form-control" type="text" id="surat_perintah_pengeluaran_pendetensian" name="surat_perintah_pengeluaran_pendetensian" value="<?php echo $data_dokumen['surat_perintah_pengeluaran_pendetensian']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-6 control-label" for="berita_acara_pengeluaran_pendetensian">Berita Acara Pengeluaran Pendetensian</label>
												<div class="col-md-6">
													<input class="form-control" type="text" id="berita_acara_pengeluaran_pendetensian" name="berita_acara_pengeluaran_pendetensian" value="<?php echo $data_dokumen['berita_acara_pengeluaran_pendetensian']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-6 control-label" for="surat_perintah_tugas">Surat Perintah Tugas</label>
												<div class="col-md-6">
													<input class="form-control" type="text" id="surat_perintah_tugas" name="surat_perintah_tugas" value="<?php echo $data_dokumen['surat_perintah_tugas']; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-6 control-label" for="surat_perintah_pengawasan_keberangkatan">Surat Perintah Pengawasan Keberangkatan</label>
												<div class="col-md-6">
													<input class="form-control" type="text" id="surat_perintah_pengawasan_keberangkatan" name="surat_perintah_pengawasan_keberangkatan" value="<?php echo $data_dokumen['surat_perintah_pengawasan_keberangkatan']; ?>">
												</div>
											</div>
											
											<div class="form-group">
												<input type="hidden" id="permohonan_id_dokumen" name="permohonan_id_dokumen" value="<?php echo $data_dokumen['permohonan_id']; ?>">
												<input type="hidden" id="dokumen_id" name="dokumen_id" value="<?php echo $data_dokumen['dokumen_id']; ?>">
												
													<div class="col-md-4 col-md-offset-2" style="margin-top:10px;">
														<button class="btn btn-block btn-success" id="do_add_dokumen" type="submit"><i class="fa fa-save pull-right"></i> Simpan</button>
													</div>
												
											</div>
												
											</form>
										</div>
										
										<div class="col-md-5 col-md-offset-1">
											<div class="block block-bordered">
												<div class="block-header bg-gray-lighter">
													<h3 class="block-title">Upload Dokumen</h3>
												</div>
												<div class="block-content">
													<div class="form-group col-md-12">
														<label class="col-md-3 control-label" for="jenis_dokumen">Jenis Dokumen</label>
														<div class="col-md-9">
															<select class="form-control" id="jenis_dokumen" name="jenis_dokumen">
																<option value="">Pilih Jenis Dokumen</option>
																<option value="Laporan Kejadian">Laporan Kejadian</option>
																<option value="Berita Acara Instansi Lain">Berita Acara Instansi Lain</option>
																<option value="Berita Acara Pemeriksaan">Berita Acara Pemeriksaan</option>
																<option value="Berita Acara Pendapat">Berita Acara Pendapat</option>
																<option value="Keputusan Kepala Kantor Imigrasi">Keputusan Kepala Kantor Imigrasi</option>
																<option value="Surat Perintah Pendetensian">Surat Perintah Pendetensian</option>
																<option value="Surat Perintah Pengeluaran Pendetensian">Surat Perintah Pengeluaran Pendetensian</option>
																<option value="Berita Acara Pengeluaran Pendetensian">Berita Acara Pengeluaran Pendetensian</option>
																<option value="Surat Perintah Tugas">Surat Perintah Tugas</option>
																<option value="Surat Perintah Pengawasan Keberangkatan">Surat Perintah Pengawasan Keberangkatan</option>
															</select>
														</div>
													</div>
													<div class="form-group col-md-12">
														<label class="col-md-3 control-label" for="nomor_dokumen">Nomor Dokumen</label>
														<div class="col-md-9">
															<input class="form-control" type="text" id="nomor_dokumen" name="nomor_dokumen">
														</div>
													</div>
													
														
													<form class="form-horizontal" method="post" enctype="multipart/form-data">
													<div class="form-group col-md-12">
														<div class="col-md-12 upload_button_area">
															<input type="file" id="upload_dokumen" name="upload_file" data-show-preview="false" accept="jpg,png,bmp">
															<div id="msgUpload_dokumen"></div>
														</div>
													</div>	
													
													</form>
													
													
													
													<div class="form-group col-md-12">
														<div class="col-md-10 col-md-offset-2" style="margin-top:10px;">
															<button class="btn btn-block btn-warning" id="reset_upload" type="reset"><i class="fa fa-remove pull-right"></i> Reset Upload Dokumen </button>
														</div>
													</div>
													
													<div class="form-group col-md-12" id="info-after-upload">
													</div>
													<div style="clear:both";></div>
												</div>
											</div>
										</div>
									</div>
									
									
							</div>
						</div>
					</div>
					
					<br/>
					<br/>
					<br/>
					
				</div>
				
			</div>
			<!-- END Dynamic Table Full -->
			
		</div>
		<!-- END Page Content -->
		
	</main>
	<!-- END Main Container -->
	
	
<?php
include_once THEME_PATH."modules/footer.php";
?>
