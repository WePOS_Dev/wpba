<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once THEME_PATH."modules/header.php";

?>		
	
	<!-- Main Container -->
	<main id="main-container">
		
		
		<div class="content">
			
			<!-- Dynamic Table Full -->
			<div class="block block-themed block-rounded" id="kanim_area">
				<div class="block-header bg-amethyst-dark" id="list-title-area">
					<h3 class="block-title">KANIM / Daftar Permohonan</h3>
				</div>
				
				<div class="block-content bg-gray-lighter" id="list-filter-area">
					<div class="row items-push">
						
						<div class="col-xs-3">
							<div><a href="javascript:void(0);" data-target="#modal-filter" data-toggle="modal"><i class="fa fa-list"></i> Kata Kunci Pencarian</a></div>
							<div class="font-w600"><a href="javascript:void(0);" id="kanim_keyword_display" data-target="#modal-filter" data-toggle="modal">-</a></div>
						</div>
						
						<div class="col-xs-3">
							<div><a href="javascript:void();" data-target="#modal-filter" data-toggle="modal"><i class="fa fa-calendar"></i> Tgl dari</a></div>
							<div class="font-w600"><a href="javascript:void();" id="kanim_tanggal_dari_display" data-target="#modal-filter" data-toggle="modal"><?php echo date("Y-m-01"); ?></a></div>
						</div>
						<div class="col-xs-3">
							<div><a href="javascript:void();" data-target="#modal-filter" data-toggle="modal"><i class="fa fa-calendar"></i> Tgl sampai</a></div>
							<div class="font-w600"><a href="javascript:void();" id="kanim_tanggal_sampai_display" data-target="#modal-filter" data-toggle="modal"><?php echo date("Y-m-t"); ?></a></div>
						</div>
					</div>
				</div>
				
				<div class="block-content" id="list-data-area">
					<div class="row items-push">
						<div class="col-lg-6">&nbsp;</div>
						<div class="col-lg-6 text-right">
							<div class="btn-group">
								<!--<button type="button" class="btn btn-warning" onclick="print_excel(0);"><i class="fa fa-print"></i>  Print</button>-->
								<button type="button" class="btn btn-success" onclick="print_excel(1);"><i class="fa fa-file-excel-o"></i>  Excel</button>
								<button type="button" class="btn btn-default btn-info" id="refresh_data"><i class="fa fa-refresh"></i> Refresh</button>
								
								<button type="button" class="btn btn-default btn-warning" id="reset_data"><i class="fa fa-remove"></i> Reset</button>
								
								<input type="hidden" id="reset_default_date" name="reset_default_date" value="<?php echo date("Y-m-01"); ?>">
								<input type="hidden" id="reset_default_date_till" name="reset_default_date_till" value="<?php echo date("Y-m-t"); ?>">
							</div>
						</div>
					</div>
					<div id="table-kanim-area">
					</div>
					
				</div>
				
				
				<div class="block-content" id="hapus-data-area">
					
					
					<input type="hidden" id="delete_permohonan_id" name="delete_permohonan_id" value="0">
						
					<div class="alert alert-danger alert-dismissable" id="form-edit-error-area">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&nbsp;</button>
						<h3 class="font-w300 push-15" id="hapus-data-title"></h3>
						<p id="hapus-data-message"></p>
					</div>
					
					<div class="form-group">
						<div class="col-md-2 col-md-offset-2" style="margin-top:10px;">
							<button class="btn btn-block btn-success" id="do_delete_permohonan"><i class="fa fa-check pull-right"></i> Hapus Data</button>
						</div>
						<div class="col-md-2" style="margin-top:10px;">
							<button class="btn btn-block btn-warning" id="close-delete-area" type="reset"><i class="fa fa-close pull-right"></i> Batal</button>
						</div>
					</div>
					
				</div>
				
			</div>
			<!-- END Dynamic Table Full -->
			
		</div>
		<!-- END Page Content -->
		
		<!-- PRINT & EXCEL -->
		<iframe id="print_excel_iframe" class="hide" src=""></iframe>
		
	</main>
	<!-- END Main Container -->
	
	<!-- Small Modal -->
	<div class="modal" id="modal-filter" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="block block-themed block-transparent remove-margin-b">
					<div class="block-header bg-primary-dark">
						<ul class="block-options">
							<li>
								<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
							</li>
						</ul>
						<h3 class="block-title">Filter Pencarian</h3>
					</div>
					<div class="block-content">
						<form class="form-horizontal" action="#" method="post" onsubmit="return false;">
							
							
							<div class="form-group">
								<div class="col-md-12" style="margin-bottom:5px;">
									<input class="form-control" type="text" id="keyword_pencarian" placeholder="No. Berita / Nama WNA / No. Paspor"/>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label" for="niora">Tanggal Permohonan</label>
								<div class="col-md-6">
									<div class="input-daterange input-group" data-date-format="yyyy-mm-dd">
										<input class="form-control" type="text" id="tanggal_dari" name="tanggal_dari" placeholder="Tgl Dari" value="<?php echo date("Y-m-01"); ?>">
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_sampai" name="tanggal_sampai" placeholder="Tgl sampai" value="<?php echo date("Y-m-t"); ?>">
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-3 col-md-offset-3" style="margin-top:10px;">
									<button class="btn btn-block btn-primary" id="do_filter_kanim"><i class="fa fa-filter pull-right"></i> Cari</button>
								</div>
								<div class="col-md-3" style="margin-top:10px;">
									<button class="btn btn-block btn-default" type="button" data-dismiss="modal" id="close-modal-filter"><i class="fa fa-remove pull-right"></i> Tutup</button>
								</div>
							</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END Small Modal -->
	
<?php
include_once THEME_PATH."modules/footer.php";
?>
