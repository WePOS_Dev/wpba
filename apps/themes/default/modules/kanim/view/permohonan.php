<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once THEME_PATH."modules/header.php";

?>		
	
	<!-- Main Container -->
	<main id="main-container">
		
		
		<div class="content">
			
			<!-- Dynamic Table Full -->
			<div class="block block-themed block-rounded" id="permohonan_area">
				<div class="block-header bg-amethyst-dark">
					<h3 class="block-title">KANIM / Pendaftaran Permohonan</h3>
				</div>
				
				<div class="block-content bg-gray-lighter">
					
					<?php
					if(!empty($is_error)){
						?>
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<h3 class="font-w300 push-15">Ada Kesalahan!</h3>
							<p><?php echo $is_error; ?></p>
						</div>
						<?php
					}
					?>
					
					<form class="js-validation-bootstrap form-horizontal" id="form-data-permohonan-kanim" action="#" method="post" onsubmit="return false;">
						
						<div class="row items-push">
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label" for="no_permohonan">No. Permohonan</label>
									<div class="col-md-8">
										<input class="form-control" type="text" id="no_permohonan" name="no_permohonan" value="<?php echo $data_edit['no_permohonan']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="tanggal_permohonan">Tanggal Permohonan</label>
									<div class="col-md-4">
										<input class="js-datepicker form-control" type="text" id="tanggal_permohonan" name="tanggal_permohonan" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $data_edit['tanggal_permohonan']; ?>">
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-4 control-label" for="niora">Produk</label>
									<div class="col-md-8">
										<input class="form-control" type="text" id="produk" name="produk" value="DEPORTASI" readonly="readonly">
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-4 control-label" for="niora">Niora</label>
									<div class="col-md-8">
										<input class="form-control" type="text" id="niora" name="niora" value="<?php echo $data_edit['niora']; ?>">
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-4 control-label" for="niora">CARI NAMA WNA / NO. PASPOR</label>
									<div class="col-md-8">
										<select class="js-select-nama-paspor form-control" id="pilih_nama_paspor" id="pilih_nama_paspor" >
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="alamat">Catatan</label>
									<div class="col-md-8">
										<textarea class="form-control" id="catatan" name="catatan" rows="2"><?php echo $data_edit['catatan']; ?></textarea>
									</div>
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="col-md-4 control-label" for="nama_wna">Nama</label>
									<div class="col-md-8">
										<input class="form-control" type="text" id="nama_wna" name="nama_wna" readonly="readonly" value="<?php echo $data_edit['nama_wna']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="tempat_lahir">Tempat Lahir</label>
									<div class="col-md-8">
										<input class="form-control" type="text" id="tempat_lahir" name="tempat_lahir" readonly="readonly" value="<?php echo $data_edit['tempat_lahir']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="tanggal_lahir">Tanggal Lahir</label>
									<div class="col-md-8">
										<input class="js-datepicker form-control" type="text" id="tanggal_lahir" name="tanggal_lahir" readonly="readonly" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $data_edit['tanggal_lahir']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="kebangsaan">Kebangsaan</label>
									<div class="col-md-8">
										<input class="form-control" type="text" id="kebangsaan" name="kebangsaan" readonly="readonly" value="<?php echo $data_edit['kebangsaan']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="no_paspor">No. Paspor</label>
									<div class="col-md-8">
										<input class="form-control" type="text" id="no_paspor" name="no_paspor" readonly="readonly" value="<?php echo $data_edit['no_paspor']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="jenis_kelamin">Jenis Kelamin (L/P)</label>
									<div class="col-md-4">
										<input class="form-control" type="text" id="jenis_kelamin" name="jenis_kelamin" readonly="readonly" value="<?php echo $data_edit['jenis_kelamin']; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="alamat">Alamat</label>
									<div class="col-md-8">
										<textarea class="form-control" id="alamat" name="alamat" rows="2" readonly="readonly"><?php echo $data_edit['alamat']; ?></textarea>
									</div>
								</div>
							</div>
						</div>
						
						
						<input type="hidden" id="is_edit" name="is_edit" value="<?php echo $data_edit['is_edit']; ?>">
						<input type="hidden" id="permohonan_id" name="permohonan_id" value="<?php echo $data_edit['permohonan_id']; ?>">
						<input type="hidden" id="id_bebas" name="id_bebas" value="<?php echo $data_edit['id_bebas']; ?>">
						<input type="hidden" id="tipe_instansi" name="tipe_instansi" value="<?php echo $data_edit['tipe_instansi']; ?>">
						<div class="form-group">
							<div class="col-md-2 col-md-offset-2" style="margin-top:10px;">
								<button class="btn btn-block btn-success" id="do_add_permohonan" type="submit"><i class="fa fa-save pull-right"></i> Simpan</button>
							</div>
							<div class="col-md-2" style="margin-top:10px;">
								<button class="btn btn-block btn-warning" id="close-modal-add" type="reset"><i class="fa fa-close pull-right"></i> Batal</button>
							</div>
						</div>
						
					</form>
					
					
					<br/>
					<br/>
					<br/>
					
				</div>
				
			</div>
			<!-- END Dynamic Table Full -->
			
		</div>
		<!-- END Page Content -->
		
	</main>
	<!-- END Main Container -->
	
	
<?php
include_once THEME_PATH."modules/footer.php";
?>
