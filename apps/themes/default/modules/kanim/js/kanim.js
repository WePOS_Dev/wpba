/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */

var BaseTableDatatables = function() {
    // Init full DataTable, for more examples you can check out https://www.datatables.net/
    var initDataTableFull = function() {
        jQuery('#table-kanim').dataTable({
            //columnDefs: [ { orderable: false, targets: [ 4 ] } ],
            pageLength: 10,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]]
        });
    };

    // DataTables Bootstrap integration
    var bsDataTables = function() {
        var $DataTable = jQuery.fn.dataTable;

        // Set the defaults for DataTables init
        jQuery.extend( true, $DataTable.defaults, {
            dom:
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            renderer: 'bootstrap',
            oLanguage: {
                sLengthMenu: "_MENU_",
                sInfo: "Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>",
                oPaginate: {
                    sPrevious: '<i class="fa fa-angle-left"></i>',
                    sNext: '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        // Default class modification
        jQuery.extend($DataTable.ext.classes, {
            sWrapper: "dataTables_wrapper form-inline dt-bootstrap",
            sFilterInput: "form-control",
            sLengthSelect: "form-control"
        });

        // Bootstrap paging button renderer
        $DataTable.ext.renderer.pageButton.bootstrap = function (settings, host, idx, buttons, page, pages) {
            var api     = new $DataTable.Api(settings);
            var classes = settings.oClasses;
            var lang    = settings.oLanguage.oPaginate;
            var btnDisplay, btnClass;

            var attach = function (container, buttons) {
                var i, ien, node, button;
                var clickHandler = function (e) {
                    e.preventDefault();
                    if (!jQuery(e.currentTarget).hasClass('disabled')) {
                        api.page(e.data.action).draw(false);
                    }
                };

                for (i = 0, ien = buttons.length; i < ien; i++) {
                    button = buttons[i];

                    if (jQuery.isArray(button)) {
                        attach(container, button);
                    }
                    else {
                        btnDisplay = '';
                        btnClass = '';

                        switch (button) {
                            case 'ellipsis':
                                btnDisplay = '&hellip;';
                                btnClass = 'disabled';
                                break;

                            case 'first':
                                btnDisplay = lang.sFirst;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'previous':
                                btnDisplay = lang.sPrevious;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'next':
                                btnDisplay = lang.sNext;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            case 'last':
                                btnDisplay = lang.sLast;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            default:
                                btnDisplay = button + 1;
                                btnClass = page === button ?
                                        'active' : '';
                                break;
                        }

                        if (btnDisplay) {
                            node = jQuery('<li>', {
                                'class': classes.sPageButton + ' ' + btnClass,
                                'aria-controls': settings.sTableId,
                                'tabindex': settings.iTabIndex,
                                'id': idx === 0 && typeof button === 'string' ?
                                        settings.sTableId + '_' + button :
                                        null
                            })
                            .append(jQuery('<a>', {
                                    'href': '#'
                                })
                                .html(btnDisplay)
                            )
                            .appendTo(container);

                            settings.oApi._fnBindAction(
                                node, {action: button}, clickHandler
                            );
                        }
                    }
                }
            };

            attach(
                jQuery(host).empty().html('<ul class="pagination"/>').children('ul'),
                buttons
            );
        };

        // TableTools Bootstrap compatibility - Required TableTools 2.1+
        if ($DataTable.TableTools) {
            // Set the classes that TableTools uses to something suitable for Bootstrap
            jQuery.extend(true, $DataTable.TableTools.classes, {
                "container": "DTTT btn-group",
                "buttons": {
                    "normal": "btn btn-default",
                    "disabled": "disabled"
                },
                "collection": {
                    "container": "DTTT_dropdown dropdown-menu",
                    "buttons": {
                        "normal": "",
                        "disabled": "disabled"
                    }
                },
                "print": {
                    "info": "DTTT_print_info"
                },
                "select": {
                    "row": "active"
                }
            });

            // Have the collection use a bootstrap compatible drop down
            jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
                "collection": {
                    "container": "ul",
                    "button": "li",
                    "liner": "a"
                }
            });
        }
    };

    return {
        init: function() {
            // Init Datatables
            bsDataTables();
            //initDataTableSimple();
            //initDataTableFull();
        }
    };
}();


var tanggal_dari = jQuery('#tanggal_dari').val();
var tanggal_sampai = jQuery('#tanggal_sampai').val();
var keyword_pencarian = jQuery('#keyword_pencarian').val();
var result_excel = 0;
var result_print = 0;
var dtTableAbsensi;

function refresh_data(load_init){
	
	var getID = '#kanim_area';
	App.blocks(getID, 'state_loading');
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();

	// Request
	var data = {
		keyword_pencarian: keyword_pencarian,
		tanggal_dari: tanggal_dari,
		tanggal_sampai: tanggal_sampai,
		xtime: sendTimer
	};
	
	var getTarget = appUrl +'kanim/kanim/load_list_data';

	//ajax-load
	$.ajax({
		url: getTarget,
		//dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata){
									
				if(load_init == true){
					BaseTableDatatables.init(); 
					$('#table-kanim-area').html(rdata);
					dtTableAbsensi = jQuery('#table-kanim').dataTable({
						//columnDefs: [ { orderable: false, targets: [ 4 ] } ],
						ordering: false,
						searching: false,
						pageLength: 10,
						lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
						oLanguage: {
							sLengthMenu: ""
						},
						dom:
							"<'row'<'col-sm-12'tr>>" +
							"<'row'<'col-sm-6'i><'col-sm-6'p>>"
					});
				}else{
					
					dtTableAbsensi.fnDestroy(true);
					$('#table-kanim-area').html(rdata);
					dtTableAbsensi = jQuery('#table-kanim').dataTable({
						//columnDefs: [ { orderable: false, targets: [ 4 ] } ],
						ordering: false,
						searching: false,
						pageLength: 10,
						lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
						oLanguage: {
							sLengthMenu: ""
						},
						dom:
							"<'row'<'col-sm-12'tr>>" +
							"<'row'<'col-sm-6'i><'col-sm-6'p>>"
					});
				}
				
			}else{
				$('#table-kanim-area').html('');
			}
			
			
			App.blocks(getID, 'state_normal');
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			App.blocks(getID, 'state_normal');
		}
	});
}

function print_excel(xval){
	
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();
	var getTarget = appUrl +'kanim/kanim/load_list_data?xtime='+sendTimer;
	
	if(!xval){
		result_print = 1;
		result_excel = 0;
		
	}else{
		result_print = 0;
		result_excel = 1;
		
	}
	
	getTarget += '&keyword_pencarian='+keyword_pencarian;
	getTarget += '&tanggal_dari='+tanggal_dari;
	getTarget += '&tanggal_sampai='+tanggal_sampai;
	getTarget += '&tipe=list_data';
	getTarget += '&result_excel='+result_excel;
	getTarget += '&result_print='+result_print;
	
	$('#print_excel_iframe').attr('src', getTarget);
	
	
}

function load_permohonan(getId){
	
	//load data
	var dtPost = {
		permohonan_id : getId
	};
	
	var d = new Date();
	
	$.ajax({
		url: appUrl+'kanim/kanim/loadData?_dc='+d.getTime(),
		dataType: 'json',
		type: 'POST',
		data: dtPost,
		success: function(data, textStatus, XMLHttpRequest)
		{
			$('#list-title-area').hide();
			$('#list-filter-area').hide();
			$('#list-data-area').hide();
			
			$('#hapus-data-area').hide();
			
			$('#no_permohonan').val(data.data.no_permohonan);		
			$('#tanggal_permohonan').val(data.data.tanggal_permohonan);		
			$('#nama_wna').val(data.data.nama_wna);		
			$('#tempat_lahir').val(data.data.tempat_lahir);		
			$('#tanggal_lahir').val(data.data.tanggal_lahir);		
			$('#kebangsaan').val(data.data.kebangsaan);		
			$('#no_paspor').val(data.data.no_paspor);		
			$('#jenis_kelamin').val(data.data.jenis_kelamin);	

			$("#jenis_kelamin").select2("val", data.data.jenis_kelamin);
			
			$('#alamat').val(data.data.alamat);		
			$('#no_ic').val(data.data.no_ic);		
			$('#tanggal_bebas').val(data.data.tanggal_bebas);

			$('#is_edit').val('1');
			$('#permohonan_id').val(data.data.id);		
			$('#tipe_instansi').val(data.data.tipe_instansi);		
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{	
			$.notify({
				icon: 'fa fa-warning',
				message: 'Load Data Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'bottom',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
			
		}

	});
	
}

function kirim_permohonan(getId){
	
	//load data
	var dtPost = {
		permohonan_id : getId
	};
	
	var d = new Date();
	
	$.ajax({
		url: appUrl+'kanim/kanim/kirimNotif?_dc='+d.getTime(),
		dataType: 'json',
		type: 'POST',
		data: dtPost,
		success: function(data, textStatus, XMLHttpRequest)
		{
			$.notify({
				icon: 'fa fa-success',
				message: data.info
			},{
				type: 'success',
				placement: {
					from: 'top',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});	
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{	
			$.notify({
				icon: 'fa fa-warning',
				message: 'Kirim Notifikasi Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'top',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
			
		}

	});
	
}

function delete_load_permohonan(getId){
	
	var dtPost = {
		permohonan_id : getId
	};
	
	var d = new Date();
	
	$.ajax({
		url: appUrl+'kanim/kanim/loadData?_dc='+d.getTime(),
		dataType: 'json',
		type: 'POST',
		data: dtPost,
		success: function(data, textStatus, XMLHttpRequest)
		{
			
			$('#list-title-area').hide();
			$('#list-filter-area').hide();
			$('#list-data-area').hide();
			
			$('#hapus-data-area').show();	
			
			$('#hapus-data-title').html('Hapus Data');
			var dt_hapus_message = 'No.Permohonan: '+data.data.no_permohonan+'<br/>';
			dt_hapus_message += 'Tanggal Permohonan: '+data.data.tanggal_permohonan+'<br/>';
			dt_hapus_message += 'Niora: '+data.data.niora+'<br/>';
			dt_hapus_message += 'Produk: '+data.data.produk+'<br/>';
			dt_hapus_message += 'Nama WNA: '+data.data.nama_wna+'<br/>';
			dt_hapus_message += 'Jenis Kelamin: '+data.data.jenis_kelamin+'<br/>';
			dt_hapus_message += 'Kebangsaan: '+data.data.kebangsaan+'<br/>';
			dt_hapus_message += 'No.Paspor: '+data.data.no_paspor+'<br/>';
			$('#hapus-data-message').html(dt_hapus_message);
	
			$('#delete_permohonan_id').val(getId);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{	
			$.notify({
				icon: 'fa fa-warning',
				message: 'Load Data Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'top',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
		}

	});
}


function do_delete_permohonan(){
	
	var delete_permohonan_id = $('#delete_permohonan_id').val();
		
	var dtPost = {
		permohonan_id : delete_permohonan_id
	};
	
	var d = new Date();
	
	$.ajax({
		url: appUrl+'kanim/kanim/deleteData?_dc='+d.getTime(),
		dataType: 'json',
		type: 'POST',
		data: dtPost,
		success: function(data, textStatus, XMLHttpRequest)
		{
			
			//refresh
			$('#close-delete-area').trigger('click');
			$('#refresh_data').trigger('click');
			
			$('#delete_permohonan_id').val(0);
			
			$.notify({
				icon: 'fa fa-check',
				message: data.info
			},{
				type: 'success',
				placement: {
					from: 'top',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{	
			$.notify({
				icon: 'fa fa-warning',
				message: 'Hapus Data Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'top',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
		}

	});
	
}

// Initialize when page loads
jQuery(function(){ 
	
	 
	$('.js-datepicker').add('.input-daterange').datepicker({
		weekStart: 1,
		autoclose: true,
		todayHighlight: true
	});
		
	$('.js-select2').select2();
	
	$('#hapus-data-area').hide();
	
	$('#list-title-area').show();
	$('#list-filter-area').show();
	$('#list-data-area').show();
	
	$('#do_filter_kanim').click(function(){
		
		keyword_pencarian = $('#keyword_pencarian').val();
		tanggal_dari = $('#tanggal_dari').val();
		tanggal_sampai = $('#tanggal_sampai').val();
		
		$('#kanim_keyword_display').html(keyword_pencarian);
		$('#kanim_tanggal_dari_display').html(tanggal_dari);
		$('#kanim_tanggal_sampai_display').html(tanggal_sampai);
		
		$('#close-modal-filter').trigger('click');
		refresh_data(false);
		
	});
	
	$('#refresh_data').click(function(){
		refresh_data(false);
	});
	
	$('#reset_data').click(function(){
		
		$('#keyword_pencarian').val("");
		$('#tanggal_dari').val($('#reset_default_date').val());
		$('#tanggal_sampai').val($('#reset_default_date_till').val());
		
		$('#do_filter_kanim').trigger("click");
		
	});
	
	$('#close-form-update').click(function(){
		
		$('#hapus-data-area').hide();
		
		$('#list-title-area').show();
		$('#list-filter-area').show();
		$('#list-data-area').show();
		
		$('#hapus-data-title').html('');
		$('#hapus-data-message').html('');
		
		$('#delete_permohonan_id').val(0);
		
	});
	
	$('#do_delete_permohonan').click(function(){
		do_delete_permohonan();
		
	});
	
	$('#close-delete-area').click(function(){
		$('#list-title-area').show();
		$('#list-filter-area').show();
		$('#list-data-area').show();
		
		$('#hapus-data-area').hide();	
		
		
	});
	
	refresh_data(true);
	
	
});