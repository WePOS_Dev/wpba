
var BaseTableDatatables = function() {
    // Init full DataTable, for more examples you can check out https://www.datatables.net/
    var initDataTableFull = function() {
        jQuery('#table-dokumen').dataTable({
            //columnDefs: [ { orderable: false, targets: [ 4 ] } ],
            pageLength: 10,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]]
        });
    };

    // DataTables Bootstrap integration
    var bsDataTables = function() {
        var $DataTable = jQuery.fn.dataTable;

        // Set the defaults for DataTables init
        jQuery.extend( true, $DataTable.defaults, {
            dom:
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            renderer: 'bootstrap',
            oLanguage: {
                sLengthMenu: "_MENU_",
                sInfo: "Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>",
                oPaginate: {
                    sPrevious: '<i class="fa fa-angle-left"></i>',
                    sNext: '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        // Default class modification
        jQuery.extend($DataTable.ext.classes, {
            sWrapper: "dataTables_wrapper form-inline dt-bootstrap",
            sFilterInput: "form-control",
            sLengthSelect: "form-control"
        });

        // Bootstrap paging button renderer
        $DataTable.ext.renderer.pageButton.bootstrap = function (settings, host, idx, buttons, page, pages) {
            var api     = new $DataTable.Api(settings);
            var classes = settings.oClasses;
            var lang    = settings.oLanguage.oPaginate;
            var btnDisplay, btnClass;

            var attach = function (container, buttons) {
                var i, ien, node, button;
                var clickHandler = function (e) {
                    e.preventDefault();
                    if (!jQuery(e.currentTarget).hasClass('disabled')) {
                        api.page(e.data.action).draw(false);
                    }
                };

                for (i = 0, ien = buttons.length; i < ien; i++) {
                    button = buttons[i];

                    if (jQuery.isArray(button)) {
                        attach(container, button);
                    }
                    else {
                        btnDisplay = '';
                        btnClass = '';

                        switch (button) {
                            case 'ellipsis':
                                btnDisplay = '&hellip;';
                                btnClass = 'disabled';
                                break;

                            case 'first':
                                btnDisplay = lang.sFirst;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'previous':
                                btnDisplay = lang.sPrevious;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'next':
                                btnDisplay = lang.sNext;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            case 'last':
                                btnDisplay = lang.sLast;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            default:
                                btnDisplay = button + 1;
                                btnClass = page === button ?
                                        'active' : '';
                                break;
                        }

                        if (btnDisplay) {
                            node = jQuery('<li>', {
                                'class': classes.sPageButton + ' ' + btnClass,
                                'aria-controls': settings.sTableId,
                                'tabindex': settings.iTabIndex,
                                'id': idx === 0 && typeof button === 'string' ?
                                        settings.sTableId + '_' + button :
                                        null
                            })
                            .append(jQuery('<a>', {
                                    'href': '#'
                                })
                                .html(btnDisplay)
                            )
                            .appendTo(container);

                            settings.oApi._fnBindAction(
                                node, {action: button}, clickHandler
                            );
                        }
                    }
                }
            };

            attach(
                jQuery(host).empty().html('<ul class="pagination"/>').children('ul'),
                buttons
            );
        };

        // TableTools Bootstrap compatibility - Required TableTools 2.1+
        if ($DataTable.TableTools) {
            // Set the classes that TableTools uses to something suitable for Bootstrap
            jQuery.extend(true, $DataTable.TableTools.classes, {
                "container": "DTTT btn-group",
                "buttons": {
                    "normal": "btn btn-default",
                    "disabled": "disabled"
                },
                "collection": {
                    "container": "DTTT_dropdown dropdown-menu",
                    "buttons": {
                        "normal": "",
                        "disabled": "disabled"
                    }
                },
                "print": {
                    "info": "DTTT_print_info"
                },
                "select": {
                    "row": "active"
                }
            });

            // Have the collection use a bootstrap compatible drop down
            jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
                "collection": {
                    "container": "ul",
                    "button": "li",
                    "liner": "a"
                }
            });
        }
    };

    return {
        init: function() {
            // Init Datatables
            bsDataTables();
            //initDataTableSimple();
            //initDataTableFull();
        }
    };
}();

function update_permohonan(){
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();

	// Request
	var data = {
		no_permohonan: $('#no_permohonan').val(),
		tanggal_permohonan: $('#tanggal_permohonan').val(),
		produk: $('#produk').val(),
		niora: $('#niora').val(),
		no_paspor: $('#no_paspor').val(),
		paspor_diberikan_tanggal: $('#paspor_diberikan_tanggal').val(),
		paspor_diberikan_di: $('#paspor_diberikan_di').val(),
		paspor_berlaku_sampai: $('#paspor_berlaku_sampai').val(),
		catatan: $('#catatan').val(),
		nama_wna: $('#nama_wna').val(),
		tempat_lahir: $('#tempat_lahir').val(),
		tanggal_lahir: $('#tanggal_lahir').val(),
		kebangsaan: $('#kebangsaan').val(),
		jenis_kelamin: $('#jenis_kelamin').select2("val"),
		status_sipil: $('#status_sipil').select2("val"),
		pekerjaan: $('#pekerjaan').val(),
		alamat: $('#alamat').val(),
		kota: $('#kota').val(),
		kantor_imigrasi: $('#kantor_imigrasi').val(),
		kantor_wilayah: $('#kantor_wilayah').val(),
		is_edit: $('#is_edit').val(),
		permohonan_id: $('#permohonan_id').val(),
		id_bebas: $('#id_bebas').val(),
		tipe_instansi:$('#tipe_instansi').val(),
		xtime: sendTimer
	};
	
	var getTarget = appUrl +'kanim/permohonan/addData';
	
	//ajax-load
	$.ajax({
		url: getTarget,
		dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata.success){
				
				$.notify({
					icon: 'fa fa-check',
					message: rdata.info
				},{
					type: 'success',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
				
				
			}else{
				
				$.notify({
					icon: 'fa fa-warning',
					message: rdata.info
				},{
					type: 'danger',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
			}
			
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			$.notify({
				icon: 'fa fa-warning',
				message: 'Simpan Permohonan Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'bottom',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
		}
	});
}

function update_sponsor(){
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();

	// Request
	var data = {
		jenis_sponsor: $('#jenis_sponsor').val(),
		status_perusahaan: $('#status_perusahaan').val(),
		sektor_kegiatan: $('#sektor_kegiatan').val(),
		nama_sponsor: $('#nama_sponsor').val(),
		nama_pimpinan: $('#nama_pimpinan').val(),
		alamat_sponsor: $('#alamat_sponsor').val(),
		kota_sponsor: $('#kota_sponsor').val(),
		kodepos_sponsor: $('#kodepos_sponsor').val(),
		telp_sponsor: $('#telp_sponsor').val(),
		permohonan_id: $('#permohonan_id_sponsor').val(),
		sponsor_id: $('#sponsor_id').val(),
		xtime: sendTimer
	};
	
	var getTarget = appUrl +'kanim/permohonan/addDataSponsor';

	//ajax-load
	$.ajax({
		url: getTarget,
		dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata.success){
				
				$('#sponsor_id').val(rdata.sponsor_id);
				
				$.notify({
					icon: 'fa fa-check',
					message: rdata.info
				},{
					type: 'success',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
				
				
			}else{
				
				$.notify({
					icon: 'fa fa-warning',
					message: rdata.info
				},{
					type: 'danger',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
			}
			
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			$.notify({
				icon: 'fa fa-warning',
				message: 'Simpan Permohonan Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'bottom',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
		}
	});
}

function update_pendaratan(){
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();

	// Request
	var data = {
		no_visa: $('#no_visa').val(),
		tempat_pengeluaran: $('#tempat_pengeluaran').val(),
		tanggal_pengeluaran: $('#tanggal_pengeluaran').val(),
		berlaku_sd: $('#berlaku_sd').val(),
		tempat_masuk: $('#tempat_masuk').val(),
		tanggal_masuk: $('#tanggal_masuk').val(),
		permohonan_id: $('#permohonan_id_pendaratan').val(),
		pendaratan_id: $('#pendaratan_id').val(),
		xtime: sendTimer
	};
	
	var getTarget = appUrl +'kanim/permohonan/addDataPendaratan';

	//ajax-load
	$.ajax({
		url: getTarget,
		dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata.success){
				
				$('#pendaratan_id').val(rdata.pendaratan_id);
				
				$.notify({
					icon: 'fa fa-check',
					message: rdata.info
				},{
					type: 'success',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
				
				
			}else{
				
				$.notify({
					icon: 'fa fa-warning',
					message: rdata.info
				},{
					type: 'danger',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
			}
			
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			$.notify({
				icon: 'fa fa-warning',
				message: 'Simpan Permohonan Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'bottom',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
		}
	});
}

function update_izin(){
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();

	// Request
	var data = {
		no_register: $('#no_register').val(),
		izin_tinggal_ke: $('#izin_tinggal_ke').val(),
		tanggal_pengeluaran: $('#izin_tanggal_pengeluaran').val(),
		berlaku_sd: $('#izin_berlaku_sd').val(),
		permohonan_id: $('#permohonan_id_izin').val(),
		izin_id: $('#izin_id').val(),
		xtime: sendTimer
	};
	
	var getTarget = appUrl +'kanim/permohonan/addDataIzin';

	//ajax-load
	$.ajax({
		url: getTarget,
		dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata.success){
				
				$('#izin_id').val(rdata.izin_id);
				
				$.notify({
					icon: 'fa fa-check',
					message: rdata.info
				},{
					type: 'success',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
				
				
			}else{
				
				$.notify({
					icon: 'fa fa-warning',
					message: rdata.info
				},{
					type: 'danger',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
			}
			
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			$.notify({
				icon: 'fa fa-warning',
				message: 'Update Izin Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'bottom',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
		}
	});
}

function update_tindakan(){
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();

	// Request
	var data = {
		pelanggaran_pasal: $('#pelanggaran_pasal').val(),
		sumber_kasus: $('#sumber_kasus').val(),
		tindakan_a: $('#tindakan_a:checked').val(),
		tindakan_b: $('#tindakan_b:checked').val(),
		tindakan_c: $('#tindakan_c:checked').val(),
		tindakan_d: $('#tindakan_d:checked').val(),
		tindakan_e: $('#tindakan_e:checked').val(),
		tindakan_f: $('#tindakan_f:checked').val(),
		tanggal_deportasi: $('#tanggal_deportasi').val(),
		sudah_dideportasi: $('#sudah_dideportasi:checked').val(),
		pengajuan_keberatan: $('#pengajuan_keberatan').val(),
		permohonan_id: $('#permohonan_id_tindakan').val(),
		tindakan_id: $('#tindakan_id').val(),
		xtime: sendTimer
	};
	
	var getTarget = appUrl +'kanim/permohonan/addDataTindakan';

	//ajax-load
	$.ajax({
		url: getTarget,
		dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata.success){
				
				$('#tindakan_id').val(rdata.tindakan_id);
				
				$.notify({
					icon: 'fa fa-check',
					message: rdata.info
				},{
					type: 'success',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
				
				
			}else{
				
				$.notify({
					icon: 'fa fa-warning',
					message: rdata.info
				},{
					type: 'danger',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
			}
			
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			$.notify({
				icon: 'fa fa-warning',
				message: 'Update Tindakan Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'bottom',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
		}
	});
}

function update_dokumen(){
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();

	// Request
	var data = {
		laporan_kejadian: $('#laporan_kejadian').val(),
		berita_acara_instansi_lain: $('#berita_acara_instansi_lain').val(),
		berita_acara_pemeriksaan: $('#berita_acara_pemeriksaan').val(),
		berita_acara_pendapat: $('#berita_acara_pendapat').val(),
		keputusan_kakanim: $('#keputusan_kakanim').val(),
		surat_perintah_pendetensian: $('#surat_perintah_pendetensian').val(),
		surat_perintah_pengeluaran_pendetensian: $('#surat_perintah_pengeluaran_pendetensian').val(),
		berita_acara_pengeluaran_pendetensian: $('#berita_acara_pengeluaran_pendetensian').val(),
		surat_perintah_tugas: $('#surat_perintah_tugas').val(),
		surat_perintah_pengawasan_keberangkatan: $('#surat_perintah_pengawasan_keberangkatan').val(),
		permohonan_id: $('#permohonan_id_tindakan').val(),
		dokumen_id: $('#dokumen_id').val(),
		xtime: sendTimer
	};
	
	var getTarget = appUrl +'kanim/permohonan/addDataDokumen';

	//ajax-load
	$.ajax({
		url: getTarget,
		dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata.success){
				
				$('#dokumen_id').val(rdata.dokumen_id);
				
				$.notify({
					icon: 'fa fa-check',
					message: rdata.info
				},{
					type: 'success',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
				
				
			}else{
				
				$.notify({
					icon: 'fa fa-warning',
					message: rdata.info
				},{
					type: 'danger',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
			}
			
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			$.notify({
				icon: 'fa fa-warning',
				message: 'Update Dokumen Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'bottom',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
		}
	});
}

 
 var upload_dokumen = {
	name: 'dokumen',
	text: 'Upload Dokumen'
};

var upload_dokumen,permohonan_id_dokumen = jQuery('#permohonan_id_dokumen').val();
var jenis_dokumen = jQuery('#jenis_dokumen').val();
var nomor_dokumen = jQuery('#nomor_dokumen').val();


 $(function () {
	
	$('#jenis_dokumen').change(function(){
		jenis_dokumen = $(this).val();
			
	});
	
	$('#nomor_dokumen').change(function(){
		nomor_dokumen = $(this).val();
	});
	
	//UPLOAD FILE
    upload_dokumen = $('#upload_'+upload_dokumen.name).fileinput({
        uploadUrl: appUrl+'kanim/permohonan/upload_dokumen', // you must set a valid URL here else you will get an error
        allowedFileExtensions : ['jpg','jpeg','png','bmp','pdf'],
		//uploadAsync: false,
		//showUpload: false, // hide upload button
		showRemove: false, // hide remove button
        maxFilesNum: 1,
		browseClass: "btn btn-success",
		browseLabel: " Pilih File",
		browseIcon: '<i class="fa fa-file-image-o"></i> ',
		uploadClass: "btn btn-info",
		//showCaption: false,
		elErrorContainer: "#msgUpload_"+upload_dokumen.name,
		//elCaptionText: "#fileUploadName_"+module_config.name, 
		uploadExtraData: {
			//permohonan_id: permohonan_id_dokumen,
			//jenis_dokumen: jenis_dokumen,
			//nomor_dokumen: nomor_dokumen,
			uploadname: 'upload_file'
		}
	}).on("filepreupload", function(event, files) {
	
		//blockUI('#preview_upload_excel_'+upload_dokumen.name);	
		$('#info-after-upload').html('');
		
	}).on("filebatchpreupload", function(event, data, jqXHR) {
	
		data.form.append('permohonan_id',  $("#permohonan_id_dokumen").val());
		data.form.append('jenis_dokumen',  $("#jenis_dokumen").val());
		data.form.append('nomor_dokumen',  $("#nomor_dokumen").val());
		
	}).on('filebatchuploadsuccess', function(event, data, previewId, index) {
		var form = data.form, files = data.files, extra = data.extra,
			response = data.response, reader = data.reader;
			
		$('#info-after-upload').html(response.info);
		$('#reset_upload').trigger('click');
		//response.data.id_upload
		refresh_data();
		
	}).on('filebatchuploadcomplete', function(event, files, extra) {
		$('#upload_'+upload_dokumen.name).fileinput("clear");
	});
	
	
	
});

var dtTableUpload;

function refresh_data(load_init){
	
	var getID = '#dokumen_upload_area';
	App.blocks(getID, 'state_loading');
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();

	// Request
	var data = {
		permohonan_id: jQuery("#permohonan_id_dokumen").val(),
		xtime: sendTimer
	};
	
	var getTarget = appUrl +'kanim/permohonan/load_list_upload';

	//ajax-load
	$.ajax({
		url: getTarget,
		//dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata){
									
				if(load_init == true){
					BaseTableDatatables.init(); 
					$('#table-dokumen-area').html(rdata);
					dtTableUpload = jQuery('#table-dokumen').dataTable({
						//columnDefs: [ { orderable: false, targets: [ 4 ] } ],
						ordering: false,
						searching: false,
						pageLength: 10,
						lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
						oLanguage: {
							sLengthMenu: ""
						},
						dom:
							"<'row'<'col-sm-12'tr>>" +
							"<'row'<'col-sm-6'i><'col-sm-6'p>>"
					});
				}else{
					
					dtTableUpload.fnDestroy(true);
					$('#table-dokumen-area').html(rdata);
					dtTableUpload = jQuery('#table-dokumen').dataTable({
						//columnDefs: [ { orderable: false, targets: [ 4 ] } ],
						ordering: false,
						searching: false,
						pageLength: 10,
						lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
						oLanguage: {
							sLengthMenu: ""
						},
						dom:
							"<'row'<'col-sm-12'tr>>" +
							"<'row'<'col-sm-6'i><'col-sm-6'p>>"
					});
				}
				
			}else{
				$('#table-dokumen-area').html('');
			}
			
			
			App.blocks(getID, 'state_normal');
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			App.blocks(getID, 'state_normal');
		}
	});
}



function delete_dokumen(getId){
	
	var dtPost = {
		dokumen_id : getId
	};
	
	var d = new Date();
	
	var get_nama_file = $('#nama_file_'+getId).val();
	var get_jenis_dokumen = $('#jenis_dokumen_'+getId).val();
	
	var r = confirm("Delete Dokumen: "+get_jenis_dokumen+"?");
	if (r == true) {
		$.ajax({
			url: appUrl+'kanim/permohonan/deleteDokumen?_dc='+d.getTime(),
			dataType: 'json',
			type: 'POST',
			data: dtPost,
			success: function(data, textStatus, XMLHttpRequest)
			{
				
				//refresh
				refresh_data();
				
				$.notify({
					icon: 'fa fa-check',
					message: data.info
				},{
					type: 'success',
					placement: {
						from: 'top',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown)
			{	
				$.notify({
					icon: 'fa fa-warning',
					message: 'Hapus Dokumen Gagal!'
				},{
					type: 'danger',
					placement: {
						from: 'top',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
			}

		});
		
	} else {
		//return false;
	}
}

// Initialize when page loads
jQuery(function(){ 
	
	$('#reset_upload').click(function(){
		$('#jenis_dokumen').val('');
		$('#nomor_dokumen').val('');
	});
	
	refresh_data(true);
	
	$('#refresh-list-dokumen').click(function(){
		refresh_data();
	});
	
	$('.js-datepicker').add('.input-daterange').datepicker({
		weekStart: 1,
		autoclose: true,
		todayHighlight: true
	});
		
	$('.js-select2').select2();	
	
	$('.js-validation-bootstrap').validate({
		errorClass: 'help-block animated fadeInDown',
		errorElement: 'div',
		errorPlacement: function(error, e) {
			jQuery(e).parents('.form-group > div').append(error);
		},
		highlight: function(e) {
			jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
			jQuery(e).closest('.help-block').remove();
		},
		success: function(e) {
			jQuery(e).closest('.form-group').removeClass('has-error');
			jQuery(e).closest('.help-block').remove();
		},
		rules: {
			'no_permohonan': {
				required: true
			},
			'tanggal_permohonan': {
				required: true
			},
			'niora': {
				required: true
			},
			'no_paspor': {
				required: true
			},
			'nama_wna': {
				required: true
			},
			'tempat_lahir': {
				required: true
			},
			'tanggal_lahir': {
				required: true
			},
			'kebangsaan': {
				required: true
			}
		},
		messages: {
			'no_permohonan': 'Inputkan No. Permohonan',
			'tanggal_permohonan': 'Inputkan Tanggal Permohonan',
			'no_paspor': 'Inputkan No.Paspor',
			'niora': 'Inputkan NIORA',
			'nama_wna': 'Inputkan Nama NA',
			'tempat_lahir': 'Inputkan Tempat Lahir',
			'tanggal_lahir': 'Inputkan Tanggal Lahir',
			'kebangsaan': 'Inputkan Kebangsaan'
		},
		submitHandler: function(form) {
			update_permohonan();
		}
	});
	
	$('.js-validation-sponsor').validate({
		errorClass: 'help-block animated fadeInDown',
		errorElement: 'div',
		errorPlacement: function(error, e) {
			jQuery(e).parents('.form-group > div').append(error);
		},
		highlight: function(e) {
			jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
			jQuery(e).closest('.help-block').remove();
		},
		success: function(e) {
			jQuery(e).closest('.form-group').removeClass('has-error');
			jQuery(e).closest('.help-block').remove();
		},
		rules: {
			'jenis_sponsor': {
				required: true
			},
			'status_perusahaan': {
				required: true
			},
			'nama_sponsor': {
				required: true
			},
			'kota_sponsor': {
				required: true
			},
			'telp_sponsor': {
				required: true
			}
		},
		messages: {
			'jenis_sponsor': 'Inputkan Jenis Sponsor',
			'status_perusahaan': 'Inputkan Status Perusahaan',
			'nama_sponsor': 'Inputkan Nama Sponsor',
			'kota_sponsor': 'Inputkan Kota Sponsor',
			'telp_sponsor': 'Inputkan No.Telp Sponsor'
		},
		submitHandler: function(form) {
			update_sponsor();
		}
	});
	
	$('.js-validation-pendaratan').validate({
		errorClass: 'help-block animated fadeInDown',
		errorElement: 'div',
		errorPlacement: function(error, e) {
			jQuery(e).parents('.form-group > div').append(error);
		},
		highlight: function(e) {
			jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
			jQuery(e).closest('.help-block').remove();
		},
		success: function(e) {
			jQuery(e).closest('.form-group').removeClass('has-error');
			jQuery(e).closest('.help-block').remove();
		},
		rules: {
			'no_visa': {
				required: true
			},
			'tempat_pengeluaran': {
				required: true
			},
			'tanggal_pengeluaran': {
				required: true
			},
			'berlaku_sd': {
				required: true
			},
			'tempat_masuk': {
				required: true
			},
			'tanggal_masuk': {
				required: true
			}
		},
		messages: {
			'no_visa': 'Inputkan No Visa',
			'tempat_pengeluaran': 'Inputkan Tempat Pengeluaran',
			'tanggal_pengeluaran': 'Inputkan Tanggal Pengeluaran',
			'berlaku_sd': 'Inputkan Tanggal Berlaku s/d',
			'tempat_masuk': 'Inputkan Tempat Masuk',
			'tanggal_masuk': 'Inputkan Tanggal Masuk'
		},
		submitHandler: function(form) {
			update_pendaratan();
		}
	});
	
	$('.js-validation-izin').validate({
		errorClass: 'help-block animated fadeInDown',
		errorElement: 'div',
		errorPlacement: function(error, e) {
			jQuery(e).parents('.form-group > div').append(error);
		},
		highlight: function(e) {
			jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
			jQuery(e).closest('.help-block').remove();
		},
		success: function(e) {
			jQuery(e).closest('.form-group').removeClass('has-error');
			jQuery(e).closest('.help-block').remove();
		},
		rules: {
			'no_register': {
				required: true
			},
			'izin_tinggal_ke': {
				required: true
			},
			'izin_tanggal_pengeluaran': {
				required: true
			},
			'izin_berlaku_sd': {
				required: true
			}
		},
		messages: {
			'no_register': 'Inputkan No Register',
			'izin_tinggal_ke': 'Inputkan Izin Tinggek Ke',
			'izin_tanggal_pengeluaran': 'Inputkan Tanggal Pengeluaran',
			'izin_berlaku_sd': 'Inputkan Tanggal Berlaku s/d'
		},
		submitHandler: function(form) {
			update_izin();
		}
	});
	
	$('.js-validation-tindakan').validate({
		errorClass: 'help-block animated fadeInDown',
		errorElement: 'div',
		errorPlacement: function(error, e) {
			jQuery(e).parents('.form-group > div').append(error);
		},
		highlight: function(e) {
			jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
			jQuery(e).closest('.help-block').remove();
		},
		success: function(e) {
			jQuery(e).closest('.form-group').removeClass('has-error');
			jQuery(e).closest('.help-block').remove();
		},
		rules: {
			'pelanggaran_pasal': {
				required: true
			},
			'sumber_kasus': {
				required: true
			}
		},
		messages: {
			'pelanggaran_pasal': 'Inputkan Pelanggaran Pasal',
			'sumber_kasus': 'Inputkan Sumber Kasus'
		},
		submitHandler: function(form) {
			update_tindakan();
		}
	});
	
	$('#do_add_dokumen').click(function(){
		update_dokumen();
	});
	
});