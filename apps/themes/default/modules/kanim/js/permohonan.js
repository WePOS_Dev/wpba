
function add_permohonan(){
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();
	
	var id_bebas = $('#id_bebas').val();
	if(id_bebas == 0 || id_bebas == ''){
		$.notify({
			icon: 'fa fa-warning',
			message: 'Silahkan pilih Nama WNA / No. Paspor'
		},{
			type: 'danger',
			placement: {
				from: 'bottom',
				align: 'center'
			},
			offset: {
				x: 0,
				y: 60
			}
		});
		
		return false;
	}

	// Request
	var data = {
		no_permohonan: $('#no_permohonan').val(),
		tanggal_permohonan: $('#tanggal_permohonan').val(),
		produk: $('#produk').val(),
		niora: $('#niora').val(),
		pilih_nama_paspor: $('#pilih_nama_paspor').select2('val'),
		catatan: $('#catatan').val(),
		nama_wna: $('#nama_wna').val(),
		tempat_lahir: $('#tempat_lahir').val(),
		tanggal_lahir: $('#tanggal_lahir').val(),
		kebangsaan: $('#kebangsaan').val(),
		no_paspor: $('#no_paspor').val(),
		jenis_kelamin: $('#jenis_kelamin').val(),
		alamat: $('#alamat').val(),
		is_edit: $('#is_edit').val(),
		permohonan_id: $('#permohonan_id').val(),
		id_bebas: $('#id_bebas').val(),
		tipe_instansi:$('#tipe_instansi').val(),
		xtime: sendTimer
	};
	
	var getTarget = appUrl +'kanim/permohonan/addData';

	
	$('#info-permohonan-kanim').html('');
	$('#info-permohonan-kanim').hide();
	
	//ajax-load
	$.ajax({
		url: getTarget,
		dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata.success){
				
				//window.location = appUrl +'kanim/permohonan';
				$('#form-data-permohonan-kanim')[0].reset();
				$('#is_edit').val(0);
				$('#permohonan_id').val(0);
				$('#id_bebas').val(0);
				$('#produk').val('DEPORTASI');
				$('#pilih_nama_paspor').select2('val','');
				
				$.notify({
					icon: 'fa fa-check',
					message: rdata.info
				},{
					type: 'success',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
				
				var d = new Date();
				setTimeout(function(){
					window.location =  appUrl +'kanim/permohonan/detail?xid='+rdata.permohonan_id+'_'+d.getTime();
				}, 3000);
				
			}else{
				
				$.notify({
					icon: 'fa fa-warning',
					message: rdata.info
				},{
					type: 'danger',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
			}
			
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			$.notify({
				icon: 'fa fa-warning',
				message: 'Simpan Permohonan Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'bottom',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
		}
	});
}

function load_permohonan(getId){
	
	$('#nama_wna').val('');		
	$('#tempat_lahir').val('');		
	$('#tanggal_lahir').val('');		
	$('#kebangsaan').val('');		
	$('#no_paspor').val('');		
	$('#jenis_kelamin').val('');
	$('#alamat').val('');
	$('#catatan').val('');
	
	//load data
	var dtPost = {
		bebas_id : getId
	};
	
	var d = new Date();
	
	$.ajax({
		url: appUrl+'lapas/lapas/loadData?_dc='+d.getTime(),
		dataType: 'json',
		type: 'POST',
		data: dtPost,
		success: function(data, textStatus, XMLHttpRequest)
		{
			$('#nama_wna').val(data.data.nama_wna);		
			$('#tempat_lahir').val(data.data.tempat_lahir);		
			$('#tanggal_lahir').val(data.data.tanggal_lahir);		
			$('#kebangsaan').val(data.data.kebangsaan);		
			$('#no_paspor').val(data.data.no_paspor);		
			$('#jenis_kelamin').val(data.data.jenis_kelamin);
			$('#alamat').val(data.data.alamat);	
			$('#id_bebas').val(data.data.id);
			$('#tipe_instansi').val(data.data.tipe_instansi);
			$('#catatan').val(data.data.catatan);

			//$('#is_edit').val(0);
			//$('#permohonan_id').val(0);				
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{	
			$.notify({
				icon: 'fa fa-warning',
				message: 'Load Data Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'bottom',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
			
		}

	});
	
}

// Initialize when page loads
jQuery(function(){ 
	
	 
	$('.js-datepicker').add('.input-daterange').datepicker({
		weekStart: 1,
		autoclose: true,
		todayHighlight: true
	});
		
	$('.js-select2').select2();	
	
	$('.js-select-nama-paspor').select2({
		placeholder: "Cari Nama WNA / No.Paspor",
		ajax: {
			url: appUrl +'kanim/permohonan/cekBebas',
			dataType: 'json',
			delay: 250,
			data: function (params) {
			  return {
				q: params.term, // search term
				page: params.page
			  };
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							text: item.text,
							id: item.id,
							nama_wna: item.nama_wna,
							tempat_lahir: item.tempat_lahir,
							tanggal_lahir: item.tanggal_lahir,
							kebangsaan: item.kebangsaan,
							no_paspor: item.no_paspor,
							jenis_kelamin: item.jenis_kelamin,
							alamat: item.alamat
						}
					})
				};
			  
			},
			cache: true
		},
		minimumInputLength: 0
		
	}).on("change", function() {
         
			//alert(this.value);
			//$('#id_bebas').val(this.value);
			load_permohonan(this.value);
			
        });	
	
       
	
	$('.js-validation-bootstrap').validate({
		errorClass: 'help-block animated fadeInDown',
		errorElement: 'div',
		errorPlacement: function(error, e) {
			jQuery(e).parents('.form-group > div').append(error);
		},
		highlight: function(e) {
			jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
			jQuery(e).closest('.help-block').remove();
		},
		success: function(e) {
			jQuery(e).closest('.form-group').removeClass('has-error');
			jQuery(e).closest('.help-block').remove();
		},
		rules: {
			'no_permohonan': {
				required: true
			},
			'tanggal_permohonan': {
				required: true
			},
			//'pilih_nama_paspor': {
			//	required: true
			//},
			'niora': {
				required: true
			}
		},
		messages: {
			'no_permohonan': 'Inputkan No. Permohonan',
			'tanggal_permohonan': 'Inputkan Tanggal Permohonan',
			//'pilih_nama_paspor': 'Pilih Nama WNA / No.Paspor',
			'niora': 'Inputkan NIORA'
		},
		submitHandler: function(form) {
			add_permohonan();
		}
	});
	
	
});