 
 var module_config = {
	name: 'early_warning',
	text: 'Data Early Warning'
};


 $(function () {
				
	//UPLOAD FILE
    $('#upload_excel_'+module_config.name).fileinput({
        uploadUrl: appUrl+'early_warning/upload_excel', // you must set a valid URL here else you will get an error
        allowedFileExtensions : ['xls'],
		//uploadAsync: false,
		//showUpload: false, // hide upload button
		showRemove: false, // hide remove button
        maxFilesNum: 1,
		browseClass: "btn btn-success",
		browseLabel: " Pilih File (Excel)",
		browseIcon: '<i class="fa fa-file-excel-o"></i> ',
		uploadClass: "btn btn-warning",
		//showCaption: false,
		elErrorContainer: "#msgUpload_"+module_config.name,
		//elCaptionText: "#fileUploadName_"+module_config.name, 
		uploadExtraData: {
			//tipe: "early_warning",
			uploadname: 'upload_file'
		}
	}).on("filepreupload", function(event, files) {
	
		//blockUI('#preview_upload_excel_'+module_config.name);	
		$('#info-after-upload').html('');
		
	}).on('filebatchuploadsuccess', function(event, data, previewId, index) {
		var form = data.form, files = data.files, extra = data.extra,
			response = data.response, reader = data.reader;
			
		$('#info-after-upload').html('Total Data Imported: '+response.total_valid_data);
		//response.data.id_upload
		
	}).on('filebatchuploadcomplete', function(event, files, extra) {
		$('#upload_excel_'+module_config.name).fileinput("clear");
	});
	
	
	
});
