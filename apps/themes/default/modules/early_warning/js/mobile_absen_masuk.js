// Initialize when page loads
jQuery(function(){ 
	jQuery('.fingerprint_button').easyPieChart({
		barColor: '#ff8f8f',
		trackColor: '#eeeeee',
		lineWidth: 5,
		size: 0,
		animate: 2500,
		scaleColor: jQuery(this).data('scale-color') ? jQuery(this).data('scale-color') : false
	});
	
	
	$('#fingerprint_now').click(function(){
		$('.fingerprint_button').data('easyPieChart').update(0);
		animate_fp('#5c90d2'); //#5c90d2
	});
});


function animate_fp(barColor){
	//$('.fingerprint_button').data('easyPieChart').update(0);
	$('.fingerprint_button').data('easyPieChart').options.barColor = barColor;
	//$('.fingerprint_button').data('easyPieChart').options.animate = 2000;
	
	
	var getTarget = $('#fingerprint_now').attr('data-action-url');
	
	// Start timer
	var sendTimer = new Date().getTime();

	// Request
	var data = {
		tipe: 'masuk',
		xtime: sendTimer
	};

	//ajax-load
	$.ajax({
		url: getTarget,
		dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata.success){
				$('.fingerprint_button').data('easyPieChart').update(100);
				setTimeout(function(){
					var success_img = $('#fingerprint_now').attr("success-img");
					$('#fingerprint_now img').attr('src', success_img);
				},3000);
			}else{
				$('.fingerprint_button').data('easyPieChart').options.barColor = '#ff6c9d';
				$('.fingerprint_button').data('easyPieChart').update(100);
			}
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			$('.fingerprint_button').data('easyPieChart').options.barColor = '#ff6c9d';
			$('.fingerprint_button').data('easyPieChart').update(100);
			
		}
	});
	
	
}