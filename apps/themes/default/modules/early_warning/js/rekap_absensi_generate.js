 
var maxTanggal = 0;
var currTanggal = 0;
var filterForm = 'filter-rekap-absensi-form';
var resultArea = 'generate-rekap-absensi-result';
		
 $(function () {
	 
	$('.js-datepicker').add('.input-daterange').datepicker({
		weekStart: 1,
		autoclose: true,
		todayHighlight: true
	});
	
	$('#generate-rekap-absensi-button').click(function(){
		
		var tgl_dari = $('#rekap-daterange1').val();
		var tgl_sampai = $('#rekap-daterange2').val();
		
		// Start timer
		var sendTimer = new Date().getTime();

		// Request
		var data = {
			tipe: 'cek',
			tgl_dari: tgl_dari,
			tgl_sampai: tgl_sampai,
			xtime: sendTimer
		};
		
		var getTarget = $('#'+filterForm).attr('action');
		
		$('#'+resultArea).html('');
		App.blocks('#'+resultArea, 'state_loading');
		
		//ajax-load
		$.ajax({
			url: getTarget,
			dataType: 'json',
			type: 'POST',
			data: data,
			success: function(rdata, textStatus, XMLHttpRequest)
			{
				maxTanggal = rdata.total;
				currTanggal = 1;
				
				generate_pertanggal(tgl_dari,currTanggal,maxTanggal);
				App.blocks('#'+resultArea, 'state_normal');
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown)
			{
				$('#'+resultArea).append('Generate Gagal!');
				App.blocks('#'+resultArea, 'state_normal');
			}
		});
		
	});
	
	
});

function generate_pertanggal(tgl_dari, currTanggal, maxTanggal){
	
	App.blocks('#'+resultArea, 'state_loading');
	var getTarget = $('#'+filterForm).attr('action');
	
	// Start timer
	var sendTimer = new Date().getTime();

	// Request
	var data = {
		tipe: 'generate',
		tgl_dari: tgl_dari,
		currTanggal: currTanggal,
		maxTanggal: maxTanggal,
		xtime: sendTimer
	};
	
	//ajax-load
	$.ajax({
		url: getTarget,
		dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			
			currTanggal++;
			if(currTanggal <= maxTanggal){
				$('#'+resultArea).append(rdata.info);
				generate_pertanggal(tgl_dari, currTanggal, maxTanggal);
			}else{
				$('#'+resultArea).append(rdata.info+'<br/>Generate Selesai');
			}
			
			App.blocks('#'+resultArea, 'state_normal');
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			$('#'+resultArea).append('<br/>Generate Gagal!');
			App.blocks('#'+resultArea, 'state_normal');
		}
	});
	
}