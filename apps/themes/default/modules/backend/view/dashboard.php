<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once THEME_PATH."modules/header.php";
?>		
	
	<!-- Main Container -->
	<main id="main-container">
		
		<!-- Page Header -->
		<div class="content bg-gray-lighter">
			<div class="row items-push">
				<div class="col-lg-12 col-sm-12">
					<h1 class="page-heading text-center">
						<small class="font-w700 text-gray-darker">Selamat Datang, <?php echo $login_data['full_name'];?></small>
					</h1>
				</div>
				<!--<div class="col-sm-5 text-right hidden-xs">
					<ol class="breadcrumb push-10-t">
						<li>UI Elements</li>
						<li><a class="link-effect" href="">Widgets</a></li>
					</ol>
				</div>-->
			</div>
		</div>
		<!-- END Page Header -->
		
		
		
		<div class="content">
			
			<div class="row">
				<div class="col-lg-4 text-center">
					<img src="<?php echo ASSETS_URL; ?>img/various/kemenkumham.png" width="120" height="120" />
					<br/>
					<h2 class="page-heading text-center">
						<small class="font-w700 text-gray-darker">Kementrian Hukum dan HAM RI</small>
					</h1>
				</div>
				<div class="col-lg-4 text-center">
					<img src="<?php echo ASSETS_URL; ?>img/various/logo.png" width="120" height="120" />
					<br/>
					<h2 class="page-heading text-center">
						<small class="font-w700 text-gray-darker"><?php echo config_item('client_name'); ?></small>
					</h1>
				</div>
				<div class="col-lg-4 text-center">
					<img src="<?php echo ASSETS_URL; ?>img/various/lapas.png" width="120" height="120" />
					<br/>
					<h2 class="page-heading text-center">
						<small class="font-w700 text-gray-darker">LAPAS - RUTAN</small>
					</h1>
				</div>
			</div>
			<br/>
			<br/>
			<br/>
			<?php
			if(in_array($login_data['role_id'], array(1,2))){
			?>
				<div class="row">
					<?php 
					if(!empty($total_pemberitahuan)){
					?>
					<div class="col-sm-6 col-lg-4">
						<a class="block block-rounded block-link-hover3 text-center" href="javascript:void(0)">
							<div class="block-content block-content-full">
								<div class="h1 font-w700"><?php echo $total_pemberitahuan; ?></div>
								<div class="h5 text-muted text-uppercase push-5-t">PEMBERITAHUAN</div>
							</div>
							<div class="block-content block-content-full block-content-mini bg-info text-white">
								<i class="fa fa-info-circle text-black-op"></i> 
								<?php 
								if($total_pemberitahuan_belum_terbaca == 0){
									echo 'semua notifikasi pemberitahuan sudah dibaca';
								}else{
									echo $total_pemberitahuan_belum_terbaca.' notifikasi pemberitahuan terbaru belum dibaca';
								}
								?>
							</div>
						</a>
					</div>
					<?php 
					}
					
					if(!empty($total_permohonan)){
					?>
					<div class="col-sm-6 col-lg-4">
						<a class="block block-rounded block-link-hover3 text-center" href="javascript:void(0)">
							<div class="block-content block-content-full">
								<div class="h1 font-w700"><?php echo $total_permohonan; ?></div>
								<div class="h5 text-muted text-uppercase push-5-t">PERMOHONAN</div>
							</div>
							<div class="block-content block-content-full block-content-mini bg-success text-white">
								<i class="fa fa-info-circle text-black-op"></i> 
								<?php 
								if($total_pemberitahuan_belum_ada_permohonan == 0){
									echo 'semua pemberitahuan sudah dibuat permohonan';
								}else{
									echo $total_pemberitahuan_belum_ada_permohonan.' pemberitahuan belum dibuat permohonan';
								}
								?>
							</div>
						</a>
					</div>
					<?php 
					}
					
					if(!empty($total_tindakan)){
					?>
					<div class="col-sm-6 col-lg-4">
						<a class="block block-rounded block-link-hover3 text-center" href="javascript:void(0)">
							<div class="block-content block-content-full">
								<div class="h1 font-w700"><?php echo $sudah_dideportasi; ?></div>
								<div class="h5 text-muted text-uppercase push-5-t">DEPORTASI</div>
							</div>
							<div class="block-content block-content-full block-content-mini bg-danger text-white">
								<i class="fa fa-info-circle text-black-op"></i> dari <?php echo $total_tindakan; ?> data yang mempunyai tindakan administrasi
							</div>
						</a>
					</div>
					<?php
					}
					?>
				</div>
			<?php
			}
			?>
		</div>
		
	</main>
	<!-- END Main Container -->
	
	<!-- Small Modal -->
	<div class="modal" id="modal-list-pegawai" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="block block-themed block-transparent remove-margin-b">
					<div class="block-header bg-primary-dark">
						<ul class="block-options">
							<li>
								<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
							</li>
						</ul>
						<h3 class="block-title" id="title-list-pegawai">BIDANG</h3>
					</div>
					<div class="block-content" id="content-list-pegawai">
						
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-sm btn-default" type="button" data-dismiss="modal" id="close-modal-list-pegawai">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- END Small Modal -->
	
<?php
include_once THEME_PATH."modules/footer.php";
?>
