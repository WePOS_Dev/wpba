<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once THEME_PATH."modules/header.php";

$module_config = array(
	'primaryKey' => 'id_user',
	'name' => 'change_password_user',
	'text' => 'Ubah Password'
);
?>	

	<!-- Main Container -->
	<main id="main-container">
		<!-- Page Header -->
		<div class="content bg-gray-lighter">
			<div class="row items-push">
				<div class="col-sm-7">
					<h1 class="page-heading">
						<?php echo $module_config['text']; ?>
					</h1>
				</div>
				<div class="col-sm-5 text-right hidden-xs">
					<ol class="breadcrumb push-10-t">
						<li>Profile</li>
						<li><a class="link-effect" href="<?php echo BASE_URL.'backend/ubah_password';?>"><?php echo $module_config['text']; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
		<!-- END Page Header -->
	
		<div class="row">
			<div class="col-lg-6">
				<!-- Bootstrap Register -->
				<div class="block block-themed">
					<div class="block-content">
						<form class="form-horizontal" role="form" id="addEditForm_<?php echo $module_config['name']; ?>">
						  <div class="form-group">
							<label for="inputOldPassword" class="col-sm-5 control-label">Password Lama</label>
							<div class="col-sm-7">
							  <input type="password" class="form-control" id="inputOldPassword" name="inputOldPassword" placeholder="Input Password Lama">
							</div>
						  </div>
						  <div class="form-group">
							<label for="inputNewPassword" class="col-sm-5 control-label">Password Baru</label>
							<div class="col-sm-7">
							  <input type="password" class="form-control" id="inputNewPassword" name="inputNewPassword" placeholder="Input Password Baru">
							</div>
						  </div>
						  <div class="form-group">
							<label for="inputConfirmNewPassword" class="col-sm-5 control-label">Konfirmasi Password Baru</label>
							<div class="col-sm-7">
							  <input type="password" class="form-control" id="inputConfirmNewPassword" name="inputConfirmNewPassword" placeholder="Input Konfirmasi Password Baru">
							</div>
						  </div>
						  <div class="form-group">
							<div id="messageAddEditForm_<?php echo $module_config['name']; ?>" class="msgInfo"></div>
						  </div>
						  <div class="form-group mt50">
							<div class="col-sm-offset-5 col-sm-7">
							  <button type="submit" class="hidden">Submit</button>
							  <button type="button" class="btn btn-primary" id="save_addEditForm_<?php echo $module_config['name']; ?>"><?php echo $module_config['text'] ; ?></button>
							</div>
						  </div>
						</form>
						
					</div>
				</div>
				<!-- END Bootstrap Register -->
			</div>
		</div>
	</main>	
	
<?php
include_once THEME_PATH."modules/footer.php";
?>
