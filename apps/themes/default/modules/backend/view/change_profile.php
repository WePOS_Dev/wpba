<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once THEME_PATH."modules/header.php";

$module_config = array(
	'primaryKey' => 'id_user',
	'name' => 'change_profile',
	'text' => 'Ubah Profil'
);
?>		
	<!-- Main Container -->
	<main id="main-container">
		<!-- Page Header -->
		<div class="content bg-gray-lighter">
			<div class="row items-push">
				<div class="col-sm-7">
					<h1 class="page-heading">
						<?php echo $module_config['text']; ?>
					</h1>
				</div>
				<div class="col-sm-5 text-right hidden-xs">
					<ol class="breadcrumb push-10-t">
						<li>Profile</li>
						<li><a class="link-effect" href="<?php echo BASE_URL.'backend/ubah_password';?>"><?php echo $module_config['text']; ?></a></li>
					</ol>
				</div>
			</div>
		</div>
		<!-- END Page Header -->
	
		<div class="row">
			<div class="col-lg-6">
				<!-- Bootstrap Register -->
				<div class="block block-themed">
					<div class="block-content">
						
						<div id="horizontal-form">
							<form class="form-horizontal" role="form" id="addEditForm_<?php echo $module_config['name']; ?>">
							  <div class="form-group">
								<label for="inputName" class="col-sm-3 control-label">Nama</label>
								<div class="col-sm-9">
								  <input type="text" class="form-control" name="full_name" id="inputName" placeholder="Input Nama" value="<?php echo $login_data['full_name']; ?>">
								</div>
							  </div>
							  <div class="form-group">
								<label for="inputName" class="col-sm-3 control-label">Email</label>
								<div class="col-sm-9">
								  <input type="text" class="form-control" name="user_email" id="inputEmail" placeholder="Input Email" value="<?php echo $login_data['user_email']; ?>">
								</div>
							  </div>
							  <div class="form-group">
								<label for="inputEmail" class="col-sm-3 control-label">No Telp</label>
								<div class="col-sm-9">
								  <input type="text" class="form-control" name="user_mobile" id="inputNoTelp" placeholder="Input No Telepon" value="<?php echo $login_data['user_mobile']; ?>">
								</div>
							  </div>
							  <div class="form-group">
								<div id="messageAddEditForm_<?php echo $module_config['name']; ?>" class="msgInfo"></div>
							  </div>
							  <div class="form-group mt50">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="hidden">Submit</button>
									<button type="button" class="btn btn-primary" id="save_addEditForm_<?php echo $module_config['name']; ?>"><?php echo $module_config['text']; ?></button>
								
								</div>
							  </div>
							</form>
						</div>
						
					</div>
				</div>
				<!-- END Bootstrap Register -->
			</div>
		</div>
	</main>	
	
<?php
include_once THEME_PATH."modules/footer.php";
?>
