<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 9]><html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-focus"> <!--<![endif]-->
    <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title><?php echo $program_name.' v'.$program_version; ?></title>
    <meta name="keywords" content="<?php echo $program_name.' v'.$program_version; ?>" />
    <meta name="description" content="<?php echo $program_name.' v'.$program_version; ?>">
    <meta name="author" content="<?php echo $program_author; ?>">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
	
	<!-- Icons -->
	<!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
	<link rel="shortcut icon" href="<?php echo ASSETS_URL; ?>img/favicons/favicon.png">

	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-192x192.png" sizes="192x192">

	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-180x180.png">
	<!-- END Icons -->
	
	<!-- Stylesheets -->
	<!-- Web fonts -->
	<link rel="stylesheet" href="<?php echo ASSETS_URL; ?>fonts/fonts.css">
	
	<!-- OneUI CSS framework -->
	<link rel="stylesheet" id="css-main" href="<?php echo ASSETS_URL; ?>css/oneui.css">
	<link rel="stylesheet" id="css-main" href="<?php echo ASSETS_URL; ?>css/themes/flat.min.css">
		                				
	<?php
	if(!empty($add_css_page)){
		echo $add_css_page;
	}
	?>
	
    <link href="<?php echo ASSETS_URL; ?>css/custom.css" rel="stylesheet" type="text/css" />
		
	<script>
		var appUrl 		= "<?php echo site_url(); ?>";
		var programName	= "<?php echo config_item('program_name'); ?>";
		var copyright	= "<?php echo config_item('copyright'); ?>";
	</script>
	
</head>

<body class="bg-image" style="background: url('<?php echo ASSETS_URL; ?>img/various/login.jpg') no-repeat center center;">
        <!-- Login Content -->
        <div class="content overflow-hidden">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4" style="opacity: 0.9; margin-top:50px;">
                    
					<!-- Lock Screen Block -->
                    <div class="block block-themed animated bounceIn">
                        <div class="block-header bg-primary">
                            <ul class="block-options">
                                <li>
                                    <a href="<?php echo BASE_URL.'logout'; ?>" data-toggle="tooltip" data-placement="left" title="Log in with another account"><i class="si si-login"></i></a>
                                </li>
                            </ul>
                            <h3 class="block-title">Account Locked</h3>
                        </div>
                        <div class="block-content block-content-full block-content-narrow">
                            <!-- Lock Screen Avatar -->
                            <div class="text-center push-15-t push-15">
                                Login dikenali sebagai<br/>
								<b><?php echo $login_data['full_name']; ?></b>
								<br/><br/>
                            </div>
                            <!-- END Lock Screen Avatar -->

                            <!-- Lock Screen Form -->
                            <!-- jQuery Validation (.js-validation-lock class is initialized in js/pages/base_pages_lock.js) -->
                            <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                            <form class="js-validation-lock form-horizontal push-30-t push-30" id="login-form" method="post" action="<?php echo BASE_URL.'backend/lock_account/open'; ?>">
                                <div class="form-group">
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
                                        <div class="form-material form-material-primary">
                                            <input class="form-control" type="password" id="lock-password" name="lock-password" placeholder="Please enter your password">
                                            <label for="lock-password">Password</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3">
                                        <button class="btn btn-block btn-primary" type="submit"><i class="si si-lock-open pull-right"></i> Unlock</button>
                                    </div>
                                </div>
								<div class="message-login-area"></div>
                            </form>
                            <!-- END Lock Screen Form -->
                        </div>
                    </div>
                    <!-- END Lock Screen Block -->
					
                </div>
            </div>
        </div>
        <!-- END Login Content -->

        <!-- Login Footer -->
        <div class="push-10-t text-center animated fadeInUp">
            <small class="text-white text-muted font-w600"><span class="js-year-copy"></span> &copy; <?php echo $program_name.' v'.$program_version; ?></small>
        </div>
        <!-- END Login Footer -->
	    
	
	<!-- CORE -->
	<script src="<?php echo APP_URL; ?>core/jquery.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/bootstrap.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.slimscroll.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.scrollLock.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.appear.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.countTo.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.placeholder.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/js.cookie.min.js"></script>
	<script src="<?php echo ASSETS_URL; ?>js/app.js"></script>

	<script src="<?php echo APP_URL; ?>libs/jquery-validation/jquery.validate.min.js"></script>
	
    <!-- Page Javascript -->
    <script type="text/javascript">
        
	var BasePagesLock = function() {
		// Init Lock Form Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
		var initValidationLock = function(){
			jQuery('.js-validation-lock').validate({
				errorClass: 'help-block text-right animated fadeInDown',
				errorElement: 'div',
				errorPlacement: function(error, e) {
					jQuery(e).parents('.form-group .form-material').append(error);
				},
				highlight: function(e) {
					jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
					jQuery(e).closest('.help-block').remove();
				},
				success: function(e) {
					jQuery(e).closest('.form-group').removeClass('has-error');
					jQuery(e).closest('.help-block').remove();
				},
				rules: {
					'lock-password': {
						required: true,
						minlength: 1
					}
				},
				messages: {
					'lock-password': {
						required: 'Please provide a password',
						minlength: 'Your password must be at least 1 characters'
					}
				},
				submitHandler: function(form) {
					//form.submit();
					
					var password = $('#lock-password').val();
					if (!password || password.length == 0)
					{
						return false;
					}
					
					// Target url
					var target = $('#login-form').attr('action');
					if (!target || target == '')
					{
						// Page url without hash
						target = document.location.href.match(/^([^#]+)/)[1];
					}
										
					// Start timer
					var sendTimer = new Date().getTime();
										
					// Request
					var data = {
						a: $('#a').val(),
						loginPassword: password,
						xtime: sendTimer
					};
					
					var redirect 	= appUrl+'backend';
								
					// Send
					$.ajax({
						url: target,
						dataType: 'json',
						type: 'POST',
						data: data,
						success: function(data, textStatus, XMLHttpRequest)
						{
							if (data.success)
							{
								document.location.href = redirect;						
							}
							else
							{
								$('.message-login-area').html('<div class="alert alert-danger alert-dismissable animated fadeIn">'+
								'<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
								'<i class="fa fa-warning pr10"></i> '+data.info+
								'</div>');
								
							}
												
						},
						error: function(XMLHttpRequest, textStatus, errorThrown)
						{
							// Message
							$('.message-login-area').html('<div class="alert alert-danger alert-dismissable animated fadeIn">'+
							'<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
							'<i class="fa fa-warning pr10"></i>'+
							' <strong>Error</strong> while contacting server, please try again!'+
							'</div>');
															
						}
					});
					
				}
			});
		};

		return {
			init: function () {
				// Init Reminder Form Validation
				initValidationLock();
			}
		};
	}();

	// Initialize when page loads
	jQuery(function(){ 
		BasePagesLock.init(); 
	});
		
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>
</html>
