$(function() {

	/*just edit module_config */
	var module_config = {
		name: 'change_password_user',
		text: 'Ubah Password',
		save_url: appUrl+'backend/change_password/save'
	};
	
	//FORM-VALIDATOR - Custom if need it: its related to form
	$('#addEditForm_'+module_config.name).bootstrapValidator({
        message: 'Data yang diinputkan tidak valid',
        fields: {
            inputOldPassword: {
                message: 'Password tidak valid',
                validators: {
                    notEmpty: {
                        message: 'Password Lama tidak boleh kosong'
                    }
                }
            },
            inputNewPassword: {
                validators: {
                    notEmpty: {
                        message: 'Password Baru tidak boleh kosong'
                    },
                    identical: {
                        field: 'inputConfirmNewPassword',
                        message: 'Password Baru dan Konfirmasi Password Baru tidak sama'
                    },
					stringLength: {
                        message: 'Password tidak boleh lebih dari 45 karakter',
						max: 45
                    }
                }
            },
            inputConfirmNewPassword: {
                validators: {
                    notEmpty: {
                        message: 'Konfirmasi Password Baru dan Konfirm password tidak sama'
                    },
                    identical: {
                        field: 'inputNewPassword',
                        message: 'Password Baru dan Konfirmasi Password Baru tidak sama'
                    },
					stringLength: {
                        message: 'Password tidak boleh lebih dari 45 karakter',
						max: 45
                    }
                }
            }
        }
    });
	
	$('#save_addEditForm_'+module_config.name).click(function(){
		$('#addEditForm_'+module_config.name).submit();
	});
	
	//ADD / EDIT - SAVE
	$('#addEditForm_'+module_config.name).on('submit', function(e) {
		
		e.preventDefault;
		
		//check has-error
		var is_has_error = false;
		$('#addEditForm_'+module_config.name+' .form-group').each(function(){			
			if($(this).hasClass('has-error')){
				is_has_error = true;
			}
		});
		
		if(is_has_error){
			return false;
		}
				
		showMessageInfo('#messageAddEditForm_'+module_config.name, 'alert-info', 'fa-spinner', 'Menyimpan Data...');
							
		var submitBt = $('#save_addEditForm_'+module_config.name);
		submitBt.removeClass('btn-blue-2');
		submitBt.addClass('disabled');
		submitBt.addClass('btn-default');
		
		$.ajax({
			url: module_config.save_url,
			dataType: 'json',
			type: 'POST',
			data: $(this).serialize(),
			success: function(data, textStatus, XMLHttpRequest)
			{
				if(data.success == false){
					showMessageInfo('#messageAddEditForm_'+module_config.name, 'alert-danger', 'fa-warning', data.info);								
					submitBt.removeClass('disabled');
					submitBt.removeClass('btn-default');
					submitBt.addClass('btn-blue-2');
				}else{
					
					submitBt.removeClass('disabled');
					submitBt.removeClass('btn-default');
					submitBt.addClass('btn-blue-2');
					$('#close_addEditForm_'+module_config.name).trigger('click');
					$('#addEditForm_'+module_config.name)[0].reset();
					$('#addEditForm_'+module_config.name).bootstrapValidator('resetForm', true); 
					
					showMessageInfo('#messageAddEditForm_'+module_config.name, 'alert-success', 'fa-save', data.info);
					setTimeout(function(){					
						hideMessageInfo('#messageAddEditForm_'+module_config.name);
					}, 5000);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown)
			{		
				showMessageInfo('#messageAddEditForm_'+module_config.name, 'alert-danger', 'fa-warning', module_config.text+' Gagal!');							
				submitBt.removeClass('disabled');
				submitBt.removeClass('btn-default');
				submitBt.addClass('btn-blue-2');
			}

		});
					
	});	
});