$(function() {

	/*just edit module_config */
	var module_config = {
		name: 'change_profile',
		text: 'Ubah Profil',
		save_url: appUrl+'backend/change_profile/save'
	};
	
	//FORM-VALIDATOR - Custom if need it: its related to form
	$('#addEditForm_'+module_config.name).bootstrapValidator({
        message: 'Data yang diinputkan tidak valid',
        fields: {
            nama_lengkap: {
                message: 'Nama Lengkap tidak valid',
                validators: {
                    notEmpty: {
                        message: 'Nama Lengkap tidak boleh kosong'
                    },
					stringLength: {
                        message: 'Nama Lengkap tidak boleh lebih dari 45 karakter',
						max: 45
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Email tidak boleh kosong'
                    },
                    emailAddress: {
                        message: 'Email tidak valid, contoh: contact@gmail.com'
                    },
					stringLength: {
                        message: 'Email tidak boleh lebih dari 45 karakter',
						max: 45
                    }
                }
            }
        }
    });
	
	$('#save_addEditForm_'+module_config.name).click(function(){
		$('#addEditForm_'+module_config.name).submit();
	});
	
	//ADD / EDIT - SAVE
	$('#addEditForm_'+module_config.name).on('submit', function(e) {
		
		e.preventDefault;
		
		//check has-error
		var is_has_error = false;
		$('#addEditForm_'+module_config.name+' .form-group').each(function(){			
			if($(this).hasClass('has-error')){
				is_has_error = true;
			}
		});
		
		if(is_has_error){
			return false;
		}
				
		showMessageInfo('#messageAddEditForm_'+module_config.name, 'alert-info', 'fa-spinner', 'Menyimpan Data...');
							
		var submitBt = $('#save_addEditForm_'+module_config.name);
		submitBt.removeClass('btn-blue-2');
		submitBt.addClass('disabled');
		submitBt.addClass('btn-default');
		
		$.ajax({
			url: module_config.save_url,
			dataType: 'json',
			type: 'POST',
			data: $(this).serialize(),
			success: function(data, textStatus, XMLHttpRequest)
			{
				if(data.success == false){
					showMessageInfo('#messageAddEditForm_'+module_config.name, 'alert-danger', 'fa-warning', data.info);								
					submitBt.removeClass('disabled');
					submitBt.removeClass('btn-default');
					submitBt.addClass('btn-blue-2');
				}else{
					
					submitBt.removeClass('disabled');
					submitBt.removeClass('btn-default');
					submitBt.addClass('btn-blue-2');
					$('#close_addEditForm_'+module_config.name).trigger('click');
										
					showMessageInfo('#messageAddEditForm_'+module_config.name, 'alert-success', 'fa-save', data.info);
					setTimeout(function(){					
						hideMessageInfo('#messageAddEditForm_'+module_config.name);
					}, 5000);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown)
			{		
				showMessageInfo('#messageAddEditForm_'+module_config.name, 'alert-danger', 'fa-warning', module_config.text+' Gagal!');							
				submitBt.removeClass('disabled');
				submitBt.removeClass('btn-default');
				submitBt.addClass('btn-blue-2');
			}

		});
					
	});	
});