/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */

var BaseTableDatatables = function() {
    // Init full DataTable, for more examples you can check out https://www.datatables.net/
    var initDataTableFull = function() {
        jQuery('#table-rutan').dataTable({
            //columnDefs: [ { orderable: false, targets: [ 4 ] } ],
            pageLength: 10,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]]
        });
    };

    // DataTables Bootstrap integration
    var bsDataTables = function() {
        var $DataTable = jQuery.fn.dataTable;

        // Set the defaults for DataTables init
        jQuery.extend( true, $DataTable.defaults, {
            dom:
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            renderer: 'bootstrap',
            oLanguage: {
                sLengthMenu: "_MENU_",
                sInfo: "Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>",
                oPaginate: {
                    sPrevious: '<i class="fa fa-angle-left"></i>',
                    sNext: '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        // Default class modification
        jQuery.extend($DataTable.ext.classes, {
            sWrapper: "dataTables_wrapper form-inline dt-bootstrap",
            sFilterInput: "form-control",
            sLengthSelect: "form-control"
        });

        // Bootstrap paging button renderer
        $DataTable.ext.renderer.pageButton.bootstrap = function (settings, host, idx, buttons, page, pages) {
            var api     = new $DataTable.Api(settings);
            var classes = settings.oClasses;
            var lang    = settings.oLanguage.oPaginate;
            var btnDisplay, btnClass;

            var attach = function (container, buttons) {
                var i, ien, node, button;
                var clickHandler = function (e) {
                    e.preventDefault();
                    if (!jQuery(e.currentTarget).hasClass('disabled')) {
                        api.page(e.data.action).draw(false);
                    }
                };

                for (i = 0, ien = buttons.length; i < ien; i++) {
                    button = buttons[i];

                    if (jQuery.isArray(button)) {
                        attach(container, button);
                    }
                    else {
                        btnDisplay = '';
                        btnClass = '';

                        switch (button) {
                            case 'ellipsis':
                                btnDisplay = '&hellip;';
                                btnClass = 'disabled';
                                break;

                            case 'first':
                                btnDisplay = lang.sFirst;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'previous':
                                btnDisplay = lang.sPrevious;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'next':
                                btnDisplay = lang.sNext;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            case 'last':
                                btnDisplay = lang.sLast;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            default:
                                btnDisplay = button + 1;
                                btnClass = page === button ?
                                        'active' : '';
                                break;
                        }

                        if (btnDisplay) {
                            node = jQuery('<li>', {
                                'class': classes.sPageButton + ' ' + btnClass,
                                'aria-controls': settings.sTableId,
                                'tabindex': settings.iTabIndex,
                                'id': idx === 0 && typeof button === 'string' ?
                                        settings.sTableId + '_' + button :
                                        null
                            })
                            .append(jQuery('<a>', {
                                    'href': '#'
                                })
                                .html(btnDisplay)
                            )
                            .appendTo(container);

                            settings.oApi._fnBindAction(
                                node, {action: button}, clickHandler
                            );
                        }
                    }
                }
            };

            attach(
                jQuery(host).empty().html('<ul class="pagination"/>').children('ul'),
                buttons
            );
        };

        // TableTools Bootstrap compatibility - Required TableTools 2.1+
        if ($DataTable.TableTools) {
            // Set the classes that TableTools uses to something suitable for Bootstrap
            jQuery.extend(true, $DataTable.TableTools.classes, {
                "container": "DTTT btn-group",
                "buttons": {
                    "normal": "btn btn-default",
                    "disabled": "disabled"
                },
                "collection": {
                    "container": "DTTT_dropdown dropdown-menu",
                    "buttons": {
                        "normal": "",
                        "disabled": "disabled"
                    }
                },
                "print": {
                    "info": "DTTT_print_info"
                },
                "select": {
                    "row": "active"
                }
            });

            // Have the collection use a bootstrap compatible drop down
            jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
                "collection": {
                    "container": "ul",
                    "button": "li",
                    "liner": "a"
                }
            });
        }
    };

    return {
        init: function() {
            // Init Datatables
            bsDataTables();
            //initDataTableSimple();
            //initDataTableFull();
        }
    };
}();


var tipe_tanggal_pencarian = jQuery('#tipe_tanggal_pencarian').val();
var tahun_pencarian = jQuery('#tahun_pencarian').val();
var result_excel = 0;
var result_print = 0;
var dtTableAbsensi;

function refresh_data(load_init){
	
	var getID = '#rutan_area';
	App.blocks(getID, 'state_loading');
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();
	
	// Request
	var data = {
		tipe_tanggal_pencarian: tipe_tanggal_pencarian,
		tahun_pencarian: tahun_pencarian,
		xtime: sendTimer
	};
	
	var getTarget = appUrl +'rutan/laporan/load_list_data';

	//ajax-load
	$.ajax({
		url: getTarget,
		//dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata){
									
				if(load_init == true){
					BaseTableDatatables.init(); 
					$('#table-rutan-area').html(rdata);
					dtTableAbsensi = jQuery('#table-rutan').dataTable({
						//columnDefs: [ { orderable: false, targets: [ 4 ] } ],
						ordering: false,
						searching: false,
						pageLength: 10,
						lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
						oLanguage: {
							sLengthMenu: ""
						},
						dom:
							"<'row'<'col-sm-12'tr>>" +
							"<'row'<'col-sm-6'i><'col-sm-6'p>>"
					});
				}else{
					
					dtTableAbsensi.fnDestroy(true);
					$('#table-rutan-area').html(rdata);
					dtTableAbsensi = jQuery('#table-rutan').dataTable({
						//columnDefs: [ { orderable: false, targets: [ 4 ] } ],
						ordering: false,
						searching: false,
						pageLength: 10,
						lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
						oLanguage: {
							sLengthMenu: ""
						},
						dom:
							"<'row'<'col-sm-12'tr>>" +
							"<'row'<'col-sm-6'i><'col-sm-6'p>>"
					});
				}
				
			}else{
				$('#table-rutan-area').html('');
			}
			
			
			App.blocks(getID, 'state_normal');
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			App.blocks(getID, 'state_normal');
		}
	});
}

function print_excel(xval){
	
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();
	var getTarget = appUrl +'rutan/laporan/load_list_data?xtime='+sendTimer;
	
	if(!xval){
		result_print = 1;
		result_excel = 0;
		
	}else{
		result_print = 0;
		result_excel = 1;
		
	}
	
	getTarget += '&tipe_tanggal_pencarian='+tipe_tanggal_pencarian;
	getTarget += '&tahun_pencarian='+tahun_pencarian;
	getTarget += '&tipe=list_data';
	getTarget += '&result_excel='+result_excel;
	getTarget += '&result_print='+result_print;
	
	$('#print_excel_iframe').attr('src', getTarget);
	
	
}


// Initialize when page loads
jQuery(function(){ 
	
	 
	$('.js-datepicker').add('.input-daterange').datepicker({
		weekStart: 1,
		autoclose: true,
		todayHighlight: true
	});
		
	$('.js-select2').select2();

	$('#do_filter_rutan').click(function(){
		
		tipe_tanggal_pencarian = $('#tipe_tanggal_pencarian').select2("val");
		tahun_pencarian = $('#tahun_pencarian').val();
		
		var tipe_tanggal_pencarian_text = '-';
		if(tipe_tanggal_pencarian == 'tanggal_berita'){
			tipe_tanggal_pencarian_text = 'Tanggal Berita';
		}
		if(tipe_tanggal_pencarian == 'tanggal_bebas'){
			tipe_tanggal_pencarian_text = 'Tanggal Bebas';
		}
		
		$('#tahun_pencarian_display').html(tahun_pencarian);
		$('#tipe_tanggal_pencarian_display').html(tipe_tanggal_pencarian_text);
		
		$('#close-modal-filter').trigger('click');
		refresh_data(false);
		
	});
	
	$('#refresh_data').click(function(){
		refresh_data(false);
	});
	
	$('#reset_data').click(function(){
		
		var d = new Date();

		$('#tipe_tanggal_pencarian').select2("val","");
		$('#tahun_pencarian').val(d.getFullYear());
		
		$('#do_filter_rutan').trigger("click");
		
	});
	
	tipe_tanggal_pencarian = $('#tipe_tanggal_pencarian').select2("val");
	tahun_pencarian = $('#tahun_pencarian').val();
	$('#tahun_pencarian_display').html(tahun_pencarian);
	$('#tipe_tanggal_pencarian_display').html('-');
	refresh_data(true);
	
	
});