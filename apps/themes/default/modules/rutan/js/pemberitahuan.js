
function add_pemberitahuan(){
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();

	// Request
	var data = {
		no_berita_bebas: $('#no_berita_bebas').val(),
		tanggal_berita: $('#tanggal_berita').val(),
		nama_wna: $('#nama_wna').val(),
		tempat_lahir: $('#tempat_lahir').val(),
		tanggal_lahir: $('#tanggal_lahir').val(),
		kebangsaan: $('#kebangsaan').val(),
		no_paspor: $('#no_paspor').val(),
		jenis_kelamin: $('#jenis_kelamin').select2('val'),
		alamat: $('#alamat').val(),
		no_ic: $('#no_ic').val(),
		tanggal_bebas: $('#tanggal_bebas').val(),
		catatan: $('#catatan').val(),
		is_edit: $('#is_edit').val(),
		pemberitahuan_id: $('#pemberitahuan_id').val(),
		tipe_instansi:$('#tipe_instansi').val(),
		xtime: sendTimer
	};
	
	var getTarget = appUrl +'rutan/pemberitahuan/addData';

	
	$('#info-pemberitahuan-rutan').html('');
	$('#info-pemberitahuan-rutan').hide();
	
	//ajax-load
	$.ajax({
		url: getTarget,
		dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata.success){
				
				//window.location = appUrl +'rutan/pemberitahuan';
				$('#form-data-pemberitahuan-rutan')[0].reset();
				$('#is_edit').val(0);
				$('#pemberitahuan_id').val(0);
				$('#tipe_instansi').val('RUTAN');
				
				$.notify({
					icon: 'fa fa-check',
					message: rdata.info
				},{
					type: 'success',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
				
			}else{
				
				$.notify({
					icon: 'fa fa-warning',
					message: rdata.info
				},{
					type: 'danger',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
			}
			
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			$.notify({
				icon: 'fa fa-warning',
				message: 'Simpan Pemberitahuan Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'bottom',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
		}
	});
}

// Initialize when page loads
jQuery(function(){ 
	
	 
	$('.js-datepicker').add('.input-daterange').datepicker({
		weekStart: 1,
		autoclose: true,
		todayHighlight: true
	});
		
	$('.js-select2').select2();	
	
	$('.js-validation-bootstrap').validate({
		errorClass: 'help-block animated fadeInDown',
		errorElement: 'div',
		errorPlacement: function(error, e) {
			jQuery(e).parents('.form-group > div').append(error);
		},
		highlight: function(e) {
			jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
			jQuery(e).closest('.help-block').remove();
		},
		success: function(e) {
			jQuery(e).closest('.form-group').removeClass('has-error');
			jQuery(e).closest('.help-block').remove();
		},
		rules: {
			'no_berita_bebas': {
				required: true
			},
			'tanggal_berita': {
				required: true
			},
			'nama_wna': {
				required: true
			},
			'no_paspor': {
				required: true
			},
			'kebangsaan': {
				required: true
			},
			'no_ic': {
				required: true
			},
			'tanggal_bebas': {
				required: true
			}
		},
		messages: {
			'no_berita_bebas': 'Inputkan No. Berita Bebas',
			'tanggal_berita': 'Inputkan Tanggal Berita',
			'nama_wna': 'Inputkan Nama WNA',
			'no_paspor': 'Inputkan No. Paspor',
			'kebangsaan': 'Inputkan Kebangsaan',
			'no_ic': 'Inputkan No.IC',
			'tanggal_bebas': 'Inputkan Tanggal Bebas'
		},
		submitHandler: function(form) {
			add_pemberitahuan();
		}
	});
	
	
});