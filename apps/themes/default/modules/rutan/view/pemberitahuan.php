<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once THEME_PATH."modules/header.php";

?>		
	
	<!-- Main Container -->
	<main id="main-container">
		
		
		<div class="content">
			
			<!-- Dynamic Table Full -->
			<div class="block block-themed block-rounded" id="pemberitahuan_area">
				<div class="block-header bg-amethyst-dark">
					<h3 class="block-title">RUTAN / Form Pemberitahuan Akan Bebas</h3>
				</div>
				
				<div class="block-content bg-gray-lighter">
					
					<?php
					if(!empty($is_error)){
						?>
						<div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<h3 class="font-w300 push-15">Ada Kesalahan!</h3>
							<p><?php echo $is_error; ?></p>
						</div>
						<?php
					}
					?>
					
					<form class="js-validation-bootstrap form-horizontal" id="form-data-pemberitahuan-rutan" action="#" method="post" onsubmit="return false;">
							
						<div class="form-group">
							<label class="col-md-2 control-label" for="no_berita_bebas">No. Register</label>
							<div class="col-md-4">
								<input class="form-control" type="text" id="no_berita_bebas" name="no_berita_bebas" value="<?php echo $data_edit['no_berita_bebas']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="tanggal_berita">Tanggal Register</label>
							<div class="col-md-2">
								<input class="js-datepicker form-control" type="text" id="tanggal_berita" name="tanggal_berita" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $data_edit['tanggal_berita']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="nama_wna">Nama</label>
							<div class="col-md-4">
								<input class="form-control" type="text" id="nama_wna" name="nama_wna" value="<?php echo $data_edit['nama_wna']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="tempat_lahir">Tempat Lahir</label>
							<div class="col-md-4">
								<input class="form-control" type="text" id="tempat_lahir" name="tempat_lahir" value="<?php echo $data_edit['tempat_lahir']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="tanggal_lahir">Tanggal Lahir</label>
							<div class="col-md-2">
								<input class="js-datepicker form-control" type="text" id="tanggal_lahir" name="tanggal_lahir" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $data_edit['tanggal_lahir']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="kebangsaan">Kebangsaan</label>
							<div class="col-md-4">
								<input class="form-control" type="text" id="kebangsaan" name="kebangsaan" value="<?php echo $data_edit['kebangsaan']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="no_paspor">No. Paspor</label>
							<div class="col-md-4">
								<input class="form-control" type="text" id="no_paspor" name="no_paspor" value="<?php echo $data_edit['no_paspor']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="jenis_kelamin">Jenis Kelamin</label>
							<div class="col-md-2">
								<select class="js-select2 form-control" id="jenis_kelamin"  id="jenis_kelamin" style="width: 100%;">
									<option value="">Pilih Jenis Kelamin</option>
									<?php
									$selected_L = '';
									$selected_P = '';
									if($data_edit['jenis_kelamin'] == 'L'){
										$selected_L = ' selected="selected"';
									}
									if($data_edit['jenis_kelamin'] == 'P'){
										$selected_P = ' selected="selected"';
									}
									echo '<option value="L"'.$selected_L.'>Laki-laki</option>';
									echo '<option value="P"'.$selected_P.'>Perempuan</option>';
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="alamat">Alamat</label>
							<div class="col-md-4">
								<textarea class="form-control" id="alamat" name="alamat" rows="2"><?php echo $data_edit['alamat']; ?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="no_ic">No. IC</label>
							<div class="col-md-4">
								<input class="form-control" type="text" id="no_ic" name="no_ic" value="<?php echo $data_edit['no_ic']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="tanggal_bebas">Tanggal Bebas</label>
							<div class="col-md-2">
								<input class="js-datepicker form-control" type="text" id="tanggal_bebas" name="tanggal_bebas" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="<?php echo $data_edit['tanggal_bebas']; ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label" for="catatan">Catatan</label>
							<div class="col-md-4">
								<textarea class="form-control" id="catatan" name="catatan" rows="2"><?php echo $data_edit['catatan']; ?></textarea>
							</div>
						</div>
						
						<input type="hidden" id="is_edit" name="is_edit" value="<?php echo $data_edit['is_edit']; ?>">
						<input type="hidden" id="pemberitahuan_id" name="pemberitahuan_id" value="<?php echo $data_edit['pemberitahuan_id']; ?>">
						<input type="hidden" id="tipe_instansi" name="tipe_instansi" value="<?php echo $data_edit['tipe_instansi']; ?>">
						<div class="form-group">
							<div class="col-md-2 col-md-offset-2" style="margin-top:10px;">
								<button class="btn btn-block btn-success" id="do_add_pemberitahuan" type="submit"><i class="fa fa-save pull-right"></i> Simpan</button>
							</div>
							<div class="col-md-2" style="margin-top:10px;">
								<button class="btn btn-block btn-warning" id="close-modal-add" type="reset"><i class="fa fa-close pull-right"></i> Batal</button>
							</div>
						</div>
						
					</form>
					
					
					<br/>
					<br/>
					<br/>
					
				</div>
				
			</div>
			<!-- END Dynamic Table Full -->
			
		</div>
		<!-- END Page Content -->
		
	</main>
	<!-- END Main Container -->
	
	
<?php
include_once THEME_PATH."modules/footer.php";
?>
