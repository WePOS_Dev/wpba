<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
		<!-- Footer -->
		<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
			<!--<div class="pull-right">
				<a class="font-w600" href="javascript:void(0)"><?php echo $client_name; ?></a>
			</div>-->
			<div class="pull-left">
				<a class="font-w600" href="javascript:void(0)"><?php echo $program_name.' v'.$program_version; ?></a> 
			</div>
		</footer>
		<!-- END Footer -->
	</div>
	<!-- END Page Container -->
	
	<!-- CORE -->
	<script src="<?php echo APP_URL; ?>core/jquery.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/bootstrap.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.slimscroll.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.scrollLock.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.appear.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.countTo.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.placeholder.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/js.cookie.min.js"></script>
	<script src="<?php echo APP_URL; ?>libs/bootstrap-validator/js/bootstrapValidator.min.js"></script>
	<script src="<?php echo APP_URL; ?>libs/isotope/jquery.isotope.js"></script>
	<script src="<?php echo APP_URL; ?>libs/bootstrap-notify/bootstrap-notify.min.js"></script>
    <script src="<?php echo APP_URL; ?>libs/datatables/jquery.dataTables.min.js"></script>
	<script src="<?php echo APP_URL; ?>libs/bootstrap-inputmask/inputmask.js"></script>
	<script src="<?php echo APP_URL; ?>libs/bootstrap-fileinput-krajee/js/fileinput.min.js"></script>
			
	<script src="<?php echo ASSETS_URL; ?>js/app.js"></script>
	<script src="<?php echo ASSETS_URL; ?>js/module_default.js"></script>
	
	<?php
		if(!empty($add_js_page)){
			echo $add_js_page;
		}
	?>
	
</body>
</html>
