<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once THEME_PATH."modules/header_frontend.php";

?>		
	<script>
		var tanggal_notify = '<?php echo date('Y-m-d'); ?>';
		var currDate = '<?php echo date('Y-m-d'); ?>';
		var redirect_selft = '<?php BASE_URL.'generate/sponsor'; ?>';
	</script>
	
	<!-- Main Container -->
	<main id="main-container">
		
		<div class="content">
			<div class="row">
				<div class="col-lg-6">
					<!-- Latest Sales Widget -->
					<div class="block block-themed block-rounded" id="sponsor-data">
						<div class="block-header bg-danger">
							<ul class="block-options hide">
								<li>
									<button data-action-url="<?php echo BASE_URL.'generate/sponsor/update'; ?>" data-refresh="3000" data-action-post="absen_masuk" data-main-id="sponsor-data" class="sponsor-table-refresh" type="button">
										<i class="si si-refresh"></i>
									</button>
								</li>
								<li>
									<button data-action-url="<?php echo BASE_URL.'generate/sponsor/wna'; ?>" class="sponsor-wna-table-refresh hide" type="button">
										<i class="si si-refresh"></i>
									</button>
								</li>
							</ul>
							<h3 class="block-title" id="sponsor-data-title">Sponsor: <?php echo count($data_sponsor); ?> Data</h3>
							<h3 class="hide" id="sponsor-data-total">0</h3>
						</div>
							
						<div class="block-content bg-gray-lighter">
							<div class="row items-push">
								<div class="col-xs-12">
									<div id="sponsor-data-info"></div>
								</div>
							</div>
						</div>
						<?php 
						$sponsor_data_html = '';
						$no_data = 0;
						
						/*if(!empty($data_sponsor)){
							
							foreach($data_sponsor as $dt){
								
								$no_data++;
								$sponsor_data_html = $sponsor_data_html.'
								<tr>
									<td class="font-w600">'.$no_data.'</td>
									<td class="font-w600">'.$dt->sponsor_data.'</td>
									<td class="font-w600">'.$dt->mobile_phone.'</td>
								</tr>';
							}
						}
						*/
						?>
						<div class="block-content">
							<div class="pull-t pull-r-l">
								<div class="dashboard-table-responsive" style="height:510px;">
									<table class="table remove-margin-b font-s13">
										<thead>
											<tr>
												<th class="font-s13 font-w600" style="width: 30px;">No</th>
												<th class="font-s13 font-w600" >Nama Sponsor</th>
												<th class="font-s13 font-w600" style="width: 150px;">Telepon</th>
												<th class="font-s13 font-w600" style="width: 100px;">Total</th>
											</tr>
										</thead>
										<tbody id="sponsor-data-data">
											<?php echo $sponsor_data_html; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
					</div>
					<!-- END Latest Sales Widget -->
				</div>
				
			</div>
			
		</div>
		<!-- END Page Content -->
		
	</main>
	<!-- END Main Container -->
	
<?php
include_once THEME_PATH."modules/footer_frontend.php";
?>
