<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once THEME_PATH."modules/header_frontend.php";

$notifikasi_sms = array();
if(!empty($data_ready_notify)){
	foreach($data_ready_notify as $dt){
		$notifikasi_sms[] = $dt;
	}
}

$autosend_sms = array();
if(!empty($data_autosend_sms)){
	foreach($data_autosend_sms as $dt){
		$autosend_sms[] = $dt;
	}
}

?>		
	<script>
		var tanggal_sms = '<?php echo date('Y-m-d'); ?>';
		var currDate = '<?php echo date('Y-m-d'); ?>';
		var redirect_self = '<?php echo BASE_URL.'generate/notifikasi'; ?>';
	</script>
	
	<!-- Main Container -->
	<main id="main-container">
		
		<div class="content">
			<div class="row">
				<div class="col-lg-6">
					<!-- Latest Sales Widget -->
					<div class="block block-themed block-rounded" id="notifikasi-wna_expired">
						<div class="block-header bg-success">
							<ul class="block-options">
								<li>
									<button data-action-url="<?php echo BASE_URL.'generate/sms'; ?>" data-refresh="15000" data-action-post="wna_expired" data-main-id="notifikasi-wna_expired" class="notifikasi-table-refresh" type="button">
										<i class="si si-refresh"></i>
									</button>
								</li>
							</ul>
							<h3 class="block-title" id="notifikasi-wna_expired-title">WNA Expired: <?php echo count($notifikasi_sms); ?></h3>
						</div>
							
						<div class="block-content bg-gray-lighter">
							<div class="row items-push">
								<div class="col-xs-12">
									<div id="notifikasi-wna_expired-info"></div>
								</div>
							</div>
						</div>
						<?php 
						$notifikasi_sms_html = '';
						$no_data = 0;
						if(!empty($notifikasi_sms)){
							
							foreach($notifikasi_sms as $dt){
								
								$no_data++;
								
								$tanggal_berlaku = date("d-m-Y",strtotime($dt->tanggal_berlaku));
								
								$notifikasi_sms_html = '
								<tr>
									<td class="font-w600">'.$no_data.'</td>
									<td class="font-w600">'.$dt->nama.'</td>
									<td class="font-w600">'.$dt->dokim_no.'</td>
									<td class="text-center">'.$tanggal_berlaku.'</td>
								</tr>'.$notifikasi_sms_html;
							}
						}
						?>
						<div class="block-content">
							<div class="pull-t pull-r-l">
								<div class="dashboard-table-responsive" style="height:510px;">
									<table class="table remove-margin-b font-s13">
										<thead>
											<tr>
												<th class="font-s13 font-w600" style="width: 10px;">No</th>
												<th class="font-s13 font-w600">Nama</th>
												<th class="font-s13 font-w600" style="width: 150px;">No.DOKIM</th>
												<th class="font-s13 font-w600 text-center" style="width: 120px;">Expired</th>
											</tr>
										</thead>
										<tbody id="notifikasi-wna_expired-data">
											<?php echo $notifikasi_sms_html; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
					</div>
					<!-- END Latest Sales Widget -->
				</div>
				
				<!-- AUTOSEND SMS -->
				<div class="col-lg-6">
					<!-- Latest Sales Widget -->
					<div class="block block-themed block-rounded" id="notifikasi-autosend_sms">
						<div class="block-header bg-danger">
							<ul class="block-options">
								<li>
									<button data-action-url="<?php echo BASE_URL.'generate/sms'; ?>" data-refresh="20000" data-action-post="autosend_sms" data-main-id="notifikasi-autosend_sms" class="notifikasi-table-refresh" type="button">
										<i class="si si-refresh"></i>
									</button>
								</li>
							</ul>
							<h3 class="block-title" id="notifikasi-autosend_sms-title">SMS SEND</h3>
						</div>
							
						<div class="block-content bg-gray-lighter">
							<div class="row items-push">
								<div class="col-xs-12">
									<div id="notifikasi-autosend_sms-info">Autosend SMS setiap 20 detik</div>
								</div>
							</div>
						</div>
						<?php 
						$autosend_sms_html = '';
						
						/*
						$no_data = 0;
						if(!empty($autosend_sms)){
							
							foreach($autosend_sms as $dt){
								
								$no_data++;
								
								$tanggal_berlaku = date("d-m-Y",strtotime($dt->tanggal_berlaku));
								$jam_kirim = '00:00';
								
								$autosend_sms_html = '
								<tr>
									<td class="font-w600">'.$no_data.'</td>
									<td class="font-w600">'.$dt->nama.'</td>
									<td class="font-w600">'.$dt->dokim_no.'</td>
									<td class="text-right">'.$tanggal_berlaku.'</td>
									<td class="text-right">'.$jam_kirim.'</td>
								</tr>'.$autosend_sms_html;
							}
						}
						*/
						?>
						<div class="block-content">
							<div class="pull-t pull-r-l">
								<div class="dashboard-table-responsive" style="height:510px;">
									<table class="table remove-margin-b font-s13">
										<thead>
											<tr>
												<th class="font-s13 font-w600" style="width: 100px;">No.Telp</th>
												<th class="font-s13 font-w600">Nama</th>
												<th class="font-s13 font-w600" style="width: 100px;">No.DOKIM</th>
												<th class="font-s13 font-w600 text-center" style="width: 100px;">Expired</th>
												<th class="font-s13 font-w600 text-center" style="width: 100px;">Jam Kirim</th>
											</tr>
										</thead>
										<tbody id="notifikasi-autosend_sms-data">
											<?php echo $autosend_sms_html; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
					</div>
					<!-- END Latest Sales Widget -->
				</div>
				
				<!-- AUTOUPDATE STATUS SMS -->
				<div class="col-lg-6 hide">
					<!-- Latest Sales Widget -->
					<div class="block block-themed block-rounded" id="notifikasi-autoupdate_status_sms">
						<div class="block-header bg-danger">
							<ul class="block-options">
								<li>
									<button data-action-url="<?php echo BASE_URL.'generate/sms'; ?>" data-refresh="10000" data-action-post="autoupdate_status_sms" data-main-id="notifikasi-autoupdate_status_sms" class="notifikasi-table-refresh" type="button">
										<i class="si si-refresh"></i>
									</button>
								</li>
							</ul>
							<h3 class="block-title" id="notifikasi-autoupdate_status_sms-title">AUTOUPDATE STATUS SMS</h3>
						</div>
							
						<div class="block-content bg-gray-lighter">
							<div class="row items-push">
								<div class="col-xs-12">
									<div id="notifikasi-autoupdate_status_sms-info">Autosend SMS setiap 10 detik</div>
								</div>
							</div>
						</div>
						<?php 
						$autoupdate_status_sms_html = '';
						?>
						<div class="block-content">
							<div class="pull-t pull-r-l">
								<div class="dashboard-table-responsive" style="height:510px;">
									<table class="table remove-margin-b font-s13">
										<thead>
											<tr>
												<th class="font-s13 font-w600" style="width: 100px;">No.Telp</th>
												<th class="font-s13 font-w600">Nama</th>
												<th class="font-s13 font-w600" style="width: 100px;">No.DOKIM</th>
												<th class="font-s13 font-w600 text-center" style="width: 100px;">Expired</th>
												<th class="font-s13 font-w600 text-center" style="width: 100px;">Jam Kirim</th>
											</tr>
										</thead>
										<tbody id="notifikasi-autoupdate_status_sms-data">
											<?php echo $autoupdate_status_sms_html; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
					</div>
					<!-- END Latest Sales Widget -->
				</div>
				
			</div>
			
			
			
		</div>
		<!-- END Page Content -->
		
	</main>
	<!-- END Main Container -->
	
<?php
include_once THEME_PATH."modules/footer_frontend.php";
?>
