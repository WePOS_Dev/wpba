 $(function () {
	 
	var timeoutTable = [];
	
	$('.notifikasi-table-refresh').click(function(){
		
		var getID = $(this).attr('data-main-id');
		var getTarget = $(this).attr('data-action-url');
		var getDataTarget = $(this).attr('data-action-post');
		var getRefresh = $(this).attr('data-refresh');
		
		App.blocks('#'+getID, 'state_loading');
		
		// Start timer
		var sendTimer = new Date().getTime();

		// Request
		var data = {
			reqdata: getDataTarget,
			reqdate: currDate,
			dataType: 'html',
			for_display: 1,
			xtime: sendTimer
		};
		
		if(timeoutTable[getID]){
			clearTimeout(timeoutTable[getID]);
		}

		//ajax-load
		$.ajax({
			url: getTarget,
			dataType: 'json',
			type: 'POST',
			data: data,
			success: function(rdata, textStatus, XMLHttpRequest)
			{
				if(rdata.title_html){
					$('#'+getID+'-title').html(rdata.title_html);	
				}
				
				if(rdata.info_html){
					$('#'+getID+'-info').html(rdata.info_html);
				}
				
				if(rdata.full_html == 1){
					if(rdata.content_html){
						$('#'+getID+'-data').html(rdata.content_html);
					}else{
						$('#'+getID+'-data').html('');
					}
				}else{
					if(rdata.is_newdate == 1){
						if(rdata.content_html){
							$('#'+getID+'-data').html(rdata.content_html);
						}
						
						document.location.href = redirect_self;
						return true;
					}else{
						if(rdata.content_html){
							$('#'+getID+'-data').prepend(rdata.content_html);
						}
					}
				}
				
				
				if(rdata.total == 0){
					//reload
					timeoutTable[getID] = setTimeout(function(){ 
						$('#'+getID+' button.notifikasi-table-refresh').trigger('click');
					}, 300000);
				}else{
					//reload
					timeoutTable[getID] = setTimeout(function(){ 
						$('#'+getID+' button.notifikasi-table-refresh').trigger('click');
					}, getRefresh);
					
				}
				
				
				App.blocks('#'+getID, 'state_normal');
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown)
			{
				//reload
				timeoutTable[getID] = setTimeout(function(){ 
					$('#'+getID+' button.notifikasi-table-refresh').trigger('click');
				}, getRefresh);
				
				App.blocks('#'+getID, 'state_normal');
				
				
			}
		});
		
	});
	
	
	setTimeout(function(){
		$('#notifikasi-wna_expired button.notifikasi-table-refresh').trigger('click');
	}, 2000);
	
	setTimeout(function(){
		$('#notifikasi-autosend_sms button.notifikasi-table-refresh').trigger('click');
	}, 15000);
	
	setTimeout(function(){
		$('#notifikasi-autoupdate_status_sms button.notifikasi-table-refresh').trigger('click');
	}, 5000);
	
	
	
});