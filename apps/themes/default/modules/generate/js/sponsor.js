 $(function () {
	 
	var timeoutTable = [];
	var noCheckUp = 0;
	
	$('.sponsor-table-refresh').click(function(){
		
		var getID = $(this).attr('data-main-id');
		var getTarget = $(this).attr('data-action-url');
		var getDataTarget = $(this).attr('data-action-post');
		var getRefresh = $(this).attr('data-refresh');
		var totalSponsor = $('#sponsor-data-total').html();
		
		App.blocks('#'+getID, 'state_loading');
		
		// Start timer
		var sendTimer = new Date().getTime();

		noCheckUp++;
		
		// Request
		var data = {
			cekNo: noCheckUp,
			reqdate: currDate,
			xtime: sendTimer
		};
		
		if(timeoutTable[getID]){
			clearTimeout(timeoutTable[getID]);
		}
		
		if(noCheckUp <= totalSponsor){
			
			//LOAD TILL MAX_VALUE
			//ajax-load
			$.ajax({
				url: getTarget,
				dataType: 'json',
				type: 'POST',
				data: data,
				success: function(rdata, textStatus, XMLHttpRequest)
				{
					
					if(rdata.info_html){
						$('#'+getID+'-info').html(rdata.info_html);
					}
					
					if(rdata.content_html){
						$('#'+getID+'-data').prepend(rdata.content_html);
					}
					
					//reload
					timeoutTable[getID] = setTimeout(function(){ 
						$('#'+getID+' button.sponsor-table-refresh').trigger('click');
					}, getRefresh);
					
					App.blocks('#'+getID, 'state_normal');
					
					if(rdata.is_newdate == 1){
						document.location.href = redirect_self;
						return true;
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown)
				{
					//reload
					timeoutTable[getID] = setTimeout(function(){ 
						$('#'+getID+' button.sponsor-table-refresh').trigger('click');
					}, getRefresh);
					
					App.blocks('#'+getID, 'state_normal');
					
					
				}
			});
			
		}else{
			$('#'+getID+'-info').html(totalSponsor+' Data Sponsor sudah selesai update');
			App.blocks('#'+getID, 'state_normal');
			
			//reload
			timeoutTable[getID] = setTimeout(function(){ 
				$('#'+getID+' button.sponsor-table-refresh').trigger('click');
			}, 3600000);
		}

		
		
	});
	
	$('.sponsor-wna-table-refresh').click(function(){
		
		var getTarget = $(this).attr('data-action-url');
		
		// Start timer
		var sendTimer = new Date().getTime();

		// Request
		var data = {
			xtime: sendTimer
		};
		
		//ajax-load
		$.ajax({
			url: getTarget,
			dataType: 'json',
			type: 'POST',
			data: data,
			success: function(rdata, textStatus, XMLHttpRequest)
			{
				$('#sponsor-data-total').html(rdata.total);
				
				setTimeout(function(){
		
					$('#sponsor-data button.sponsor-table-refresh').trigger('click');
					
				}, 2000);
			}
		});

		
		
	});
	
	setTimeout(function(){
		
		$('#sponsor-data button.sponsor-wna-table-refresh').trigger('click');
		
	}, 1000);
	
	
	
	
});