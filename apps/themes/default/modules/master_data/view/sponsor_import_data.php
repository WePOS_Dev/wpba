<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once THEME_PATH."modules/header.php";

?>		
	
	<!-- Main Container -->
	<main id="main-container">
		
		
		<div class="content">
			
			<!-- Dynamic Table Full -->
			<div class="block" id="rekap_absensi_area">
				<div class="block-header bg-gray-lighter">
					<h3 class="block-title">Import Data Sponsor</h3>
				</div>
				<div class="block-content">
					<form class="form-horizontal" method="post" enctype="multipart/form-data">	
						<div class="col-sm-12 preview_peta">	
							<div class="form-group col-sm-12">
								<label class="col-sm-2 control-label">Import Excel</label>
								<div class="col-sm-8 upload_button_area">
									<input type="file" id="upload_excel_sponsor" name="upload_file" data-show-preview="false" accept="xls">
									<div id="msgUpload_sponsor"></div>
								</div>
							</div>	
							<div class="form-group col-sm-12" id="info-after-upload">
							</div>
						</div>
						<div style="clear:both";></div>
					</form>
				</div>
			</div>
			<!-- END Dynamic Table Full -->
			
		</div>
		<!-- END Page Content -->
		
	</main>
	<!-- END Main Container -->
	
<?php
include_once THEME_PATH."modules/footer.php";
?>
