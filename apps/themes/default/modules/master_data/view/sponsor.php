<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once THEME_PATH."modules/header.php";

?>		
	
	<!-- Main Container -->
	<main id="main-container">
		
		
		<div class="content">
			
			<!-- Dynamic Table Full -->
			<div class="block block-themed block-rounded" id="sponsor_area">
				<div class="block-header bg-primary">
					<h3 class="block-title">Data Sponsor</h3>
				</div>
				
				<div class="block-content bg-gray-lighter">
					<div class="row items-push">
						<!--<div class="col-xs-3">
							<div><a href="javascript:void();" data-target="#modal-filter" data-toggle="modal"><i class="fa fa-calendar"></i> Tgl dari</a></div>
							<div class="font-w600"><a href="javascript:void();" id="sponsor_tanggal_dari_display" data-target="#modal-filter" data-toggle="modal"><?php echo date("Y-m-d"); ?></a></div>
						</div>
						<div class="col-xs-3">
							<div><a href="javascript:void();" data-target="#modal-filter" data-toggle="modal"><i class="fa fa-calendar"></i> Tgl sampai</a></div>
							<div class="font-w600"><a href="javascript:void();" id="sponsor_tanggal_sampai_display" data-target="#modal-filter" data-toggle="modal"><?php echo date("Y-m-d"); ?></a></div>
						</div>-->
						<div class="col-xs-3">
							<div><a href="javascript:void(0);" data-target="#modal-filter" data-toggle="modal"><i class="fa fa-list"></i> Kata Kunci</a></div>
							<div class="font-w600"><a href="javascript:void(0);" id="sponsor_keyword_display" data-target="#modal-filter" data-toggle="modal">-</a></div>
						</div>
					</div>
				</div>
				
				<div class="block-content">
					<div class="row items-push">
						<div class="col-lg-6">&nbsp;</div>
						<div class="col-lg-6 text-right">
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-gray" id="tambah_sponsor"  data-target="#modal-add-data" data-toggle="modal"><i class="fa fa-plus"></i> Tambah</button>
								<!--<button type="button" class="btn btn-default btn-danger"><i class="fa fa-remove"></i> Hapus</button>-->
								<button type="button" class="btn btn-warning" onclick="print_excel(0);"><i class="fa fa-print"></i>  Print</button>
								<button type="button" class="btn btn-success" onclick="print_excel(1);"><i class="fa fa-file-excel-o"></i>  Excel</button>
								<button type="button" class="btn btn-default btn-info" id="refresh_sponsor"><i class="fa fa-refresh"></i> Refresh</button>
							</div>
						</div>
					</div>
					<div id="table-sponsor-area">
					</div>
					
				</div>
			</div>
			<!-- END Dynamic Table Full -->
			
		</div>
		<!-- END Page Content -->
		
		<!-- PRINT & EXCEL -->
		<iframe id="print_excel_iframe" class="hide" src=""></iframe>
		
	</main>
	<!-- END Main Container -->
	
	<!-- Small Modal -->
	<div class="modal" id="modal-filter" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="block block-themed block-transparent remove-margin-b">
					<div class="block-header bg-primary-dark">
						<ul class="block-options">
							<li>
								<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
							</li>
						</ul>
						<h3 class="block-title">Filter</h3>
					</div>
					<div class="block-content">
						<form class="form-horizontal" action="#" method="post" onsubmit="return false;">
							<!--<div class="form-group">
								<div class="col-md-12">
									<div class="input-daterange input-group" data-date-format="yyyy-mm-dd">
										<input class="form-control" type="text" id="tanggal_dari" name="tanggal_dari" placeholder="Tgl Dari" value="<?php echo date("Y-m-d"); ?>">
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_sampai" name="tanggal_sampai" placeholder="Tgl sampai" value="<?php echo date("Y-m-d"); ?>">
									</div>
								</div>
							</div>-->
							<div class="form-group">
								<div class="col-md-12">
									<input class="form-control" type="text" id="keyword" placeholder="Kata Kunci"/>
								</div>
							</div>
							
							
							<div class="form-group">
								<div class="col-md-6" style="margin-top:10px;">
									<button class="btn btn-block btn-primary" id="do_filter_sponsor"><i class="fa fa-filter pull-right"></i> Filter</button>
								</div>
								<div class="col-md-6" style="margin-top:10px;">
									<button class="btn btn-block btn-default" type="button" data-dismiss="modal" id="close-modal-filter">Close</button>
								</div>
							</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END Small Modal -->
	
	<!-- Small Modal -->
	<div class="modal" id="modal-add-data" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="block block-themed block-transparent remove-margin-b">
					<div class="block-header bg-primary-dark">
						<ul class="block-options">
							<li>
								<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
							</li>
						</ul>
						<h3 class="block-title">Tambah Sponsor</h3>
					</div>
					<div class="block-content">
						<form class="form-horizontal" id="form-data-sponsor" action="#" method="post" onsubmit="return false;">
							
							<div class="form-group">
								<div class="col-md-12">
									<textarea class="form-control" id="sponsor_sponsor_data" name="sponsor_data" placeholder="Sponsor" rows="2"></textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input class="form-control" type="text" id="sponsor_contact_person" name="contact_person" placeholder="Contact Person">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input class="form-control" type="text" id="sponsor_mobile_phone" name="mobile_phone" placeholder="Mobile Phone">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input class="form-control" type="text" id="sponsor_email" name="email" placeholder="Email">
								</div>
							</div>
							<input type="hidden" id="is_edit" name="is_edit">
							<input type="hidden" id="sponsor_id" name="sponsor_id">
							<div class="form-group">
								<div class="col-md-6" style="margin-top:10px;">
									<button class="btn btn-block btn-primary" id="do_add_sponsor"><i class="fa fa-save pull-right"></i> Simpan</button>
								</div>
								<div class="col-md-6" style="margin-top:10px;">
									<button class="btn btn-block btn-default" type="button" data-dismiss="modal" id="close-modal-add">Close</button>
								</div>
							</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END Small Modal -->
	
	
<?php
include_once THEME_PATH."modules/footer.php";
?>
