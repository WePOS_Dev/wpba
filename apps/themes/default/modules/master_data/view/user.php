<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$page_active = 'user';
include_once THEME_PATH."modules/header.php";

?>		
	
	<!-- Main Container -->
	<main id="main-container">
		
		
		<div class="content">
			
			<!-- Dynamic Table Full -->
			<div class="block block-themed block-rounded" id="user_area">
				<div class="block-header bg-primary">
					<h3 class="block-title">Data User</h3>
				</div>
				
				<div class="block-content bg-gray-lighter">
					<div class="row items-push">
						<!--<div class="col-xs-3">
							<div><a href="javascript:void();" data-target="#modal-filter" data-toggle="modal"><i class="fa fa-calendar"></i> Tgl dari</a></div>
							<div class="font-w600"><a href="javascript:void();" id="user_tanggal_dari_display" data-target="#modal-filter" data-toggle="modal"><?php echo date("Y-m-d"); ?></a></div>
						</div>
						<div class="col-xs-3">
							<div><a href="javascript:void();" data-target="#modal-filter" data-toggle="modal"><i class="fa fa-calendar"></i> Tgl sampai</a></div>
							<div class="font-w600"><a href="javascript:void();" id="user_tanggal_sampai_display" data-target="#modal-filter" data-toggle="modal"><?php echo date("Y-m-d"); ?></a></div>
						</div>-->
						<div class="col-xs-3">
							<div><a href="javascript:void(0);" data-target="#modal-filter" data-toggle="modal"><i class="fa fa-list"></i> Kata Kunci</a></div>
							<div class="font-w600"><a href="javascript:void(0);" id="user_keyword_display" data-target="#modal-filter" data-toggle="modal">-</a></div>
						</div>
					</div>
				</div>
				
				<div class="block-content">
					<div class="row items-push">
						<div class="col-lg-6">&nbsp;</div>
						<div class="col-lg-6 text-right">
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-success" id="tambah_user" data-target="#modal-add-data" data-toggle="modal"><i class="fa fa-plus"></i> Tambah</button>
								<!--<button type="button" class="btn btn-default btn-danger"><i class="fa fa-remove"></i> Hapus</button>-->
								<button type="button" class="btn btn-default btn-info" id="refresh_user"><i class="fa fa-refresh"></i> Refresh</button>
							</div>
						</div>
					</div>
					<div id="table-user-area">
					</div>
					
				</div>
			</div>
			<!-- END Dynamic Table Full -->
			
		</div>
		<!-- END Page Content -->
		
	</main>
	<!-- END Main Container -->
	
	<!-- Small Modal -->
	<div class="modal" id="modal-filter" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="block block-themed block-transparent remove-margin-b">
					<div class="block-header bg-primary-dark">
						<ul class="block-options">
							<li>
								<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
							</li>
						</ul>
						<h3 class="block-title">Filter</h3>
					</div>
					<div class="block-content">
						<form class="form-horizontal" action="#" method="post" onsubmit="return false;">
							<!--<div class="form-group">
								<div class="col-md-12">
									<div class="input-daterange input-group" data-date-format="yyyy-mm-dd">
										<input class="form-control" type="text" id="tanggal_dari" name="tanggal_dari" placeholder="Tgl Dari" value="<?php echo date("Y-m-d"); ?>">
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_sampai" name="tanggal_sampai" placeholder="Tgl sampai" value="<?php echo date("Y-m-d"); ?>">
									</div>
								</div>
							</div>-->
							<div class="form-group">
								<div class="col-md-12">
									<input class="form-control" type="text" id="keyword" placeholder="Kata Kunci"/>
								</div>
							</div>
							
							
							<div class="form-group">
								<div class="col-md-6" style="margin-top:10px;">
									<button class="btn btn-block btn-primary" id="do_filter_user"><i class="fa fa-filter pull-right"></i> Filter</button>
								</div>
								<div class="col-md-6" style="margin-top:10px;">
									<button class="btn btn-block btn-default" type="button" data-dismiss="modal" id="close-modal-filter">Close</button>
								</div>
							</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END Small Modal -->
	
	<!-- Small Modal -->
	<div class="modal" id="modal-add-data" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="block block-themed block-transparent remove-margin-b">
					<div class="block-header bg-primary-dark">
						<ul class="block-options">
							<li>
								<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
							</li>
						</ul>
						<h3 class="block-title">Tambah User</h3>
					</div>
					<div class="block-content">
						<form class="form-horizontal" id="form-data-user" action="#" method="post" onsubmit="return false;">
							
							<div class="form-group">
								<div class="col-md-6">
									<input class="form-control" type="text" id="user_username" name="user_username" placeholder="Username">
								</div>
								<div class="col-md-6">
									<select class="form-control" id="role_id" style="width: 100%;">
										<option value="">Pilih Role</option>
										<?php
										if(!empty($data_roles)){
											foreach($data_roles as $dt){
												
												echo '<option value="'.$dt->id.'">'.$dt->role_name.'</option>';
												
											}
										}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6">
									<input class="form-control" type="password" id="user_password" name="user_password" placeholder="Password">
								</div>
								<div class="col-md-6">
									<input class="form-control" type="password" id="user_password_confirm" name="user_password_confirm" placeholder="Confirm Password">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input class="form-control" type="text" id="user_fullname" name="user_fullname" placeholder="Nama">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-6">
									<input class="form-control" type="text" id="user_email" name="user_email" placeholder="Email">
								</div>
								<div class="col-md-6">
									<input class="form-control" type="text" id="user_mobile" name="user_mobile" placeholder="Mobile">
								</div>
							</div>
							<input type="hidden" id="is_edit" name="is_edit">
							<input type="hidden" id="user_id" name="user_id">
							<div class="form-group">
								<div class="col-md-12 text-red">
								ketika update, Kosongkan password jika tidak ada perubahan password!
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-6" style="margin-top:10px;">
									<button class="btn btn-block btn-success" id="do_add_user"><i class="fa fa-save pull-right"></i> Simpan</button>
								</div>
								<div class="col-md-6" style="margin-top:10px;">
									<button class="btn btn-block btn-default" type="button" data-dismiss="modal" id="close-modal-add">Close</button>
								</div>
							</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END Small Modal -->
	
	
<?php
include_once THEME_PATH."modules/footer.php";
?>
