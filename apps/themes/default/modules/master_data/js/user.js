/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */

var BaseTableDatatables = function() {
    // Init full DataTable, for more examples you can check out https://www.datatables.net/
    var initDataTableFull = function() {
        jQuery('#table-user').dataTable({
            //columnDefs: [ { orderable: false, targets: [ 4 ] } ],
            pageLength: 10,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]]
        });
    };

    // DataTables Bootstrap integration
    var bsDataTables = function() {
        var $DataTable = jQuery.fn.dataTable;

        // Set the defaults for DataTables init
        jQuery.extend( true, $DataTable.defaults, {
            dom:
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            renderer: 'bootstrap',
            oLanguage: {
                sLengthMenu: "_MENU_",
                sInfo: "Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>",
                oPaginate: {
                    sPrevious: '<i class="fa fa-angle-left"></i>',
                    sNext: '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        // Default class modification
        jQuery.extend($DataTable.ext.classes, {
            sWrapper: "dataTables_wrapper form-inline dt-bootstrap",
            sFilterInput: "form-control",
            sLengthSelect: "form-control"
        });

        // Bootstrap paging button renderer
        $DataTable.ext.renderer.pageButton.bootstrap = function (settings, host, idx, buttons, page, pages) {
            var api     = new $DataTable.Api(settings);
            var classes = settings.oClasses;
            var lang    = settings.oLanguage.oPaginate;
            var btnDisplay, btnClass;

            var attach = function (container, buttons) {
                var i, ien, node, button;
                var clickHandler = function (e) {
                    e.preventDefault();
                    if (!jQuery(e.currentTarget).hasClass('disabled')) {
                        api.page(e.data.action).draw(false);
                    }
                };

                for (i = 0, ien = buttons.length; i < ien; i++) {
                    button = buttons[i];

                    if (jQuery.isArray(button)) {
                        attach(container, button);
                    }
                    else {
                        btnDisplay = '';
                        btnClass = '';

                        switch (button) {
                            case 'ellipsis':
                                btnDisplay = '&hellip;';
                                btnClass = 'disabled';
                                break;

                            case 'first':
                                btnDisplay = lang.sFirst;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'previous':
                                btnDisplay = lang.sPrevious;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'next':
                                btnDisplay = lang.sNext;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            case 'last':
                                btnDisplay = lang.sLast;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            default:
                                btnDisplay = button + 1;
                                btnClass = page === button ?
                                        'active' : '';
                                break;
                        }

                        if (btnDisplay) {
                            node = jQuery('<li>', {
                                'class': classes.sPageButton + ' ' + btnClass,
                                'aria-controls': settings.sTableId,
                                'tabindex': settings.iTabIndex,
                                'id': idx === 0 && typeof button === 'string' ?
                                        settings.sTableId + '_' + button :
                                        null
                            })
                            .append(jQuery('<a>', {
                                    'href': '#'
                                })
                                .html(btnDisplay)
                            )
                            .appendTo(container);

                            settings.oApi._fnBindAction(
                                node, {action: button}, clickHandler
                            );
                        }
                    }
                }
            };

            attach(
                jQuery(host).empty().html('<ul class="pagination"/>').children('ul'),
                buttons
            );
        };

        // TableTools Bootstrap compatibility - Required TableTools 2.1+
        if ($DataTable.TableTools) {
            // Set the classes that TableTools uses to something suitable for Bootstrap
            jQuery.extend(true, $DataTable.TableTools.classes, {
                "container": "DTTT btn-group",
                "buttons": {
                    "normal": "btn btn-default",
                    "disabled": "disabled"
                },
                "collection": {
                    "container": "DTTT_dropdown dropdown-menu",
                    "buttons": {
                        "normal": "",
                        "disabled": "disabled"
                    }
                },
                "print": {
                    "info": "DTTT_print_info"
                },
                "select": {
                    "row": "active"
                }
            });

            // Have the collection use a bootstrap compatible drop down
            jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
                "collection": {
                    "container": "ul",
                    "button": "li",
                    "liner": "a"
                }
            });
        }
    };

    return {
        init: function() {
            // Init Datatables
            bsDataTables();
            //initDataTableSimple();
            //initDataTableFull();
        }
    };
}();


var keyword = jQuery('#keyword').val();

var dtTableAbsensi;

function refresh_user(load_init){
	
	var getID = '#user_area';
	App.blocks(getID, 'state_loading');
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();

	// Request
	var data = {
		keyword: keyword,
		xtime: sendTimer
	};
	
	var getTarget = appUrl +'master_data/user/load';

	//ajax-load
	$.ajax({
		url: getTarget,
		//dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata){
									
				if(load_init == true){
					BaseTableDatatables.init(); 
					$('#table-user-area').html(rdata);
					dtTableAbsensi = jQuery('#table-user').dataTable({
						//columnDefs: [ { orderable: false, targets: [ 4 ] } ],
						ordering: false,
						searching: false,
						pageLength: 10,
						lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
						oLanguage: {
							sLengthMenu: ""
						},
						dom:
							"<'row'<'col-sm-12'tr>>" +
							"<'row'<'col-sm-6'i><'col-sm-6'p>>"
					});
				}else{
					
					dtTableAbsensi.fnDestroy(true);
					$('#table-user-area').html(rdata);
					dtTableAbsensi = jQuery('#table-user').dataTable({
						//columnDefs: [ { orderable: false, targets: [ 4 ] } ],
						ordering: false,
						searching: false,
						pageLength: 10,
						lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
						oLanguage: {
							sLengthMenu: ""
						},
						dom:
							"<'row'<'col-sm-12'tr>>" +
							"<'row'<'col-sm-6'i><'col-sm-6'p>>"
					});
				}
				
			}else{
				$('#table-user-area').html('');
			}
			
			
			App.blocks(getID, 'state_normal');
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			App.blocks(getID, 'state_normal');
		}
	});
}

function add_user(){
	
	user_username = $('#user_username').val();
	user_password = $('#user_password').val();
	user_password_confirm = $('#user_password_confirm').val();
	role_id = $('#role_id').val();
	user_fullname = $('#user_fullname').val();
	user_mobile = $('#user_mobile').val();
	user_email = $('#user_email').val();
	is_edit = $('#is_edit').val();
	user_id = $('#user_id').val();
	
	if(user_username == ''){
		alert('Username harus diinputkan!');
		return false;
	}
	
	if(role_id == ''){
		alert('Role harus dipilih!');
		return false;
	}
	
	if(user_fullname == ''){
		alert('Nama harus diinputkan!');
		return false;
	}
	
	if(user_id != ''){
		if(user_password != '' || user_password_confirm != ''){
			if(user_password != user_password_confirm){
				alert('konfirmasi Password tidak sama!');
				return false;
			}
		}
	}else{
		if(user_password == ''){
			alert('Password harus diinputkan!');
			return false;
		}else
		if(user_password != user_password_confirm){
			alert('konfirmasi Password tidak sama!');
			return false;
		}
	}
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();

	// Request
	var data = {
		user_username: user_username,
		user_password: user_password,
		role_id: role_id,
		user_fullname: user_fullname,
		user_mobile: user_mobile,
		user_email: user_email,
		is_edit: is_edit,
		user_id: user_id,
		xtime: sendTimer
	};
	
	var getTarget = appUrl +'master_data/user/add';

	//ajax-load
	$.ajax({
		url: getTarget,
		dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata.success){
				
				$('#close-modal-add').trigger('click');
				refresh_user(false);
				
			}else{
				alert(rdata.info);
			}
			
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			alert('Tambah Data Gagal!');
		}
	});
}

function update_user(getId){
	var username = $('#user_username_'+getId).val();
	
	$('#tambah_user').trigger('click');
	
	//load data
	var dtPost = {
		username : username,
		user_id : getId
	};
	
	var d = new Date();
	
	$.ajax({
		url: appUrl+'master_data/user/loadUser?_dc='+d.getTime(),
		dataType: 'json',
		type: 'POST',
		data: dtPost,
		success: function(data, textStatus, XMLHttpRequest)
		{
			$('#user_username').val(data.data.user_username);		
			$('#user_fullname').val(data.data.user_fullname);				
			$('#user_email').val(data.data.user_email);				
			$('#user_mobile').val(data.data.user_mobile);
			$('#is_edit').val('1');
			$('#user_id').val(data.data.id);			
			$('#role_id').val(data.data.role_id);	
			//alert(data.data.role_id);
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{	
			$('#close-modal-add').trigger('click');
			alert('load user failed!');
			
		}

	});
	
}

function delete_user(getId){
	
	var username = $('#user_username_'+getId).val();

	var r = confirm("Delete Username: "+username+"?");
	if (r == true) {
		var dtPost = {
			username : username,
			user_id : getId
		};
		
		var d = new Date();
		
		$.ajax({
			url: appUrl+'master_data/user/deleteUser?_dc='+d.getTime(),
			dataType: 'json',
			type: 'POST',
			data: dtPost,
			success: function(data, textStatus, XMLHttpRequest)
			{
				refresh_user(false);
								
			},
			error: function(XMLHttpRequest, textStatus, errorThrown)
			{	
				alert('delete user failed!');
			}

		});
	} else {
		//return false;
	}
}

// Initialize when page loads
jQuery(function(){ 
	
	 
	$('.js-datepicker').add('.input-daterange').datepicker({
		weekStart: 1,
		autoclose: true,
		todayHighlight: true
	});
		
	$('.js-select2').select2();	
	
	$('#tambah_user').click(function(){
		
		$('#form-data-user')[0].reset();
		$('#is_edit').val(0);
		$('#user_id').val(0);	
	
	});
	
	$('#do_add_user').click(function(){
		
		add_user();
		
	});
	
	$('#do_filter_user').click(function(){
		
		keyword = $('#keyword').val();
		
		//user
		$('#user_keyword_display').html(keyword);
		
		$('#close-modal-filter').trigger('click');
		refresh_user(false);
		
	});
	
	$('#refresh_user').click(function(){
		refresh_user(false);
	});
	
	refresh_user(true);
	
	
});