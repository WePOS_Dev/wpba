<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once THEME_PATH."modules/header_frontend.php";

$total_pegawai_terjadwal = 0;
if(!empty($data_jadwal_pegawai['pegawai_total'])){
	$total_pegawai_terjadwal = $data_jadwal_pegawai['pegawai_total'];
}
?>		
	<script>
		var tanggal_notify = '<?php echo date('Y-m-d'); ?>';
		var currDate = '<?php echo date('Y-m-d'); ?>';
		var redirect_selft = '<?php BASE_URL.'display'; ?>';
	</script>
	
	<!-- Main Container -->
	<main id="main-container">
		
		<div class="content">
			<div class="row">
				<div class="col-lg-4">
					<!-- Latest Sales Widget -->
					<div class="block block-themed block-rounded" id="dashboard-absen-masuk">
						<div class="block-header bg-success">
							<ul class="block-options">
								<li>
									<button data-action-url="<?php echo BASE_URL.'load_data/data_absensi'; ?>" data-refresh="150000" data-action-post="absen_masuk" data-main-id="dashboard-absen-masuk" class="dashboard-table-refresh" type="button">
										<i class="si si-refresh"></i>
									</button>
								</li>
							</ul>
							<h3 class="block-title" id="dashboard-absen-masuk-title">Masuk (0 Pegawai)</h3>
						</div>
						<div id="dashboard-absen-masuk-area">
							<div class="block-content bg-gray-lighter" id="dashboard-absen-masuk-summary">
								<div class="row items-push">
									<div class="col-xs-4">
										<div><small>Tepat Waktu</small></div>
										<div class="font-w600">0 pegawai</div>
									</div>
									<div class="col-xs-4">
										<div><small class="visible-lg visible-md">Terlambat</small><small class="hidden-lg hidden-md">Terlambat Absen</small></div>
										<div class="font-w600">0 pegawai</div>
									</div>
								</div>
							</div>
							<div class="block-content">
								<div class="pull-t pull-r-l">
									<div id="dashboard-absen-masuk-slick" class="remove-margin-b table-slick" data-slider-autoplay="true" data-slider-autoplay-speed="6000">
										<div>
											<table class="table remove-margin-b font-s13">
												<thead>
													<tr>
														<th class="font-s13 font-w600" style="width: 10px;">No</th>
														<th class="font-s13 font-w600">Pegawai</th>
														<th class="font-s13 text-right" style="width: 50px;">Masuk</th>
														<th class="font-s13 font-w600 text-danger text-right" style="width: 70px;"><font class="visible-lg visible-md">Terlambat</font><font class="hidden-lg hidden-md">Telat</font></th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="text-right" style="width: 70px;">&nbsp;</td>
														<td class="font-w600 text-danger text-right">&nbsp;</td>
													</tr>
													<tr>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="text-right" style="width: 70px;">&nbsp;</td>
														<td class="font-w600 text-danger text-right">&nbsp;</td>
													</tr>
													<tr>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="text-right" style="width: 70px;">&nbsp;</td>
														<td class="font-w600 text-danger text-right">&nbsp;</td>
													</tr>
													<tr>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="text-right" style="width: 70px;">&nbsp;</td>
														<td class="font-w600 text-danger text-right">&nbsp;</td>
													</tr>
													<tr>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="text-right" style="width: 70px;">&nbsp;</td>
														<td class="font-w600 text-danger text-right">&nbsp;</td>
													</tr>
													<tr>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="text-right" style="width: 70px;">&nbsp;</td>
														<td class="font-w600 text-danger text-right">&nbsp;</td>
													</tr>
													<tr>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="text-right" style="width: 70px;">&nbsp;</td>
														<td class="font-w600 text-danger text-right">&nbsp;</td>
													</tr>
													<tr>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="text-right" style="width: 70px;">&nbsp;</td>
														<td class="font-w600 text-danger text-right">&nbsp;</td>
													</tr>
													<tr>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="text-right" style="width: 70px;">&nbsp;</td>
														<td class="font-w600 text-danger text-right">&nbsp;</td>
													</tr>
													<tr>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="text-right" style="width: 70px;">&nbsp;</td>
														<td class="font-w600 text-danger text-right">&nbsp;</td>
													</tr>
													<tr>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="font-w600"><small>&nbsp;</small></td>
														<td class="text-right" style="width: 70px;">&nbsp;</td>
														<td class="font-w600 text-danger text-right">&nbsp;</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END Latest Sales Widget -->
				</div>
				
				<div class="col-lg-4">
					<!-- Latest Sales Widget -->
					<div class="block block-themed block-rounded" id="dashboard-tidak-absen">
						<div class="block-header bg-warning">
							<ul class="block-options">
								<li>
									<button data-action-url="<?php echo BASE_URL.'load_data/data_absensi'; ?>" data-refresh="150000" data-action-post="tidak_absen" data-main-id="dashboard-tidak-absen" class="dashboard-table-refresh" type="button">
										<i class="si si-refresh"></i>
									</button>
								</li>
							</ul>
							<h3 class="block-title" id="dashboard-tidak-absen-title">Tidak Masuk (0 Pegawai)</h3>
						</div>
						
						<div class="block-content bg-gray-lighter" id="dashboard-tidak-absen-summary">
							<div class="row items-push">
								<div class="col-xs-2">
									<div><small>Izin</small></div>
									<div class="font-w600">0</div>
								</div>
								<div class="col-xs-2">
									<div><small>Sakit</small></div>
									<div class="font-w600">0</div>
								</div>
								<div class="col-xs-2">
									<div><small>Cuti</small></div>
									<div class="font-w600">0</div>
								</div>
								<div class="col-xs-2">
									<div><small>Dinas</small></div>
									<div class="font-w600">0</div>
								</div>
								<div class="col-xs-2">
									<div><small>Off</small></div>
									<div class="font-w600">0</div>
								</div>
								<div class="col-xs-2">
									<div><small>T/K</small></div>
									<div class="font-w600">0</div>
								</div>
							</div>
						</div>
						<div class="block-content">
							<div class="pull-t pull-r-l">
								
								<div id="dashboard-tidak-absen-slick" class="remove-margin-b table-slick" data-slider-autoplay="true" data-slider-autoplay-speed="6000">
									<div>
										<table class="table remove-margin-b font-s13">
											<thead>
												<tr>
													<th class="font-s13 font-w600" style="width: 10px;">No</th>
													<th class="font-s13 font-w600">Pegawai</th>
													<th class="font-s13 font-w600 text-right" style="width: 100px;">Status</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600 text-right">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600 text-right">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600 text-right">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600 text-right">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600 text-right">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600 text-right">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600 text-right">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600 text-right">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600 text-right">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600 text-right">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600 text-right">&nbsp;</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<!-- END Latest Sales Widget -->
				</div>
				
				<div class="col-lg-4">
					<!-- Latest Sales Widget -->
					<div class="block block-themed block-rounded" id="dashboard-absen-pulang">
						<div class="block-header bg-info">
							<ul class="block-options">
								<li>
									<button data-action-url="<?php echo BASE_URL.'load_data/data_absensi'; ?>" data-refresh="150000" data-action-post="absen_pulang" data-main-id="dashboard-absen-pulang" class="dashboard-table-refresh" type="button">
										<i class="si si-refresh"></i>
									</button>
								</li>
							</ul>
							<h3 class="block-title" id="dashboard-absen-pulang-title">Pulang (0 Pegawai)</h3>
						</div>
						
						<div class="block-content bg-gray-lighter" id="dashboard-absen-pulang-summary">
							<div class="row items-push">
								<div class="col-xs-4">
									<div><small>Tepat Waktu</small></div>
									<div class="font-w600">0 pegawai</div>
								</div>
								<div class="col-xs-4">
									<div><small>Pulang Awal</small></div>
									<div class="font-w600">0 pegawai</div>
								</div>
							</div>
						</div>
						<div class="block-content">
							<div class="pull-t pull-r-l">
								<div id="dashboard-absen-pulang-slick" class="remove-margin-b table-slick" data-slider-autoplay="true" data-slider-autoplay-speed="6000">
									<div>
										<table class="table remove-margin-b font-s13">
											<thead>
												<tr>
													<th class="font-s13 font-w600" style="width: 30px;">No.</th>
													<th class="font-s13 font-w600">Pegawai</th>
													<th class="font-s13 text-right" style="width: 70px;">Pulang</th>
													<th class="font-s13 font-w600 text-danger text-right" style="width: 70px;">Plg.Awal</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="text-right" style="width: 70px;">&nbsp;</td>
													<td class="font-w600 text-danger text-right" style="width: 70px;">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="text-right" style="width: 70px;">&nbsp;</td>
													<td class="font-w600 text-danger text-right" style="width: 70px;">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="text-right" style="width: 70px;">&nbsp;</td>
													<td class="font-w600 text-danger text-right" style="width: 70px;">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="text-right" style="width: 70px;">&nbsp;</td>
													<td class="font-w600 text-danger text-right" style="width: 70px;">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="text-right" style="width: 70px;">&nbsp;</td>
													<td class="font-w600 text-danger text-right" style="width: 70px;">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="text-right" style="width: 70px;">&nbsp;</td>
													<td class="font-w600 text-danger text-right" style="width: 70px;">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="text-right" style="width: 70px;">&nbsp;</td>
													<td class="font-w600 text-danger text-right" style="width: 70px;">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="text-right" style="width: 70px;">&nbsp;</td>
													<td class="font-w600 text-danger text-right" style="width: 70px;">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="text-right" style="width: 70px;">&nbsp;</td>
													<td class="font-w600 text-danger text-right" style="width: 70px;">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="text-right" style="width: 70px;">&nbsp;</td>
													<td class="font-w600 text-danger text-right" style="width: 70px;">&nbsp;</td>
												</tr>
												<tr>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="font-w600"><small>&nbsp;</small></td>
													<td class="text-right" style="width: 70px;">&nbsp;</td>
													<td class="font-w600 text-danger text-right" style="width: 70px;">&nbsp;</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						
					</div>
					<!-- END Latest Sales Widget -->
				</div>
				
			</div>
			
			<div class="row">
				<div class="col-lg-3">
					<div class="block block-themed">
						<div class="block-header bg-gray-darker">
							<h3 class="block-title" id="dashboard-total-pegawai">Total: <?php echo $total_pegawai_terjadwal; ?> Pegawai</h3>
						</div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="block block-themed">
						<div class="block-header bg-success">
							<h3 class="block-title" id="dashboard-persentase-masuk">Masuk: 0 %</h3>
						</div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="block block-themed">
						<div class="block-header bg-warning">
							<h3 class="block-title" id="dashboard-persentase-terlambat">Terlambat: 0 %</h3>
						</div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="block block-themed">
						<div class="block-header bg-danger">
							<h3 class="block-title" id="dashboard-persentase-tk">Tanpa Ket: 0 %</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END Page Content -->
		
	</main>
	<!-- END Main Container -->
	
<?php
include_once THEME_PATH."modules/footer_frontend.php";
?>
