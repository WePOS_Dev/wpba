 $(function () {
				
	// Get each slider element (with .js-slider class)
	/*$("#dashboard-tidak-absen-slick").each(function(){
		var $slider = $(this);

		// Get each slider's init data
		var $sliderArrows       = $slider.data('slider-arrows') ? $slider.data('slider-arrows') : false;
		var $sliderDots         = $slider.data('slider-dots') ? $slider.data('slider-dots') : false;
		var $sliderNum          = $slider.data('slider-num') ? $slider.data('slider-num') : 1;
		var $sliderAuto         = $slider.data('slider-autoplay') ? $slider.data('slider-autoplay') : false;
		var $sliderAutoSpeed    = $slider.data('slider-autoplay-speed') ? $slider.data('slider-autoplay-speed') : 3000;

		// Init slick slider
		$slider.slick({
			arrows: $sliderArrows,
			dots: $sliderDots,
			slidesToShow: $sliderNum,
			autoplay: $sliderAuto,
			autoplaySpeed: $sliderAutoSpeed
		});
	});*/
	
	var slickFirsttime = [];
	var timeoutTable = [];
	
	$('.dashboard-table-refresh').click(function(){
		
		var getID = $(this).attr('data-main-id');
		var getTarget = $(this).attr('data-action-url');
		var getDataTarget = $(this).attr('data-action-post');
		var getRefresh = $(this).attr('data-refresh');
		
		App.blocks('#'+getID, 'state_loading');
		
		// Start timer
		var sendTimer = new Date().getTime();

		// Request
		var data = {
			reqdata: getDataTarget,
			reqdate: currDate,
			dataType: 'html',
			for_display: 1,
			xtime: sendTimer
		};
		
		if(!slickFirsttime['#'+getID+'-slick']){
			
		}else{
			$('#'+getID+'-slick').slick('unslick');
		}
		
		if(!timeoutTable[getID]){
			
		}else{
			clearTimeout(timeoutTable[getID]);
		}

		//ajax-load
		$.ajax({
			url: getTarget,
			dataType: 'json',
			type: 'POST',
			data: data,
			success: function(rdata, textStatus, XMLHttpRequest)
			{
				if(rdata.pegawai_terjadwal){
					$('#dashboard-total-pegawai-terjadwal').html(rdata.pegawai_terjadwal);
				}
				if(rdata.persentase_masuk){
					$('#dashboard-persentase-masuk').html(rdata.persentase_masuk);
				}
				if(rdata.persentase_telat){
					$('#dashboard-persentase-terlambat').html(rdata.persentase_telat);
				}
				if(rdata.persentase_tk){
					$('#dashboard-persentase-tk').html(rdata.persentase_tk);
				}
								
				if(rdata.title_html){	
					$('#'+getID+'-title').html(rdata.title_html);				
				}
				
							
				if(rdata.summary_html){	
					$('#'+getID+'-summary').html(rdata.summary_html);					
				}

				if(rdata.is_newdate == 1){
					if(rdata.content_html){
						$('#'+getID+'-slick').html(rdata.content_html);	
					}
					
					document.location.href = redirect_selft;
					return true;
				}else{
					if(rdata.content_html){
						$('#'+getID+'-slick').html(rdata.content_html);	
					}
				}				
				
				
				slickFirsttime['#'+getID+'-slick'] = true;
				var $slider = $('#'+getID+'-slick');
				
				var $sliderArrows       = $slider.data('slider-arrows') ? $slider.data('slider-arrows') : false;
				var $sliderDots         = $slider.data('slider-dots') ? $slider.data('slider-dots') : false;
				var $sliderNum          = $slider.data('slider-num') ? $slider.data('slider-num') : 1;
				var $sliderAuto         = $slider.data('slider-autoplay') ? $slider.data('slider-autoplay') : false;
				var $sliderAutoSpeed    = $slider.data('slider-autoplay-speed') ? $slider.data('slider-autoplay-speed') : 6000;

				// Init slick slider
				$slider.slick({
					arrows: $sliderArrows,
					dots: $sliderDots,
					slidesToShow: $sliderNum,
					autoplay: $sliderAuto,
					autoplaySpeed: $sliderAutoSpeed,
					//vertical: true,
					//verticalSwiping: true
				});
				
				//reload
				timeoutTable[getID] = setTimeout(function(){ 
					$('#'+getID+' button.dashboard-table-refresh').trigger('click');
				}, getRefresh);
				
				App.blocks('#'+getID, 'state_normal');
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown)
			{
				//reload
				timeoutTable[getID] = setTimeout(function(){ 
					$('#'+getID+' button.dashboard-table-refresh').trigger('click');
				}, getRefresh);
				
				App.blocks('#'+getID, 'state_normal');
				
				
			}
		});
		
	});
	
	//firstload
	$('#dashboard-absen-masuk button.dashboard-table-refresh').trigger('click');
	$('#dashboard-tidak-absen button.dashboard-table-refresh').trigger('click');
	$('#dashboard-absen-pulang button.dashboard-table-refresh').trigger('click');
	
});