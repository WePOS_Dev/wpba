<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once THEME_PATH."modules/header.php";

?>		
	
	<!-- Main Container -->
	<main id="main-container">
		
		
		<div class="content">
			
			<!-- Dynamic Table Full -->
			<div class="block block-themed block-rounded" id="lapas_area">
				<div class="block-header bg-amethyst-dark" id="list-title-area">
					<h3 class="block-title">LAPAS / Daftar Pemberitahuan Akan Bebas</h3>
				</div>
				
				<div class="block-content bg-gray-lighter" id="list-filter-area">
					<div class="row items-push">
						
						<div class="col-xs-3">
							<div><a href="javascript:void(0);" data-target="#modal-filter" data-toggle="modal"><i class="fa fa-list"></i> Kata Kunci Pencarian</a></div>
							<div class="font-w600"><a href="javascript:void(0);" id="lapas_keyword_display" data-target="#modal-filter" data-toggle="modal">-</a></div>
						</div>
						
						
						<div class="col-xs-3">
							<div><a href="javascript:void(0);" data-target="#modal-filter" data-toggle="modal"><i class="fa fa-list"></i> Filter Tanggal</a></div>
							<div class="font-w600"><a href="javascript:void(0);" id="filter_tanggal_display" data-target="#modal-filter" data-toggle="modal">-</a></div>
						</div>
						
						<div class="col-xs-3">
							<div><a href="javascript:void();" data-target="#modal-filter" data-toggle="modal"><i class="fa fa-calendar"></i> Tgl dari</a></div>
							<div class="font-w600"><a href="javascript:void();" id="lapas_tanggal_dari_display" data-target="#modal-filter" data-toggle="modal"><?php echo date("Y-m-01"); ?></a></div>
						</div>
						<div class="col-xs-3">
							<div><a href="javascript:void();" data-target="#modal-filter" data-toggle="modal"><i class="fa fa-calendar"></i> Tgl sampai</a></div>
							<div class="font-w600"><a href="javascript:void();" id="lapas_tanggal_sampai_display" data-target="#modal-filter" data-toggle="modal"><?php echo date("Y-m-t"); ?></a></div>
						</div>
					</div>
				</div>
				
				<div class="block-content" id="list-data-area">
					<div class="row items-push">
						<div class="col-lg-6">&nbsp;</div>
						<div class="col-lg-6 text-right">
							<div class="btn-group">
								<!--<button type="button" class="btn btn-warning" onclick="print_excel(0);"><i class="fa fa-print"></i>  Print</button>-->
								<button type="button" class="btn btn-success" onclick="print_excel(1);"><i class="fa fa-file-excel-o"></i>  Excel</button>
								<button type="button" class="btn btn-default btn-info" id="refresh_data"><i class="fa fa-refresh"></i> Refresh</button>
								
								<button type="button" class="btn btn-default btn-warning" id="reset_data"><i class="fa fa-remove"></i> Reset</button>
								
								<input type="hidden" id="reset_default_date" name="reset_default_date" value="<?php echo date("Y-m-01"); ?>">
								<input type="hidden" id="reset_default_date_till" name="reset_default_date_till" value="<?php echo date("Y-m-t"); ?>">
							</div>
						</div>
					</div>
					<div id="table-lapas-area">
					</div>
					
				</div>
				
				<div class="block-header bg-amethyst-dark" id="form-edit-title-area">
					<h3 class="block-title">LAPAS / Form Pemberitahuan Akan Bebas</h3>
				</div>
				
				<div class="block-content bg-gray-lighter" id="form-edit-area">
					
					<div class="alert alert-danger alert-dismissable" id="form-edit-error-area">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h3 class="font-w300 push-15"></h3>
						<p id="content-error-area"></p>
					</div>
					
					<div class="row items-push">
						<div class="col-md-6">
							<form class="js-validation-bootstrap form-horizontal" id="form-data-pemberitahuan-lapas" action="#" method="post" onsubmit="return false;">
								
								<div class="form-group">
									<label class="col-md-4 control-label" for="no_berita_bebas">No. Register</label>
									<div class="col-md-8">
										<input class="form-control" type="text" id="no_berita_bebas" name="no_berita_bebas">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="tanggal_berita">Tanggal Register</label>
									<div class="col-md-4">
										<input class="js-datepicker form-control" type="text" id="tanggal_berita" name="tanggal_berita" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="nama_wna">Nama</label>
									<div class="col-md-8">
										<input class="form-control" type="text" id="nama_wna" name="nama_wna">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="tempat_lahir">Tempat Lahir</label>
									<div class="col-md-8">
										<input class="form-control" type="text" id="tempat_lahir" name="tempat_lahir">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="tanggal_lahir">Tanggal Lahir</label>
									<div class="col-md-4">
										<input class="js-datepicker form-control" type="text" id="tanggal_lahir" name="tanggal_lahir" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="kebangsaan">Kebangsaan</label>
									<div class="col-md-8">
										<input class="form-control" type="text" id="kebangsaan" name="kebangsaan">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="no_paspor">No. Paspor</label>
									<div class="col-md-8">
										<input class="form-control" type="text" id="no_paspor" name="no_paspor">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="jenis_kelamin">Jenis Kelamin</label>
									<div class="col-md-8">
										<select class="js-select2 form-control" id="jenis_kelamin"  id="jenis_kelamin" style="width: 100%;">
											<option value="">Pilih Jenis Kelamin</option>
											<?php
											$selected_L = '';
											$selected_P = '';
											if($data_edit['jenis_kelamin'] == 'L'){
												$selected_L = ' selected="selected"';
											}
											if($data_edit['jenis_kelamin'] == 'P'){
												$selected_P = ' selected="selected"';
											}
											echo '<option value="L"'.$selected_L.'>Laki-laki</option>';
											echo '<option value="P"'.$selected_P.'>Perempuan</option>';
											?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="alamat">Alamat</label>
									<div class="col-md-8">
										<textarea class="form-control" id="alamat" name="alamat" rows="2"></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="no_ic">No. IC</label>
									<div class="col-md-8">
										<input class="form-control" type="text" id="no_ic" name="no_ic">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="tanggal_bebas">Tanggal Bebas</label>
									<div class="col-md-8">
										<input class="js-datepicker form-control" type="text" id="tanggal_bebas" name="tanggal_bebas" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="catatan">Catatan</label>
									<div class="col-md-8">
										<textarea class="form-control" id="catatan" name="catatan" rows="2"></textarea>
									</div>
								</div>
								
								<input type="hidden" id="is_edit" name="is_edit" value="0">
								<input type="hidden" id="pemberitahuan_id" name="pemberitahuan_id" value="0">
								<input type="hidden" id="tipe_instansi" name="tipe_instansi" value="">
								<input type="hidden" id="file_surat_putusan_old" name="file_surat_putusan_old" value="">
								<div class="form-group">
									<div class="col-md-4 col-md-offset-2" style="margin-top:10px;">
										<button class="btn btn-block btn-success" id="do_add_pemberitahuan" type="submit"><i class="fa fa-save pull-right"></i> Simpan</button>
									</div>
									<div class="col-md-4" style="margin-top:10px;">
										<button class="btn btn-block btn-warning" id="close-form-update" type="reset"><i class="fa fa-close pull-right"></i> Batal</button>
									</div>
								</div>
								
							</form>
						</div>
						<div class="col-md-6">
							<div class="block block-bordered">
								<div class="block-header bg-gray-lighter">
									<h3 class="block-title">Upload Surat Putusan</h3>
								</div>
								<div class="block-content">
									<div class="form-group col-md-12">
										<label class="col-md-3 control-label" for="no_surat_putusan">No Surat Putusan</label>
										<div class="col-md-9">
											<input class="form-control" type="text" id="no_surat_putusan" name="no_surat_putusan">
										</div>
									</div>
									<div class="form-group col-md-12">
										<label class="col-md-3 control-label" for="file_surat_putusan">File Surat Putusan</label>
										<div class="col-md-9">
											<input class="form-control" type="text" id="file_surat_putusan" name="file_surat_putusan" readonly="readonly">
										</div>
									</div>
									
										
									<form class="form-horizontal" id="form-upload" method="post" enctype="multipart/form-data">
									<div class="form-group col-md-12">
										<div class="col-md-12 upload_button_area">
											<input type="file" id="upload_dokumen" name="upload_file" data-show-preview="false" accept="jpg,jpeg,png,bmp,pdf">
											<div id="msgUpload_dokumen"></div>
										</div>
									</div>	
									
									</form>
									
									
									
									<div class="form-group col-md-12">
										<div class="col-md-12" style="margin-top:10px;">
											<button class="btn btn-block btn-warning" id="reset_upload" type="reset"><i class="fa fa-remove pull-right"></i> Delete File </button>
										</div>
									</div>
									
									<div class="form-group col-md-12" id="info-after-upload">
									</div>
									<div style="clear:both";></div>
								</div>
							</div>
						</div>
					</div>
					<br/>
					<br/>
					<br/>
					
				</div>
				
				
				
				
				
				<div class="block-content" id="hapus-data-area">
					
					
					<input type="hidden" id="delete_pemberitahuan_id" name="delete_pemberitahuan_id" value="0">
						
					<div class="alert alert-danger alert-dismissable" id="form-edit-error-area">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&nbsp;</button>
						<h3 class="font-w300 push-15" id="hapus-data-title"></h3>
						<p id="hapus-data-message"></p>
					</div>
					
					<div class="form-group">
						<div class="col-md-2 col-md-offset-2" style="margin-top:10px;">
							<button class="btn btn-block btn-success" id="do_delete_pemberitahuan"><i class="fa fa-check pull-right"></i> Hapus Data</button>
						</div>
						<div class="col-md-2" style="margin-top:10px;">
							<button class="btn btn-block btn-warning" id="close-delete-area" type="reset"><i class="fa fa-close pull-right"></i> Batal</button>
						</div>
					</div>
					
				</div>
				
			</div>
			<!-- END Dynamic Table Full -->
			
		</div>
		<!-- END Page Content -->
		
		<!-- PRINT & EXCEL -->
		<iframe id="print_excel_iframe" class="hide" src=""></iframe>
		
	</main>
	<!-- END Main Container -->
	
	<!-- Small Modal -->
	<div class="modal" id="modal-filter" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				<div class="block block-themed block-transparent remove-margin-b">
					<div class="block-header bg-primary-dark">
						<ul class="block-options">
							<li>
								<button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
							</li>
						</ul>
						<h3 class="block-title">Filter Pencarian</h3>
					</div>
					<div class="block-content">
						<form class="form-horizontal" action="#" method="post" onsubmit="return false;">
							
							
							<div class="form-group">
								<div class="col-md-12" style="margin-bottom:5px;">
									<input class="form-control" type="text" id="keyword_pencarian" placeholder="No. Berita / Nama WNA / No. Paspor"/>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-4">
									<select class="js-select2 form-control" id="tipe_tanggal_pencarian" name="tipe_tanggal_pencarian" style="width: 100%;">
										<option value="">Pilih Tipe Tanggal</option>
										<option value="tanggal_berita">Tanggal Register</option>
										<option value="tanggal_bebas">Tanggal Bebas</option>
									</select>
								</div>
								
								<div class="col-md-8">
									<div class="input-daterange input-group" data-date-format="yyyy-mm-dd">
										<input class="form-control" type="text" id="tanggal_dari" name="tanggal_dari" placeholder="Tgl Dari" value="<?php echo date("Y-m-01"); ?>">
										<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
										<input class="form-control" type="text" id="tanggal_sampai" name="tanggal_sampai" placeholder="Tgl sampai" value="<?php echo date("Y-m-t"); ?>">
									</div>
								</div>
							</div>
							
							
							
							<div class="form-group">
								<div class="col-md-3 col-md-offset-3" style="margin-top:10px;">
									<button class="btn btn-block btn-primary" id="do_filter_lapas"><i class="fa fa-filter pull-right"></i> Cari</button>
								</div>
								<div class="col-md-3" style="margin-top:10px;">
									<button class="btn btn-block btn-default" type="button" data-dismiss="modal" id="close-modal-filter"><i class="fa fa-remove pull-right"></i> Tutup</button>
								</div>
							</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END Small Modal -->
	
<?php
include_once THEME_PATH."modules/footer.php";
?>
