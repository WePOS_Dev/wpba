/*
 *  Document   : base_tables_datatables.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tables Datatables Page
 */

var BaseTableDatatables = function() {
    // Init full DataTable, for more examples you can check out https://www.datatables.net/
    var initDataTableFull = function() {
        jQuery('#table-lapas').dataTable({
            //columnDefs: [ { orderable: false, targets: [ 4 ] } ],
            pageLength: 10,
            lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]]
        });
    };

    // DataTables Bootstrap integration
    var bsDataTables = function() {
        var $DataTable = jQuery.fn.dataTable;

        // Set the defaults for DataTables init
        jQuery.extend( true, $DataTable.defaults, {
            dom:
                "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            renderer: 'bootstrap',
            oLanguage: {
                sLengthMenu: "_MENU_",
                sInfo: "Showing <strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>",
                oPaginate: {
                    sPrevious: '<i class="fa fa-angle-left"></i>',
                    sNext: '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        // Default class modification
        jQuery.extend($DataTable.ext.classes, {
            sWrapper: "dataTables_wrapper form-inline dt-bootstrap",
            sFilterInput: "form-control",
            sLengthSelect: "form-control"
        });

        // Bootstrap paging button renderer
        $DataTable.ext.renderer.pageButton.bootstrap = function (settings, host, idx, buttons, page, pages) {
            var api     = new $DataTable.Api(settings);
            var classes = settings.oClasses;
            var lang    = settings.oLanguage.oPaginate;
            var btnDisplay, btnClass;

            var attach = function (container, buttons) {
                var i, ien, node, button;
                var clickHandler = function (e) {
                    e.preventDefault();
                    if (!jQuery(e.currentTarget).hasClass('disabled')) {
                        api.page(e.data.action).draw(false);
                    }
                };

                for (i = 0, ien = buttons.length; i < ien; i++) {
                    button = buttons[i];

                    if (jQuery.isArray(button)) {
                        attach(container, button);
                    }
                    else {
                        btnDisplay = '';
                        btnClass = '';

                        switch (button) {
                            case 'ellipsis':
                                btnDisplay = '&hellip;';
                                btnClass = 'disabled';
                                break;

                            case 'first':
                                btnDisplay = lang.sFirst;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'previous':
                                btnDisplay = lang.sPrevious;
                                btnClass = button + (page > 0 ? '' : ' disabled');
                                break;

                            case 'next':
                                btnDisplay = lang.sNext;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            case 'last':
                                btnDisplay = lang.sLast;
                                btnClass = button + (page < pages - 1 ? '' : ' disabled');
                                break;

                            default:
                                btnDisplay = button + 1;
                                btnClass = page === button ?
                                        'active' : '';
                                break;
                        }

                        if (btnDisplay) {
                            node = jQuery('<li>', {
                                'class': classes.sPageButton + ' ' + btnClass,
                                'aria-controls': settings.sTableId,
                                'tabindex': settings.iTabIndex,
                                'id': idx === 0 && typeof button === 'string' ?
                                        settings.sTableId + '_' + button :
                                        null
                            })
                            .append(jQuery('<a>', {
                                    'href': '#'
                                })
                                .html(btnDisplay)
                            )
                            .appendTo(container);

                            settings.oApi._fnBindAction(
                                node, {action: button}, clickHandler
                            );
                        }
                    }
                }
            };

            attach(
                jQuery(host).empty().html('<ul class="pagination"/>').children('ul'),
                buttons
            );
        };

        // TableTools Bootstrap compatibility - Required TableTools 2.1+
        if ($DataTable.TableTools) {
            // Set the classes that TableTools uses to something suitable for Bootstrap
            jQuery.extend(true, $DataTable.TableTools.classes, {
                "container": "DTTT btn-group",
                "buttons": {
                    "normal": "btn btn-default",
                    "disabled": "disabled"
                },
                "collection": {
                    "container": "DTTT_dropdown dropdown-menu",
                    "buttons": {
                        "normal": "",
                        "disabled": "disabled"
                    }
                },
                "print": {
                    "info": "DTTT_print_info"
                },
                "select": {
                    "row": "active"
                }
            });

            // Have the collection use a bootstrap compatible drop down
            jQuery.extend(true, $DataTable.TableTools.DEFAULTS.oTags, {
                "collection": {
                    "container": "ul",
                    "button": "li",
                    "liner": "a"
                }
            });
        }
    };

    return {
        init: function() {
            // Init Datatables
            bsDataTables();
            //initDataTableSimple();
            //initDataTableFull();
        }
    };
}();


var tanggal_dari = jQuery('#tanggal_dari').val();
var tanggal_sampai = jQuery('#tanggal_sampai').val();
var tipe_tanggal_pencarian = jQuery('#tipe_tanggal_pencarian').val();
var keyword_pencarian = jQuery('#keyword_pencarian').val();
var result_excel = 0;
var result_print = 0;
var dtTableAbsensi;

function refresh_data(load_init){
	
	var getID = '#lapas_area';
	App.blocks(getID, 'state_loading');
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();

	// Request
	var data = {
		keyword_pencarian: keyword_pencarian,
		tipe_tanggal_pencarian: tipe_tanggal_pencarian,
		tanggal_dari: tanggal_dari,
		tanggal_sampai: tanggal_sampai,
		xtime: sendTimer
	};
	
	var getTarget = appUrl +'lapas/lapas/load_list_data';

	//ajax-load
	$.ajax({
		url: getTarget,
		//dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata){
									
				if(load_init == true){
					BaseTableDatatables.init(); 
					$('#table-lapas-area').html(rdata);
					dtTableAbsensi = jQuery('#table-lapas').dataTable({
						//columnDefs: [ { orderable: false, targets: [ 4 ] } ],
						ordering: false,
						searching: false,
						pageLength: 10,
						lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
						oLanguage: {
							sLengthMenu: ""
						},
						dom:
							"<'row'<'col-sm-12'tr>>" +
							"<'row'<'col-sm-6'i><'col-sm-6'p>>"
					});
				}else{
					
					dtTableAbsensi.fnDestroy(true);
					$('#table-lapas-area').html(rdata);
					dtTableAbsensi = jQuery('#table-lapas').dataTable({
						//columnDefs: [ { orderable: false, targets: [ 4 ] } ],
						ordering: false,
						searching: false,
						pageLength: 10,
						lengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
						oLanguage: {
							sLengthMenu: ""
						},
						dom:
							"<'row'<'col-sm-12'tr>>" +
							"<'row'<'col-sm-6'i><'col-sm-6'p>>"
					});
				}
				
			}else{
				$('#table-lapas-area').html('');
			}
			
			
			App.blocks(getID, 'state_normal');
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			App.blocks(getID, 'state_normal');
		}
	});
}

function print_excel(xval){
	
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();
	var getTarget = appUrl +'lapas/lapas/load_list_data?xtime='+sendTimer;
	
	if(!xval){
		result_print = 1;
		result_excel = 0;
		
	}else{
		result_print = 0;
		result_excel = 1;
		
	}
	
	getTarget += '&keyword_pencarian='+keyword_pencarian;
	getTarget += '&tipe_tanggal_pencarian='+tipe_tanggal_pencarian;
	getTarget += '&tanggal_dari='+tanggal_dari;
	getTarget += '&tanggal_sampai='+tanggal_sampai;
	getTarget += '&tipe=list_data';
	getTarget += '&result_excel='+result_excel;
	getTarget += '&result_print='+result_print;
	
	$('#print_excel_iframe').attr('src', getTarget);
	
	
}

function update_pemberitahuan(){
	
	//load data-table
	// Start timer
	var sendTimer = new Date().getTime();

	// Request
	var data = {
		no_berita_bebas: $('#no_berita_bebas').val(),
		tanggal_berita: $('#tanggal_berita').val(),
		nama_wna: $('#nama_wna').val(),
		tempat_lahir: $('#tempat_lahir').val(),
		tanggal_lahir: $('#tanggal_lahir').val(),
		kebangsaan: $('#kebangsaan').val(),
		no_paspor: $('#no_paspor').val(),
		jenis_kelamin: $('#jenis_kelamin').select2('val'),
		alamat: $('#alamat').val(),
		no_ic: $('#no_ic').val(),
		tanggal_bebas: $('#tanggal_bebas').val(),
		catatan: $('#catatan').val(),
		no_surat_putusan: $('#no_surat_putusan').val(),
		file_surat_putusan: $('#file_surat_putusan').val(),
		file_surat_putusan_old: $('#file_surat_putusan_old').val(),
		is_edit: $('#is_edit').val(),
		pemberitahuan_id: $('#pemberitahuan_id').val(),
		tipe_instansi:$('#tipe_instansi').val(),
		xtime: sendTimer
	};
	
	var getTarget = appUrl +'lapas/pemberitahuan/addData';

	
	$('#info-pemberitahuan-lapas').html('');
	$('#info-pemberitahuan-lapas').hide();
	
	//ajax-load
	$.ajax({
		url: getTarget,
		dataType: 'json',
		type: 'POST',
		data: data,
		success: function(rdata, textStatus, XMLHttpRequest)
		{
			if(rdata.success){
				
				
				//refresh
				$('#close-form-update').trigger('click');
				$('#refresh_data').trigger('click');
				
				$('#form-data-pemberitahuan-lapas')[0].reset();
				$('#is_edit').val(0);
				$('#pemberitahuan_id').val(0);
				$('#tipe_instansi').val('');
				
				$.notify({
					icon: 'fa fa-check',
					message: rdata.info
				},{
					type: 'success',
					placement: {
						from: 'top',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
				
			}else{
				
				$.notify({
					icon: 'fa fa-warning',
					message: rdata.info
				},{
					type: 'danger',
					placement: {
						from: 'bottom',
						align: 'center'
					},
					offset: {
						x: 0,
						y: 60
					}
				});
			}
			
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{
			$.notify({
				icon: 'fa fa-warning',
				message: 'Simpan Pemberitahuan Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'bottom',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
		}
	});
}

function load_pemberitahuan(getId){
	
	//load data
	var dtPost = {
		bebas_id : getId
	};
	
	var d = new Date();
	
	$.ajax({
		url: appUrl+'lapas/lapas/loadData?_dc='+d.getTime(),
		dataType: 'json',
		type: 'POST',
		data: dtPost,
		success: function(data, textStatus, XMLHttpRequest)
		{
			$('#list-title-area').hide();
			$('#list-filter-area').hide();
			$('#list-data-area').hide();
			
			$('#form-edit-title-area').show();
			$('#form-edit-area').show();
			$('#form-edit-error-area').hide();
			$('#hapus-data-area').hide();
			
			$('#no_berita_bebas').val(data.data.no_berita_bebas);		
			$('#tanggal_berita').val(data.data.tanggal_berita);		
			$('#nama_wna').val(data.data.nama_wna);		
			$('#tempat_lahir').val(data.data.tempat_lahir);		
			$('#tanggal_lahir').val(data.data.tanggal_lahir);		
			$('#kebangsaan').val(data.data.kebangsaan);		
			$('#no_paspor').val(data.data.no_paspor);		
			$('#jenis_kelamin').val(data.data.jenis_kelamin);	

			$("#jenis_kelamin").select2("val", data.data.jenis_kelamin);
			
			$('#alamat').val(data.data.alamat);		
			$('#no_ic').val(data.data.no_ic);		
			$('#tanggal_bebas').val(data.data.tanggal_bebas);
			$('#catatan').val(data.data.catatan);
			
			$('#no_surat_putusan').val(data.data.no_surat_putusan);
			$('#file_surat_putusan').val(data.data.file_surat_putusan);
			$('#file_surat_putusan_old').val(data.data.file_surat_putusan);

			$('#is_edit').val('1');
			$('#pemberitahuan_id').val(data.data.id);		
			$('#tipe_instansi').val(data.data.tipe_instansi);		
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{	
			$.notify({
				icon: 'fa fa-warning',
				message: 'Load Data Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'bottom',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
			
		}

	});
	
}

function kirim_pemberitahuan(getId){
	
	//load data
	var dtPost = {
		bebas_id : getId
	};
	
	var d = new Date();
	
	$.ajax({
		url: appUrl+'lapas/lapas/kirimNotif?_dc='+d.getTime(),
		dataType: 'json',
		type: 'POST',
		data: dtPost,
		success: function(data, textStatus, XMLHttpRequest)
		{
			$.notify({
				icon: 'fa fa-success',
				message: data.info
			},{
				type: 'success',
				placement: {
					from: 'top',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});	
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{	
			$.notify({
				icon: 'fa fa-warning',
				message: 'Kirim Notifikasi Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'top',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
			
		}

	});
	
}

function delete_load_pemberitahuan(getId){
	
	var dtPost = {
		bebas_id : getId
	};
	
	var d = new Date();
	
	$.ajax({
		url: appUrl+'lapas/lapas/loadData?_dc='+d.getTime(),
		dataType: 'json',
		type: 'POST',
		data: dtPost,
		success: function(data, textStatus, XMLHttpRequest)
		{
			
			$('#list-title-area').hide();
			$('#list-filter-area').hide();
			$('#list-data-area').hide();
			
			$('#form-edit-title-area').hide();
			$('#form-edit-area').hide();
			$('#form-edit-error-area').hide();
			$('#hapus-data-area').show();	
			
			$('#hapus-data-title').html('Hapus Data');
			var dt_hapus_message = 'No.Berita Bebas: '+data.data.no_berita_bebas+'<br/>';
			dt_hapus_message += 'Tanggal Berita: '+data.data.tgl_berita+'<br/>';
			dt_hapus_message += 'Nama WNA: '+data.data.nama_wna+'<br/>';
			dt_hapus_message += 'Jenis Kelamin: '+data.data.jenis_kelamin+'<br/>';
			dt_hapus_message += 'Kebangsaan: '+data.data.kebangsaan+'<br/>';
			dt_hapus_message += 'No.Paspor: '+data.data.no_paspor+'<br/>';
			dt_hapus_message += 'No.IC: '+data.data.no_ic+'<br/>';
			dt_hapus_message += 'Tanggal Bebas: '+data.data.tanggal_bebas+'<br/>';
			dt_hapus_message += 'Catatan: '+data.data.catatan+'<br/>';
			dt_hapus_message += 'Surat Putusan: '+data.data.no_surat_putusan+'<br/>';
			$('#hapus-data-message').html(dt_hapus_message);
	
			$('#delete_pemberitahuan_id').val(getId);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{	
			$.notify({
				icon: 'fa fa-warning',
				message: 'Load Data Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'top',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
		}

	});
}


function do_delete_pemberitahuan(){
	
	var delete_pemberitahuan_id = $('#delete_pemberitahuan_id').val();
		
	var dtPost = {
		bebas_id : delete_pemberitahuan_id
	};
	
	var d = new Date();
	
	$.ajax({
		url: appUrl+'lapas/lapas/deleteData?_dc='+d.getTime(),
		dataType: 'json',
		type: 'POST',
		data: dtPost,
		success: function(data, textStatus, XMLHttpRequest)
		{
			
			//refresh
			$('#close-delete-area').trigger('click');
			$('#refresh_data').trigger('click');
			
			$('#delete_pemberitahuan_id').val(0);
			
			$.notify({
				icon: 'fa fa-check',
				message: data.info
			},{
				type: 'success',
				placement: {
					from: 'top',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{	
			$.notify({
				icon: 'fa fa-warning',
				message: 'Hapus Data Gagal!'
			},{
				type: 'danger',
				placement: {
					from: 'top',
					align: 'center'
				},
				offset: {
					x: 0,
					y: 60
				}
			});
		}

	});
	
}



 var upload_dokumen = {
	name: 'dokumen',
	text: 'Upload Dokumen'
};



 $(function () {
	
	$('#reset_upload').click(function(){
		$('#file_surat_putusan').val('');
	});
	
	//UPLOAD FILE
    upload_dokumen = $('#upload_'+upload_dokumen.name).fileinput({
        uploadUrl: appUrl+'lapas/lapas/upload_dokumen', // you must set a valid URL here else you will get an error
        allowedFileExtensions : ['jpg','jpeg','png','bmp','pdf'],
		//uploadAsync: false,
		//showUpload: false, // hide upload button
		showRemove: false, // hide remove button
        maxFilesNum: 1,
		browseClass: "btn btn-success",
		browseLabel: " Pilih File",
		browseIcon: '<i class="fa fa-file-image-o"></i> ',
		uploadClass: "btn btn-info",
		//showCaption: false,
		elErrorContainer: "#msgUpload_"+upload_dokumen.name,
		//elCaptionText: "#fileUploadName_"+module_config.name, 
		uploadExtraData: {
			//permohonan_id: permohonan_id_dokumen,
			//jenis_dokumen: jenis_dokumen,
			//nomor_dokumen: nomor_dokumen,
			uploadname: 'upload_file'
		}
	}).on("filepreupload", function(event, files) {
	
		//blockUI('#preview_upload_excel_'+upload_dokumen.name);	
		$('#info-after-upload').html('');
		
	}).on("filebatchpreupload", function(event, data, jqXHR) {
	
		data.form.append('pemberitahuan_id',  $("#pemberitahuan_id").val());
		
	}).on('filebatchuploadsuccess', function(event, data, previewId, index) {
		var form = data.form, files = data.files, extra = data.extra,
			response = data.response, reader = data.reader;
			
		$('#info-after-upload').html(response.info);
		if(response.filename != ''){
			$('#file_surat_putusan').val(response.filename);
		}
		
		//response.data.id_upload
		
	}).on('filebatchuploadcomplete', function(event, files, extra) {
		$('#upload_'+upload_dokumen.name).fileinput("clear");
	});
	
	
	
});

// Initialize when page loads
jQuery(function(){ 
	
	 
	$('.js-datepicker').add('.input-daterange').datepicker({
		weekStart: 1,
		autoclose: true,
		todayHighlight: true
	});
		
	$('.js-select2').select2();

	$('.js-validation-bootstrap').validate({
		errorClass: 'help-block animated fadeInDown',
		errorElement: 'div',
		errorPlacement: function(error, e) {
			jQuery(e).parents('.form-group > div').append(error);
		},
		highlight: function(e) {
			jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
			jQuery(e).closest('.help-block').remove();
		},
		success: function(e) {
			jQuery(e).closest('.form-group').removeClass('has-error');
			jQuery(e).closest('.help-block').remove();
		},
		rules: {
			'no_berita_bebas': {
				required: true
			},
			'tanggal_berita': {
				required: true
			},
			'nama_wna': {
				required: true
			},
			'no_paspor': {
				required: true
			},
			'kebangsaan': {
				required: true
			},
			'no_ic': {
				required: true
			},
			'tanggal_bebas': {
				required: true
			}
		},
		messages: {
			'no_berita_bebas': 'Inputkan No. Berita Bebas',
			'tanggal_berita': 'Inputkan Tanggal Berita',
			'nama_wna': 'Inputkan Nama WNA',
			'no_paspor': 'Inputkan No. Paspor',
			'kebangsaan': 'Inputkan Kebangsaan',
			'no_ic': 'Inputkan No.IC',
			'tanggal_bebas': 'Inputkan Tanggal Bebas'
		},
		submitHandler: function(form) {
			update_pemberitahuan();
		}
	});	
	
	
	$('#form-edit-title-area').hide();
	$('#form-edit-area').hide();
	$('#form-edit-error-area').hide();
	$('#hapus-data-area').hide();
	
	$('#list-title-area').show();
	$('#list-filter-area').show();
	$('#list-data-area').show();
	
	
	$('#do_filter_lapas').click(function(){
		
		keyword_pencarian = $('#keyword_pencarian').val();
		tipe_tanggal_pencarian = $('#tipe_tanggal_pencarian').select2("val");
		tanggal_dari = $('#tanggal_dari').val();
		tanggal_sampai = $('#tanggal_sampai').val();
		
		var filter_tanggal_display = '-';
		if(tipe_tanggal_pencarian == 'tanggal_berita'){
			filter_tanggal_display = 'Tanggal Berita';
		}else
		if(tipe_tanggal_pencarian == 'tanggal_bebas'){
			filter_tanggal_display = 'Tanggal Bebas';
		}
		
		$('#lapas_keyword_display').html(keyword_pencarian);
		$('#filter_tanggal_display').html(filter_tanggal_display);
		$('#lapas_tanggal_dari_display').html(tanggal_dari);
		$('#lapas_tanggal_sampai_display').html(tanggal_sampai);
		
		$('#close-modal-filter').trigger('click');
		refresh_data(false);
		
	});
	
	$('#refresh_data').click(function(){
		refresh_data(false);
	});
	
	$('#reset_data').click(function(){
		
		$('#keyword_pencarian').val("");
		$('#tipe_tanggal_pencarian').select2("val", "");
		$('#tanggal_dari').val($('#reset_default_date').val());
		$('#tanggal_sampai').val($('#reset_default_date_till').val());
		
		$('#do_filter_lapas').trigger("click");
		
	});
	
	$('#close-form-update').click(function(){
		
		$('#form-edit-title-area').hide();
		$('#form-edit-area').hide();
		$('#form-edit-error-area').hide();
		$('#hapus-data-area').hide();
		
		$('#list-title-area').show();
		$('#list-filter-area').show();
		$('#list-data-area').show();
		
		$('#hapus-data-title').html('');
		$('#hapus-data-message').html('');
		
		$('#delete_pemberitahuan_id').val(0);
		
	});
	
	$('#do_delete_pemberitahuan').click(function(){
		do_delete_pemberitahuan();
		
	});
	
	$('#close-delete-area').click(function(){
		$('#list-title-area').show();
		$('#list-filter-area').show();
		$('#list-data-area').show();
		
		$('#form-edit-title-area').hide();
		$('#form-edit-area').hide();
		$('#form-edit-error-area').hide();
		$('#hapus-data-area').hide();	
		
		$('#form-data-pemberitahuan-lapas')[0].reset();
		
	});
	
	refresh_data(true);
	
	
});