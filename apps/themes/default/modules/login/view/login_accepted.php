<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 9]><html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-focus"> <!--<![endif]-->
    <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title><?php echo $program_name.' v'.$program_version; ?></title>
    <meta name="keywords" content="<?php echo $program_name.' v'.$program_version; ?>" />
    <meta name="description" content="<?php echo $program_name.' v'.$program_version; ?>">
    <meta name="author" content="<?php echo $program_author; ?>">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
	
	<!-- Icons -->
	<!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
	<link rel="shortcut icon" href="<?php echo ASSETS_URL; ?>img/favicons/favicon.png">

	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-192x192.png" sizes="192x192">

	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-180x180.png">
	<!-- END Icons -->
	
	<!-- Stylesheets -->
	<!-- Web fonts -->
	<link rel="stylesheet" href="<?php echo ASSETS_URL; ?>fonts/fonts.css">
	
	<!-- OneUI CSS framework -->
	<link rel="stylesheet" id="css-main" href="<?php echo ASSETS_URL; ?>css/oneui.css">
	<link rel="stylesheet" id="css-main" href="<?php echo ASSETS_URL; ?>css/themes/city.min.css">
		                				
	<?php
	if(!empty($add_css_page)){
		echo $add_css_page;
	}
	?>
	
    <link href="<?php echo ASSETS_URL; ?>css/custom.css" rel="stylesheet" type="text/css" />
		
	<script>
		var appUrl 		= "<?php echo site_url(); ?>";
		var programName	= "<?php echo config_item('program_name'); ?>";
		var copyright	= "<?php echo config_item('copyright'); ?>";
	</script>
	
</head>

<body>	    
	
	<!-- CORE -->
	<script src="<?php echo APP_URL; ?>core/jquery.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/bootstrap.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.slimscroll.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.scrollLock.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.appear.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.countTo.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.placeholder.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/js.cookie.min.js"></script>
	<script src="<?php echo ASSETS_URL; ?>js/app.js"></script>

	<script src="<?php echo APP_URL; ?>libs/jquery-validation/jquery.validate.min.js"></script>
	
</body>
</html>
