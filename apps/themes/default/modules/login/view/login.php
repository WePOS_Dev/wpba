<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if IE 9]><html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-focus"> <!--<![endif]-->
    <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <title><?php echo $program_name.' v'.$program_version; ?></title>
    <meta name="keywords" content="<?php echo $program_name.' v'.$program_version; ?>" />
    <meta name="description" content="<?php echo $program_name.' v'.$program_version; ?>">
    <meta name="author" content="<?php echo $program_author; ?>">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
	
	<!-- Icons -->
	<!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
	<link rel="shortcut icon" href="<?php echo ASSETS_URL; ?>img/favicons/favicon.png">

	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="<?php echo ASSETS_URL; ?>img/favicons/favicon-192x192.png" sizes="192x192">

	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo ASSETS_URL; ?>img/favicons/apple-touch-icon-180x180.png">
	<!-- END Icons -->
	
	<!-- Stylesheets -->
	<!-- Web fonts -->
	<link rel="stylesheet" href="<?php echo ASSETS_URL; ?>fonts/fonts.css">
	
	<!-- OneUI CSS framework -->
	<link rel="stylesheet" id="css-main" href="<?php echo ASSETS_URL; ?>css/oneui.css">
	<link rel="stylesheet" id="css-main" href="<?php echo ASSETS_URL; ?>css/themes/city.min.css">
		                				
	<?php
	if(!empty($add_css_page)){
		echo $add_css_page;
	}
	?>
	
    <link href="<?php echo ASSETS_URL; ?>css/custom.css" rel="stylesheet" type="text/css" />
		
	<script>
		var appUrl 		= "<?php echo site_url(); ?>";
		var programName	= "<?php echo config_item('program_name'); ?>";
		var copyright	= "<?php echo config_item('copyright'); ?>";
	</script>
	
</head>

<body class="bg-image login-page" style="background: url('<?php echo ASSETS_URL; ?>img/various/login.jpg') no-repeat center center;">

		<!-- Login Content -->
        <div class="content overflow-hidden" style="opacity:0.9;">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
                    <!-- Login Block -->
                    <div class="block block-themed animated fadeIn">
                        <div class="block-header bg-primary">
                            <!--<ul class="block-options">
                                <li>
                                    <a href="base_pages_reminder.html">Forgot Password?</a>
                                </li>
                                <li>
                                    <a href="base_pages_register.html" data-toggle="tooltip" data-placement="left" title="New Account"><i class="si si-plus"></i></a>
                                </li>
                            </ul>-->
                            <h3 class="block-title">Login</h3>
                        </div>
                        <div class="block-content block-content-full block-content-narrow">
                            <!-- Login Title -->
                            <p class="text-center"><b><?php echo $program_name.'<br/>'.$client_name; ?><b/></p>
                            <!-- END Login Title -->

                            <!-- Login Form -->
                            <!-- jQuery Validation (.js-validation-login class is initialized in js/pages/base_pages_login.js) -->
                            <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                            <form class="js-validation-login form-horizontal push-30-t push-50"  id="login-form" method="post" action="<?php echo BASE_URL.'login'; ?>">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material form-material-primary floating">
                                            <input class="form-control" type="text" id="login-username" name="login-username">
                                            <label for="login-username">Username</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material form-material-primary floating">
                                            <input class="form-control" type="password" id="login-password" name="login-password">
                                            <label for="login-password">Password</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <!--<label class="css-input switch switch-sm switch-primary">
                                            <input type="checkbox" id="login-remember-me" name="login-remember-me"><span></span> Remember Me?
                                        </label>-->
										<div class="message-login-area"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <button class="btn btn-block btn-primary" type="submit"><i class="si si-login pull-right"></i> Log in</button>
                                    </div>
                                </div>
                            </form>
                            <!-- END Login Form -->
                        </div>
                    </div>
                    <!-- END Login Block -->
                </div>
            </div>
        </div>
        <!-- END Login Content -->
		
        <!-- Login Footer -->
        <div class="push-10-t text-center animated fadeInUp">
            <small class="text-muted font-w600 text-white"><span class="js-year-copy"></span> &copy; <?php echo $program_name.' v'.$program_version; ?></small>
        </div>
        <!-- END Login Footer -->
	    
	
	<!-- CORE -->
	<script src="<?php echo APP_URL; ?>core/jquery.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/bootstrap.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.slimscroll.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.scrollLock.min.js"></script>
	<!--<script src="<?php echo APP_URL; ?>core/jquery.appear.min.js"></script>
	<script src="<?php echo APP_URL; ?>core/jquery.countTo.min.js"></script>	
	<script src="<?php echo APP_URL; ?>core/js.cookie.min.js"></script>-->
	<script src="<?php echo APP_URL; ?>core/jquery.placeholder.min.js"></script>
	<script src="<?php echo ASSETS_URL; ?>js/app.js"></script>

	<script src="<?php echo APP_URL; ?>libs/jquery-validation/jquery.validate.min.js"></script>
	
    <!-- Page Javascript -->
    <script type="text/javascript">
	var BasePagesLogin = function() {
		// Init Login Form Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
		var initValidationLogin = function(){
			jQuery('.js-validation-login').validate({
				errorClass: 'help-block text-right animated fadeInDown',
				errorElement: 'div',
				errorPlacement: function(error, e) {
					jQuery(e).parents('.form-group .form-material').append(error);
				},
				highlight: function(e) {
					jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
					jQuery(e).closest('.help-block').remove();
				},
				success: function(e) {
					jQuery(e).closest('.form-group').removeClass('has-error');
					jQuery(e).closest('.help-block').remove();
				},
				rules: {
					'login-username': {
						required: true,
						minlength: 1
					},
					'login-password': {
						required: true,
						minlength: 1
					}
				},
				messages: {
					'login-username': {
						required: 'Please enter a username',
						minlength: 'Your username must consist of at least 1 characters'
					},
					'login-password': {
						required: 'Please provide a password',
						minlength: 'Your password must be at least 1 characters long'
					}
				},
				submitHandler: function(form) {
					
					// Check fields
					var username = $('#login-username').val();
					var password = $('#login-password').val();
					$('.message-login-area').html('');
					
					var submitBt = $('#login-form').find('button[type=submit]');
					submitBt.removeClass('btn-info');
					submitBt.addClass('disabled');
					submitBt.addClass('btn-default');
					
					// Target url
					var target = $('#login-form').attr('action');
					if (!target || target == '')
					{
						// Page url without hash
						target = document.location.href.match(/^([^#]+)/)[1];
					}
										
					// Start timer
					var sendTimer = new Date().getTime();
										
					// Request
					var data = {
						a: 'send',
						loginUsername: username,
						loginPassword: password,
						xtime: sendTimer
					};
					
					var redirect 	= appUrl+'backend';
					
					//message loading
					$('.message-login-area').html('');
					$('.progress.progress-xs').removeClass('hidden');
					$('.progress div.progress-bar').attr('style','width:10%');
					pbar_time = 10;
					is_clrProgressBar = false;
					is_clrProgressBar_color = 'info';
					animate_progressbar();
					
					// Send
					$.ajax({
						url: target,
						dataType: 'json',
						type: 'POST',
						data: data,
						success: function(rdata, textStatus, XMLHttpRequest)
						{
							if (rdata.success)
							{
								
								//load all empty backend file for first load
								$('.message-login-area').html('<div class="alert alert-info alert-dismissable animated fadeIn">'+
								'<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
								'<i class="fa fa-sun-o fa-spin pr10"></i> Redirecting...'+
								'</div>');
								
								pbar_time = 10;
								is_clrProgressBar = false;
								is_clrProgressBar_color = 'success';
								animate_progressbar('success');
								
								$.ajax({
									url: '<?php echo BASE_URL; ?>login/accepted',
									dataType: 'json',
									type: 'POST',
									data: data,
									success: function(rdata, textStatus, XMLHttpRequest)
									{
										is_clrProgressBar = true;
										document.location.href = redirect;
									},
									error: function(XMLHttpRequest, textStatus, errorThrown)
									{
										is_clrProgressBar = true;
										document.location.href = redirect;
									}
						
								});
								
								
							}
							else
							{
								$('.message-login-area').html('<div class="alert alert-danger alert-dismissable animated fadeIn">'+
								'<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
								'<i class="fa fa-warning pr10"></i> '+rdata.info+
								'</div>');
										
								submitBt.removeClass('disabled');
								submitBt.removeClass('btn-default');
								submitBt.addClass('btn-info');
								
							}
							
							is_clrProgressBar = true;
							
						},
						error: function(XMLHttpRequest, textStatus, errorThrown)
						{
							// Message
							$('.message-login-area').html('<div class="alert alert-danger alert-dismissable animated fadeIn">'+
							'<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
							'<i class="fa fa-warning pr10"></i>'+
							' <strong>Error</strong> while contacting server, please try again!'+
							'</div>');
					
							submitBt.removeClass('disabled');
							submitBt.removeClass('btn-dark');
							submitBt.addClass('btn-primary');
							
							is_clrProgressBar = true;
							
						}
					});
				}
			});
		};

		return {
			init: function () {
				// Init Login Form Validation
				initValidationLogin();
			}
		};
	}();
		
	jQuery(document).ready(function() {
		
		BasePagesLogin.init();
		
	});
	
	var pbar_time = 10;
	var pbar_maxtime = 100;
	var myProgressbar;
	var is_clrProgressBar = false;
	var is_clrProgressBar_color = 'info';
	
	function animate_progressbar(){
		myProgressbar = setTimeout(loopProgressBar, 300);
	}
	
	function loopProgressBar(){
		pbar_time += 30;
		
		if(pbar_time > pbar_maxtime){
			pbar_time = 10;
		}
		
		$('.progress div.progress-bar').removeClass('progress-bar-info');
		$('.progress div.progress-bar').removeClass('progress-bar-success');
		
		$('.progress div.progress-bar').addClass('progress-bar-'+is_clrProgressBar_color);			
		if(is_clrProgressBar == true){
			pbar_time = 10;
			$('.progress.progress-xs').removeClass('hidden');
			$('.progress div.progress-bar').attr('style', 'width:100%');
			setTimeout(clrProgressBar, 300);
			return false;
		}
		
		$('.progress.progress-xs').removeClass('hidden');
		$('.progress div.progress-bar').attr('style', 'width:'+pbar_time+'%');
		
		myProgressbar = setTimeout(loopProgressBar, 300);
	}
	
	function clrProgressBar(){
		$('.progress div.progress-bar').attr('style','width:100%');
		$('.progress.progress-xs').addClass('hidden');
		clearTimeout(myProgressbar);
		
	}
		
    </script>

    <!-- END: PAGE SCRIPTS -->

</body>
</html>
