$(function() {
		
	//app-logout
	$('#app-logout').click(function(){
		var d = new Date();
		$.ajax({
			url: wepos_srv+'logout?_dc='+d.getTime(),
			dataType: 'json',
			type: 'POST',
			success: function(data, textStatus, XMLHttpRequest)
			{
				document.location.href = appUrl+'logout';
								
			},
			error: function(XMLHttpRequest, textStatus, errorThrown)
			{	
				document.location.href = appUrl+'logout';
			}

		});
	});
	
	setTimeout(function(){
		do_check_berita_bebas();
	}, 5000);
		
	
});

function do_check_berita_bebas(){
	
	var d = new Date();
	var dtPost = {
		
	};
	
	$.ajax({
		url: appUrl+'kanim/kanim/notifBebas?_dc='+d.getTime(),
		dataType: 'json',
		type: 'POST',
		data: dtPost,
		success: function(data, textStatus, XMLHttpRequest)
		{
			if(data.success){
				$.notify({
					icon: 'fa fa-info-circle',
					message: data.info_notif
				},{
					type: 'info',
					placement: {
						from: 'top',
						align: 'right'
					},
					offset: {
						x: 0,
						y: 60
					},
					delay: 5000,
					timer: 5000
				});
				
				setTimeout(function(){
					do_check_berita_bebas();
				}, 20000);	
				
			}else{
				if(data.stop == 1){
					
				}else{
					
					setTimeout(function(){
						do_check_berita_bebas();
					}, 20000);	
					
				}
			}
			
			

			
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{	
			
		}

	});
}

function hideNotif(getId){
	
	var d = new Date();
	var dtPost = {
		bebas_id: getId
	};
	
	$.ajax({
		url: appUrl+'kanim/kanim/hideNotif?_dc='+d.getTime(),
		dataType: 'json',
		type: 'POST',
		data: dtPost,
		success: function(data, textStatus, XMLHttpRequest)
		{
			$('.close-notif-'+getId).trigger('click');
		},
		error: function(XMLHttpRequest, textStatus, errorThrown)
		{	
			
		}

	});	
		
}

function doNotify(msg){
		
	$.notify({
		icon: 'fa fa-check',
		message: msg,
		url: ''
	},
	{
		element: 'body',
		type: 'success',
		allow_dismiss: true,
		newest_on_top: true,
		showProgressbar: false,
		placement: {
			from: 'center',
			align: 'top'
		},
		offset: 20,
		spacing: 10,
		z_index: 1031,
		delay: 5000,
		timer: 1000,
		animate: {
			enter: 'animated fadeIn',
			exit: 'animated fadeOutDown'
		}
	});
}

//SHOW NOTIF STATUS BEBAS

