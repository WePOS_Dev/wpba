<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * Thermal Printer Helper
 *
 * PHP version 5
 *
 * @category  CodeIgniter
 * @package   Framework System
 * @author    angga nugraha (angga.nugraha@gmail.com)
 * @version   0.1
 * Copyright (c) 2011 Angga Nugraha  (http://whazzup.web.id)
*/

if( ! function_exists('replace_to_printer_command')){
	function replace_to_printer_command($text = ''){
		
		//set tab - calculation -----------------
		//get param set tab
		$use_set_tab = false;
		$set_tab = array(4,17,20);
		$getParamSetTab = explode("[set_tab=",$text);
		
		$newText_setTab = array();
		$newText_setTab[] = $getParamSetTab[0];
		if(!empty($getParamSetTab[1])){
			//exp by "]" then trim
			$use_set_tab = true;
			$getParamSetTab2 = explode("]",$getParamSetTab[1]);
			if(!empty($getParamSetTab2[1])){
				//exp by ","
				$getParamSetTab3 = explode(",", trim($getParamSetTab2[0]));
				if(!empty($getParamSetTab3)){
					$set_tab = $getParamSetTab3;
				}
				
				$getParamSetTab2[0] = implode(",", $set_tab);
			}
			
			$newText_setTab[] = implode("]",$getParamSetTab2);
			
			//renew text
			$text = implode("[set_tab=", $newText_setTab);
		}
				
		if(!empty($use_set_tab)){
			$set_tab_hexa_array = array();
			
			$set_tab_hexa_array[] = "1b";
			$set_tab_hexa_array[] = "44";
			
			foreach($set_tab as $col_tab){
			
				if(strlen($col_tab) == 1){
					$set_tab_hexa_array[] = "0".$col_tab;
				}else{
					$set_tab_hexa_array[] = $col_tab;
				}
				
			}
			
			//count cols
			$totCols_hexa_array = "";
			$totCols = count($set_tab) + 1;
			if(strlen($totCols) == 1){
				$totCols_hexa_array = "0".$totCols;
			}else{
				$totCols_hexa_array = $totCols;
			}					
			
		}
		
		$string_to_hexa = array(
			"]\n"	=> "]", //auto trim
			"[align=0]"	=> "\x1b\x61\x00", //left
			"[align=1]"	=> "\x1b\x61\x01", //center
			"[align=2]"	=> "\x1b\x61\x02", //right
			"[size=0]"	=> "\x1d\x21\x00", //all=0
			"[size=1]"	=> "\x1d\x21\x01", //width=0, height=1
			"[size=2]"	=> "\x1d\x21\x11", //width=1, height=2
			"[size=3]"	=> "\x1d\x21\x11", //width =2, height=3
			"[set_tab1]"	=> "\x1b\x44\x04\x19\x21,x04",
			"[set_tab2]"	=> "\x1b\x44\x0f\x21,x03",
			"[tab]"	=> "\x09",
			"[newline]"	=> "\x0A",
			"[fullcut]"	=> "\x1b\x69",
			"[cut]"	=> "\x1b\x6d"
		);
		
		if(!empty($use_set_tab) AND !empty($set_tab)){
			//--replace set tab
			$setTabHexa = '$setTabHexa = "\x'.implode('\x',$set_tab_hexa_array).',x'.$totCols_hexa_array.'";';
			$set_tab_txt = implode(",", $set_tab);
			//echo $setTabHexa;
			eval($setTabHexa);
			$string_to_hexa["[set_tab=".$set_tab_txt."]"] = $setTabHexa;
			
		}
		
		//echo "<pre>";
		//print_r($string_to_hexa);
		//die();
		
		$newText = strtr($text, $string_to_hexa);
		
		return $newText;
	}
}

if( ! function_exists('printer_command_align_right')){
	function printer_command_align_right($text = '', $length_set = 0){
		
		$text_show = $text;
		if(!empty($length_set)){
			$length_txt = strlen($text);
			$text_show = $text;
			if($length_txt < $length_set){
				$gapTxt = $length_set - $length_txt;
				$text_show = str_repeat(" ", $gapTxt).$text_show;							
			}
		}
		
		return $text_show;
	}
}

?>