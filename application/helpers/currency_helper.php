<?php

if( ! function_exists('priceFormat')){
	function priceFormat($nominal = 0, $decimal = 2, $decimal_point = ',', $thousand_separator = '.', $show_00 = false){
		
		$priceFormat = number_format($nominal, $decimal, $decimal_point, $thousand_separator);
		
		if($show_00 == false){
			$priceFormat = str_replace($decimal_point."00", "", $priceFormat);
		}
		
		return $priceFormat;
	}
	
}