<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * Framework System Helper
 *
 * PHP version 5
 *
 * @category  CodeIgniter
 * @package   Framework System
 * @author    angga nugraha (angga.nugraha@gmail.com)
 * @version   0.1
 * Copyright (c) 2011 Angga Nugraha  (http://whazzup.web.id)
*/

/*ENVIRONTMENT*/
if( ! function_exists('apps_environtment')){
	function apps_environtment(){
	
		$ret_data = array(
			'program_name' 		=> config_item('program_name'),
			'program_version' 	=> config_item('program_version'),
			'program_release' 	=> config_item('program_release'),
			'program_name_short'=> config_item('program_name_short'),
			'copyright' 		=> config_item('copyright'),
			'client_name' 		=> config_item('client_name'),
			'program_author' 	=> config_item('program_author'),
			'base_url' 	=> base_url()
		);
		
		return $ret_data;
	}
}

/*OPTIONS*/
if( ! function_exists('get_option_value')){
	function get_option_value($data = array(), $result = 'array'){
		$prefix = config_item('db_prefix');
		if(empty($scope)){
			$scope =& get_instance();
		}
		
		if(empty($data)){
			return false;
		}
		
		$ret_result = 'array';
		if(!empty($result)){
			if($result == 'object'){
				$ret_result = 'object';
			}
		}
		
		if(is_array($data)){
			$all_var = implode("','", $data);
		}else{
			$all_var = $data;
		}
		
		$scope->db->select("option_var, option_value");
		$scope->db->from($prefix."options");
		$scope->db->where("option_var IN ('".$all_var."')");
		$get_lap_param = $scope->db->get();
		
		$all_val = array();
		if($get_lap_param->num_rows() > 0){
			foreach($get_lap_param->result() as $dt){
				$all_val[$dt->option_var] = $dt->option_value;
			}
						
			if($ret_result == 'object'){
				$all_return = (object) $all_val; 
			}else{
				$all_return = $all_val; 
			}
			
			return $all_return;
		}else{
			return false;
		}
		
	}
	
}

if( ! function_exists('get_option')){

	function get_option($data, $echoed = true){
		
		//DEFAULT
		/*$data = array(
			'var' 		=> '',
			'result'	=> 'array',
			'scope'		=> '',
			'echoed'	=> true
		);*/
		
		/*single, echoed*/
		$prefix = config_item('db_prefix');
		if(empty($scope)){
			$scope =& get_instance();
		}	
				
		$tipe = 'single';
		if(is_array($data)){
			extract($data);
			$tipe = 'data';
		}else{
			//string
			$var = $data;
		}
		
		//single condition
		if(empty($data['echoed']) AND $tipe == 'data'){
			$echoed = true;
		}
		
		$ret_result = 'array';
		if(!empty($result)){
			if($result == 'object'){
				$ret_result = 'object';
			}
		}
		
		$data_res = array();
				
		$scope->db->select('a.*');
		$scope->db->from($prefix.'options as a');		
		$scope->db->where('a.is_deleted', 0);
		$scope->db->where('a.is_active', 1);
		
		if(is_array($var)){
			$var_all = implode("','", $var);
			$scope->db->where("a.option_var IN ('".$var_all."')");
		}else{
			$scope->db->where('a.option_var', $var);	
		}
		
		$query = $scope->db->get();
		if($query->num_rows() > 0){
			
			$newData = array();
			foreach($query->result_array() as $dt){
				$newData = $dt;
			}
			
			if($tipe == 'data'){
				if($ret_result == 'object'){
					$data_res = (object) $newData; 
				}else{
					$data_res = $newData; 
				}
			}else{
				$data_res = (object) $newData; 
			}
			
		}
		
		if(!empty($data_res)){
			if($tipe == 'data'){
				return $data_res;
			}else{
				if($echoed == true){
					echo $data_res->option_value;
				}else{
					return $data_res->option_value;
				}
			}
		}
		
		return '';
	}
	
}

if( ! function_exists('update_option')){

	function update_option($data = array()){
		
		//DEFAULT
		/*$data = array(
			'var' 		=> '' (var | array)
		);*/
		
		/*single, echoed*/
		$prefix = config_item('db_prefix');
		if(empty($scope)){
			$scope =& get_instance();
		}	
		
		if(empty($data)){
			return false;
		}
		
		$get_var = array();
		foreach($data as $key => $dt){
			if(!in_array($key, $get_var)){
				$get_var[] = $key;
			}
		}
				
		$option_update = array();
		$option_update_key = array();
				
		$scope->db->select('a.*');
		$scope->db->from($prefix.'options as a');		
		$scope->db->where('a.is_deleted', 0);
		$scope->db->where('a.is_active', 1);
		
		if(is_array($get_var)){
			$var_all = implode("','", $get_var);
			$scope->db->where("a.option_var IN ('".$var_all."')");
		}else{
			$scope->db->where("a.option_var != '-1' ");	//just for skip
		}
		
		$query = $scope->db->get();
		if($query->num_rows() > 0){
			
			//UPDATE OPTION
			foreach($query->result_array() as $dt){
			
				if(!in_array($dt['option_var'], $option_update_key)){
					$option_update_key[] = $dt['option_var'];
				}
				
				if(!empty($data[$dt['option_var']])){
					$option_update[] = array(
						"option_var" => $dt['option_var'],
						"option_value" => $data[$dt['option_var']]
					);
				}else{
					$option_update[] = array(
						"option_var" => $dt['option_var'],
						"option_value" => ""
					);
				}
				
			}
			
		}
		
		//UPDATE ALL
		if(!empty($option_update)){
			$scope->db->update_batch($prefix.'options', $option_update, 'option_var'); 
		}
		
		$all_insert_key = array();
		$option_insert = array();
		//INSERT ALL
		foreach($get_var as $opt_var){
			if(!in_array($opt_var, $option_update_key)){
				
				if(!in_array($opt_var, $all_insert_key)){
					$all_insert_key[] = $opt_var;
					
					if(!empty($data[$opt_var])){
						$option_insert[] = array(
							"option_var" => $opt_var,
							"option_value" => $data[$opt_var]
						);
					}
					
				}
				
			}
		}
		
		if(!empty($option_insert)){
			$scope->db->insert_batch($prefix.'options', $option_insert); 
		}		
		
		return true;
	}
	
}


if( ! function_exists('get_login_data')){

	function get_login_data($session_var = 'login_data'){
		$scope =& get_instance();
		$login_data = $scope->session->userdata($session_var);
		
		if(!empty($login_data)){
			$login_data = json_decode($login_data, true);
		}
		
		return $login_data;
	}
}

if( ! function_exists('set_status_fileupload')){

	function set_status_fileupload($id_upload = 0, $is_used = 0){
		
		if(empty($id_upload)){
			return false;
		}
		
		$scope =& get_instance();
		
		$set_update = array(
			'is_used'	=> $is_used
		);
		$update_status = $scope->db->update('upload',$set_update,"id_upload = ".$id_upload);
		
		return $update_status;
	}
}
?>