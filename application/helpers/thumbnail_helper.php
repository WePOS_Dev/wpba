<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * Thumbnail Helper
 *
 * PHP version 5
 *
 * @category  CodeIgniter
 * @package   Framework System
 * @author    angga nugraha (angga.nugraha@gmail.com)
 * @version   0.1
 * Copyright (c) 2011 Angga Nugraha  (http://whazzup.web.id)
*/

/*thumbnail*/
function do_thumb($config = array())
{
	$return_data = array('success' => false, 'error' => 'Create Thumb Failed!');
	
	if(empty($config)){
		$return_data['error'] = 'Create Thumb Failed, Config Not Set!';
		return $return_data;
	}
	
	extract($config);
	
	$return_data = array('success' => false, 'error' => 'Create Thumb Failed!');
	
	if(empty($data)){
		$return_data['error'] = 'Create Thumb Failed, Data Not Found!';
		return $return_data;
	}
	
	if(empty($file_path)){
		$return_data['error'] = 'Create Thumb Failed, File Path Destination Not Set!';
		return $return_data;
	}
	
	if(empty($thumb_file_path)){
		$return_data['error'] = 'Create Thumb Failed, Thumb Folder Destination Not Set!';
		return $return_data;
	}
	
	if(empty($upload_thumb_url)){
		$return_data['error'] = 'Create Thumb Failed, Thumb URL Destination Not Set!';
		return $return_data;
	}
	
	if(empty($prefix_thumb)){
		$prefix_thumb = '';
	}
	
	if(empty($limit_thumb)){
		$limit_thumb = 128;
	}
	
	if(empty($limit_height)){
		$limit_height = 128;
	}
	
	if(empty($maintain_ratio)){
		$maintain_ratio = true;
	}
	
	if(empty($master_dim)){
		$master_dim = 'auto';
	}
	
	$objCI =& get_instance();
	
	/* PATH */
	$source = $file_path.$data["file_name"];
	$destination_thumb = $thumb_file_path;
								
	// Permission Configuration
	chmod($source, 0777) ;
						 
	/* Resizing Processing */
	// Configuration Of Image Manipulation :: Static
	$objCI->load->library('image_lib') ;
	$img['image_library'] = 'GD2';
	$img['create_thumb']  = TRUE;
	$img['maintain_ratio']= $maintain_ratio;
	$img['master_dim']= $master_dim;
				 
	/// Limit Width Resize
	//$limit_thumb    = 64 ;
						 
	// Size Image Limit was using (LIMIT TOP)
	$limit_use  = $data['image_width'] > $data['image_height'] ? $data['image_width'] : $data['image_height'] ;
						 
	// Percentase Resize
	if($data['image_width'] > $data['image_height']){
		if ($limit_use > $limit_thumb) {
			$percent  = $limit_thumb/$limit_use ;
		}else{
			$percent  = 1;
		}
	}else{	
		if ($limit_use > $limit_height) {
			$percent  = $limit_height/$limit_use ;
		}else{
			$percent  = 1;
		}
	}
	
						 
	//// Making THUMBNAIL ///////
	$img['width']  = $limit_use > $limit_thumb ?  $data['image_width'] * $percent : $data['image_width'] ;
	$img['height'] = $limit_use > $limit_height ?  $data['image_height'] * $percent : $data['image_height'] ;
	
	if($maintain_ratio == FALSE){
		
		if(!empty($limit_thumb)){
			$img['width'] = $limit_thumb;
		}
		
		if(!empty($limit_height)){
			$img['height'] = $limit_height;
		}
		
	}
						 
	// Configuration Of Image Manipulation :: Dynamic
	$img['thumb_marker'] = $prefix_thumb;
	$img['quality']      = '100%' ;
	$img['source_image'] = $source ;
	$img['new_image']    = $destination_thumb ;
						 
	// Do Resizing
	$objCI->image_lib->initialize($img);
	$objCI->image_lib->resize();
	$objCI->image_lib->clear() ;	
									
	$img_thumb = $data["raw_name"].$prefix_thumb.$data["file_ext"];
	
	$return_data = array('success' => true, 'thumb_name' => $img_thumb, 'thumb_url' => $upload_thumb_url.$img_thumb);
	
	return $return_data;
}


?>