<?php

class Model_early_warning extends DB_Model {
	
	function __construct()
	{
		parent::__construct();
		
		$this->prefix1	= config_item('db_prefix1'); //seaw_
		$this->table_early_warning 	= $this->prefix1.'data';
		$this->table_sponsor 	= $this->prefix1.'sponsor';
		$this->primary_key = 'id';
		
	}
	
	function list_data($params){
		
		if(!empty($params)){
			extract($params);
		}
		
		//data bidang
		$data_bidang_nama = array();
		$data_bidang_detail = array();
		$this->db->select('a.*, b.nama_sponsor, b.mobile_phone as sponsor_mobile_phone, b.email as sponsor_email');
		$this->db->from($this->table_early_warning." as a");
		$this->db->join($this->table_sponsor." as b","b.id = a.sponsor_id", "LEFT");
		
		if(!empty($tanggal_dari)){
			$tgl_dari = strtotime($tanggal_dari);
		}else{
			$tgl_dari = strtotime(date("Y-m-d"));
		}
		
		if(!empty($tanggal_sampai)){
			$tgl_sampai = strtotime($tanggal_sampai);
		}else{
			$tgl_sampai = $tgl_dari;
		}
		
		$date_from = date("Y-m-d");
		$date_till = date("Y-m-d");
		if(!empty($tanggal_dari)){
			$date_from = date("Y-m-d", $tgl_dari);
		}
		
		if(!empty($tanggal_sampai)){
			$date_till = date("Y-m-d", $tgl_sampai);
		}
		
		
		if(!empty($sponsor_data)){
			$this->db->where("a.sponsor = '".$sponsor_data."'");
			$this->db->where("a.tanggal_berlaku >= '".$date_from."'");
		}else{
			$this->db->where("(a.tanggal_berlaku BETWEEN '".$date_from."' AND '".$date_till."')");
		}
		
		if(!empty($keyword)){
			$this->db->where("a.nama LIKE '%".$keyword."%' OR a.niora LIKE '%".$keyword."%' OR a.no_paspor LIKE '%".$keyword."%' OR a.negara LIKE '%".$keyword."%'");
		}
		
		
		$this->db->order_by("a.nama","ASC");
		$this->db->order_by("a.tanggal_berlaku","ASC");
		
		$get_data = $this->db->get();
		
		$data_early_warning = array();
		
		if($get_data->num_rows() > 0){
			
			//$data_early_warning = $get_data->result();
			
			foreach($get_data->result() as $dt){
				
				if(!empty($dt->tanggal_lahir)){
					$tanggal_lahir_mktime = strtotime($dt->tanggal_lahir);
					$dt->tanggal_lahir = date("d-m-Y", $tanggal_lahir_mktime);
				}else{
					$dt->tanggal_lahir = '';
				}
				
				if(!empty($dt->tanggal_pembuatan)){
					$tanggal_pembuatan_mktime = strtotime($dt->tanggal_pembuatan);
					$dt->tanggal_pembuatan = date("d-m-Y", $tanggal_pembuatan_mktime);
				}else{
					$dt->tanggal_pembuatan = '';
				}
				
				if(!empty($dt->tanggal_berlaku)){
					$tanggal_berlaku_mktime = strtotime($dt->tanggal_berlaku);
					$dt->tanggal_berlaku = date("d-m-Y", $tanggal_berlaku_mktime);
				}else{
					$dt->tanggal_berlaku = '';
				}
				
				if(!empty($tanggal_berlaku_mktime)){
					$dt->status_berlaku = 'habis';
					$tanggal_skrg_mktime = strtotime(date("d-m-Y"));
					
					if($tanggal_berlaku_mktime > $tanggal_skrg_mktime){
						$dt->status_berlaku = 'berlaku';
					}
					
				}else{
					$dt->status_berlaku = 'habis';
				}
				
				$data_early_warning[] = $dt;
				
			}
			
		}
		
		return $data_early_warning;
		
	}
	
	
}
