<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Early_warning extends MY_Controller {
	
	protected $post_data = array();
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('model_early_warning', 'early_warning');
		$this->table_early_warning = $this->early_warning->table_early_warning;
		$this->table_sponsor = $this->early_warning->table_sponsor;
		//$this->primary_key = $this->anggaran->primary_key;
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
		$login_data = get_login_data();
		$this->post_data['login_data'] = $login_data;
		
	}

	public function index()
	{
		
		$this->list_data();
		
		
	}
	
	public function list_data()
	{
		$post_data = $this->post_data;	

		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker3.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/datatables/jquery.dataTables.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.APP_URL.'libs/datatables/jquery.dataTables.min.js"></script>
			<script src="'.THEME_URL.'modules/early_warning/js/list_data.js"></script>
		';
	
		$tanggal_hari_ini = date("d/m/Y");
		
		$params = array(
			'tanggal_log'	=> $tanggal_hari_ini 
		);
		$list_data = $this->early_warning->list_data($params);
		
		//return
		$post_data['list_data'] = $list_data;
		
		$login_data = get_login_data();
		if($login_data['tipe_user'] == 'user'){
			$this->load->view(THEME_VIEW_PATH.'modules/early_warning/view/list_data', $post_data);
		}else{
			$this->load->view(THEME_VIEW_PATH.'modules/early_warning/view/list_data', $post_data);
		}
	}
	
	public function load_list_data()
	{
		$tanggal_dari = $this->input->post_get('tanggal_dari', true);
		$tanggal_sampai = $this->input->post_get('tanggal_sampai', true);
		$keyword = $this->input->post_get('keyword', true);
		$tipe = $this->input->post_get('tipe', true);
		$is_user = $this->input->post_get('is_user', true);
		
		$result_excel = $this->input->post_get('result_excel', true);
		$result_print = $this->input->post_get('result_print', true);
		$show_responsive_mode = true;
		if(!empty($result_excel) OR !empty($result_print)){
			$show_responsive_mode = false;
		}
		
		
		$params = array(
			'tanggal_dari'		=> $tanggal_dari,
			'tanggal_sampai'	=> $tanggal_sampai,
			'keyword'			=> $keyword,
			'tipe'				=> $tipe
		);
		$data_early_warning = $this->early_warning->list_data($params);
		
		//echo '<pre>';
		//print_r($data_early_warning );
		//die();
		
		$early_warning_html = '';
		if(!empty($data_early_warning)){
			foreach($data_early_warning as $dt){	
				
				$early_warning_html_detail = '';
					
				//$tanggal_mktime = strtotime($dt->tanggal);
				//$tanggal = date("d/m/Y", $tanggal_mktime);
				
				$ket_sponsor = array();
				
				if(!empty($dt->nama_sponsor)){
					$ket_sponsor[] = $dt->nama_sponsor;
				}
				
				if(!empty($dt->sponsor_mobile_phone)){
					$ket_sponsor[] = $dt->sponsor_mobile_phone;
				}
				
				if(!empty($dt->sponsor_email)){
					$ket_sponsor[] = $dt->sponsor_email;
				}
				
				$ket_sponsor_all = '';
				if(!empty($ket_sponsor)){
					$ket_sponsor_all = implode(", ", $ket_sponsor);
				}
				
				if($dt->status_berlaku == 'berlaku'){
					$ket_status_berlaku = '<font class="text-success"><b><small>MASIH BERLAKU</small></b></a>';
				}else{
					$ket_status_berlaku = '<font class="text-danger"><b><small>HABIS</small></b></a>';
				}
				
					
				$early_warning_html_detail .= '
				<tr>
					<td class="hidden-xs"><small>'.$dt->niora.'</small></td>
					<td class="hidden-xs"><small>'.$dt->no_paspor.'</small></td>
					<td class="hidden-xs"><small>'.$dt->nama.'</small></td>
					<td class="hidden-xs"><small>'.$dt->jenis_kelamin.'</small></td>
					<td class="hidden-xs"><small>'.$dt->tempat_lahir.'</small></td>
					<td class="hidden-xs"><small>'.$dt->tanggal_lahir.'</small></td>
					<td class="hidden-xs"><small>'.$dt->negara.'</small></td>
					<td class="hidden-xs"><small>'.$dt->dokim.'</small></td>
					<td class="hidden-xs text-center"><small>'.$ket_status_berlaku.'</small></td>';
					
				if($show_responsive_mode){
					$early_warning_html_detail .= '
					<td class="visible-xs">
						<small>
						<br/>Niora: '.$dt->niora.'
						<br/>No.Paspor: '.$dt->no_paspor.'
						<br/>Nama: '.$dt->nama.'
						<br/>L/P: '.$dt->jenis_kelamin.'
						<br/>Tempat Lahir: '.$dt->tempat_lahir.'
						<br/>Tgl Lahir: '.$dt->tempat_lahir.'
						<br/>Negara: '.$dt->negara.'
						<br/>Dokim: '.$dt->dokim.'
						<br/>Status: '.$ket_status_berlaku.'
						</small>
					</td>
					';
				}
				
					
				$early_warning_html_detail .= '
				</tr>
				';
				
				$early_warning_html .= $early_warning_html_detail;
				
			}
		}
		
		$html_display = '
		<table class="table table-bordered table-striped js-dataTable-full" id="table-list-data">
			<thead>
				<tr>
					';
					
				$html_display .= '
					<th class="hidden-xs text-left" width="80"><small>NIORA</small></th>
					<th class="hidden-xs text-left" width="80"><small>NO PASPOR</small></th>
					<th class="hidden-xs text-left"><small>NAMA</small></th>
					<th class="hidden-xs text-center" width="40"><small>L/P</small></th>
					<th class="hidden-xs text-left" width="100"><small>TEMPAT LAHIR</small></th>
					<th class="hidden-xs text-center" width="80"><small>TANGGAL LAHIR</small></th>
					<th class="hidden-xs text-left" width="100"><small>BANGSA</small></th>
					<th class="hidden-xs text-left"><small>DOKIM</small></th>
					<th class="hidden-xs text-center"><small>STATUS</small></th>';
					
				if($show_responsive_mode){
					$html_display .= '
					<th class="visible-xs"><small>DATA EARLY WARNING</small></th>
					';
				}
				
					
				$html_display .= '
				</tr>
			</thead>
			<tbody id="early_warning_data">
				'.$early_warning_html.'
			</tbody>
		</table>
		</br>
		';
					
		
		if(!empty($result_excel) OR !empty($result_print)){
			
			if(!empty($result_excel)){
				$post_data['print_name'] = 'Data Early Warning '.$tanggal_dari.' sd '.$tanggal_sampai;
				$post_data['file_name'] = url_title($post_data['print_name']);
				$post_data['html_display'] = $html_display;
				$this->load->view(THEME_VIEW_PATH.'modules/excel_layout', $post_data);
				
			}else{
				
				$post_data = $this->post_data;	
				$post_data['add_css_page'] = '
				';
				
				$post_data['add_js_page'] = '		
					<script>window.print();</script>
				';	
				
				$post_data['print_name'] = 'Data Early Warning - '.$tanggal_dari.' sd '.$tanggal_sampai;
				$post_data['html_display'] = $html_display;
				
				$this->load->view(THEME_VIEW_PATH.'modules/print_layout', $post_data);
			}
			
			
		}else{
			
			echo $html_display;
			die();
			
		}
		
		
	}
	
	public function import_data()
	{
		$post_data = $this->post_data;	

		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker3.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/datatables/jquery.dataTables.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.APP_URL.'libs/datatables/jquery.dataTables.min.js"></script>
			<script src="'.THEME_URL.'modules/early_warning/js/import_data.js"></script>
		';
	
		$this->load->view(THEME_VIEW_PATH.'modules/early_warning/view/import_data', $post_data);
	}
	
	public function upload_excel()
	{
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Import Excel Gagal!'
		);
		
		$login_data = get_login_data();
				
		$this->file_import_excel = UPLOAD_PATH.'import_excel/';
		
		$r = ''; 
		$is_upload_file = false;	
		
		if(!empty($_FILES['upload_file']['name'])){
			
			$config['upload_path'] = $this->file_import_excel;
			//$config['allowed_types'] = '*';
			$config['allowed_types'] = 'xls';
			$config['max_size']	= '102400000';

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload("upload_file"))
			{
				$data = $this->upload->display_errors();
				$r = array('success' => false, 'info' => $data );
				die(json_encode($r));
			}
			else
			{
				$is_upload_file = true;
				$data_upload_temp = $this->upload->data();
				
				if($data_upload_temp['file_ext'] != '.xls'){
					$file =  $this->file_import_excel.$data_upload_temp['file_name']."" ;
					unlink($file);
					$r = array('success' => false, 'info' => 'only file excel (.xls) is allowed');
					die(json_encode($r));
				}
				
				// Load the spreadsheet reader library
				$this->load->library('spreadsheet_excel_reader');
				$xls = new Spreadsheet_Excel_Reader();
				$xls->setOutputEncoding('CP1251'); 
				$file =  $this->file_import_excel.$data_upload_temp['file_name']."" ;
				$xls->read($file);
				
				if(!empty($file)){
					unlink($file);
				}
				
				//echo '<pre>';
				//print_r($xls->sheets);die();
				
				error_reporting(E_ALL ^ E_NOTICE);
				
				$nr_sheets = count($xls->sheets);    
				
				$total_cek_data = 0;
				$total_valid_data = 0;
				
				$detail_early_warning = array();
				$detail_early_warning_available = array();
				$detail_early_warning_not_available = array();
				$all_paspor = array();
				$all_dokim = array();
				$all_NO_PASPOR_TERBIT_BERLAKU = array();
				$all_paspor_valid = array();
				
				$all_sponsor = array();
				$all_sponsor_valid = array();
				
				for($i=0; $i<$nr_sheets; $i++) {
					//echo $xls->boundsheets[$i]['name'];
					//print_r($xls->sheets[$i]);
					
					$total_sheet++;
					$NO_ID = '';
					$NIORA_ID = '';
					$NAMA_ID = '';
					$JK_ID = '';
					$TEMPAT_TGL_LAHIR_ID = '';
					$BANGSA_ID = '';
					$NO_PASPOR_ID = '';
					$TGL_TERBIT_PASPOR_ID = '';
					$TGL_HABIS_PASPOR_ID = '';
					$DOKIM_ID = '';
					$ALAMAT_ID = '';
					$SPONSOR_ID = '';
					
					for ($row_num = 1; $row_num <= $xls->sheets[$i]['numRows']; $row_num++) {	
						
						
						
						if(!empty($xls->sheets[$i]['cells'][$row_num]) AND $has_find_header == false){
							foreach($xls->sheets[$i]['cells'][$row_num] as $key => $val){
							
								//echo '<pre>';
								//print_r($xls->sheets[$i]['cells'][$row_num]);
								//die();
								
								switch($val){
									case 'NO.': $NO_ID = $key; $has_find_header = true;
										break;
									case 'NIORA': $NIORA_ID = $key; $has_find_header = true;
										break;
									case 'NAMA': $NAMA_ID = $key; $has_find_header = true;
										break;
									case 'L/P': $JK_ID = $key; $has_find_header = true;
										break;
									case 'TEMPAT / TGL LAHIR': $TEMPAT_TGL_LAHIR_ID = $key; $has_find_header = true;
										break;
									case 'BANGSA': $BANGSA_ID = $key; $has_find_header = true;
										break;
									case 'NO PASPOR': $NO_PASPOR_ID = $key; $has_find_header = true;
										break;
									case 'TGL TERBIT PASPOR': $TGL_TERBIT_PASPOR_ID = $key; $has_find_header = true;
										break;
									case 'TGL HABIS BERLAKU PASPOR': $TGL_HABIS_PASPOR_ID = $key; $has_find_header = true;
										break;
									case 'DOKIM': $DOKIM_ID = $key; $has_find_header = true;
										break;
									case 'ALAMAT / KOTA': $ALAMAT_ID = $key; $has_find_header = true;
										break;
									case 'SPONSOR': $SPONSOR_ID = $key; $has_find_header = true;
										break;
										
								}
								
							}
							
						}else
						if($has_find_header == true){
							
							
							
							//echo '<pre>';
							//print_r($xls->sheets[$i]['cells'][$row_num]);
							//die();
							
							$NO = trim($xls->sheets[$i]['cells'][$row_num][$NO_ID]);
							$NIORA = trim($xls->sheets[$i]['cells'][$row_num][$NIORA_ID]);
							$NAMA = trim($xls->sheets[$i]['cells'][$row_num][$NAMA_ID]);
							$JK = trim($xls->sheets[$i]['cells'][$row_num][$JK_ID]);
							$TEMPAT_TGL_LAHIR = trim($xls->sheets[$i]['cells'][$row_num][$TEMPAT_TGL_LAHIR_ID]);
							$BANGSA = trim($xls->sheets[$i]['cells'][$row_num][$BANGSA_ID]);
							$NO_PASPOR = trim($xls->sheets[$i]['cells'][$row_num][$NO_PASPOR_ID]);
							$TGL_TERBIT_PASPOR = trim($xls->sheets[$i]['cells'][$row_num][$TGL_TERBIT_PASPOR_ID]); //example: 27-FEB-2015
							$TGL_HABIS_PASPOR = trim($xls->sheets[$i]['cells'][$row_num][$TGL_HABIS_PASPOR_ID]); //example: 27-FEB-2015
							$DOKIM = trim($xls->sheets[$i]['cells'][$row_num][$DOKIM_ID]);
							$ALAMAT = trim($xls->sheets[$i]['cells'][$row_num][$ALAMAT_ID]);
							$SPONSOR = trim($xls->sheets[$i]['cells'][$row_num][$SPONSOR_ID]);
							
							
							
							//cek sponsor
							$sponsor_id = '';
										
							if(!empty($NO_PASPOR) AND !empty($DOKIM)){
								
								
								$proses_data = true;
								
								if($proses_data){
									
									$total_cek_data++;
									
									$mk_TGL_TERBIT_PASPOR = strtotime($TGL_TERBIT_PASPOR);
									$TGL_TERBIT_PASPOR = date("Y-m-d", $mk_TGL_TERBIT_PASPOR);
									
									$mk_TGL_HABIS_PASPOR = strtotime($TGL_HABIS_PASPOR);
									$TGL_HABIS_PASPOR = date("Y-m-d", $mk_TGL_HABIS_PASPOR);
									
									//TTL
									$tempat_lahir = '';
									$tanggal_lahir = '';
									$exp_ttl = explode("/", $TEMPAT_TGL_LAHIR);
									if(count($exp_ttl) == 2){
										$tempat_lahir = trim($exp_ttl[0]);
										$get_tanggal_lahir = trim($exp_ttl[1]);
										$tanggal_lahir_mktime = strtotime($get_tanggal_lahir);
										$tanggal_lahir = date("Y-m-d", $tanggal_lahir_mktime);
									}else{
										$tempat_lahir = trim($exp_ttl[0]);
									}
									
									
									//spit dokim
									$DOKIM_no = '';
									$DOKIM_pembuatan = '';
									$DOKIM_berlaku = '';
									$exp_dokim = explode("s/d", $DOKIM);
									if(!empty($exp_dokim[1])){
										//no + tgl terbit
										$expl_dokim2 = explode(" ", trim($exp_dokim[0]));
										
										if(!empty($expl_dokim2[1])){
											$DOKIM_pembuatan = trim($expl_dokim2[1]);
										}
										
										$DOKIM_no = $expl_dokim2[0];
										$DOKIM_berlaku = trim($exp_dokim[1]);
										
									}
									
									if(!empty($DOKIM_pembuatan)){
										$mk_DOKIM_pembuatan = strtotime($DOKIM_pembuatan);
										$DOKIM_pembuatan = date("Y-m-d", $mk_DOKIM_pembuatan);
									}
									
									if(!empty($DOKIM_berlaku)){
										$mk_DOKIM_berlaku = strtotime($DOKIM_berlaku);
										$DOKIM_berlaku = date("Y-m-d", $mk_DOKIM_berlaku);
									}
									
									$NO_PASPOR_TERBIT_BERLAKU = $NO_PASPOR.'_'.$TGL_TERBIT_PASPOR.'_'.$TGL_HABIS_PASPOR.'_'.$DOKIM;
									
									if(!in_array($NO_PASPOR_TERBIT_BERLAKU, $all_NO_PASPOR_TERBIT_BERLAKU)){
										
										$total_valid_data++;
										
										$detail_early_warning[$total_valid_data] = array(
											'niora'			=> $NIORA,
											'nama'			=> $NAMA,
											'jenis_kelamin'	=> $JK,
											'tempat_lahir'	=> $tempat_lahir,
											'tanggal_lahir'	=> $tanggal_lahir,
											'negara'		=> $BANGSA,
											'no_paspor'		=> $NO_PASPOR,
											'tanggal_terbit_paspor'	=> $TGL_TERBIT_PASPOR,
											'tanggal_habis_paspor'	=> $TGL_HABIS_PASPOR,
											'dokim'				=> $DOKIM,
											'dokim_no'			=> $DOKIM_no,
											'tanggal_pembuatan'	=> $DOKIM_pembuatan,
											'tanggal_berlaku'	=> $DOKIM_berlaku,
											'alamat'			=> $ALAMAT,
											'sponsor_id'		=> $sponsor_id,
											'sponsor'			=> $SPONSOR,
											'created'	=> date("Y-m-d H:i:s"),
											'createdby'	=> $login_data['user_username'],
											'updated'	=> date("Y-m-d H:i:s"),
											'updatedby'	=> $login_data['user_username']
										);
										
										//$all_paspor[] = $NO_PASPOR;
										
										if(!in_array($NO_PASPOR, $all_paspor) AND !empty($NO_PASPOR)){
											$all_paspor[] = $NO_PASPOR;
										}
										if(!in_array($DOKIM_no, $all_dokim) AND !empty($DOKIM_no)){
											$all_dokim[] = $DOKIM_no;
										}
										
										$all_NO_PASPOR_TERBIT_BERLAKU[] = $NO_PASPOR_TERBIT_BERLAKU;
										$all_paspor_valid[$total_valid_data] = $NO_PASPOR_TERBIT_BERLAKU;
										
										$all_sponsor[] = $SPONSOR;
										$all_sponsor_valid[$total_valid_data] = $SPONSOR;
										
									}else{
										$detail_early_warning_available[] = $NO_PASPOR_TERBIT_BERLAKU;
									}
									
								}
								
							}
							
							
							//echo '<pre>';
							//print_r($xls->sheets[$i]['cells'][$row_num]);
							
						}
						
						
					}
					
				}    
				
				
				//echo 'TOTAL DATA = '.$total_valid_data.'<pre>';
				//print_r($detail_early_warning);
				//die();
				
				//SPONSOR---
				/*
				if(!empty($all_sponsor)){
					$this->db->select('*');
					$this->db->from($this->table_sponsor);
					$dt_sponsor = $this->db->get();
					
					foreach($all_sponsor as $dt){
						
					}
					
				}
				*/
				
				//CEK DATA ON table_early_warning DATA
				//where = NAMA, paspor_no
				$this->db->select('*');
				$this->db->from($this->table_early_warning);
				
				if(!empty($all_paspor)){
					$all_paspor_txt = implode("','", $all_paspor);
					$this->db->where("no_paspor IN ('".$all_paspor_txt."')");
				}
				if(!empty($all_dokim)){
					$all_dokim_txt = implode("','", $all_dokim);
					$this->db->or_where("dokim_no IN ('".$all_dokim_txt."')");
				}
				
				$get_data = $this->db->get();
				
				$all_update = array();
				$all_update_id = array();
				$all_update_var = array();
				if($get_data->num_rows() > 0){
					foreach($get_data->result() as $dt){
						
						$NO_PASPOR_TERBIT_BERLAKU = $dt->no_paspor.'_'.$dt->tanggal_terbit_paspor.'_'.$dt->tanggal_habis_paspor.'_'.$dt->dokim;
						
						//cek array value
						$get_key = array_keys($all_paspor_valid, $NO_PASPOR_TERBIT_BERLAKU);
						
						if(!empty($get_key)){
							foreach($get_key as $nomor){
								if(!empty($detail_early_warning[$nomor])){
									
									if(!in_array($dt->id, $all_update_id)){
										
										$all_update_id[] = $dt->id;
										
										if(!in_array($NO_PASPOR_TERBIT_BERLAKU, $all_update_var)){
											$all_update_var[] = $NO_PASPOR_TERBIT_BERLAKU;
										}
										
										$dt_detail_early_warning = $detail_early_warning[$nomor];
										$dt_detail_early_warning['id'] = $dt->id;
										
										unset($dt_detail_early_warning['created']);
										unset($dt_detail_early_warning['createdby']);
										
										$all_update[] = $dt_detail_early_warning;
										
									}
									
								}
							}
						}
						
						
						
					}
				}
				
				/*echo 'TOTAL DATA GET = '.$get_data->num_rows().'<pre>';
				echo 'TOTAL DATA = '.count($all_update).'<pre>';
				print_r($all_update);
				die();*/
				
				$all_insert = array();
				if(!empty($detail_early_warning)){
					foreach($detail_early_warning as $dt_det){
						
						$dt_det = (object) $dt_det;
						$get_var = $dt_det->no_paspor.'_'.$dt_det->tanggal_terbit_paspor.'_'.$dt_det->tanggal_habis_paspor.'_'.$dt_det->dokim;
						//echo 'cek '.$get_var.'<br/>';
						if(!in_array($get_var, $all_update_var)){
							//input new
							$all_insert[] = $dt_det;
						}else{
							//update
						}
					}
				}
				
				//echo 'TOTAL DATA = '.count($all_insert).'<pre>';
				//print_r($all_insert);
				//die();
				
				$q = true;
				$this->lib_trans->begin();
					
					
					if(!empty($all_update)){
						$q2 = $this->db->update_batch($this->table_early_warning, $all_update, "id");
					}
					
					if(!empty($all_insert)){
						$q1 = $this->db->insert_batch($this->table_early_warning, $all_insert);
						if($q1){
							//$q = true;
						}else{
							$q = false;
						}
					}
					
				$this->lib_trans->commit();	
				
				if($q)
				{ 
					$r = array(
						'success' => true, 
						'total_cek_data' => $total_cek_data, 
						'total_valid_data' => $total_valid_data, 
						'available' => count($detail_early_warning_available), 
						'all_insert' => count($all_insert), 
						'all_update' => count($all_update), 
						'dt_available' => $detail_early_warning_available, 
						'not_available' => count($detail_early_warning_not_available), 
						'dt_not_available' => $detail_early_warning_not_available
					); 				
				}  
				else
				{  				
					$r = array('success' => false);
				}
				
				
			}
		}
		
		
		
		die(json_encode(($r==null or $r=='')? array('success'=>false) : $r));	
 
	}
	
}
