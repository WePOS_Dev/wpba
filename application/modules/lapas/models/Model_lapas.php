<?php

class Model_lapas extends DB_Model {
	
	function __construct()
	{
		parent::__construct();
		
		$this->prefix	= config_item('db_prefix');
		$this->db_prefix2	= config_item('db_prefix2'); 
		$this->table_bebas 	= $this->db_prefix2.'bebas';
		$this->primary_key = 'id';
		
	}
	
	function data_bebas($params){
		
		$login_data = get_login_data();
		
		if(!empty($params)){
			extract($params);
		}else{
			
			$dt_return = array();
			
			return $dt_return;
		}
		
		$this->db->select('a.*');
		$this->db->from($this->table_bebas.' as a');
		
		if(!empty($keyword)){
			$this->db->where("(a.tipe_instansi LIKE '%".$keyword."%' OR a.nama_wna LIKE '%".$keyword."%' OR a.no_berita_bebas LIKE '%".$keyword."%' OR a.no_paspor LIKE '%".$keyword."%' OR a.no_ic LIKE '%".$keyword."%' OR a.kebangsaan LIKE '%".$keyword."%')");
		}
		
		if(!empty($tipe_instansi)){
			$this->db->where("a.tipe_instansi = '".$tipe_instansi."'");
		}
		
		if(!in_array($login_data['role_id'], array(1,2))){
			//$this->db->where("a.createdby = '".$login_data['user_username']."'");
		}
		
		$this->db->where("a.is_deleted = 0");
		
		
		if(!empty($tipe_tanggal_pencarian) AND !empty($tanggal_dari) AND !empty($tanggal_sampai)){
			$this->db->where("(a.".$tipe_tanggal_pencarian." >= '".$tanggal_dari."' AND a.".$tipe_tanggal_pencarian." <= '".$tanggal_sampai."')");
			$this->db->order_by("a.".$tipe_tanggal_pencarian,"DESC");
		}
		
		if(!empty($tahun_pencarian)){
			if(empty($tipe_tanggal_pencarian)){
				$tipe_tanggal_pencarian = 'tanggal_berita';
			}
			
			$this->db->where("(a.".$tipe_tanggal_pencarian." >= '01-01-".$tahun_pencarian."' AND a.".$tipe_tanggal_pencarian." <= '31-12-".$tahun_pencarian."')");
			$this->db->order_by("a.kebangsaan","ASC");
			$this->db->order_by("a.".$tipe_tanggal_pencarian,"DESC");
		}
		
		$this->db->order_by("a.id","DESC");
		
		$get_bebas = $this->db->get();
		
		$data_bebas = array();
		if($get_bebas->num_rows() > 0){
			foreach($get_bebas->result() as $dt_bebas){
				
				$data_bebas[] = $dt_bebas;
			}
		}
		
		$dt_return = $data_bebas;
		
		return $dt_return;
	}
	
	function addUpdate(){
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Tambah Data Gagal!'
		);
		
		$login_data = get_login_data();
		
		$no_berita_bebas = $this->input->post('no_berita_bebas', true);
		$get_tanggal_berita = $this->input->post('tanggal_berita', true);
		$nama_wna = $this->input->post('nama_wna', true);
		$tempat_lahir = $this->input->post('tempat_lahir', true);
		$get_tanggal_lahir = $this->input->post('tanggal_lahir', true);
		$kebangsaan = $this->input->post('kebangsaan', true);
		$no_paspor = $this->input->post('no_paspor', true);
		$jenis_kelamin = $this->input->post('jenis_kelamin', true);
		$alamat = $this->input->post('alamat', true);
		$no_ic = $this->input->post('no_ic', true);
		$get_tanggal_bebas = $this->input->post('tanggal_bebas', true);
		$no_surat_putusan = $this->input->post('no_surat_putusan', true);
		$file_surat_putusan = $this->input->post('file_surat_putusan', true);
		$file_surat_putusan_old = $this->input->post('file_surat_putusan_old', true);
		$catatan = $this->input->post('catatan', true);
		$is_edit = $this->input->post('is_edit', true);
		$pemberitahuan_id = $this->input->post('pemberitahuan_id', true);
		$tipe_instansi = $this->input->post('tipe_instansi', true);
		
		$tanggal_berita = date("Y-m-d",strtotime($get_tanggal_berita));
		$tanggal_lahir = date("Y-m-d",strtotime($get_tanggal_lahir));
		$tanggal_bebas = date("Y-m-d",strtotime($get_tanggal_bebas));
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Simpan Data Pemberitahuan Gagal!'
		);
		
		if(empty($is_edit)){
			$save_data = array(	
				'no_berita_bebas'=> $no_berita_bebas,
				'tanggal_berita'=> $tanggal_berita,
				'nama_wna'		=> $nama_wna,
				'tempat_lahir'	=> $tempat_lahir,
				'tanggal_lahir'	=> $tanggal_lahir,
				'kebangsaan'	=> $kebangsaan,
				'no_paspor'		=> $no_paspor,
				'jenis_kelamin'	=> $jenis_kelamin,
				'alamat'		=> $alamat,
				'no_ic'			=> $no_ic,
				'tanggal_bebas'	=> $tanggal_bebas,
				'catatan'	=> $catatan,
				'no_surat_putusan'	=> $no_surat_putusan,
				'file_surat_putusan'	=> $file_surat_putusan,
				'tipe_instansi'	=> $tipe_instansi,
				'createdby'	=> $login_data['user_username'],
				'created'	=> date("Y-m-d H:i:s"),
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			$save = $this->db->insert($this->table_bebas, $save_data);
			if($save){			
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Data Pemberitahuan Tersimpan!'
				);
				
				$info_apps = config_item('program_name');
				$email = config_item('email_penerima');
				//SEND EMAIL / SMS
				$to_Email   	= $email; //Replace with recipient email address
				$subject        = 'Info Deportasi: '.$tipe_instansi; //Subject line for emails
				$pesan   		= 'Berikut Pemberitahuan Akan Bebas<br/>
									<br/>
									Info Dari: '.$tipe_instansi.'<br/>
									No. Register: '.$no_berita_bebas.'<br/>
									Tgl Berita: '.$get_tanggal_berita.'<br/>
									Nama WNA: '.$nama_wna.'<br/>
									Tempat, Tgl Lahir: '.$tempat_lahir.', '.$get_tanggal_lahir.'<br/>
									Kebangsaan: '.$kebangsaan.'<br/>
									No. Paspor: '. $no_paspor.'<br/>
									Jenis Kelamin: '.$jenis_kelamin.'<br/>
									Alamat: '.$alamat.'<br/>
									No.IC: '.$no_ic.'<br/>
									Tanggal Bebas: '.$tanggal_bebas.'<br/>
									Catatan: '.$catatan.'<br/>
									<br/>
									<br/>
									'.$info_apps.'
									<br/>
									<br/>';
				
				$pengirim = config_item('email_pengirim');
				
				$this->load->library('email');
				$config = Array(       
						//'protocol' => 'smtp',
						'smtp_host' => SMTP_HOST,
						'smtp_port' => SMTP_USER,
						'smtp_user' => SMTP_USER,
						'smtp_pass' => SMTP_PASS,
						//'smtp_timeout' => '4',
						'mailtype'  => 'html',
						'charset'   => 'iso-8859-1'
					);
				
				
				$this->email->initialize($config);
				
				$this->email->set_newline("\r\n");

				$this->email->from($pengirim, $info_apps);
				$this->email->to($email);
				
				$email_cc = config_item('email_penerima_cc');
				if(!empty($email_cc)){
					$this->email->cc($email_cc);
				}
				//$this->email->cc('another@another-example.com');
				//$this->email->bcc('them@their-example.com');
				$this->email->subject($subject);
				$this->email->message($pesan);
				$sentMail = $this->email->send();
				
				
				if(!$sentMail)
				{
					//$data_ret['info'] .= '<br/>Kirim Email Gagal!';
				}else{
					//$data_ret['info'] .= '<br/>Data sudah di-informasikan via email';
				}
				
			}
		}else{
			$save_data = array(	
				'no_berita_bebas'=> $no_berita_bebas,
				'tanggal_berita'=> $tanggal_berita,
				'nama_wna'		=> $nama_wna,
				'tempat_lahir'	=> $tempat_lahir,
				'tanggal_lahir'	=> $tanggal_lahir,
				'kebangsaan'	=> $kebangsaan,
				'no_paspor'		=> $no_paspor,
				'jenis_kelamin'	=> $jenis_kelamin,
				'alamat'		=> $alamat,
				'no_ic'			=> $no_ic,
				'tanggal_bebas'	=> $tanggal_bebas,
				'catatan'	=> $catatan,
				'no_surat_putusan'	=> $no_surat_putusan,
				'file_surat_putusan'	=> $file_surat_putusan,
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			
			if(!empty($pemberitahuan_id)){
				
				//if(in_array($login_data['role_id'], array(1,2))){
					$save = $this->db->update($this->table_bebas, $save_data, "id = ".$pemberitahuan_id." AND tipe_instansi = '".$tipe_instansi."'");
				//}else{
				//	$save = $this->db->update($this->table_bebas, $save_data, "id = ".$pemberitahuan_id." AND tipe_instansi = '".$tipe_instansi."' AND createdby = '".$login_data['user_username']."'");
				//}
				
				
				if($save){			

					if(!empty($file_surat_putusan_old)){
						if(empty($file_surat_putusan) OR $file_surat_putusan_old != $file_surat_putusan){
							@unlink(UPLOAD_PATH.'surat_putusan/'.$file_surat_putusan_old);
						}
					}
					
					$data_ret = array(
						'success'	=> true,
						'info'	=> 'Ubah Data Selesai!'
					);
				}
				
			}else{
				$data_ret = array(
					'success'	=> false,
					'info'	=> 'Ubah Data Gagal!'
				);
			}
			
		}
		
		return $data_ret;
	}
	
	function loadData($xid = ''){
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Load Data Gagal!'
		);
		
		$login_data = get_login_data();
		
		$bebas_id = $this->input->post('bebas_id', true);
		
		if(!empty($xid)){
			$bebas_id = $xid;
		}
		
		if(!empty($bebas_id)){
			
			$this->db->select('a.*');
			$this->db->from($this->table_bebas.' as a');
			$this->db->where("a.id = ".$bebas_id);
			
			$load = $this->db->get();
			
			if($load->num_rows() > 0){	
	
				$dt_bebas = $load->row();
				
				$dt_bebas->tgl_berita = date("d-m-Y",strtotime($dt_bebas->tanggal_berita));
				$dt_bebas->tanggal_lahir = date("d-m-Y",strtotime($dt_bebas->tanggal_lahir));
				$dt_bebas->tanggal_bebas = date("d-m-Y",strtotime($dt_bebas->tanggal_bebas));
				
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Load Data Selesai!',
					'data'	=> $dt_bebas
				);
			}
			
		}else{
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Load Data Gagal!',
				'data'	=> array()
			);
		}
		
		return $data_ret;
		
	}
	
	function kirimNotif($xid = ''){
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Load Data Gagal!'
		);
		
		$login_data = get_login_data();
		
		$bebas_id = $this->input->post('bebas_id', true);
		
		if(!empty($xid)){
			$bebas_id = $xid;
		}
		
		if(!empty($bebas_id)){
			
			$this->db->select('a.*');
			$this->db->from($this->table_bebas.' as a');
			$this->db->where("a.id = ".$bebas_id);
			
			$load = $this->db->get();
			
			if($load->num_rows() > 0){	
	
				$dt_bebas = $load->row();
				
				$dt_bebas->tgl_berita = date("d-m-Y",strtotime($dt_bebas->tanggal_berita));
				$dt_bebas->tanggal_lahir = date("d-m-Y",strtotime($dt_bebas->tanggal_lahir));
				$dt_bebas->tanggal_bebas = date("d-m-Y",strtotime($dt_bebas->tanggal_bebas));
				
				$info_apps = config_item('program_name');
				$email = config_item('email_penerima');
				//SEND EMAIL / SMS
				$to_Email   	= $email; //Replace with recipient email address
				$subject        = 'Notifikasi Ulang Deportasi: '.$dt_bebas->tipe_instansi; //Subject line for emails
				$pesan   		= 'Berikut Pemberitahuan Akan Bebas<br/>
									<br/>
									Info Dari: '.$dt_bebas->tipe_instansi.'<br/>
									No. Register: '.$dt_bebas->no_berita_bebas.'<br/>
									Tgl Berita: '.$dt_bebas->tanggal_berita.'<br/>
									Nama WNA: '.$dt_bebas->nama_wna.'<br/>
									Tempat, Tgl Lahir: '.$dt_bebas->tempat_lahir.', '.$dt_bebas->tanggal_lahir.'<br/>
									Kebangsaan: '.$dt_bebas->kebangsaan.'<br/>
									No. Paspor: '. $dt_bebas->no_paspor.'<br/>
									Jenis Kelamin: '.$dt_bebas->jenis_kelamin.'<br/>
									Alamat: '.$dt_bebas->alamat.'<br/>
									No.IC: '.$dt_bebas->no_ic.'<br/>
									Tanggal Bebas: '.$dt_bebas->tanggal_bebas.'<br/>
									Catatan: '.$dt_bebas->catatan.'<br/>
									<br/>
									<br/>
									'.$info_apps.'
									<br/>
									<br/>';
				
				$pengirim = config_item('email_pengirim');
				
				$this->load->library('email');
				$config = Array(       
						//'protocol' => 'smtp',
						'smtp_host' => SMTP_HOST,
						'smtp_port' => SMTP_USER,
						'smtp_user' => SMTP_USER,
						'smtp_pass' => SMTP_PASS,
						//'smtp_timeout' => '4',
						'mailtype'  => 'html',
						'charset'   => 'iso-8859-1'
					);
				
				
				$this->email->initialize($config);
				
				$this->email->set_newline("\r\n");

				$this->email->from($pengirim, $info_apps);
				$this->email->to($email);
				
				$email_cc = config_item('email_penerima_cc');
				if(!empty($email_cc)){
					$this->email->cc($email_cc);
				}
				
				//$this->email->cc('another@another-example.com');
				//$this->email->bcc('them@their-example.com');
				$this->email->subject($subject);
				$this->email->message($pesan);
				$sentMail = $this->email->send();
				
				
				if(!$sentMail)
				{
					//$data_ret['info'] .= '<br/>Kirim Email Gagal!';
				}else{
					//$data_ret['info'] .= '<br/>Data sudah di-informasikan via email';
				}
				
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Notifikasi Terkirim!',
					'data'	=> $dt_bebas
				);
			}
			
		}else{
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Kirim Notif Gagal!',
				'data'	=> array()
			);
		}
		
		return $data_ret;
		
	}
	
	function deleteData(){
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'hapus Data Gagal!'
		);
		
		$login_data = get_login_data();
		
		$bebas_id = $this->input->post('bebas_id', true);
		
		if(!empty($bebas_id)){
			
			$update_lapas = array('is_deleted' => 1);
			
			//if(in_array($login_data['role_id'], array(1,2))){
				$save = $this->db->update($this->table_bebas, $update_lapas, "id = ".$bebas_id);
			//}else{
			//	$save = $this->db->update($this->table_bebas, $update_lapas, "id = ".$bebas_id." AND createdby = '".$login_data['user_username']."'");
			//}
			
			if($save){			
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Data sudah di hapus!'
				);
			}
			
		}else{
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'hapus Data Gagal!'
			);
		}
		
		return $data_ret;
		
	}
	
}
