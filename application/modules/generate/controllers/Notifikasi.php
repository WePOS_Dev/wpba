<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifikasi extends MY_Controller {
	
	protected $post_data = array();
	protected $use_session_check = FALSE;
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('sms/model_sms', 'sms');
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
	}

	public function index()
	{				
		$post_data = $this->post_data;		
		
		$post_data['add_css_page'] = '
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.THEME_URL.'modules/generate/js/notifikasi.js"></script>
		';	
		
		$tanggal_sms = date("Y-m-d");
		$params = array(
			'tanggal_dari'	=> $tanggal_sms,
			'do_update'	=> 1
		);
		$data_ready_notify = $this->sms->data_ready_notify($params);
		
		$params = array(
			'tanggal_dari'	=> $tanggal_sms,
			'limit'	=> 1
		);
		$data_autosend_sms = $this->sms->data_autosend_sms($params);
		
		$post_data['data_ready_notify'] = $data_ready_notify;
		$post_data['data_autosend_sms'] = $data_autosend_sms;
		$post_data['tanggal_sms'] = $tanggal_sms;
		
		/*echo '<pre>';
		print_r($data_log_notify);
		die();*/
		
		$this->load->view(THEME_VIEW_PATH.'modules/generate/view/notifikasi', $post_data);
		
	}
	
}
