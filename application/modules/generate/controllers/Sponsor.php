<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sponsor extends MY_Controller {
	
	protected $post_data = array();
	protected $use_session_check = FALSE;
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('master_data/model_sponsor', 'sponsor');
		$this->load->model('early_warning/model_early_warning', 'early_warning');
		$this->prefix	= $this->early_warning->prefix1;
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
	}

	public function index()
	{				
		$post_data = $this->post_data;		
		
		$post_data['add_css_page'] = '
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.THEME_URL.'modules/generate/js/sponsor.js"></script>
		';	
		
		$params = array(
			'keyword'	=> ''
		);
		$data_sponsor = $this->sponsor->data_sponsor($params);
		
		$post_data['data_sponsor'] = $data_sponsor;
		
		$this->load->view(THEME_VIEW_PATH.'modules/generate/view/sponsor', $post_data);
		
	}

	public function wna()
	{				
		
		$this->db->select("DISTINCT(sponsor)");
		$this->db->from($this->prefix.'data');
		$this->db->where("sponsor_id = 0");
		$get_data = $this->db->get();
		
		$dt_sponsor = array();
		if($get_data->num_rows() > 0){
			foreach($get_data->result() as $dt){
				$dt_sponsor[] = $dt->sponsor;
			}
		}
		
		$params = array(
			'keyword'	=> ''
		);
		$data_sponsor = $this->sponsor->data_sponsor($params);
		
		$db_sponsor = array();
		if(!empty($data_sponsor)){
			foreach($data_sponsor as $dt){
				$db_sponsor[] = $dt->sponsor_data;
			}
		}
		
		$new_sponsor = array();
		$available_sponsor = array();
		if(!empty($dt_sponsor)){
			foreach($dt_sponsor as $dtS){
				if(!in_array($dtS, $db_sponsor)){
					$new_sponsor[] = array(
						'sponsor_data'	=> $dtS
					);
				}else{
					$available_sponsor[] = $dtS;
				}
			}
		}
		
		
		if(!empty($new_sponsor)){
			$insert_all_new_sponsor = $this->db->insert_batch($this->prefix.'sponsor', $new_sponsor);
			
			$all_return = array(
				'success'	=> false,
				'info_html'	=> 'Ada '.count($new_sponsor).' sponsor baru..',
				'total'	=> count($data_sponsor)
			);
			
		}else{
			
			$all_return = array(
				'success'	=> true,
				'info_html'	=> 'Tidak ada data sponsor baru..',
				'total'	=> count($data_sponsor)
			);
			
		}
		
		echo json_encode($all_return);
		die();
	}
	
	public function update()
	{	
		$cekNo = $this->input->post('cekNo', true);	
		$reqdate = $this->input->post('reqdate', true);
		
		$params = array(
			'keyword'	=> ''
		);
		$data_sponsor = $this->sponsor->data_sponsor($params);
		
		$nama_sponsor = '';
		$telp_sponsor = '';
		$id_sponsor = 0;
		$no=0;
		foreach($data_sponsor as $dtS){
			$no++;
			if($no == $cekNo AND empty($nama_sponsor)){
				$nama_sponsor = trim($dtS->sponsor_data);
				$telp_sponsor = $dtS->mobile_phone;
				$id_sponsor = $dtS->id;
				$sponsor_data = $dtS;
			}
		}
		
		$all_return = array(
			'info_html'	=> 'Data Tidak Ditemukan..'
		);
		
		if(!empty($nama_sponsor)){
			
			$params = array(
				'tanggal_dari'		=> date("Y-m-d"),
				'tanggal_sampai'	=> date("Y-m-d"),
				'sponsor_data'		=> $nama_sponsor
			);
			$data_early_warning = $this->early_warning->list_data($params);
			
			//UPDATE DATA
			$update_dt = array();
			foreach($data_early_warning as $drE){
				$update_dt[] = array(
					'id'			=> $drE->id,
					'sponsor_id'	=> $id_sponsor
				);
			}
			
			
			$content_html = '
				<tr>
					<td class="font-w600">'.$cekNo.'</td>
					<td class="font-w600">'.$nama_sponsor.'</td>
					<td class="font-w600">'.$telp_sponsor.'</td>
					<td class="font-w600">'.count($data_early_warning).'</td>
				</tr>
			';
				
			
			if(!empty($update_dt)){
				$do_update = $this->db->update_batch($this->prefix.'data', $update_dt, "id");
				
			}
			
			
			$all_return = array(
				'info_html'	=> 'Jumlah WNA ditemukan = '.count($data_early_warning).' data',
				'content_html'	=> $content_html
			);
			
		}
		
		$is_newdate = 0;
		if(!empty($reqdate)){
			if($reqdate != date("Y-m-d")){
				$is_newdate = 1;
			}
			
		}
		$all_return['is_newdate'] = $is_newdate;
		
		echo json_encode($all_return);
		die();
		
		//sponsor_data -> trim
		
		
	}
	
}
