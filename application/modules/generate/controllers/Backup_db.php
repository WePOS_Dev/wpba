<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backup_db extends MY_Controller {
	
	protected $post_data = array();
	protected $use_session_check = FALSE;
	
	function __construct()
	{
		parent::__construct();
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
	}

	public function index($is_download = false)
	{				
		$post_data = $this->post_data;	

		if(empty($is_download)){
			$is_download = $this->input->post('is_download', true);	
		}
		
		// Load the DB utility class
		$this->load->dbutil();

		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup();

		// Load the file helper and write the file to your server
		$this->load->helper('file');
		$file_name = 'backupdb_'.date("Ymd_Hi").'.gz';
		write_file(BACKUP_DB_PATH.$file_name, $backup);

		if(!empty($is_download)){
			// Load the download helper and send the file to your desktop
			$this->load->helper('download');
			force_download(BACKUP_DB_URL.'mybackup.gz', $backup);
		}else{
			echo 'DB sudah di backup: <a href="'.BACKUP_DB_URL.$file_name.'">'.$file_name.'</a>';
			die();
		}
		
	}
	
	public function download()
	{
		$this->index(true);
	}
	
}
