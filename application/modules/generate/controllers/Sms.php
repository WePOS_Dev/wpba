<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms extends MY_Controller {
	
	protected $post_data = array();
	protected $use_session_check = FALSE;
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('sms/model_sms', 'sms');
		$this->table_sentitems = $this->sms->table_sentitems;
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
	}

	public function index()
	{
		
		$reqdata = $this->input->post('reqdata', true);
		$reqdate = $this->input->post('reqdate', true);
		$dataType = $this->input->post('dataType', true);
		$for_display = $this->input->post('for_display', true);
		
		
		if(empty($reqdata)){
			die();
		}
		
		$tanggal_hari_ini = date("d/m/Y");
		$tanggal_notify = date("Y-m-d");
		
		switch($reqdata) {
			case 'wna_expired':
			
				$params = array(
					'tanggal_hari_ini'	=> $tanggal_hari_ini,
					'tanggal_notify'	=> $tanggal_notify,
					'reqdate'			=> $reqdate,
					'dataType'			=> $dataType,
					'for_display'	=> $for_display
				);
				
				$this->notifikasi_wna_expired($params);
				break;
			case 'autosend_sms':
			
				$params = array(
					'tanggal_hari_ini'	=> $tanggal_hari_ini,
					'tanggal_notify'	=> $tanggal_notify,
					'reqdate'			=> $reqdate,
					'dataType'			=> $dataType,
					'for_display'	=> $for_display
				);
				
				$this->notifikasi_autosend_sms($params);
				break;
			case 'autoupdate_status_sms':
			
				$params = array(
					'tanggal_hari_ini'	=> $tanggal_hari_ini,
					'tanggal_notify'	=> $tanggal_notify,
					'reqdate'			=> $reqdate,
					'dataType'			=> $dataType,
					'for_display'	=> $for_display
				);
				
				$this->notifikasi_autoupdate_status_sms($params);
				break;
			default:
				$all_return = array(
					'info_html'	=> 'Not Ready..'
				);
				
				echo json_encode($all_return);
				die();
		} 
		
	}
	
	public function notifikasi_wna_expired($params = '')
	{
		if(!empty($params)){
			extract($params);
		}
		
		if(empty($reqdate)){
			$all_return = array(
				'info_html'	=> 'Tanggal Pencarian tidak ada..'
			);
			echo json_encode($all_return);
			die();
		}
		
		$title_html = '';
		$info_html = '';
		$content_html = '';
		
		$tanggal_sms = $reqdate;
		$params = array(
			'tanggal_dari'	=> $tanggal_sms,
			'do_update'	=> 1
		);
		$data_ready_notify = $this->sms->data_ready_notify($params);
	
		$no_data = 0;
		if(!empty($data_ready_notify)){
			foreach($data_ready_notify as $dt){
				$no_data++;
				
				$tanggal_berlaku = date("d-m-Y",strtotime($dt->tanggal_berlaku));
				
				$content_html = '
					<tr>
						<td class="font-w600">'.$no_data.'</td>
						<td class="font-w600">'.$dt->nama.'</td>
						<td class="font-w600">'.$dt->dokim_no.'</td>
						<td class="text-center">'.$tanggal_berlaku.'</td>
					</tr>
				'.$content_html;
			}
		}
		
		$title_html = 'WNA EXPIRED: '.count($data_ready_notify).' DATA';
		$info_html = 'Auto update data dalam 15 detik';
		
		
		$is_newdate = 0;
		if(!empty($reqdate)){
			if($reqdate != date("Y-m-d")){
				$is_newdate = 1;
			}
			
		}
		
		$all_return = array(
			'is_newdate'	=> $is_newdate,
			'title_html'	=> $title_html,
			'info_html'	=> $info_html,
			'content_html'	=> $content_html,
			'full_html'	=> 1,
			'total'	=> count($data_ready_notify)
		);
		
		echo json_encode($all_return);
		
		
		//echo '<pre>';
		//print_r($sms_notify_pegawai);
		
		//echo 'Running Notifikasi Absen Masuk Selesai <br><br>';
		
	}
	
	public function notifikasi_autosend_sms($params = '')
	{
		if(!empty($params)){
			extract($params);
		}
		
		if(empty($reqdate)){
			$all_return = array(
				'info_html'	=> 'Tanggal Pencarian tidak ada..'
			);
			echo json_encode($all_return);
			die();
		}
		
		$title_html = '';
		$info_html = '';
		$content_html = '';
		
		$tanggal_sms = $reqdate;
		$params = array(
			'tanggal_dari'	=> $tanggal_sms,
			'limit'	=> 1
		);
		$data_autosend_sms = $this->sms->data_autosend_sms($params);
	
		$tgl_kirim = date("Y-m-d H:i:s");
		$jam_kirim_show = date("H:i");
		$no_data = 0;
		$all_data_sms = array();
		if(!empty($data_autosend_sms)){
			foreach($data_autosend_sms as $dt){
				$no_data++;
				
				$tanggal_berlaku = date("d-m-Y",strtotime($dt->tanggal_berlaku));
				if(empty($dt->mobile_phone)){
					$dt->mobile_phone = '-';
				}
				
				$content_html = '
					<tr>
						<td class="font-w600">'.$dt->mobile_phone.'</td>
						<td class="font-w600">'.$dt->nama.'</td>
						<td class="font-w600">'.$dt->dokim_no.'</td>
						<td class="text-center">'.$tanggal_berlaku.'</td>
						<td class="text-center">'.$jam_kirim_show.'</td>
					</tr>
				'.$content_html;
				
				$all_data_sms = (array) $dt;
			}
		}
		
		$title_html = 'WNA AUTOSEND SMS';
		$info_html = 'Auto update data dalam 20 detik';
		
		//SEND SMS
		$get_setup = $this->sms->get_setup();
		if(!empty($all_data_sms['mobile_phone']) AND !empty($get_setup)){
			
			$tanggal_berlaku = date("d-m-Y",strtotime($all_data_sms['tanggal_berlaku']));
			
			
			$data_id = $all_data_sms['id'];
			$nama = $all_data_sms['nama'];
			$kitas = $all_data_sms['dokim_no'];
			$expired = $tanggal_berlaku;
			$no_tujuan = $all_data_sms['mobile_phone'];
			$isi_sms = $get_setup['isi_sms'];
			
			$trans = array("{nama}" => $nama, "{kitas}" => $kitas, "{expired}" => $expired);
			$isi_sms_send = strtr($isi_sms, $trans);
			
			//SMS DB - SEAW
			$data_sms = array(
				'isi_sms'	=> $isi_sms_send,
				'no_tujuan'	=> $no_tujuan,
				'data_id'	=> $data_id,
				'kitas'	=> $kitas,
				'nama'	=> $nama,
				'expired'	=> $expired,
				'sms_status'	=> 'proses',
				'tanggal_terkirim'	=> $tgl_kirim
			);
			
			$add_sms = $this->db->insert($this->sms->table_sms, $data_sms);
			$get_id_SMS = $this->db->insert_id();
			
			//SMS DB - GAMMU
			if(SMSSEND_READY == 1){
				
				$sms_notify_data = array();
				
				//BETA--------------------
				// Pak OKi 087717263666
				if(SMSSEND_NOTESTER == 1){
					$no_tujuan = $get_setup['no_mobile_test'];
				}
				
				$trans = array("-" => "");
				$no_tujuan = strtr($no_tujuan, $trans);
				
				$data_sms_gammu = array(
					'TextDecoded'	=> $isi_sms_send,
					'DestinationNumber'	=> $no_tujuan,
					'CreatorID'	=> $get_id_SMS
				);
				$sms_notify_data[] = $data_sms_gammu;
				
				$params = array(
					'data'	=> $sms_notify_data
				);
				
				$this->sms->add_sms_notify($params);
			}
			
			//update 
			if(!empty($data_id)){
				$data_update = array('is_notify'	=> 1);
				$update_dt = $this->db->update($this->sms->table_early_warning, $data_update, "id IN (".$data_id.")");
			}
			
			
		}
		
		
		$is_newdate = 0;
		if(!empty($reqdate)){
			if($reqdate != date("Y-m-d")){
				$is_newdate = 1;
			}
			
		}
		
		$all_return = array(
			'is_newdate'	=> $is_newdate,
			'title_html'	=> $title_html,
			'info_html'	=> $info_html,
			'content_html'	=> $content_html,
			'full_html'	=> 0,
			'total'	=> count($data_autosend_sms)
		);
		
		echo json_encode($all_return);
		
		
		//echo '<pre>';
		//print_r($sms_notify_pegawai);
		
		//echo 'Running Notifikasi Absen Masuk Selesai <br><br>';
		
	}
	
	public function notifikasi_autoupdate_status_sms($params = '')
	{
		if(!empty($params)){
			extract($params);
		}
		
		if(empty($reqdate)){
			$all_return = array(
				'info_html'	=> 'Tanggal Pencarian tidak ada..'
			);
			echo json_encode($all_return);
			die();
		}
		
		$title_html = '';
		$info_html = '';
		$content_html = '';
		
		$tanggal_sms = $reqdate;
		$tanggal_sms_mktime = strtotime($tanggal_sms);
		$tanggal_sms_before = $tanggal_sms_mktime+(7*ONE_DAY_UNIX);
		
		$params = array(
			'tanggal_dari'		=> $tanggal_sms_before,
			'tanggal_sampai'	=> $tanggal_sms,
			'tipe'	=> array('tertunda','proses')
		);
		$data_sms = $this->sms->data_sms($params);
	
		$no_data = 0;
		$all_id = array();
		if(!empty($data_sms)){
			foreach($data_sms as $dt){
				
				if(!in_array($dt->id, $all_id)){
					$all_id[] = $dt->id;
				}
				
			}
		}
		
		if(!empty($all_id)){
			$all_id_txt = implode(",", $all_id);
			$all_sms_sent = array();
			
			$DB2 = $this->load->database('sms', TRUE);
			$DB2->from($this->table_sentitems);
			$DB2->where("CreatorID IN (".$all_id_txt.")");
			$get_sent_item = $DB2->get();
			
			if($get_sent_item->num_rows() > 0){
				foreach($get_sent_item->result() as $dtS){
					
					if($dtS->Status == 'SendingOKNoReport' OR $dtS->Status == 'SendingOK'){
						if(!in_array($dtS->CreatorID, $all_sms_sent)){
							$all_sms_sent[] = $dtS->CreatorID;
						}
					}
					
				}
			}
			
			
		}
		
		$content_html = '';
		if(!empty($data_sms)){
			foreach($data_sms as $dt){
				
				if(in_array($dt->id, $all_sms_sent)){
					
					$tanggal_berlaku = date("d-m-Y",strtotime($dt->tanggal_berlaku));
					$tanggal_terkirim = date("d-m-Y H:i:s",strtotime($dt->tanggal_terkirim));
					
					$content_html = '
					<tr>
						<td class="font-w600">'.$dt->no_tujuan.'</td>
						<td class="font-w600">'.$dt->nama.'</td>
						<td class="font-w600">'.$dt->kitas.'</td>
						<td class="text-center">'.$tanggal_berlaku.'</td>
						<td class="text-center">'.$tanggal_terkirim.'</td>
					</tr>
					'.$content_html ;
					
				}
				
			}
		}
		
		if(!empty($all_sms_sent)){
			
			$all_sms_sent_txt = implode(",", $all_sms_sent);
			$data_update = array(
				'sms_status'	=> 'terkirim'
			);
			
			$update_dt = $this->db->update($this->sms->table_sms, $data_update, "id IN (".$all_sms_sent_txt.")");
		}
		
		$title_html = 'AUTOUPDATE SMS STATUS';
		$info_html = 'Auto update status sms: '.count($all_sms_sent).' sms';
		
		
		$is_newdate = 0;
		if(!empty($reqdate)){
			if($reqdate != date("Y-m-d")){
				$is_newdate = 1;
			}
			
		}
		
		$all_return = array(
			'is_newdate'	=> $is_newdate,
			'title_html'	=> $title_html,
			'info_html'	=> $info_html,
			'content_html'	=> $content_html,
			'full_html'	=> 1,
			'total'	=> 0
		);
		
		echo json_encode($all_return);
		
		
		//echo '<pre>';
		//print_r($sms_notify_pegawai);
		
		//echo 'Running Notifikasi Absen Masuk Selesai <br><br>';
		
	}
	
}
