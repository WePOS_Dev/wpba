<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Autosend extends MX_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->model('absensi/model_absensi', 'absensi');
		$this->load->model('model_sms', 'sms');
		
	}
	
	public function index()
	{		
		
		$tanggal_hari_ini = date("d/m/Y");
		$tanggal_notify = date("Y-m-d");
		$params = array(
			'tanggal_log'	=> $tanggal_hari_ini 
		);
		$data_absensi = $this->absensi->data_absensi($params);
		
		//Ijin pegawai
		$params = array(
			'tanggal_absen'	=> $tanggal_hari_ini 
		);
		$data_ijin_pegawai = $this->absensi->data_ijin_pegawai($params); //all_id, total, detail, tanggal_absen
		
		if(empty($data_absensi['pegawai_masuk'])){
			$data_absensi['pegawai_masuk'] = array();
		}
		
		$params = array(
			'tanggal_absen'	=> $tanggal_hari_ini,
			'data_ijin_pegawai'	=> $data_ijin_pegawai,
			'pegawai_masuk_detail'	=> $data_absensi['pegawai_masuk_detail']
		);
		$data_jadwal_pegawai = $this->absensi->data_jadwal_pegawai($params); 
		//pegawai_total, pegawai_detail, 
		//pegawai_belum_masuk_total, pegawai_belum_masuk_detail, tanggal_absen
		
		$params = array(
			'tanggal_hari_ini'	=> $tanggal_hari_ini,
			'tanggal_notify'	=> $tanggal_notify,
			'data_jadwal_pegawai'	=> $data_jadwal_pegawai
		);
		
		$this->send_notifikasi_absen_masuk($params);
		//$this->send_notifikasi_absen_terlambat($params);
		//$this->send_notifikasi_absen_masuk_diluar_jadwal();
		//$this->send_notifikasi_absen_pulang();
		//$this->send_notifikasi_absen_pulang_diluar_jadwal();
		//$this->send_notifikasi_belum_absen();
		
	}
	
	function send_notifikasi_absen_masuk($params){
		
		echo 'Initialize.. send_notifikasi_absen_masuk<br/>';
		
		extract($params);
		
		//$tanggal_hari_ini = date("d/m/Y");
		//$tanggal_notify = date("Y-m-d");
		$exp_tgl = explode("/",$tanggal_hari_ini);
		
		$gap_notify = 1800; //15 menit sebelum
		$tipe_log_sms = 'notifikasi_absen_masuk';
		$jam_masuk_awal_default = "05:30"; //range generate awal
		$jam_masuk_default = "08:00"; //range generate akhir
		
		//jam masuk awal (default)
		$exp_jam_awal_default = explode(":", $jam_masuk_awal_default);
		$mktime_jam_masuk_awal_default = mktime($exp_jam_awal_default[0], $exp_jam_awal_default[1], 0, $exp_tgl[1], $exp_tgl[0], $exp_tgl[2]);
		
		//jam masuk (default)
		$exp_jam_default = explode(":", $jam_masuk_default);
		$mktime_jam_masuk_default = mktime($exp_jam_default[0], $exp_jam_default[1], 0, $exp_tgl[1], $exp_tgl[0], $exp_tgl[2]);
		
		$jam_masuk_current = date("H:i");
		$exp_jam_current = explode(":", $jam_masuk_current);
		$mktime_jam_masuk_current = mktime($exp_jam_current[0], $exp_jam_current[1], 0, $exp_tgl[1], $exp_tgl[0], $exp_tgl[2]);
		
		//stop jika diatas jam default
		if($mktime_jam_masuk_current < $mktime_jam_masuk_awal_default OR $mktime_jam_masuk_current > $mktime_jam_masuk_default){
			echo 'Generate Notifikasi Absen Masuk '.$tanggal_hari_ini.' dari Jam '.date("H:i", $mktime_jam_masuk_awal_default).' s/d Jam '.date("H:i", $mktime_jam_masuk_default).'<br/>';
			echo 'Running Notifikasi Absen Masuk Selesai <br><br>';
			return true;
		}
		
		$params = array(
			'tipe'	=> $tipe_log_sms,
			'tanggal'	=> $tanggal_notify
		);
		$data_log_notify = $this->sms->data_log_notify($params); //all_id, total, detail
		
		$sms_message = 'Sdr/i {$nama}, Jangan lupa absen pagi ini, Jam Masuk: {$jam_masuk} atau {$menit} lagi - by sistem absensi';
		
		$sms_notify_pegawai = array();
		$log_notify_pegawai = array();
		$max_log = 50;
		$total_log = 0;
		
		if(!empty($data_jadwal_pegawai['pegawai_belum_masuk_detail'])){
			
			foreach($data_jadwal_pegawai['pegawai_belum_masuk_detail'] as $dt){
				
				//set jam masuk shift
				$mktime_notify = 0;
				$mktime_jam_masuk = 0;
				if(!empty($dt->Jam_masuk)){
					$exp_jam = explode(":", $dt->Jam_masuk);
					$mktime_jam_masuk = mktime($exp_jam[0], $exp_jam[1], 0, $exp_tgl[1], $exp_tgl[0], $exp_tgl[2]);
					
					$mktime_notify = $mktime_jam_masuk - $gap_notify;
				}
				
				//stop jika diatas jam default
				if($mktime_notify < $mktime_jam_masuk_current AND !empty($mktime_notify)){
					
					$create_log = true;
					//check jika sudah ada dalam log
					if(!empty($data_log_notify['all_id'])){
						if(!in_array($dt->FID, $data_log_notify['all_id'])){
							$create_log = true;
						}else{
							$create_log = false;
						}
					}
					
					if($create_log AND !empty($dt->Notelp)){
						
						$total_log++;
						if($total_log > $max_log){
							break;
						}
						
						$brp_menit = floor(($mktime_jam_masuk - $mktime_jam_masuk_current) / 60);
						if($brp_menit > 60){
							$brp_menit = floor($brp_menit/60);
							$brp_menit_txt = $brp_menit.' jam';
						}else{
							$brp_menit_txt = $brp_menit.' menit';
						}
						
						$sms_message_txt = $sms_message;
						$sms_message_txt = str_replace('{$nama}',$dt->Nama, $sms_message_txt);
						$sms_message_txt = str_replace('{$jam_masuk}',$dt->Jam_masuk, $sms_message_txt);
						$sms_message_txt = str_replace('{$menit}', $brp_menit_txt, $sms_message_txt);
						
						//create log & autosend SMS
						$dt_notify = array(
							'DestinationNumber'	=> $dt->Notelp,
							'TextDecoded'	=> $sms_message_txt
						);
						
						$log_notify = array(
							'no_telp'	=> $dt->Notelp,
							'message'	=> $sms_message_txt,
							'FID'		=> $dt->FID,
							'tipe'		=> $tipe_log_sms,
							'tanggal'	=> $tanggal_notify
						);
						
						$sms_notify_pegawai[] = $dt_notify;
						$log_notify_pegawai[] = $log_notify;
					}
					
					
				}
				
			}
			
		}
		
		if(!empty($sms_notify_pegawai)){
			$params = array(
				'data'	=> $sms_notify_pegawai
			);
			$this->sms->add_sms_notify($params);
			
			echo count($sms_notify_pegawai).' SMS Notifikasi Absen Masuk sudah siap dikirim..<br/>';
		}
		
		//echo '<pre>';
		//print_r($sms_notify_pegawai);
		
		if(!empty($log_notify_pegawai)){
			$params = array(
				'data'	=> $log_notify_pegawai
			);
			$save_log = $this->sms->add_log_notify($params);
			
			echo count($log_notify_pegawai).' Data Log Notifikasi Absen Masuk Sudah disimpan..<br/>';
		}
		
		echo 'Running Notifikasi Absen Masuk Selesai <br><br>';
		
	}
	
	function send_notifikasi_absen_terlambat($params){
		
		echo 'Initialize.. send_notifikasi_absen_terlambat<br/>';
		extract($params);
		
		//$tanggal_hari_ini = date("d/m/Y");
		//$tanggal_notify = date("Y-m-d");
		$exp_tgl = explode("/",$tanggal_hari_ini);
		
		$gap_notify = 60; //1 menit setelah jam masuk akhir
		$tipe_log_sms = 'notifikasi_absen_terlambat';
		$jam_masuk_akhir_default = "08:00"; //range generate awal
		$jam_masuk_default = "10:00"; //range generate akhir
		
		//jam masuk awal (default)
		$exp_jam_akhir_default = explode(":", $jam_masuk_akhir_default);
		$mktime_jam_masuk_akhir_default = mktime($exp_jam_akhir_default[0], $exp_jam_akhir_default[1], 0, $exp_tgl[1], $exp_tgl[0], $exp_tgl[2]);
		
		//jam masuk awal (default)
		$exp_jam_default = explode(":", $jam_masuk_default);
		$mktime_jam_masuk_default = mktime($exp_jam_default[0], $exp_jam_default[1], 0, $exp_tgl[1], $exp_tgl[0], $exp_tgl[2]);
		
		$jam_masuk_current = date("H:i");
		$exp_jam_current = explode(":", $jam_masuk_current);
		$mktime_jam_masuk_current = mktime($exp_jam_current[0], $exp_jam_current[1], 0, $exp_tgl[1], $exp_tgl[0], $exp_tgl[2]);
		
		//stop jika diatas jam default
		if($mktime_jam_masuk_current < $mktime_jam_masuk_akhir_default OR $mktime_jam_masuk_current > $mktime_jam_masuk_default){
			echo 'Generate Notifikasi Absen Terlambat '.$tanggal_hari_ini.' dari Jam '.date("H:i", $mktime_jam_masuk_akhir_default).' s/d Jam '.date("H:i", $mktime_jam_masuk_default).'<br/>';
			echo 'Running Notifikasi Absen terlambat Selesai <br><br>';
			return true;
		}
		
		$params = array(
			'tipe'	=> $tipe_log_sms,
			'tanggal'	=> $tanggal_notify
		);
		$data_log_notify = $this->sms->data_log_notify($params); //all_id, total, detail
		
		$sms_message = 'Sdr/i {$nama}, anda sudah terlambat absen {$menit}, mohon untuk segera absen, abaikan pesan ini jika anda izin, sakit atau cuti - by sistem absensi';
		
		$sms_notify_pegawai = array();
		$log_notify_pegawai = array();
		
		$max_log = 50;
		$total_log = 0;
		
		if(!empty($data_jadwal_pegawai['pegawai_belum_masuk_detail'])){
			
			foreach($data_jadwal_pegawai['pegawai_belum_masuk_detail'] as $dt){
				
				//set jam masuk shift
				$mktime_notify = 0;
				$mktime_jam_masuk = 0;
				if(!empty($dt->Jam_masuk)){
					$exp_jam = explode(":", $dt->Jam_masuk);
					$mktime_jam_masuk = mktime($exp_jam[0], $exp_jam[1], 0, $exp_tgl[1], $exp_tgl[0], $exp_tgl[2]);
					
					$mktime_notify = $mktime_jam_masuk + $gap_notify;
				}
				
				//stop jika diatas jam default
				if($mktime_notify < $mktime_jam_masuk_current AND !empty($mktime_notify)){
						
					$create_log = true;
					//check jika sudah ada dalam log
					if(!empty($data_log_notify['all_id'])){
						if(!in_array($dt->FID, $data_log_notify['all_id'])){
							$create_log = true;
						}else{
							$create_log = false;
						}
					}
					
					if($create_log AND !empty($dt->Notelp)){
						
						$total_log++;
						if($total_log > $max_log){
							break;
						}
						
						$brp_menit = floor(($mktime_jam_masuk_current - $mktime_jam_masuk) / 60);
						if($brp_menit > 60){
							$brp_menit = floor($brp_menit/60);
							$brp_menit_txt = $brp_menit.' jam';
						}else{
							$brp_menit_txt = $brp_menit.' menit';
						}
						
						$sms_message_txt = $sms_message;
						$sms_message_txt = str_replace('{$nama}',$dt->Nama, $sms_message_txt);
						$sms_message_txt = str_replace('{$menit}',$brp_menit_txt, $sms_message_txt);
						
						//create log & autosend SMS
						$dt_notify = array(
							'DestinationNumber'	=> $dt->Notelp,
							'TextDecoded'	=> $sms_message_txt
						);
						
						$log_notify = array(
							'no_telp'	=> $dt->Notelp,
							'message'	=> $sms_message_txt,
							'FID'		=> $dt->FID,
							'tipe'		=> $tipe_log_sms,
							'tanggal'	=> $tanggal_notify
						);
						
						$sms_notify_pegawai[] = $dt_notify;
						$log_notify_pegawai[] = $log_notify;
					}
					
					
				}
				
			}
			
		}
		
		/*echo '<pre>';
		print_r($sms_notify_pegawai);
		die();*/
		
		if(!empty($sms_notify_pegawai)){
			$params = array(
				'data'	=> $sms_notify_pegawai
			);
			$this->sms->add_sms_notify($params);
			
			echo count($sms_notify_pegawai).' SMS Notifikasi Absen terlambat sudah siap dikirim..<br/>';
		}
		
		if(!empty($log_notify_pegawai)){
			$params = array(
				'data'	=> $log_notify_pegawai
			);
			$save_log = $this->sms->add_log_notify($params);
			
			echo count($log_notify_pegawai).' Data Log Notifikasi Absen terlambat Sudah disimpan..<br/>';
		}
		
		echo 'Running Notifikasi Absen terlambat Selesai <br><br>';
		
	}
	
}
