<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms_tertunda extends MY_Controller {
	
	protected $post_data = array();
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('model_sms', 'sms');
		$this->table_sms = $this->sms->table_sms;
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
		$login_data = get_login_data();
		$this->post_data['login_data'] = $login_data;
		
	}
	
	public function index()
	{
		
		$this->list_data();
		
	}
	
	public function list_data()
	{
		$post_data = $this->post_data;	

		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker3.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/datatables/jquery.dataTables.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.APP_URL.'libs/datatables/jquery.dataTables.min.js"></script>
			<script src="'.THEME_URL.'modules/sms/js/sms_tertunda.js"></script>
		';
	
		$this->load->view(THEME_VIEW_PATH.'modules/sms/view/sms_tertunda', $post_data);
	}
	
	public function load_list_data()
	{
		$tanggal_dari = $this->input->post_get('tanggal_dari', true);
		$tanggal_sampai = $this->input->post_get('tanggal_sampai', true);
		$keyword = $this->input->post('keyword', true);
		$is_user = $this->input->post_get('is_user', true);
		
		$result_excel = $this->input->post_get('result_excel', true);
		$result_print = $this->input->post_get('result_print', true);
		$show_responsive_mode = true;
		if(!empty($result_excel) OR !empty($result_print)){
			$show_responsive_mode = false;
		}
				
		$params = array(
			'tanggal_dari'		=> $tanggal_dari,
			'tanggal_sampai'	=> $tanggal_sampai,
			'keyword'	=> $keyword,
			'tipe'	=> array('tertunda','proses')
		);
		$data_sms = $this->sms->data_sms($params); 
		
		$data_sms_html = '';
		if(!empty($data_sms)){
			foreach($data_sms as $dt){	
				
				$data_sms_html_detail = '';
				
				$diterima = 'TIDAK';
				if($dt->is_reply == 1){
					$diterima = 'YA';
				}
				
				if(empty($dt->contact_person)){
					$dt->contact_person = '-';
				}
				
				$data_sms_html_detail .= '
				<tr>';
				if($show_responsive_mode){
					$data_sms_html_detail .= '
					<td class="text-center">
						<input type="hidden" id="sms_id_'.$dt->id.'" value="'.$dt->id.'">
						<input type="hidden" id="sms_nama_'.$dt->id.'" value="'.$dt->nama.'">
						<input type="hidden" id="sms_kitas_'.$dt->id.'" value="'.$dt->kitas.'">
						<input type="hidden" id="sms_no_tujuan_'.$dt->id.'" value="'.$dt->no_tujuan.'">
						<a class="btn btn-xs btn-success" href="javascript:resend_sms('.$dt->id.');"><i class="fa fa-envelope"></i></a>	
						&nbsp;
						<a class="btn btn-xs btn-danger" href="javascript:delete_sms('.$dt->id.');"><i class="fa fa-remove"></i></a>
					</td>';
				}
				
				$data_sms_html_detail .= '
					<td class="hidden-xs"><small>'.$dt->sponsor.'</small></td>
					<td class="hidden-xs"><small>'.$dt->contact_person.'</small></td>
					<td class="hidden-xs text-center"><small>'.$dt->no_tujuan.'</small></td>
					<td class="hidden-xs"><small>'.$dt->nama.'</small></td>
					<td class="hidden-xs text-center"><small>'.$dt->no_paspor.'</small></td>
					<td class="hidden-xs text-center"><small>'.$dt->kitas.'</small></td>
					<td class="hidden-xs text-center"><small>'.$dt->tanggal_terkirim.'</small></td>
					
				';
				//<td class="hidden-xs text-center"><small>'.$diterima.'</small></td>
				
				if($show_responsive_mode){
					$data_sms_html_detail .= '
					<td class="visible-xs">
						<small>Sponsor: '.$dt->sponsor.',
						Kontak: '.$dt->contact_person.'
						Nama: '.$dt->nama.'
						<br/>No Paspor: '.$dt->no_paspor.'
						<br/>No Kitas: '.$dt->kitas.'
						<br/>No HP: '.$dt->no_tujuan.'
						<br/>Terkirim: '.$dt->tanggal_terkirim.'
					</small></td>
					';
					//<br/>Diterima: '.$diterima.'
				}
				
					
				$data_sms_html_detail .= '
				</tr>
				';
				
				$data_sms_html .= $data_sms_html_detail;
			}
		}
		
		
		$html_display = '
		<table class="table table-bordered table-striped js-dataTable-full" id="table-list-data">
			<thead>
				<tr>
					';
				
				if($show_responsive_mode){
					$html_display .= '
					<th class="hidden-xs text-center" width="100"><small>Option</small></th>
					';
				}
				
				$html_display .= '
					<th class="hidden-xs"><small>SPONSOR</small></th>
					<th class="hidden-xs" width="100"><small>KONTAK</small></th>
					<th class="hidden-xs text-center" width="90"><small>NO TELEPON</small></th>
					<th class="hidden-xs" width="180"><small>NAMA WNA</small></th>
					<th class="hidden-xs text-center" width="100"><small>NO PASPOR</small></th>
					<th class="hidden-xs text-center" width="100"><small>NO KITAS</small></th>
					<th class="hidden-xs text-center" width="70"><small>TERKIRIM</small></th>
					';
				//<th class="hidden-xs text-center" width="60"><small>DI REPLY</small></th>
					
				if($show_responsive_mode){
					$html_display .= '
					<th class="visible-xs"><small>DATA SMS TERKIRIM</small></th>
					';
				}
				
					
				$html_display .= '
				</tr>
			</thead>
			<tbody id="sms_data">
				'.$data_sms_html.'
			</tbody>
		</table>
		</br>
		';
		
			
		
		if(!empty($result_excel) OR !empty($result_print)){
			
			if(!empty($result_excel)){
				$post_data['print_name'] = 'Data SMS Tertunda '.$tanggal_dari.' sd '.$tanggal_sampai;
				$post_data['file_name'] = url_title($post_data['print_name']);
				$post_data['html_display'] = $html_display;
				$this->load->view(THEME_VIEW_PATH.'modules/excel_layout', $post_data);
				
			}else{
				
				$post_data = $this->post_data;	
				$post_data['add_css_page'] = '
				';
				
				$post_data['add_js_page'] = '		
					<script>window.print();</script>
				';	
				
				$post_data['print_name'] = 'Data SMS Tertunda - '.$tanggal_dari.' sd '.$tanggal_sampai;
				$post_data['html_display'] = $html_display;
				
				$this->load->view(THEME_VIEW_PATH.'modules/print_layout', $post_data);
			}
			
			
		}else{
			
			echo $html_display;
			die();
			
		}
	}
	
	public function delete_sms()
	{
		$data_ret = $this->sms->delete_sms();
		echo json_encode($data_ret);
	}
	
	public function resend_sms()
	{
		$data_ret = $this->sms->resend_sms();
		echo json_encode($data_ret);
	}
	
}
