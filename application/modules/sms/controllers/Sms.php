<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sms extends MY_Controller {
	
	protected $post_data = array();
	protected $use_session_check = FALSE;
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('model_sms', 'sms');
		$this->table_sms_setup = $this->sms->table_sms_setup;
		
		$apps_env = apps_environtment();
		$this->post_data = array_merge($this->post_data, $apps_env);	
		
		$login_data = get_login_data();
		$this->post_data['login_data'] = $login_data;
		
	}

	public function index()
	{
		
		$this->setup_sms();
		
	}

	public function setup_sms()
	{
		
		$post_data = $this->post_data;	
			

		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
		';
		
		$post_data['add_js_page'] = '
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.THEME_URL.'modules/sms/js/setup_sms.js"></script>
		';	
		
		$setup_sms = $this->sms->get_setup();
		$post_data['setup_sms'] = $setup_sms;
		
		$this->load->view(THEME_VIEW_PATH.'modules/sms/view/setup_sms', $post_data);
	}

	public function save_setup()
	{
		$isi_sms = $this->input->post('isi_sms');
		$no_mobile_test = $this->input->post('no_mobile_test');
		$data_ret = array(
			'success' => false,
			'info'	  => 'Setup Isi SMS Gagal'
		);
		
		if(empty($no_mobile_test)){
			$data_ret['info'] = 'No HP Tester tidak boleh kosong!';
		}
		
		if(empty($isi_sms)){
			$data_ret['info'] = 'Isi SMS tidak boleh kosong!';
		}
		
		$data_update = array(
			'no_mobile_test'	=> $no_mobile_test,
			'isi_sms'			=> $isi_sms,
		);
		
		$update_sms = $this->db->update($this->table_sms_setup, $data_update, "id = 1");
		if($update_sms){
			$data_ret['success'] = true;
			$data_ret['info'] = 'Setup isi sms sudah disimpan!';
			$data_ret['isi_sms'] = $isi_sms;
			$data_ret['no_mobile_test'] = $no_mobile_test;
		}
		
		echo json_encode($data_ret);
		die();
		
	}

	public function send_sms()
	{
		$data_ret = array(
			'success' => false,
			'info'	  => 'Kirim SMS Gagal'
		);
		
		$isi_sms = $this->input->post('isi_sms');
		$no_mobile_test = $this->input->post('no_mobile_test');
		
		
		if(empty($no_mobile_test)){
			$data_ret['info'] = 'No HP Tester tidak boleh kosong!';
		}
		
		if(empty($isi_sms)){
			$data_ret['info'] = 'Isi SMS tidak boleh kosong!';
		}
		
		//READY TO SEND
		$sms_send_data = array();
		$data_sms_gammu = array(
			'TextDecoded'	=> $isi_sms,
			'DestinationNumber'	=> $no_mobile_test
		);
		$sms_send_data[] = $data_sms_gammu;
		
		$params = array(
			'data'	=> $sms_send_data
		);
		
		$send_sms = $this->sms->add_sms_notify($params);
		
		if($send_sms){
			$data_ret['success'] = true;
			$data_ret['info'] = 'SMS Sudah Terkirim';
		}
		
		echo json_encode($data_ret);
		die();
		
	}
	
}
