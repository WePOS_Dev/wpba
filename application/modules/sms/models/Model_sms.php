<?php

class Model_sms extends MY_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->prefix	= config_item('db_prefix');
		$this->prefix1	= config_item('db_prefix1'); 
		$this->table_early_warning = $this->prefix1.'data';
		$this->table_sponsor = $this->prefix1.'sponsor';
		$this->table_sms = $this->prefix1.'sms';
		$this->table_sms_setup = $this->prefix1.'sms_setup';
		$this->table_sms_outbox	= 'outbox';
		$this->table_sentitems	= 'sentitems';
		$this->primary_key = 'id';
		
	}
	
	function get_setup(){
		
		$this->db->select('a.*');
		$this->db->from($this->table_sms_setup.' as a');
		$get_sms = $this->db->get();
		
		$data_sms = array();
		if($get_sms->num_rows() > 0){
			$data_sms = $get_sms->row_array();
		}
		
		$dt_return = $data_sms;
		
		return $dt_return;
	}
	
	function data_sms($params){
		
		if(!empty($params)){
			extract($params);
		}else{
			
			$dt_return = array();
			
			return $dt_return;
		}
		
		$this->db->select('a.*, b.tanggal_berlaku, b.no_paspor, b.sponsor, c.contact_person');
		$this->db->from($this->table_sms.' as a');
		$this->db->join($this->table_early_warning.' as b',"b.id = a.data_id", "LEFT");
		$this->db->join($this->table_sponsor.' as c',"c.sponsor_data = b.sponsor", "LEFT");
		
		if(!empty($tanggal_dari)){
			$tgl_dari = strtotime($tanggal_dari);
		}else{
			$tgl_dari = strtotime(date("Y-m-d"));
		}
		
		if(!empty($tanggal_sampai)){
			$tgl_sampai = strtotime($tanggal_sampai);
		}else{
			$tgl_sampai = $tgl_dari;
		}
		
		$date_from = date("Y-m-d");
		$date_till = date("Y-m-d");
		if(!empty($tanggal_dari)){
			$date_from = date("Y-m-d", $tgl_dari);
		}
		
		if(!empty($tanggal_sampai)){
			$date_till = date("Y-m-d", $tgl_sampai);
		}
		
		$date_from = $date_from.' 00:00:00';
		$date_till = $date_till.' 23:59:59';
		
		$this->db->where("(a.tanggal_terkirim BETWEEN '".$date_from."' AND '".$date_till."')");
		
		if(!empty($keyword)){
			$this->db->where("a.paspor_no LIKE '%".$keyword."%' OR a.nama LIKE '%".$keyword."%'");
		}
		if(!empty($tipe)){
			
			if(is_array($tipe)){
				$all_tipe = implode("','", $tipe);
				$this->db->where("a.sms_status IN ('".$all_tipe."')");
			}else{
				$this->db->where("a.sms_status = '".$tipe."'");
			}
			
		}
		
		$this->db->where("a.is_deleted = 0");
		
		$get_sms = $this->db->get();
		
		$data_sms = array();
		if($get_sms->num_rows() > 0){
			foreach($get_sms->result() as $dt_sms){
				
				$data_sms[] = $dt_sms;
			}
		}
		
		$dt_return = $data_sms;
		
		return $dt_return;
	}
	
	function data_ready_notify($params){
		
		if(!empty($params)){
			extract($params);
		}else{
			
			$dt_return = array();
			
			return $dt_return;
		}
		
		$this->db->select('a.*, b.mobile_phone, b.email');
		$this->db->from($this->table_early_warning.' as a');
		$this->db->join($this->table_sponsor.' as b', "b.sponsor_data = a.sponsor", "LEFT");
		
		if(!empty($tanggal_dari)){
			$tgl_dari = strtotime($tanggal_dari);
		}else{
			$tgl_dari = strtotime(date("Y-m-d"));
		}
		
		if(!empty($tanggal_sampai)){
			$tgl_sampai = strtotime($tanggal_sampai);
		}else{
			$tgl_sampai = $tgl_dari;
		}
		
		
		
		$date_from = date("Y-m-d");
		$date_till = date("Y-m-d");
		if(!empty($tanggal_dari)){
			$date_from = date("Y-m-d", $tgl_dari);
		}
		
		if(!empty($tanggal_sampai)){
			$date_till = date("Y-m-d", $tgl_sampai);
		}
		
		//OVERRIDE
		$tgl_sampai = $tgl_dari + (DAY_EARLY_WARNING*ONE_DAY_UNIX);
		$date_till = date("Y-m-d", $tgl_sampai);
		
		$this->db->where("(a.tanggal_berlaku BETWEEN '".$date_from."' AND '".$date_till."')");
		$this->db->where("a.is_notify = 0");
		$this->db->where("(b.mobile_phone != '' OR b.email != '')");
		
		$this->db->order_by("a.tanggal_berlaku","ASC");
		
		$get_data = $this->db->get();
		
		$data_sms = array();
		$all_id = array();
		if($get_data->num_rows() > 0){
			foreach($get_data->result() as $dt_sms){
				
				$data_sms[] = $dt_sms;
				
				if(!in_array($dt_sms->id, $all_id)){
					$all_id[] = $dt_sms->id;
				}
				
			}
		}
		
		if($do_update == 1){
			
			if(!empty($all_id)){
				$all_id_txt = implode(",", $all_id);
				
				$data_update = array('ready_sms' => 1);
				$update_dt = $this->db->update($this->table_early_warning, $data_update, "id IN (".$all_id_txt.")");
				
			}
		}
		
		$dt_return = $data_sms;
		
		return $dt_return;
	}
	
	function data_autosend_sms($params){
		
		if(!empty($params)){
			extract($params);
		}else{
			
			$dt_return = array();
			
			return $dt_return;
		}
		
		$this->db->select('a.*, b.mobile_phone, b.email');
		$this->db->from($this->table_early_warning.' as a');
		$this->db->join($this->table_sponsor.' as b', "b.sponsor_data = a.sponsor", "LEFT");
		
		if(!empty($tanggal_dari)){
			$tgl_dari = strtotime($tanggal_dari);
		}else{
			$tgl_dari = strtotime(date("Y-m-d"));
		}
		
		if(!empty($tanggal_sampai)){
			$tgl_sampai = strtotime($tanggal_sampai);
		}else{
			$tgl_sampai = $tgl_dari;
		}
		
		$date_from = date("Y-m-d");
		$date_till = date("Y-m-d");
		if(!empty($tanggal_dari)){
			$date_from = date("Y-m-d", $tgl_dari);
		}
		
		if(!empty($tanggal_sampai)){
			$date_till = date("Y-m-d", $tgl_sampai);
		}
		
		//OVERRIDE
		$tgl_sampai = $tgl_dari + (DAY_EARLY_WARNING*ONE_DAY_UNIX);
		$date_till = date("Y-m-d", $tgl_sampai);
		
		$this->db->where("(a.tanggal_berlaku BETWEEN '".$date_from."' AND '".$date_till."')");
		$this->db->where("(b.mobile_phone != '' OR b.email != '')");
		$this->db->where("a.ready_sms = 1");
		$this->db->where("a.is_notify = 0");
		$this->db->order_by("a.tanggal_berlaku", "ASC");
		
		if(!empty($limit)){
			$this->db->limit($limit);
		}
		
		$get_data = $this->db->get();
		
		$data_sms = array();
		if($get_data->num_rows() > 0){
			foreach($get_data->result() as $dt_sms){
				
				$data_sms[] = $dt_sms;
			}
		}
		
		$dt_return = $data_sms;
		
		return $dt_return;
	}
	
	function add_sms_notify($params)
	{
		extract($params);
		
		if(empty($data)){
			return false;
		}
		
		$DB2 = $this->load->database('sms', TRUE);
		$save = $DB2->insert_batch($this->table_sms_outbox, $data);
		
		return $save;
	}
	
	function delete_sms(){
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Hapus SMS Gagal!'
		);
		
		$login_data = get_login_data();
		
		$sms_id = $this->input->post('sms_id', true);
		
		if(!empty($sms_id)){
			
			$this->db->where('id', $sms_id);
			$delete = $this->db->delete($this->table_sms); 
			
			if($delete){			
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Hapus SMS Selesai!'
				);
			}
			
		}else{
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Hapus SMS Gagal!'
			);
		}
		
		return $data_ret;
		
	}
	
	function resend_sms(){
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Kirim Ulang SMS Gagal!'
		);
		
		$login_data = get_login_data();
		
		$sms_id = $this->input->post('sms_id', true);
		$nama = $this->input->post('sms_nama', true);
		$kitas = $this->input->post('sms_kitas', true);
		$expired = $this->input->post('sms_expired', true);
		$no_tujuan = $this->input->post('sms_no_tujuan', true);
		
		if(!empty($sms_id)){
			
			//HAPUS DI OUTBOX DAN SENTITEM = $sms_id
			$DB2 = $this->load->database('sms', TRUE);
			$DB2->where("CreatorID IN (".$sms_id.")");
			$get_sent_item = $DB2->delete($this->table_sms_outbox);
			
			$DB2 = $this->load->database('sms', TRUE);
			$DB2->where("CreatorID IN (".$sms_id.")");
			$get_sent_item = $DB2->delete($this->table_sentitems);
			
			$tgl_kirim = date("Y-m-d H:i:s");
			
			//UPDATE SMS to tertunda
			$data_update = array(
				'sms_status'	=> 'tertunda',
				'tanggal_terkirim'	=> $tgl_kirim
			);
			
			$update_dt = $this->db->update($this->sms->table_sms, $data_update, "id IN (".$sms_id.")");
			
			$get_setup = $this->get_setup();
			$isi_sms = $get_setup['isi_sms'];
			$trans = array("{nama}" => $nama, "{kitas}" => $kitas, "{expired}" => $expired);
			$isi_sms_send = strtr($isi_sms, $trans);
			
			//SMS DB - GAMMU
			if(SMSSEND_READY == 1){
				
				$sms_notify_data = array();
				
				//BETA--------------------
				// Pak OKi 087717263666
				if(SMSSEND_NOTESTER == 1){
					$no_tujuan = $get_setup['no_mobile_test'];
				}
				
				$trans = array("-" => "");
				$no_tujuan = strtr($no_tujuan, $trans);
				
				$data_sms_gammu = array(
					'TextDecoded'	=> $isi_sms_send,
					'DestinationNumber'	=> $no_tujuan,
					'CreatorID'	=> $sms_id
				);
				$sms_notify_data[] = $data_sms_gammu;
				
				$params = array(
					'data'	=> $sms_notify_data
				);
				
				$this->add_sms_notify($params);
			}
			
		}else{
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Kirim Ulang SMS Gagal!'
			);
		}
		
		return $data_ret;
		
	}
	
}
