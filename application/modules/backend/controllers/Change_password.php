<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Change_password extends MY_Controller {
	
	public $table;
	public $primary_key;
	protected $post_data = array();
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('model_user', 'm');
		$this->table = $this->m->table;
		$this->primary_key = $this->m->primary_key;
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
		$login_data = get_login_data();
		$this->post_data['login_data'] = $login_data;
		
	}

	public function index()
	{				
		$post_data = $this->post_data;	

		$post_data['add_js_page'] = '		
			<script src="'.THEME_URL.'modules/backend/js/change_password.js"></script>
		';
		
		$this->load->view(THEME_VIEW_PATH.'modules/backend/view/change_password', $post_data);
		
	}

	public function save()
	{	
		$return_data = array('success' => false);
		$inputOldPassword = $this->input->post('inputOldPassword');
		$inputNewPassword = $this->input->post('inputNewPassword');
		$inputConfirmNewPassword = $this->input->post('inputConfirmNewPassword');
		$login_data = get_login_data();
		
		
		if(empty($login_data['id_user'])){
			$return_data['info'] = 'Akun tidak dikenali!';
			die(json_encode($return_data));
		}
		if(empty($inputOldPassword)){
			$return_data['info'] = 'Password Lama harus di inputkan!';
			die(json_encode($return_data));
		}
		if(empty($inputNewPassword)){
			$return_data['info'] = 'Password Baru harus di inputkan!';
			die(json_encode($return_data));
		}
		if($inputNewPassword != $inputConfirmNewPassword){
			$return_data['info'] = 'Konfirmasi Password Baru tidak sama!';
			die(json_encode($return_data));
		}
		
		//cek old pass			
		$var = array(
			'fields'	=>	'*'
		);
		$getData = $this->m->get($var, $login_data['id_user']);		
		if($getData->num_rows() > 0){
			$dtUser = $getData->row_array();
			
			if($dtUser['user_password'] != md5($inputOldPassword)){
				$return_data['info'] = 'Password Lama tidak sama!';
				die(json_encode($return_data));
			}
		}
		
		if(!empty($inputNewPassword)){
			//$user_password = $inputNewPassword;
			$inputNewPassword = md5($inputNewPassword);
		}
		
		$var = array(
			'fields'	=>	array(
				'user_password'  	=> 	$inputNewPassword,
				//'updatedby'  		=> 	$login_data['id_user'],
				//'updated'  			=> 	date('Y-m-d H:i:s')
			)
		);		
		
		//UPDATE
		$this->lib_trans->begin();
			$save = $this->m->save($var, $login_data['id_user']);
		$this->lib_trans->commit();			
		if($save)
		{  
			$login_data = get_login_data();
			
			//update session
			$login_data['user_password'] = $inputNewPassword;
			
			$update_session =array(
				'full_name'  	=> 	$login_data['full_name'],
				//'NIK'  			=> 	$login_data['NIK'],
				//'Notelp'  		=> 	$login_data['Notelp'],
				'login_data'	=>  json_encode($login_data)
			);
			$this->session->set_userdata($update_session);	
			
			$return_data = array('success' => true, 'id' => $login_data['id_user'], 'info' => 'Ubah Password Berhasil!'); 				
		}  
		else
		{  
			$return_data = array('success' => false, 'info' => 'Ubah Password Gagal!');
		}
		
		die(json_encode($return_data));
		
	}
	
}
