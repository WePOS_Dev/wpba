<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lock_account extends MY_Controller {
	
	protected $post_data = array();
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('model_backend', 'm');
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
		$login_data = get_login_data();
		$this->post_data['login_data'] = $login_data;
		
	}

	public function index()
	{				
		$post_data = $this->post_data;		
		$this->load->view(THEME_VIEW_PATH.'modules/backend/view/lock_account', $post_data);
		
	}

	public function open()
	{				
		$post_data = $this->post_data;	
		$loginPassword = $this->input->post('loginPassword', true);
		
		$r = array();
		if($post_data['login_data']['user_password'] == md5($loginPassword)){
			$r['success'] = true;
		}else{
			$r['success'] = false;
			$r['info'] = '<strong>Login Failed!</strong> Please Try again! '.$post_data['login_data']['user_password'].' == '.md5($loginPassword);
		}
		
		die(json_encode($r));
		
	}
	
}
