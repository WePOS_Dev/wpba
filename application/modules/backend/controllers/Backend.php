<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backend extends MY_Controller {
	
	protected $post_data = array();
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('model_backend', 'm');
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
		$login_data = get_login_data();
		$this->post_data['login_data'] = $login_data;
		
		if($login_data['tipe_user'] == 'user'){
			//redirect('backend');
		}
		
		
	}

	public function index()
	{				
		$post_data = $this->post_data;		

		$post_data['add_css_page'] = '
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.THEME_URL.'modules/backend/js/dashboard.js"></script>
		';	
		
		if(in_array($post_data['login_data']['role_id'], array(1,2))){
			
			//load total
			$this->db_prefix2	= config_item('db_prefix2'); 
			$this->table_permohonan = $this->db_prefix2.'permohonan';
			$this->table_bebas = $this->db_prefix2.'bebas';
			$this->table_tindakan = $this->db_prefix2.'tindakan';
			
			$this->db->from($this->table_bebas);
			$this->db->where("is_deleted = 0");
			
			$get_dt = $this->db->get();
			
			$total_pemberitahuan = $get_dt->num_rows();
			$total_pemberitahuan_belum_terbaca = 0;
			$total_pemberitahuan_belum_ada_permohonan = 0;
			if($get_dt->num_rows() > 0){
				foreach($get_dt->result() as $dt){
					if($dt->is_show == 0){
						$total_pemberitahuan_belum_terbaca++;
					}
					if($dt->is_used == 0){
						$total_pemberitahuan_belum_ada_permohonan++;
					}
				}
			}
			
			$this->db->from($this->table_permohonan);
			$this->db->where("is_deleted = 0");
			$get_dt = $this->db->get();
			$total_permohonan = $get_dt->num_rows();
			
			//TINDAKAN
			$this->db->from($this->table_tindakan.' as a');
			$this->db->join($this->table_permohonan.' as b',"b.id = a.permohonan_id","LEFT");
			$this->db->where("b.is_deleted = 0");
			$this->db->where("(a.tindakan_a = 1 OR a.tindakan_b = 1 OR a.tindakan_c = 1 OR a.tindakan_d = 1 OR a.tindakan_e = 1 OR a.tindakan_f = 1)");
			$get_dt = $this->db->get();
			
			$total_tindakan = $get_dt->num_rows();
			$sudah_dideportasi = 0;
			if($get_dt->num_rows() > 0){
				foreach($get_dt->result() as $dt){
					if($dt->sudah_dideportasi == 1){
						$sudah_dideportasi++;
					}
				}
			}
			
			$post_data['total_pemberitahuan'] = $total_pemberitahuan;
			$post_data['total_pemberitahuan_belum_terbaca'] = $total_pemberitahuan_belum_terbaca;
			$post_data['total_pemberitahuan_belum_ada_permohonan'] = $total_pemberitahuan_belum_ada_permohonan;
			$post_data['total_permohonan'] = $total_permohonan;
			$post_data['total_tindakan'] = $total_tindakan;
			$post_data['sudah_dideportasi'] = $sudah_dideportasi;
			
		}
		
		$this->load->view(THEME_VIEW_PATH.'modules/backend/view/dashboard', $post_data);
		
	}
	
}
