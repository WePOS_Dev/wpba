<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Change_profile extends MY_Controller {
	
	public $table;
	public $primary_key;
	public $table_staff_info;
	public $primary_key2;
	protected $post_data = array();
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('model_user', 'm');
		$this->table = $this->m->table;
		$this->primary_key = $this->m->primary_key;
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
		$login_data = get_login_data();
		
		$this->post_data['login_data'] = $login_data;
		
	}

	public function index()
	{				
		$post_data = $this->post_data;	
		
		$post_data['add_js_page'] = '		
			<script src="'.THEME_URL.'modules/backend/js/change_profile.js"></script>
		';
		
		$this->load->view(THEME_VIEW_PATH.'modules/backend/view/change_profile', $post_data);
		
	}

	public function save()
	{	
		$return_data = array('success' => false);
		$full_name = $this->input->post('full_name');
		$user_email = $this->input->post('user_email');
		$user_mobile = $this->input->post('user_mobile');
		$login_data = get_login_data();
				
		if(empty($login_data['id_user'])){
			$return_data['info'] = 'Akun tidak dikenali!';
			die(json_encode($return_data));
		}
		if(empty($full_name)){
			$return_data['info'] = 'Nama harus di inputkan!';
			die(json_encode($return_data));
		}
		/*
		if(empty($user_mobile)){
			$return_data['info'] = 'No Telepon harus di inputkan!';
			die(json_encode($return_data));
		}
		*/
		
		$var = array(
			'fields'	=>	array(
				'user_fullname' 			=> 	$full_name,
				'user_email'  			=> 	$user_email,
				'user_mobile'  		=> 	$user_mobile,
				'updated'  		=> 	date('Y-m-d H:i:s')
			),
			'table' => $this->table,
			'primary_key' => $this->primary_key
		);
		
		//UPDATE
		$this->lib_trans->begin();
			$save = $this->m->save($var, $login_data['user_username']);
		$this->lib_trans->commit();			
		if($save)
		{ 
			$login_data = get_login_data();
			
			//update session
			$login_data['full_name'] = $full_name;
			$login_data['user_email'] = $user_email;
			$login_data['user_mobile'] = $user_mobile;
			
			$update_session =array(
				'full_name'  	=> 	$full_name,
				'user_email'  			=> 	$user_email,
				'user_mobile'  		=> 	$user_mobile,
				'login_data'	=>  json_encode($login_data)
			);
			$this->session->set_userdata($update_session);	
			
			$return_data = array('success' => true, 'id' => $login_data['id_user'], 'info' => 'Ubah Profil Berhasil!'); 	
		}  
		else
		{  
			$return_data = array('success' => false, 'info' => 'Ubah Profil Gagal!');
		}
		
		die(json_encode($return_data));
		
	}
	
}
