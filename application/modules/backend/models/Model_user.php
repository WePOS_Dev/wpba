<?php

class Model_user extends DB_Model {
	
	public $table;
	
	function __construct()
	{
		parent::__construct();
		
		$this->prefix	= config_item('db_prefix');
		$this->table 	= $this->prefix.'users';
		$this->primary_key = 'id';
		
	}
	
}
