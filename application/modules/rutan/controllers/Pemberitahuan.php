<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pemberitahuan extends MY_Controller {
	
	protected $post_data = array();
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('lapas/model_lapas', 'rutan');
		$this->table_bebas = $this->rutan->table_bebas;
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
		$login_data = get_login_data();
		$this->post_data['login_data'] = $login_data;
		
		
	}
	
	public function index()
	{
		
		$post_data = $this->post_data;	
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,4))){
			redirect('backend');
		}

		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker3.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/datatables/jquery.dataTables.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.APP_URL.'libs/jquery-validation/jquery.validate.min.js"></script>
			<script src="'.THEME_URL.'modules/rutan/js/pemberitahuan.js"></script>
		';
		
		
		$data_edit = array(
			'is_edit'	=> 0,
			'pemberitahuan_id'	=> 0,
			'tipe_instansi'	=> 'RUTAN',
			'no_berita_bebas'	=> '',
			'tanggal_berita'	=> date("d-m-Y"),
			'nama_wna'	=> '',
			'tempat_lahir'	=> '',
			'tanggal_lahir'	=> date("01-01-1980"),
			'kebangsaan'	=> '',
			'no_paspor'	=> '',
			'jenis_kelamin'	=> 'L',
			'alamat'	=> '',
			'no_ic'	=> '',
			'tanggal_bebas'	=> date("d-m-Y"),
			'catatan'	=> ''
		);
		
		$post_data['data_edit'] = $data_edit;
		
		$this->load->view(THEME_VIEW_PATH.'modules/rutan/view/pemberitahuan', $post_data);
	}
	
	public function editData($getId = 0)
	{
		$post_data = $this->post_data;	
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,4))){
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Simpan Data Gagal, akses pengguna tidak diijinkan!'
			);
			die(json_encode($data_ret));
		}
		

		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker3.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/datatables/jquery.dataTables.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.APP_URL.'libs/jquery-validation/jquery.validate.min.js"></script>
			<script src="'.THEME_URL.'modules/rutan/js/pemberitahuan.js"></script>
		';
		
		//get id
		$get_xid = $this->input->post_get('xid', true);
		$exp_xid = explode("_", $get_xid);
		$xid = $exp_xid[0];
		
		$get_data = $this->rutan->loadData($xid);
		
		if(!empty($get_data['data'])){
			$data_edit = (array) $get_data['data'];
		
			$data_edit['is_edit'] = 1;
			$data_edit['pemberitahuan_id'] = $xid;
			$post_data['data_edit'] = $data_edit;
			
		}else{
			
			$data_edit = array(
				'is_edit'	=> 1,
				'pemberitahuan_id'	=> 0,
				'tipe_instansi'	=> 'RUTAN',
				'no_berita_bebas'	=> '',
				'tanggal_berita'	=> date("d-m-Y"),
				'nama_wna'	=> '',
				'tempat_lahir'	=> '',
				'tanggal_lahir'	=> date("01-01-1980"),
				'kebangsaan'	=> '',
				'no_paspor'	=> '',
				'jenis_kelamin'	=> 'L',
				'alamat'	=> '',
				'no_ic'	=> '',
				'tanggal_bebas'	=> date("d-m-Y"),
				'catatan'	=> ''
			);
			
			$post_data['data_edit'] = $data_edit;
			$post_data['is_error'] =  'Data tidak ditemukan!';
		}
		
		$this->load->view(THEME_VIEW_PATH.'modules/rutan/view/pemberitahuan', $post_data);
		
	}
		
	
	public function addData()
	{
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,4))){
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Simpan Data Gagal, akses pengguna tidak diijinkan!'
			);
			die(json_encode($data_ret));
		}
		
		$data_ret = $this->rutan->addUpdate();
		echo json_encode($data_ret);
	}
	
	
}
