<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('model_login', 'm');
	}
	
	public function index()
	{		
		if(!empty($_POST)){
			$this->submit();
		}
		
		if($this->session->userdata('id_user') != ''){ 
			redirect(BASE_URL.'backend'); 
		}
		
		$post_data = array();
		$apps_env = apps_environtment();
		$post_data = array_merge($post_data, $apps_env);
		
		$this->load->view(THEME_VIEW_PATH.'modules/login/view/login', $post_data);
	}
		
	public function accepted()
	{
		$post_data = array();
		$apps_env = apps_environtment();
		$post_data = array_merge($post_data, $apps_env);
		
		$this->load->view(THEME_VIEW_PATH.'modules/login/view/login_accepted', $post_data);
	}
		
	public function submit()
	{	
		
		$username = $this->input->post('loginUsername', true);
    	$password = $this->input->post('loginPassword', true);
        $r = $this->m->submit($username, $password);
        if($r['count'] == 1)
        {
            $this->reg_session($r['data']);
			$r['success'] = true;
        }
        else
        {
            $r['success'] = false;
			$r['info'] = '<strong>Login Failed!</strong> Please Try again!';
        }
		
		die(json_encode($r));
	}
	
	private function reg_session($d)
	{
		$data = array(
			'id_user'				=>	$d->id,
			'user_username'			=>	$d->user_username,
			'login_data'			=>	json_encode($d)
		);
		$this->session->set_userdata($data);
	}
}
