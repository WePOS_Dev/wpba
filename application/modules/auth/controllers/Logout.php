<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends MX_Controller {
	
	function __construct(){
		parent::__construct();
	}
	
	public function index()
	{
		if($this->session->userdata('id_user') == ''){ 
			redirect(BASE_URL.'login'); 
		}
		
		$get_all_sess = $this->session->userdata();
		$all_sess = array();
		foreach($get_all_sess as $key => $val){
			$all_sess[] = $key;
		}
		
		$this->session->unset_userdata($all_sess);		
		//$this->session->sess_destroy();
				
		//if(!empty($_SESSION)){
		//	unset($_SESSION);
		//}
		
		redirect(BASE_URL.'login');
	}
}
