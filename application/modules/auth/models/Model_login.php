<?php

class Model_login extends MY_Model {
	
	function __construct()
	{
		parent::__construct();
		$this->prefix	= config_item('db_prefix');
		$this->table	= $this->prefix.'users';
		$this->table_role	= $this->prefix.'roles';
		
	}

	function submit($username, $password)
	{
		$this->db->select('a.*, a.id as id_user, a.user_fullname as full_name, b.role_name, b.role_description, b.role_name as tipe_user');
		$this->db->from($this->table.' as a');
		$this->db->join($this->table_role.' as b', "b.id = a.role_id", "LEFT");
		$this->db->where('a.user_username',$username);
		$this->db->where('a.user_password',md5($password));
		$this->db->where('a.is_active', "1");
		
		$query 	= 	$this->db->get();
		
		log_message('INFO', 'QUERY: '.$this->db->last_query());
				
		$total = $query->num_rows();
		$ret_data = array();
		if($total > 0)
		{
			$ret_data = $query->row();
		} 
		
		return array(
			'count' => $total,
			'data'	=> $ret_data
		);
	}
	
}
