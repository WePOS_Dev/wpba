<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Display extends MY_Controller {
	
	protected $post_data = array();
	protected $use_session_check = FALSE;
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('absensi/model_absensi', 'absensi');
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
	}

	public function index()
	{				
		$post_data = $this->post_data;		
		
		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/slick/slick.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/slick/slick-theme.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/slick/slick.min.js"></script>
			<script src="'.APP_URL.'libs/chartjs/Chart.min.js"></script>
			<script src="'.THEME_URL.'modules/frontend/js/display.js"></script>
		';	
		
		$tanggal_hari_ini = date("d/m/Y");
		
		$params = array(
			'tanggal_log'	=> $tanggal_hari_ini 
		);
		$data_absensi = $this->absensi->data_absensi($params);
		//pegawai_masuk_total, pegawai_masuk_id, pegawai_masuk_detail, pegawai_pulang, pegawai_terlambat_total,
		//pegawai_terlambat_total, pegawai_terlambat_detail, pegawai_pulang_awal_total, pegawai_pulang_awal_detal,
		//absensi_bidang_total, absensi_bidang_detail, absensi_bidang_terlambat_total, absensi_bidang_terlambat_detail
		//tanggal_log
		
		//Ijin pegawai
		$params = array(
			'tanggal_absen'	=> $tanggal_hari_ini 
		);
		$data_ijin_pegawai = $this->absensi->data_ijin_pegawai($params); //all_id, total, detail, tanggal_absen
		
		if(empty($data_absensi['pegawai_masuk_id'])){
			$data_absensi['pegawai_masuk_id'] = array();
		}
		
		$params = array(
			'tanggal_absen'	=> $tanggal_hari_ini,
			'data_ijin_pegawai'	=> $data_ijin_pegawai,
			'pegawai_masuk_id'	=> $data_absensi['pegawai_masuk_id']
		);
		$data_jadwal_pegawai = $this->absensi->data_jadwal_pegawai($params); 
		//pegawai_total, pegawai_detail, 
		//pegawai_belum_masuk_total, pegawai_belum_masuk_detail, tanggal_absen
		$post_data['data_jadwal_pegawai'] = $data_jadwal_pegawai;
		
		$this->load->view(THEME_VIEW_PATH.'modules/frontend/view/display', $post_data);
		
	}
	
}
