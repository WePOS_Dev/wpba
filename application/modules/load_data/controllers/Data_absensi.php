<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_absensi extends MY_Controller {
	
	protected $post_data = array();
	protected $use_session_check = FALSE;
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('absensi/model_absensi', 'absensi');
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
	}

	public function index()
	{
		
		$reqdata = $this->input->post('reqdata', true);
		$reqfilter = $this->input->post('reqfilter', true);
		$dataType = $this->input->post('dataType', true);
		$for_display = $this->input->post('for_display', true);
		
		if(empty($reqdata)){
			die();
		}
		
		if(empty($reqfilter)){
			$reqfilter = 'reset';
		}
		
		$data_bidang = $this->absensi->data_bidang(); //nama, detail
		
		$tanggal_hari_ini = date("d/m/Y");
		
		$params = array(
			'tanggal_log'	=> $tanggal_hari_ini 
		);
		$data_absensi = $this->absensi->data_absensi($params);
		//pegawai_masuk_total, pegawai_masuk_id, pegawai_masuk_detail, pegawai_pulang, pegawai_terlambat_total,
		//pegawai_terlambat_total, pegawai_terlambat_detail, pegawai_pulang_awal_total, pegawai_pulang_awal_detal,
		//absensi_bidang_total, absensi_bidang_detail, absensi_bidang_terlambat_total, absensi_bidang_terlambat_detail
		//tanggal_log
		
		$pegawai_masuk_total = 0;
		if(!empty($data_absensi['pegawai_masuk_total'])){
			$pegawai_masuk_total = $data_absensi['pegawai_masuk_total'];
		}
		
		//Ijin pegawai
		$params = array(
			'tanggal_absen'	=> $tanggal_hari_ini 
		);
		$data_ijin_pegawai = $this->absensi->data_ijin_pegawai($params); //all_id, total, detail, tanggal_absen
		
		$params = array(
			'tanggal_absen'	=> $tanggal_hari_ini,
			'data_ijin_pegawai'	=> $data_ijin_pegawai,
			'pegawai_masuk_id'	=> $data_absensi['pegawai_masuk_id']
		);
		$data_jadwal_pegawai = $this->absensi->data_jadwal_pegawai($params); 
		//pegawai_total, pegawai_detail, 
		//pegawai_belum_masuk_total, pegawai_belum_masuk_detail, tanggal_absen
		
		
		switch($reqdata) {
			case 'absen_bidang':
			
				$params = array(
					'tanggal_absen'	=> $tanggal_hari_ini,
					'data_bidang'	=> $data_bidang,
					'data_absensi'	=> $data_absensi,
					'data_jadwal_pegawai'	=> $data_jadwal_pegawai,
					'dataType'	=> $dataType,
					'for_display'	=> $for_display,
					'reqfilter'	=> $reqfilter
				);
				
				$this->absen_bidang($params);
				break;
			case 'absen_masuk':
			
				$params = array(
					'tanggal_absen'	=> $tanggal_hari_ini,
					'data_bidang'	=> $data_bidang,
					'data_absensi'	=> $data_absensi,
					'data_jadwal_pegawai'	=> $data_jadwal_pegawai,
					'dataType'	=> $dataType,
					'for_display'	=> $for_display,
					'reqfilter'	=> $reqfilter
				);
				
				$this->absen_masuk($params);
				break;
			case 'tidak_absen':
			
				$params = array(
					'tanggal_absen'	=> $tanggal_hari_ini,
					'data_bidang'	=> $data_bidang,
					'data_absensi'	=> $data_absensi,
					'data_jadwal_pegawai'	=> $data_jadwal_pegawai,
					'dataType'	=> $dataType,
					'for_display'	=> $for_display,
					'reqfilter'	=> $reqfilter
				);
				$this->tidak_absen($params);
				
				break;
			case 'absen_pulang':
				$params = array(
					'tanggal_absen'	=> $tanggal_hari_ini,
					'data_bidang'	=> $data_bidang,
					'data_absensi'	=> $data_absensi,
					'data_jadwal_pegawai'	=> $data_jadwal_pegawai,
					'dataType'	=> $dataType,
					'for_display'	=> $for_display,
					'reqfilter'	=> $reqfilter
				);
				$this->absen_pulang($params);
				break;
			default:
				die();
		} 
		
	}

	public function absen_bidang($params = '')
	{
		if(!empty($params)){
			extract($params);
		}
		
		$pegawai_masuk_total = 0;
		if(!empty($data_absensi['pegawai_masuk_total'])){
			$pegawai_masuk_total = $data_absensi['pegawai_masuk_total'];
		}
		
		//sesuai jadwal pegawai
		$total_perbidang = 0;
		$absensi_perbidang_total = array();
		$absensi_perbidang_detail = array();
		if(!empty($data_jadwal_pegawai['pegawai_detail'])){
			foreach($data_jadwal_pegawai['pegawai_detail'] as $dt){
				
				if(!in_array($dt->status_absen, array('libur','cuti','dinas','izin','sakit'))){
					//group per bidang
					if(empty($absensi_perbidang_detail[$dt->DEPT_NAME])){
						$absensi_perbidang_total[$dt->DEPT_NAME] = 0;
						$absensi_perbidang_detail[$dt->DEPT_NAME] = array();
					}
					
					$total_perbidang++;
					$absensi_perbidang_total[$dt->DEPT_NAME]++;
					$absensi_perbidang_detail[$dt->DEPT_NAME][] = $dt;
					
				}
				
			}
		}
		
		$total_perbidang_tidak_masuk = ($total_perbidang - $pegawai_masuk_total);
		
		$no_all = 0;
		$data_html = '';
		$data_absen_bidang = array();
		$except_unit = array('Kepala Kantor');
		$nama_bidang = ''; //dataType=list-pegawai
		if(!empty($data_bidang['detail'])){
			foreach($data_bidang['detail'] as $dt){
				
				if(!empty($reqfilter) AND !empty($dataType)){
					if($dataType == 'list-pegawai' AND $reqfilter == $dt->IdUnit){
						$nama_bidang = $dt->Namaunit;
					}
				}
				
				$total_sudah_masuk = 0;
				$total_belum_masuk = 0;
				
				if(!empty($data_absensi['absensi_bidang_total'][$dt->IdUnit])){
					$total_sudah_masuk = $data_absensi['absensi_bidang_total'][$dt->IdUnit];
				}
				
				if(!empty($absensi_perbidang_total[$dt->IdUnit])){
					$total_pegawai = $absensi_perbidang_total[$dt->IdUnit];
					$total_belum_masuk = ($total_pegawai - $total_sudah_masuk);
				}
				
				if(!in_array($dt->Namaunit, $except_unit)){			

					$no_all++;
					$data_html .= '
					<tr>
						<td class="font-w600">'.$dt->Namaunit.'</td>
						<td class="text-right" style="width: 70px;">'.$total_sudah_masuk.'</td>
						<td class="font-w600 text-success text-right" style="width: 70px;"><a href="javascript:show_list_pegawai(\''.$dt->IdUnit.'\');" class="show-pegawai" data-id="'.$dt->IdUnit.'">'.$total_belum_masuk.'</a></td>
					</tr>
					';
					
					$data_absen_bidang[] = array(
						'Namaunit'	=> $dt->Namaunit,
						'total_sudah_masuk'	=> $total_sudah_masuk,
						'total_belum_masuk'	=> $total_belum_masuk
					);
				}
			}
		}
		
		if($dataType == 'html'){
			
			if(!empty($for_display)){
				
				//resume + data
				
			}else{
				echo '
					<div class="block-content bg-gray-lighter">
						<div class="row items-push">
							<div class="col-xs-4">
								<div><small>Absen Masuk</small></div>
								<div class="font-w600">'.$pegawai_masuk_total.'</div>
							</div>
							<div class="col-xs-4">
								<div><small>Belum Absen</small></div>
								<div class="font-w600">'.$total_perbidang_tidak_masuk.'</div>
							</div>
							<div class="col-xs-4">
								<div><small>Total Pegawai</small></div>
								<div class="font-w600">'.$total_perbidang.'</div>
							</div>
						</div>
					</div>
					<div class="block-content">
						<div class="pull-t pull-r-l">
							<div class="dashboard-table-responsive">
								<table class="table remove-margin-b font-s13">
									<thead>
										<tr>
											<th class="font-s13 font-w600" style="vertical-align: top">Nama Bidang</th>
											<th class="font-s13 text-right" style="width: 70px;">Absen Masuk</th>
											<th class="font-s13 font-w600 text-success text-right" style="width: 70px;">Belum Absen</th>
										</tr>
									</thead>
									<tbody>
										'.$data_html.'
									</tbody>
								</table>
							</div>
						</div>
					</div>
				';
				
			}
			
			
		}else
		if($dataType == 'list-pegawai'){
			
			$data_list_pegawai = '';
			if(!empty($data_jadwal_pegawai['pegawai_belum_masuk_detail'])){
				$no_all = 0;
				foreach($data_jadwal_pegawai['pegawai_belum_masuk_detail'] as $dt){
					
					if(!in_array($dt->status_absen, array('libur','cuti','dinas','izin','sakit'))){
						if($reqfilter == $dt->DEPT_NAME){
							$no_all++;
							$data_list_pegawai .= '
							<tr>
								<td class="font-w600"><small>'.$no_all.'</small></td>
								<td class="font-w600"><small>'.$dt->Nama.'</small></td>
							</tr>
							';
						}
					}
					
				}
				
			}
			
			if(!empty($data_list_pegawai)){
				$data_list_pegawai = '<div class="dashboard-table-responsive">
					<table class="table remove-margin-b font-s13">
						<thead>
							<tr>
								<th class="font-s13 font-w600">No</th>
								<th class="font-s13 font-w600">Pegawai</th>
							</tr>
						</thead>
						<tbody>
							'.$data_list_pegawai.'
						</tbody>
					</table>
				</div>';
			}
			
			
			$data = array(
				'title'	=> $nama_bidang,
				'content'	=> $data_list_pegawai
			);
			
			echo json_encode($data);
			die();
			
		}else{
			
			$data = array(
				'pegawai_masuk_total'	=> $pegawai_masuk_total,
				'total_perbidang_tidak_masuk'	=> $total_perbidang_tidak_masuk,
				'total_perbidang'	=> $total_perbidang,
				'data_absen_bidang'	=> $data_absen_bidang
			);
			
			return json_encode($data);
		}
		
		
	}
	
	public function absen_masuk($params){
		
		if(!empty($params)){
			extract($params);
		}else{
			$params = array();
		}
		
		/*$params['dataType'] = 'json';
		$absen_bidang_json = $this->absen_bidang($params);
		$absen_bidang = json_decode($absen_bidang_json, true);
		$total_perbidang = 0;
		if(!empty($absen_bidang['total_perbidang'])){
			$total_perbidang = $absen_bidang['total_perbidang'];
		*/
		
		
		$pegawai_terlambat_total = 0;			
		if(!empty($data_absensi['pegawai_terlambat_total'])){
			$pegawai_terlambat_total = $data_absensi['pegawai_terlambat_total'];
		}
		
		$pegawai_masuk_total = 0;
		if(!empty($data_absensi['pegawai_masuk_total'])){
			$pegawai_masuk_total = $data_absensi['pegawai_masuk_total'];
		}
		
		$perpage = 10;
		$no_page = 1;
		$no_data = 0;
		$no_all = 0;
		$data_html = '';
		$data_absen_masuk = array();
		$data_absen_masuk_display = array();
		if(!empty($data_absensi['pegawai_masuk_detail'])){
			foreach($data_absensi['pegawai_masuk_detail'] as $dt){
				
				
				
				$jam_masuk = $dt->Jam_Log;
				
				$exp_tgl = explode("/",$dt->Tanggal_Log);
				
				$exp_jam = explode(":", $dt->Jam_masuk);
				$mktime_jam_masuk = mktime($exp_jam[0], $exp_jam[1], 0, $exp_tgl[1], $exp_tgl[0], $exp_tgl[2]);
				
				$exp_jam = explode(":",$dt->Jam_Log);
				$mktime_jam_masuk_pegawai = mktime($exp_jam[0],$exp_jam[1],$exp_jam[2],$exp_tgl[1],$exp_tgl[0],$exp_tgl[2]);
				$jam_masuk = date("H:i", $mktime_jam_masuk_pegawai);
				
				//jam
				$gap_terlambat = floor(($mktime_jam_masuk_pegawai - $mktime_jam_masuk) / 60);
				
				if($gap_terlambat <= 0){
					$terlambat_text = '-';
				}else{
					
					if($gap_terlambat > 60){
						$gap_terlambat = number_format($gap_terlambat/60,1,",",".");
						$gap_terlambat = str_replace(",0","",$gap_terlambat);
						$terlambat_text = $gap_terlambat.' Jam';
					}else{
						$terlambat_text = $gap_terlambat.' Menit';
					}
					
				}
				
				//CHECK VALID DISPLAY - FILTERING
				$is_add_data = true;				
				if(!empty($reqfilter)){
					
					switch($reqfilter){
						case 'masuk': 	//cek valid
										if($gap_terlambat > 0){
											$is_add_data = false;
										}
										break;
						case 'terlambat':  //cek valid
										if($gap_terlambat <= 0){
											$is_add_data = false;
										}
										break;
						case 'all':  $is_add_data = true;	
										break;
					}
					
				}
				
				if($is_add_data == true){
					$no_data++;
					$no_all++;
					
					$data_html .= '
						<tr>
							<td class="font-w600"><small>'.$no_all.'</small></td>
							<td class="font-w600"><small>'.$dt->Nama.'</small></td>
							<td class="text-right" style="width: 70px;">'.$jam_masuk.'</td>
							<td class="font-w600 text-danger text-right" style="width: 70px;">'.$terlambat_text.'</td>
						</tr>
					';
					
					
					$data_absen_masuk[] = array(
						'no'	=> $no_all,
						'Nama'	=> $dt->Nama,
						'jam_masuk'	=> $jam_masuk,
						'terlambat'	=> $terlambat_text
					);	
					
					//set page
					if(empty($data_absen_masuk_display[$no_page])){
						$data_absen_masuk_display[$no_page] = '';
					}
					
					$data_absen_masuk_display[$no_page] .= '
						<tr>
							<td class="font-w600"><small>'.$no_all.'</small></td>
							<td class="font-w600"><small>'.$dt->Nama.'</small></td>
							<td class="text-right" style="width: 70px;">'.$jam_masuk.'</td>
							<td class="font-w600 text-danger text-right" style="width: 70px;">'.$terlambat_text.'</td>
						</tr>
					';
						
					if($no_data+1 > $perpage){
						$no_data = 0;
						$no_page++;
					}	
				}
							
				
			}
		}
		
		$total_pegawai_terjadwal = 0;
		if(!empty($data_jadwal_pegawai['pegawai_total'])){
			$total_pegawai_terjadwal = $data_jadwal_pegawai['pegawai_total'];
		}
		
		
		$reqdate = $this->input->post('reqdate', true);
		$is_newdate = 0;
		if(!empty($reqdate)){
			if($reqdate != date("Y-m-d")){
				$is_newdate = 1;
			}
			
		}
		
		if($dataType == 'html'){
			
			if(!empty($for_display)){
				
				//add empty data
				if($no_data < $perpage AND $no_data != 0){
					for($i=$no_data; $i < $perpage; $i++){
						
						if(empty($data_absen_masuk_display[$no_page])){
							$data_absen_masuk_display[$no_page] = '';
						}
						$data_absen_masuk_display[$no_page] .= '
							<tr>
								<td class="font-w600">&nbsp;</td>
								<td class="font-w600">&nbsp;</td>
								<td class="text-right">&nbsp;</td>
								<td class="font-w600 text-danger text-right">&nbsp;</td>
							</tr>
						';
					}
				}
				
				$all_html_display = '';
				//resume + data
				if(!empty($data_absen_masuk_display)){
					foreach($data_absen_masuk_display as $dtPage){
						
						$all_html_display .= '
							<div>
								<table class="table remove-margin-b font-s13">
									<thead>
										<tr>
											<th class="font-s13 font-w600">No</th>
											<th class="font-s13 font-w600">Pegawai</th>
											<th class="font-s13 text-right" style="width: 70px;">Masuk</th>
											<th class="font-s13 font-w600 text-danger text-right" style="width: 70px;">Terlambat</th>
										</tr>
									</thead>
									<tbody>
									'.$dtPage.'
									</tbody>
								</table>
							</div>
						';
						
					}
				}
				
				$persentase_masuk = ($pegawai_masuk_total/$total_pegawai_terjadwal)*100;
				$persentase_masuk = priceFormat($persentase_masuk,2);
				
				if(empty($pegawai_masuk_total)){
					$persentase_telat = 0;
					$persentase_masuk = 0;
				}else{
					$persentase_telat = ($pegawai_terlambat_total/$pegawai_masuk_total)*100;
					$persentase_telat = priceFormat($persentase_telat,2);
				}
				
				
				$pegawai_terjadwal = 'Terjadwal: '.$total_pegawai_terjadwal.' Pegawai';
				$persentase_masuk = 'Masuk: '.$persentase_masuk.' %';
				$persentase_telat = 'Terlambat: '.$persentase_telat.' %';
				$title_html = 'Masuk : '.$pegawai_masuk_total.' Pegawai';
				$summary_html = '
					
						<div class="row items-push">
							<div class="col-xs-4">
								<div><small>Tepat Waktu</small></div>
								<div class="font-w600">'.($pegawai_masuk_total-$pegawai_terlambat_total).' pegawai</div>
							</div>
							<div class="col-xs-4">
								<div><small class="visible-lg visible-md">Terlambat</small><small class="hidden-lg hidden-md">Terlambat Absen</small></div>
								<div class="font-w600">'.$pegawai_terlambat_total.' pegawai</div>
							</div>
						</div>
					
				';
				
				$all_return = array(
					'is_newdate'	=> $is_newdate,
					'pegawai_terjadwal'	=> $pegawai_terjadwal,
					'persentase_masuk'	=> $persentase_masuk,
					'persentase_telat'	=> $persentase_telat,
					'title_html'	=> $title_html,
					'summary_html'	=> $summary_html,
					'content_html'	=> $all_html_display
				);
				
				echo json_encode($all_return);
				
			}else{
				echo '
					<div class="block-content bg-gray-lighter">
						<div class="row items-push">
							<div class="col-xs-4">
								<div><small>Tepat Waktu</small></div>
								<div class="font-w600"><a href="javascript:dashboard_table_refresh(\'#load-absen-tepat-waktu\');" id="load-absen-tepat-waktu" data-action-url="'.BASE_URL.'load_data/data_absensi" data-action-post="absen_masuk" data-action-filter="masuk" data-main-id="dashboard-absen-masuk">'.($pegawai_masuk_total-$pegawai_terlambat_total).'</a></div>
							</div>
							<div class="col-xs-4">
								<div><small>Terlambat</small></div>
								<div class="font-w600"><a href="javascript:dashboard_table_refresh(\'#load-absen-terlambat\');" id="load-absen-terlambat" data-action-url="'.BASE_URL.'load_data/data_absensi" data-action-post="absen_masuk" data-action-filter="terlambat" data-main-id="dashboard-absen-masuk">'.$pegawai_terlambat_total.'</a></div>
							</div>
							<div class="col-xs-4">
								<div><small>Total</small></div>
								<div class="font-w600"><a href="javascript:dashboard_table_refresh(\'#load-absen-masuk\');" id="load-absen-masuk" data-action-url="'.BASE_URL.'load_data/data_absensi" data-action-post="absen_masuk" data-action-filter="all" data-main-id="dashboard-absen-masuk">'.$pegawai_masuk_total.'</a></div>
							</div>
						</div>
					</div>
					<div class="block-content">
						<div class="pull-t pull-r-l">
							<div class="dashboard-table-responsive">
								<table class="table remove-margin-b font-s13">
									<thead>
										<tr>
											<th class="font-s13 font-w600" style="width: 10px;">No</th>
											<th class="font-s13 font-w600">Pegawai</th>
											<th class="font-s13 text-right" style="width: 50px;">Masuk</th>
											<th class="font-s13 font-w600 text-danger text-right" style="width: 70px;"><font class="visible-lg visible-md">Terlambat</font><font class="hidden-lg hidden-md">Telat</font></th>
										</tr>
									</thead>
									<tbody>
										'.$data_html.'
									</tbody>
								</table>
							</div>
						</div>
					</div>
				';
			}
			
		}else{
			$data = array(
				'is_newdate'	=> $is_newdate,
				'pegawai_masuk_total'	=> $pegawai_masuk_total,
				'pegawai_terlambat_total'	=> $pegawai_terlambat_total,
				//'total_perbidang'	=> $total_perbidang,
				'data_absen_masuk'	=> $data_absen_masuk
			);
			
			return json_encode($data);
			
		}
		
	}
	
	public function tidak_absen($params){
		
		if(!empty($params)){
			extract($params);
		}else{
			$params = array();
		}
		
		/*$params['dataType'] = 'json';
		$absen_bidang_json = $this->absen_bidang($params);
		$absen_bidang = json_decode($absen_bidang_json, true);
		$total_perbidang = 0;
		if(!empty($absen_bidang['total_perbidang'])){
			$total_perbidang = $absen_bidang['total_perbidang'];
		}*/
		
		$status_absen = config_item('status_absen');
		$total_tidak_masuk = array();
		foreach($status_absen as $key => $dt){
			$total_tidak_masuk[$key] = 0;
		}
		
		
		$status_absen_color = config_item('status_absen_color');
		$status_absen = config_item('status_absen');
		
		$perpage = 10;
		$no_page = 1;
		$no_data = 0;
		$no_all = 0;
		$data_html = '';
		$data_tidak_absen = array();
		$data_tidak_absen_display = array();
		if(!empty($data_jadwal_pegawai['pegawai_belum_masuk_detail'])){
			foreach($data_jadwal_pegawai['pegawai_belum_masuk_detail'] as $dt){	
					
				
				if(empty($status_absen[$dt->status_absen])){
					$dt->status_absen = 't/k';
				}
			
				$status_absen_txt = '<span class="label text-warning">'.$status_absen[$dt->status_absen].'</span>';
				if(!empty($dt->kelompok_status_absen)){
					
					if(!empty($status_absen_color[$dt->kelompok_status_absen])){
						
						$show_status_color = $status_absen_color[$dt->status_absen];
						$show_status_color = str_replace("text","label",$show_status_color);
						
						$status_absen_txt = '<span class="label '.$show_status_color.'">'.$status_absen[$dt->status_absen].'</span>';
					}
					
					//total sesuai keterangan/kelompok
					if(empty($total_tidak_masuk[$dt->kelompok_status_absen])){
						$total_tidak_masuk[$dt->kelompok_status_absen] = 0;
					}
					
					$total_tidak_masuk[$dt->kelompok_status_absen]++;
					
				}else{
					
					//tanpa keterangan
					if(empty($total_tidak_masuk['t/k'])){
						$total_tidak_masuk['t/k'] = 0;
					}
					$total_tidak_masuk['t/k']++;
				}
				
				
				
				//CHECK VALID DISPLAY - FILTERING
				$is_add_data = true;				
				if(!empty($reqfilter)){
					
					switch($reqfilter){
						case 'izin': 	//cek valid
										if($dt->kelompok_status_absen != 'izin'){
											$is_add_data = false;
										}
										break;
						case 'sakit':  //cek valid
										if($dt->kelompok_status_absen != 'sakit'){
											$is_add_data = false;
										}
										break;
						case 'cuti':  //cek valid
										if($dt->kelompok_status_absen != 'cuti'){
											$is_add_data = false;
										}
										break;
						case 'dinas':  //cek valid
										if($dt->kelompok_status_absen != 'dinas'){
											$is_add_data = false;
										}
										break;
						case 'libur':  //cek valid
										if($dt->kelompok_status_absen != 'libur'){
											$is_add_data = false;
										}
										break;
						case 'tk':  //cek valid
										if($dt->kelompok_status_absen != 't/k'){
											$is_add_data = false;
										}
										break;
						case 'all':  $is_add_data = true;	
										break;
					}
					
				}
				
				if($is_add_data == true){
					
					$no_data++;	
					$no_all++;
				
					$data_html .= '
						<tr>
							<td class="font-w600"><small>'.$no_all.'</small></td>
							<td class="font-w600"><small>'.$dt->Nama.'</small></td>
							<td class="font-w600 text-right" style="width: 100px;">'.$status_absen_txt.'</td>
						</tr>
					';
					
					$data_tidak_absen[] = array(
						'no'	=> $no_all,
						'Nama'	=> $dt->Nama,
						'status_absen_txt'	=> $status_absen_txt
					);
					
					//set page
					if(empty($data_tidak_absen_display[$no_page])){
						$data_tidak_absen_display[$no_page] = '';
					}
					
					$data_tidak_absen_display[$no_page] .= '
						<tr>
							<td class="font-w600"><small>'.$no_all.'</small></td>
							<td class="font-w600"><small>'.$dt->Nama.'</small></td>
							<td class="font-w600 text-right" style="width: 100px;">'.$status_absen_txt.'</td>
						</tr>
					';
						
					if($no_data+1 > $perpage){
						$no_data = 0;
						$no_page++;
					}		
				}
				
			}
		}
		
		
		$total_pegawai_terjadwal = 0;
		if(!empty($data_jadwal_pegawai['pegawai_total'])){
			$total_pegawai_terjadwal = $data_jadwal_pegawai['pegawai_total'];
		}
		
		
		$reqdate = $this->input->post('reqdate', true);
		$is_newdate = 0;
		if(!empty($reqdate)){
			if($reqdate != date("Y-m-d")){
				$is_newdate = 1;
			}
			
		}
		
		//echo '<pre>';
		//print_r($data_jadwal_pegawai['pegawai_belum_masuk_detail']);
		//die();
		
		if($dataType == 'html'){
			
			if(!empty($for_display)){
				
				//add empty data
				if($no_data < $perpage AND $no_data != 0){
					for($i=$no_data; $i < $perpage; $i++){
						
						if(empty($data_tidak_absen_display[$no_page])){
							$data_tidak_absen_display[$no_page] = '';
						}
						$data_tidak_absen_display[$no_page] .= '
							<tr>
								<td class="font-w600">&nbsp;</td>
								<td class="font-w600">&nbsp;</td>
								<td class="text-right">&nbsp;</td>
							</tr>
						';
					}
				}
				
				$all_html_display = '';
				//resume + data
				if(!empty($data_tidak_absen_display)){
					foreach($data_tidak_absen_display as $dtPage){
						
						$all_html_display .= '
							<div>
								<table class="table remove-margin-b font-s13">
									<thead>
										<tr>
											<th class="font-s13 font-w600">No</th>
											<th class="font-s13 font-w600">Pegawai</th>
											<th class="font-s13 font-w600 text-right" style="width: 100px;">Status</th>
										</tr>
									</thead>
									<tbody>
									'.$dtPage.'
									</tbody>
								</table>
							</div>
						';
						
					}
				}
				
				$persentase_tk = ($total_tidak_masuk['t/k']/$total_pegawai_terjadwal)*100;
				$persentase_tk = priceFormat($persentase_tk,2);
				
				$persentase_tk = 'Tanpa Ket: '.$persentase_tk.' %';
				
				$title_html = 'Tidak Masuk : '.count($data_tidak_absen).' Pegawai';
				$summary_html = '
					
						<div class="row items-push">
							<div class="col-xs-2">
								<div><small>Izin</small></div>
								<div class="font-w600">'.$total_tidak_masuk['izin'].'</div>
							</div>
							<div class="col-xs-2">
								<div><small>Sakit</small></div>
								<div class="font-w600">'.$total_tidak_masuk['sakit'].'</div>
							</div>
							<div class="col-xs-2">
								<div><small>Cuti</small></div>
								<div class="font-w600">'.$total_tidak_masuk['cuti'].'</div>
							</div>
							<div class="col-xs-2">
								<div><small>Dinas</small></div>
								<div class="font-w600">'.$total_tidak_masuk['dinas'].'</div>
							</div>
							<div class="col-xs-2">
								<div><small>Off</small></div>
								<div class="font-w600">'.$total_tidak_masuk['libur'].'</div>
							</div>
							<div class="col-xs-2">
								<div><small>T/K</small></div>
								<div class="font-w600">'.$total_tidak_masuk['t/k'].'</div>
							</div>
						</div>
					
				';
				
				$all_return = array(
					'is_newdate'	=> $is_newdate,
					'title_html'	=> $title_html,
					'persentase_tk'	=> $persentase_tk,
					'summary_html'	=> $summary_html,
					'content_html'	=> $all_html_display
				);
				
				echo json_encode($all_return);
				
			}else{
				
				echo '
					<div class="block-content bg-gray-lighter">
						<div class="row items-push">
							<div class="col-xs-2">
								<div><small>Izin</small></div>
								<div class="font-w600"><a href="javascript:dashboard_table_refresh(\'#load-tidak-absen-izin\');" id="load-tidak-absen-izin" data-action-url="'.BASE_URL.'load_data/data_absensi" data-action-post="tidak_absen" data-action-filter="izin" data-main-id="dashboard-tidak-absen">'.$total_tidak_masuk['izin'].'</a></div>
							</div>
							<div class="col-xs-2">
								<div><small>Sakit</small></div>
								<div class="font-w600"><a href="javascript:dashboard_table_refresh(\'#load-tidak-absen-sakit\');" id="load-tidak-absen-sakit" data-action-url="'.BASE_URL.'load_data/data_absensi" data-action-post="tidak_absen" data-action-filter="sakit" data-main-id="dashboard-tidak-absen">'.$total_tidak_masuk['sakit'].'</a></div>
							</div>
							<div class="col-xs-2">
								<div><small>Cuti</small></div>
								<div class="font-w600"><a href="javascript:dashboard_table_refresh(\'#load-tidak-absen-cuti\');" id="load-tidak-absen-cuti" data-action-url="'.BASE_URL.'load_data/data_absensi" data-action-post="tidak_absen" data-action-filter="cuti" data-main-id="dashboard-tidak-absen">'.$total_tidak_masuk['cuti'].'</a></div>
							</div>
							<div class="col-xs-2">
								<div><small>Dinas</small></div>
								<div class="font-w600"><a href="javascript:dashboard_table_refresh(\'#load-tidak-absen-dinas\');" id="load-tidak-absen-dinas" data-action-url="'.BASE_URL.'load_data/data_absensi" data-action-post="tidak_absen" data-action-filter="dinas" data-main-id="dashboard-tidak-absen">'.$total_tidak_masuk['dinas'].'</a></div>
							</div>
							<div class="col-xs-2">
								<div><small>Off</small></div>
								<div class="font-w600"><a href="javascript:dashboard_table_refresh(\'#load-tidak-absen-libur\');" id="load-tidak-absen-libur" data-action-url="'.BASE_URL.'load_data/data_absensi" data-action-post="tidak_absen" data-action-filter="libur" data-main-id="dashboard-tidak-absen">'.$total_tidak_masuk['libur'].'</a></div>
							</div>
							<div class="col-xs-2">
								<div><small>T/K</small></div>
								<div class="font-w600"><a href="javascript:dashboard_table_refresh(\'#load-tidak-absen-tk\');" id="load-tidak-absen-tk" data-action-url="'.BASE_URL.'load_data/data_absensi" data-action-post="tidak_absen" data-action-filter="tk" data-main-id="dashboard-tidak-absen">'.$total_tidak_masuk['t/k'].'</a></div>
							</div>
						</div>
					</div>
					<div class="block-content">
						<div class="pull-t pull-r-l">
							
							<div class="dashboard-table-responsive">
								<table class="table remove-margin-b font-s13">
									<thead>
										<tr>
											<th class="font-s13 font-w600" style="width: 10px;">No</th>
											<th class="font-s13 font-w600">Pegawai</th>
											<th class="font-s13 font-w600 text-right" style="width: 100px;">Status</th>
										</tr>
									</thead>
									<tbody>
										'.$data_html.'
									</tbody>
								</table>
							</div>
						</div>
					</div>
				';
			}
			
		}else{
			$data = array(
				'is_newdate'	=> $is_newdate,
				'pegawai_masuk_total'	=> $pegawai_masuk_total,
				'pegawai_terlambat_total'	=> $pegawai_terlambat_total,
				//'total_perbidang'	=> $total_perbidang,
				'data_absen_masuk'	=> $data_absen_masuk
			);
			
			return json_encode($data);
			
		}
		
	}
	
	public function absen_pulang($params){
		
		if(!empty($params)){
			extract($params);
		}else{
			$params = array();
		}
		
		/*$params['dataType'] = 'json';
		$absen_bidang_json = $this->absen_bidang($params);
		$absen_bidang = json_decode($absen_bidang_json, true);
		$total_perbidang = 0;
		if(!empty($absen_bidang['total_perbidang'])){
			$total_perbidang = $absen_bidang['total_perbidang'];
		}*/
		
		//echo '<pre>';
		//print_r($data_absensi);
		//die();
		
		$pegawai_pulang_awal_total = 0;			
		if(!empty($data_absensi['pegawai_pulang_awal_total'])){
			$pegawai_pulang_awal_total = $data_absensi['pegawai_pulang_awal_total'];
		}
		
		$pegawai_pulang_total = 0;
		if(!empty($data_absensi['pegawai_pulang_total'])){
			$pegawai_pulang_total = $data_absensi['pegawai_pulang_total'];
		}
		
		$perpage = 10;
		$no_page = 1;
		$no_data = 0;
		$no_all = 0;
		$data_html = '';
		$data_absen_pulang = array();
		$data_absen_pulang_display = array();
		
		if(!empty($data_absensi['pegawai_pulang_detail'])){
			foreach($data_absensi['pegawai_pulang_detail'] as $dt){
				
				$jam_pulang = $dt->Jam_Log;
						
				$exp_tgl = explode("/",$dt->Tanggal_Log);
				
				$exp_jam = explode(":", $dt->Jam_keluar);
				$mktime_jam_pulang = mktime($exp_jam[0], $exp_jam[1], 0, $exp_tgl[1], $exp_tgl[0], $exp_tgl[2]);
					
				$exp_jam = explode(":",$dt->Jam_Log);
				$mktime_jam_pulang_pegawai = mktime($exp_jam[0],$exp_jam[1],$exp_jam[2],$exp_tgl[1],$exp_tgl[0],$exp_tgl[2]);
				
				//jam
				$gap_pulang_awal = floor(($mktime_jam_pulang - $mktime_jam_pulang_pegawai) / 60);
				
				if($gap_pulang_awal <= 0){
					$pulang_awal_text = '-';
				}else{
					
					if($gap_pulang_awal > 60){
						$gap_pulang_awal = number_format($gap_pulang_awal/60,1,",",".");
						$gap_pulang_awal = str_replace(",0","",$gap_pulang_awal);
						$pulang_awal_text = $gap_pulang_awal.' Jam';
					}else{
						$pulang_awal_text = $gap_pulang_awal.' Menit';
					}
					
				}
				
				
				
				
				
				//CHECK VALID DISPLAY - FILTERING
				$is_add_data = true;				
				if(!empty($reqfilter)){
					
					switch($reqfilter){
						case 'pulang': 	//cek valid
										if($gap_pulang_awal > 0){
											$is_add_data = false;
										}
										break;
						case 'pulang_awal':  //cek valid
										if($gap_pulang_awal <= 0){
											$is_add_data = false;
										}
										break;
						case 'all':  $is_add_data = true;	
										break;
					}
					
				}
				
				if($is_add_data == true){
				
					$no_data++;	
					$no_all++;	
					
					$data_html .= '
						<tr>
							<td class="font-w600"><small>'.$no_all.'</small></td>
							<td class="font-w600"><small>'.$dt->Nama.'</small></td>
							<td class="text-right" style="width: 70px;">'.$jam_pulang.'</td>
							<td class="font-w600 text-danger text-right" style="width: 70px;">'.$pulang_awal_text.'</td>
						</tr>
					';
					
					$data_absen_pulang[] = array(
						'no'	=> $no_all,
						'nama'	=> $dt->Nama,
						'jam_pulang'	=> $jam_pulang,
						'pulang_awal'	=> $pulang_awal_text,
					);
					
					//set page
					if(empty($data_absen_pulang_display[$no_page])){
						$data_absen_pulang_display[$no_page] = '';
					}
					
					$data_absen_pulang_display[$no_page] .= '
						<tr>
							<td class="font-w600"><small>'.$no_all.'</small></td>
							<td class="font-w600"><small>'.$dt->Nama.'</small></td>
							<td class="text-right" style="width: 70px;">'.$jam_pulang.'</td>
							<td class="font-w600 text-danger text-right" style="width: 70px;">'.$pulang_awal_text.'</td>
						</tr>
					';
						
					if($no_data+1 > $perpage){
						$no_data = 0;
						$no_page++;
					}		

				}
			}
		}
		
		$reqdate = $this->input->post('reqdate', true);
		$is_newdate = 0;
		if(!empty($reqdate)){
			if($reqdate != date("Y-m-d")){
				$is_newdate = 1;
			}
			
		}
		
		if($dataType == 'html'){
			
			if(!empty($for_display)){
				
				//add empty data
				if($no_data < $perpage AND $no_data != 0){
					for($i=$no_data; $i < $perpage; $i++){
						
						if(empty($data_absen_pulang_display[$no_page])){
							$data_absen_pulang_display[$no_page] = '';
						}
						$data_absen_pulang_display[$no_page] .= '
							<tr>
								<td class="font-w600">&nbsp;</td>
								<td class="font-w600">&nbsp;</td>
								<td class="text-right">&nbsp;</td>
								<td class="text-right">&nbsp;</td>
							</tr>
						';
					}
				}
				
				$all_html_display = '';
				//resume + data
				if(!empty($data_absen_pulang_display)){
					foreach($data_absen_pulang_display as $dtPage){
						
						$all_html_display .= '
							<div>
								<table class="table remove-margin-b font-s13">
									<thead>
										<tr>
											<th class="font-s13 font-w600" style="width: 30px;">No.</th>
											<th class="font-s13 font-w600">Pegawai</th>
											<th class="font-s13 text-right" style="width: 70px;">Pulang</th>
											<th class="font-s13 font-w600 text-danger text-right" style="width: 70px;">Plg.Awal</th>
										</tr>
									</thead>
									<tbody>
									'.$dtPage.'
									</tbody>
								</table>
							</div>
						';
						
					}
				}
				
				$title_html = 'Pulang : '.$pegawai_pulang_total.' Pegawai';
				$summary_html = '
					
						<div class="row items-push">
							<div class="col-xs-4">
								<div><small>Tepat Waktu</small></div>
								<div class="font-w600">'.($pegawai_pulang_total-$pegawai_pulang_awal_total).' pegawai</div>
							</div>
							<div class="col-xs-4">
								<div><small>Pulang Awal</small></div>
								<div class="font-w600">'.$pegawai_pulang_awal_total.' pegawai</div>
							</div>
						</div>
					
				';
				
				$all_return = array(
					'is_newdate'	=> $is_newdate,
					'title_html'	=> $title_html,
					'summary_html'	=> $summary_html,
					'content_html'	=> $all_html_display
				);
				
				echo json_encode($all_return);
				
			}else{
				echo '
					<div class="block-content bg-gray-lighter">
						<div class="row items-push">
							<div class="col-xs-4">
								<div><small>Tepat Waktu</small></div>
								<div class="font-w600"><a href="javascript:dashboard_table_refresh(\'#load-pulang-tepat-waktu\');" id="load-pulang-tepat-waktu" data-action-url="'.BASE_URL.'load_data/data_absensi" data-action-post="absen_pulang" data-action-filter="pulang" data-main-id="dashboard-absen-pulang">'.($pegawai_pulang_total-$pegawai_pulang_awal_total).'</a></div>
							</div>
							<div class="col-xs-4">
								<div><small>Pulang Awal</small></div>
								<div class="font-w600"><a href="javascript:dashboard_table_refresh(\'#load-pulang-awal\');" id="load-pulang-awal" data-action-url="'.BASE_URL.'load_data/data_absensi" data-action-post="absen_pulang" data-action-filter="pulang_awal" data-main-id="dashboard-absen-pulang">'.$pegawai_pulang_awal_total.'</a></div>
							</div>
							<div class="col-xs-4">
								<div><small>Total</small></div>
								<div class="font-w600"><a href="javascript:dashboard_table_refresh(\'#load-absen-pulang\');" id="load-absen-pulang" data-action-url="'.BASE_URL.'load_data/data_absensi" data-action-post="absen_pulang" data-action-filter="all" data-main-id="dashboard-absen-pulang">'.$pegawai_pulang_total.'</a></div>
							</div>
						</div>
					</div>
					<div class="block-content">
						<div class="pull-t pull-r-l">
							
							<div class="dashboard-table-responsive">
								<table class="table remove-margin-b font-s13">
									<thead>
										<tr>
											<th class="font-s13 font-w600" style="width: 30px;">No.</th>
											<th class="font-s13 font-w600">Pegawai</th>
											<th class="font-s13 text-right" style="width: 70px;">Pulang</th>
											<th class="font-s13 font-w600 text-danger text-right" style="width: 70px;">Plg.Awal</th>
										</tr>
									</thead>
									<tbody>
									<tbody>
										'.$data_html.'
									</tbody>
								</table>
							</div>
						</div>
					</div>
				';
			}
			
		}else{
			$data = array(
				'is_newdate'	=> $is_newdate,
				'pegawai_pulang_total'	=> $pegawai_pulang_total,
				'pegawai_pulang_awal_total'	=> $pegawai_pulang_awal_total,
				//'total_perbidang'	=> $total_perbidang,
				'data_absen_pulang'	=> $data_absen_pulang
			);
			
			return json_encode($data);
			
		}
		
	}
	
	
}
