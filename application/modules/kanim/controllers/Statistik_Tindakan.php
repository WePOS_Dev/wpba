<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statistik_Tindakan extends MY_Controller {
	
	protected $post_data = array();
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('kanim/model_kanim', 'kanim');
		$this->table_bebas = $this->kanim->table_bebas;
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
		$login_data = get_login_data();
		$this->post_data['login_data'] = $login_data;
		
	}
	
	public function index()
	{
		
		$this->list_data();
		
	}
	
	public function list_data()
	{
		$post_data = $this->post_data;	
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			redirect('backend');
		}

		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker3.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/datatables/jquery.dataTables.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.APP_URL.'libs/datatables/jquery.dataTables.min.js"></script>
			<script src="'.APP_URL.'libs/jquery-validation/jquery.validate.min.js"></script>
			<script src="'.THEME_URL.'modules/kanim/js/statistik_tindakan.js"></script>
		';
	
		$this->load->view(THEME_VIEW_PATH.'modules/kanim/view/statistik_tindakan', $post_data);
	}
	
	public function load_list_data()
	{
		$tahun_pencarian = $this->input->post('tahun_pencarian', true);
		$is_user = $this->input->post_get('is_user', true);
		
		$result_excel = $this->input->post_get('result_excel', true);
		$result_print = $this->input->post_get('result_print', true);
		$show_responsive_mode = true;
		if(!empty($result_excel) OR !empty($result_print)){
			$show_responsive_mode = false;
		}
				
		$params = array(
			'tahun_pencarian'	=> $tahun_pencarian
		);
		$data_lapas = $this->kanim->data_tindakan($params); 
		
		
		$rekap_pertahun = array();
		$rekap_negara = array();
		$rekap_negara = array();
		
		for($i=1; $i<=12; $i++){
			$rekap_pertahun[$i] = array(
				'nama'	=> get_month($i),
				'total'	=> 0,
				//'negara'=> array()
			);
		}
		
		
		if(!empty($data_lapas)){
			foreach($data_lapas as $dt){	
			
				$get_bulan = date("n",strtotime($dt->tanggal_permohonan));
				
				$dt->kebangsaan = trim($dt->kebangsaan);
				$dt->kebangsaan = strtoupper($dt->kebangsaan);
				
				if(!empty($dt->kebangsaan)){
					
					if(empty($rekap_negara[$dt->kebangsaan])){
						$rekap_negara[$dt->kebangsaan] = array(
							'bulan'	=> array(),
							'total'	=> 0
						);
					}
					
					if(empty($rekap_negara[$dt->kebangsaan]['bulan'][$get_bulan])){
						$rekap_negara[$dt->kebangsaan]['bulan'][$get_bulan] = 0;
					}
					
					$rekap_negara[$dt->kebangsaan]['bulan'][$get_bulan]++;
					$rekap_negara[$dt->kebangsaan]['total']++;
					
					//if(!in_array($dt->kebangsaan, $rekap_pertahun[$get_bulan]['negara'])){
					//	$rekap_pertahun[$get_bulan]['negara'][] = $dt->kebangsaan;
					//}
					$rekap_pertahun[$get_bulan]['total']++;
					
				}
				
			}
		}
		
		$data_lapas_html = '';
		$no = 0;
		if(!empty($rekap_negara)){
			foreach($rekap_negara as $kebangsaan => $dtK){		
				
				$no++;
				$data_lapas_html_detail = '<tr>';
				
				$data_lapas_html_detail .= '
				<td class="text-center">'.$no.'</td>
				';
				
				$data_lapas_html_detail .= '
					<td class="hidden-xs"><small>'.$kebangsaan.'</small></td>
					';
					
				
				
				foreach($rekap_pertahun as $bulan => $dtB){	
					
					$total_negara_bulan = 0;
					if(!empty($dtK['bulan'][$bulan])){
						$total_negara_bulan = $dtK['bulan'][$bulan];
					}
					$data_lapas_html_detail .= '<th class="hidden-xs" width="100"><small>'.$total_negara_bulan.'</small></th>';
					
				}
				
				
				
				if($show_responsive_mode){
					$data_lapas_html_detail .= '
					<td class="visible-xs"><small>Kebangsaan: '.$kebangsaan.'<br/>';
					
					foreach($rekap_pertahun as $bulan => $dtB){	
						if($show_responsive_mode){
							$total_negara_bulan = 0;
							if(!empty($dtK['bulan'][$bulan])){
								$total_negara_bulan = $dtK['bulan'][$bulan];
							}
							$data_lapas_html_detail .= $dtB['nama'].' = '.$total_negara_bulan.'<br/>';
						}
					}
					
					$data_lapas_html_detail .= '</small></td>';
				}
				
				
				$data_lapas_html_detail .= '
				</tr>
				';
				
				$data_lapas_html .= $data_lapas_html_detail;
			}
		}
		
		$html_display = '
		<table class="table table-bordered table-striped js-dataTable-full" id="table-lapas">
			<thead>
				<tr>';
				
			if($show_responsive_mode){
				$html_display .= '
				<th class="text-center" width="40"><small>NO</small></th>
				';
			}else{
				$html_display .= '
				<th class="text-center" width="40"><small>NO</small></th>
				';
			}
			
			$html_display .= '<th class="hidden-xs" width="100"><small>KEBANGSAAN</small></th>';
			if(!empty($rekap_pertahun)){
				foreach($rekap_pertahun as $bulan => $dt){	
					$html_display .= '<th class="hidden-xs" width="100"><small>'.strtoupper($dt['nama']).'</small></th>';
				}
			}
			
				
			
			if($show_responsive_mode){
				$html_display .= '
				<th class="visible-xs"><small>TOTAL PER-BULAN</small></th>
				';
			}
			
		$html_display .= '
				</tr>
			</thead>
			<tbody id="lapas_data">
				'.$data_lapas_html.'
			</tbody>
		</table>
		';
		
			
		
		if(!empty($result_excel) OR !empty($result_print)){
			
			if(!empty($result_excel)){
				$post_data['print_name'] = 'Statistik Tindakan Keimigrasian';
				$post_data['file_name'] = url_title($post_data['print_name']);
				$post_data['html_display'] = $html_display;
				$this->load->view(THEME_VIEW_PATH.'modules/excel_layout', $post_data);
				
			}else{
				
				$post_data = $this->post_data;	
				$post_data['add_css_page'] = '
				';
				
				$post_data['add_js_page'] = '		
					<script>window.print();</script>
				';	
				
				$post_data['print_name'] = 'Statistik Tindakan Keimigrasian';
				$post_data['html_display'] = $html_display;
				
				$this->load->view(THEME_VIEW_PATH.'modules/print_layout', $post_data);
			}
			
			
		}else{
			
			echo $html_display;
			die();
			
		}
	}
	
}
