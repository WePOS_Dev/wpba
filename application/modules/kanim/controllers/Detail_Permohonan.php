<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Detail_Permohonan extends MY_Controller {
	
	protected $post_data = array();
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('model_kanim', 'kanim');
		$this->table_tindakan = $this->kanim->table_tindakan;
		$this->table_kelengkapan = $this->kanim->table_kelengkapan;
		$this->table_sponsor = $this->kanim->table_sponsor;
		$this->table_pendaratan = $this->kanim->table_pendaratan;
		$this->table_izin_tinggal = $this->kanim->table_izin_tinggal;
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
		$login_data = get_login_data();
		$this->post_data['login_data'] = $login_data;
		
		
	}
	
	public function index()
	{
		
		$post_data = $this->post_data;	
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			redirect('backend');
		}

		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker3.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/datatables/jquery.dataTables.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.APP_URL.'libs/jquery-validation/jquery.validate.min.js"></script>
			<script src="'.THEME_URL.'modules/kanim/js/permohonan.js"></script>
		';
		
		//get id
		$get_xid = $this->input->post_get('xid', true);
		$exp_xid = explode("_", $get_xid);
		$xid = $exp_xid[0];
		
		$get_data = $this->kanim->loadDataDetail($xid);
		
		if(!empty($get_data['data'])){
			$data_edit = (array) $get_data['data'];
		
			$data_edit['is_edit'] = 1;
			$data_edit['permohonan_id'] = $xid;
			$post_data['data_edit'] = $data_edit;
			
		}else{
			
			$data_edit = array(
				'is_edit'	=> 1,
				'id_bebas'	=> 0,
				'permohonan_id'	=> 0,
				'tipe_instansi'	=> '',
				'no_permohonan'	=> '',
				'tanggal_permohonan'	=> date("d-m-Y"),
				'produk'	=> 'DEPORTASI',
				'niora'	=> '',
				'nama_wna'	=> '',
				'tempat_lahir'	=> '',
				'tanggal_lahir'	=> date("01-01-1980"),
				'kebangsaan'	=> '',
				'no_paspor'	=> '',
				'jenis_kelamin'	=> 'L',
				'alamat'	=> '',
				'kota'	=> '',
				'kantor_imigrasi'	=> '',
				'kantor_wilayah'	=> '',
				'pekerjaan'	=> '',
				'status_sipil'	=> '',
				'catatan'	=> '',
				'paspor_diberikan_tanggal'	=> '',
				'paspor_diberikan_di'	=> '',
				'paspor_berlaku_sampai'	=> ''
			);
			
			$post_data['data_edit'] = $data_edit;
		}
		
		
		
		if(!empty($get_data['data_sponsor'])){
			//data_sponsor
			$data_sponsor = (array) $get_data['data_sponsor'];
			$data_sponsor['permohonan_id'] = $xid;
			$post_data['data_sponsor'] = $data_sponsor;
			
		}else{
			
			$data_sponsor = array(
				'sponsor_id'	=> 0,
				'permohonan_id'	=> 0,
				'jenis_sponsor'	=> '',
				'status_perusahaan'	=> '',
				'sektor_kegiatan'	=> '',
				'nama_sponsor'	=> '',
				'nama_pimpinan'	=> '',
				'alamat_sponsor'	=> '',
				'kota_sponsor'	=> '',
				'kodepos_sponsor'	=> '',
				'telp_sponsor'	=> '',
			);
			
			$post_data['data_sponsor'] = $data_sponsor;
		}
		
		$this->load->view(THEME_VIEW_PATH.'modules/kanim/view/permohonan', $post_data);
	}
		
	
	public function addData()
	{
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Simpan Data Gagal, akses pengguna tidak diijinkan!'
			);
			die(json_encode($data_ret));
		}
		
		$data_ret = $this->kanim->addUpdate();
		echo json_encode($data_ret);
	}
		
	
	public function cekBebas()
	{
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Simpan Data Gagal, akses pengguna tidak diijinkan!',
				'items'	=> array(),
			);
			die(json_encode($data_ret));
		}
		
		$data_ret = $this->kanim->cekBebas();
		echo json_encode($data_ret['items']);
	}
	
	
}
