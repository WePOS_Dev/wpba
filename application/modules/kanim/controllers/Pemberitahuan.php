<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pemberitahuan extends MY_Controller {
	
	protected $post_data = array();
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('lapas/model_lapas', 'lapas');
		$this->table_bebas = $this->lapas->table_bebas;
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
		$login_data = get_login_data();
		$this->post_data['login_data'] = $login_data;
		
		
	}
	
	public function index()
	{
		
		$post_data = $this->post_data;	
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			redirect('backend');
		}


		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker3.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/datatables/jquery.dataTables.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.APP_URL.'libs/datatables/jquery.dataTables.min.js"></script>
			<script src="'.APP_URL.'libs/jquery-validation/jquery.validate.min.js"></script>
			<script src="'.THEME_URL.'modules/kanim/js/pemberitahuan.js"></script>
		';
	
		$this->load->view(THEME_VIEW_PATH.'modules/kanim/view/pemberitahuan', $post_data);
	}
	
	public function load_list_data()
	{
		$keyword = $this->input->post('keyword_pencarian', true);
		$tipe_tanggal_pencarian = $this->input->post('tipe_tanggal_pencarian', true);
		$tanggal_dari = $this->input->post('tanggal_dari', true);
		$tanggal_sampai = $this->input->post('tanggal_sampai', true);
		$is_user = $this->input->post_get('is_user', true);
		
		$result_excel = $this->input->post_get('result_excel', true);
		$result_print = $this->input->post_get('result_print', true);
		$show_responsive_mode = true;
		if(!empty($result_excel) OR !empty($result_print)){
			$show_responsive_mode = false;
		}
				
		$params = array(
			'keyword'	=> $keyword,
			'tipe_tanggal_pencarian'	=> $tipe_tanggal_pencarian,
			'tanggal_dari'	=> $tanggal_dari,
			'tanggal_sampai'	=> $tanggal_sampai,
			'tipe_instansi'	=> ''
		);
		$data_lapas = $this->lapas->data_bebas($params); 
		
		$data_lapas_html = '';
		$no = 0;
		if(!empty($data_lapas)){
			foreach($data_lapas as $dt){	
				
				$no++;
				$data_lapas_html_detail = '<tr>';
				
				if($show_responsive_mode){
					
					$data_lapas_html_detail .= '
					<td class="text-center">'.$no.'</td>
					';
				}else{
					$data_lapas_html_detail .= '
					<td class="text-center">'.$no.'</td>
					';
				}
				
				$tgl_berita = date("d-m-Y",strtotime($dt->tanggal_berita));
				$tanggal_lahir = date("d-m-Y",strtotime($dt->tanggal_lahir));
				$tanggal_bebas = date("d-m-Y",strtotime($dt->tanggal_bebas));
				
				$permohonan = ' - ';
				if($dt->is_used){
					$permohonan = 'YA';
				}
				
				
				$no_surat_putusan = '-';
				if(!empty($dt->no_surat_putusan)){
					$no_surat_putusan = $dt->no_surat_putusan;
					
					if(!empty($dt->file_surat_putusan)){
						$no_surat_putusan = '<a href="'.BASE_URL.'uploads/surat_putusan/'.$dt->file_surat_putusan.'" class="text-info" target="_blank">'.$dt->no_surat_putusan.'</a>';
					}
				}
				
				$data_lapas_html_detail .= '
					<td class="hidden-xs"><small>'.$dt->no_berita_bebas.'</small></td>
					<td class="hidden-xs"><small>&nbsp;'.$tgl_berita.'</small></td>
					<td class="hidden-xs"><small>'.$dt->nama_wna.'</small></td>
					<td class="hidden-xs"><small>'.$dt->jenis_kelamin.'</small></td>
					<td class="hidden-xs"><small>'.$dt->no_paspor.'</small></td>
					<td class="hidden-xs"><small>'.$dt->kebangsaan.'</small></td>
					<td class="hidden-xs"><small>'.$dt->no_ic.'</small></td>
					<td class="hidden-xs"><small>&nbsp;'.$tanggal_bebas.'</small></td>
					<td class="hidden-xs"><small>'.$dt->catatan.'</small></td>
					<td class="hidden-xs"><small>'.$no_surat_putusan.'</small></td>
					<td class="hidden-xs"><small>'.$permohonan.'</small></td>
					<td class="hidden-xs"><small>'.$dt->tipe_instansi.'</small></td>
					';
					
				
				if($show_responsive_mode){
					$data_lapas_html_detail .= '
					<td class="visible-xs"><small>No.Berita Bebas: '.$dt->no_berita_bebas.'
						<br/>Tgl Berita: '.$tgl_berita.'
						<br/>Nama: '.$dt->nama_wna.'
						<br/>No.Paspor: '.$dt->no_paspor.'
						<br/>TTL: '.$dt->tempat_lahir.','.$tanggal_lahir.'
						<br/>Kebangsaan: '.$dt->kebangsaan.'
						<br/>Jenis Kelamin: '.$dt->jenis_kelamin.'
						<br/>No.IC: '.$dt->no_ic.'
						<br/>Tgl Bebas: '.$tanggal_bebas.'
						<br/>Catatan: '.$dt->catatan.'
						<br/>Surat Putusan: '.$no_surat_putusan.'
						<br/>Permohonan: '.$permohonan.'
						<br/>Instansi: '.$dt->tipe_instansi.'
					</small></td>
					';
				}
				
				$data_lapas_html_detail .= '
				</tr>
				';
				
				$data_lapas_html .= $data_lapas_html_detail;
			}
		}
		
		$html_display = '
		<table class="table table-bordered table-striped js-dataTable-full" id="table-lapas">
			<thead>
				<tr>';
				
			if($show_responsive_mode){
				$html_display .= '
				<th class="text-center" width="40"><small>NO</small></th>
				';
			}else{
				$html_display .= '
				<th class="text-center" width="40"><small>No</small></th>
				';
			}
			
			
			$html_display .= '
					<th class="hidden-xs" width="150"><small>NO.REG</small></th>
					<th class="hidden-xs" width="100"><small>TGl.REG</small></th>
					<th class="hidden-xs" width="150"><small>NAMA</small></th>
					<th class="hidden-xs" width="40"><small>L/P</small></th>
					<th class="hidden-xs" width="100"><small>NO.PASPOR</small></th>
					<th class="hidden-xs" width="100"><small>KEBANGSAAN</small></th>
					<th class="hidden-xs" width="100"><small>NO.IC</small></th>
					<th class="hidden-xs" width="100"><small>TGL BEBAS</small></th>
					<th class="hidden-xs" width="100"><small>CATATAN</small></th>
					<th class="hidden-xs" width="100"><small>NO.SURAT PUTUSAN</small></th>
					<th class="hidden-xs" width="100"><small>PERMOHONAN</small></th>
					<th class="hidden-xs" width="100"><small>INSTANSI</small></th>
			';		
				
			
			if($show_responsive_mode){
				$html_display .= '
				<th class="visible-xs"><small>DAFTAR PEMBERITAHUAN AKAN BEBAS</small></th>
				';
			}
			
		$html_display .= '
				</tr>
			</thead>
			<tbody id="lapas_data">
				'.$data_lapas_html.'
			</tbody>
		</table>
		';
		
			
		
		if(!empty($result_excel) OR !empty($result_print)){
			
			if(!empty($result_excel)){
				$post_data['print_name'] = 'Daftar Pemberitahuan Akan Bebas';
				$post_data['file_name'] = url_title($post_data['print_name']);
				$post_data['html_display'] = $html_display;
				$this->load->view(THEME_VIEW_PATH.'modules/excel_layout', $post_data);
				
			}else{
				
				$post_data = $this->post_data;	
				$post_data['add_css_page'] = '
				';
				
				$post_data['add_js_page'] = '		
					<script>window.print();</script>
				';	
				
				$post_data['print_name'] = 'Daftar Pemberitahuan Akan Bebas';
				$post_data['html_display'] = $html_display;
				
				$this->load->view(THEME_VIEW_PATH.'modules/print_layout', $post_data);
			}
			
			
		}else{
			
			echo $html_display;
			die();
			
		}
	}
	
	
}
