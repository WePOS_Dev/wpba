<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_Tindakan extends MY_Controller {
	
	protected $post_data = array();
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('model_kanim', 'kanim');
		$this->table_permohonan = $this->kanim->table_permohonan;
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
		$login_data = get_login_data();
		$this->post_data['login_data'] = $login_data;
		
	}
	
	public function index()
	{
		
		$this->list_data();
		
	}
	
	public function list_data()
	{
		$post_data = $this->post_data;	
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			redirect('backend');
		}

		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker3.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/datatables/jquery.dataTables.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.APP_URL.'libs/datatables/jquery.dataTables.min.js"></script>
			<script src="'.APP_URL.'libs/jquery-validation/jquery.validate.min.js"></script>
			<script src="'.THEME_URL.'modules/kanim/js/laporan_tindakan.js"></script>
		';
	
		$this->load->view(THEME_VIEW_PATH.'modules/kanim/view/laporan_tindakan', $post_data);
	}
	
	public function load_list_data()
	{
		$keyword = $this->input->post('keyword_pencarian', true);
		$tanggal_dari = $this->input->post('tanggal_dari', true);
		$tanggal_sampai = $this->input->post('tanggal_sampai', true);
		$is_user = $this->input->post_get('is_user', true);
		
		$result_excel = $this->input->post_get('result_excel', true);
		$result_print = $this->input->post_get('result_print', true);
		$show_responsive_mode = true;
		if(!empty($result_excel) OR !empty($result_print)){
			$show_responsive_mode = false;
		}
				
		$params = array(
			'keyword'	=> $keyword,
			'tanggal_dari'	=> $tanggal_dari,
			'tanggal_sampai'	=> $tanggal_sampai
		);
		$data_kanim = $this->kanim->data_tindakan($params); 
		
		$data_kanim_html = '';
		$no = 0;
		if(!empty($data_kanim)){
			foreach($data_kanim as $dt){	
				
				$no++;
				$data_kanim_html_detail = '<tr>';
				
				if($show_responsive_mode){
					
					$data_kanim_html_detail .= '
					<td class="text-center">'.$no.'</td>
					';
					
				}else{
					$data_kanim_html_detail .= '
					<td class="text-center">'.$no.'</td>
					';
				}
				
				$tanggal_deportasi = date("d-m-Y",strtotime($dt->tanggal_deportasi));
				
				$all_tindakan = '';
				$tindakan_a = ' - ';
				if($dt->tindakan_a == 1){
					$tindakan_a = 'Ya';
					$all_tindakan[] = 'a';
				}
				
				$tindakan_b = ' - ';
				if($dt->tindakan_b == 1){
					$tindakan_b = 'Ya';
					$all_tindakan[] = 'b';
				}
				
				$tindakan_c = ' - ';
				if($dt->tindakan_c == 1){
					$tindakan_c = 'Ya';
					$all_tindakan[] = 'c';
				}
				
				$tindakan_d = ' - ';
				if($dt->tindakan_d == 1){
					$tindakan_d = 'Ya';
					$all_tindakan[] = 'd';
				}
				
				$tindakan_e = ' - ';
				if($dt->tindakan_e == 1){
					$tindakan_e = 'Ya';
					$all_tindakan[] = 'e';
				}
				
				$tindakan_f = ' - ';
				if($dt->tindakan_f == 1){
					$tindakan_f = 'Ya';
					$all_tindakan[] = 'f';
				}
				
				$keterangan = ' - ';
				if(!empty($dt->tipe_instansi)){
					$keterangan = 'EX-'.$dt->tipe_instansi;
				}
				
				$data_kanim_html_detail .= '
					<td class="hidden-xs"><small>'.$dt->nama_wna.'</small></td>
					<td class="hidden-xs"><small>'.$dt->tempat_lahir.'</small></td>
					<td class="hidden-xs"><small>'.$dt->kebangsaan.'</small></td>
					<td class="hidden-xs"><small>'.$dt->no_paspor.'</small></td>
					<td class="hidden-xs"><small>'.$dt->izin_tinggal_ke.'</small></td>
					<td class="hidden-xs"><small>'.$dt->pelanggaran_pasal.'</small></td>
					<td class="hidden-xs"><small>'.$dt->sumber_kasus.'</small></td>
					<td class="hidden-xs"><small>'.$tindakan_a.'</small></td>
					<td class="hidden-xs"><small>'.$tindakan_b.'</small></td>
					<td class="hidden-xs"><small>'.$tindakan_c.'</small></td>
					<td class="hidden-xs"><small>'.$tindakan_d.'</small></td>
					<td class="hidden-xs"><small>'.$tindakan_e.'</small></td>
					<td class="hidden-xs"><small>'.$tindakan_f.'</small></td>
					<td class="hidden-xs"><small>'.$tanggal_deportasi.'</small></td>
					<td class="hidden-xs"><small>'.$dt->pengajuan_keberatan.'</small></td>
					<td class="hidden-xs"><small>'.$keterangan.'</small></td>
					';
					
				if($show_responsive_mode){
					
					$all_tindakan_text = implode(",", $all_tindakan);
					$data_kanim_html_detail .= '
					<td class="visible-xs">Nama: '.$dt->nama_wna.'
						<br/>TTL: '.$dt->tempat_lahir.'
						<br/>Kewarganegaraan: '.$dt->kebangsaan.'
						<br/>No.Paspor: '.$dt->no_paspor.'
						<br/>Pelanggaran Pasal: '.$dt->pelanggaran_pasal.'
						<br/>Sumber Kasus: '.$dt->sumber_kasus.'
						<br/>Tindakan: '.$all_tindakan_text.'
						<br/>Tgl. Deportasi: '.$tanggal_deportasi.'
						<br/>Pengajuan Keberatan: '.$dt->pengajuan_keberatan.'
						<br/>Keterangan: '.$keterangan.'
					</small></td>
					';
				}
				
				$data_kanim_html_detail .= '
				</tr>
				';
				
				$data_kanim_html .= $data_kanim_html_detail;
			}
		}
		
		$html_display = '
		<table class="table table-bordered table-striped js-dataTable-full" id="table-kanim">
			<thead>
				<tr>';
				
			if($show_responsive_mode){
				$html_display .= '
				<th class="text-center" width="40" rowspan="2"><small>NO</small></th>
				';
			}else{
				$html_display .= '
				<th class="text-center" width="40" rowspan="2"><small>NO</small></th>
				';
			}
			
			
			$html_display .= '
					<th class="hidden-xs" width="150" rowspan="2"><small>NAMA</small></th>
					<th class="hidden-xs" width="120" rowspan="2"><small>TEMPAT LAHIR</small></th>
					<th class="hidden-xs" width="150" rowspan="2"><small>KEWARGANEGARAAN</small></th>
					<th class="hidden-xs" width="150" rowspan="2"><small>NO.PASPOR</small></th>
					<th class="hidden-xs" width="150" rowspan="2"><small>IZIN<br/>TINGGAL</small></th>
					<th class="hidden-xs" width="150" rowspan="2"><small>PELANGGARAN<br/>PASAL</small></th>
					<th class="hidden-xs" width="150" rowspan="2"><small>SUMBER<br/>KASUS</small></th>
					<th class="hidden-xs" colspan="6"><small>TINDAKAN ADMINISTRASI KEIMIGRASIAN - PASAL 75 AYAT 2</small></th>
					<th class="hidden-xs" width="150" rowspan="2"><small>TANGGAL<br/>DEPORTASI</small></th>
					<th class="hidden-xs" width="150" rowspan="2"><small>PENGAJUAN<br/>KEBERATAN</small></th>
					<th class="hidden-xs" width="150" rowspan="2"><small>KETERANGAN</small></th>
			';		
				
			
			if($show_responsive_mode){
				$html_display .= '
				<th class="visible-xs" rowspan="2"><small>Laporan Tindakan Administrasi</small></th>
				';
			}
			
		$html_display .= '
				</tr>
				
				<tr>
					<th class="hidden-xs" width="50"><small>a</small></th>
					<th class="hidden-xs" width="50"><small>b</small></th>
					<th class="hidden-xs" width="50"><small>c</small></th>
					<th class="hidden-xs" width="50"><small>d</small></th>
					<th class="hidden-xs" width="50"><small>e</small></th>
					<th class="hidden-xs" width="50"><small>f</small></th>
				</tr>
				
			</thead>
			<tbody id="kanim_data">
				'.$data_kanim_html.'
			</tbody>
		</table>
		';
		
			
		
		if(!empty($result_excel) OR !empty($result_print)){
			
			if(!empty($result_excel)){
				$post_data['print_name'] = 'Laporan Tindakan Administrasi Keimigrasian';
				$post_data['file_name'] = url_title($post_data['print_name']);
				$post_data['html_display'] = $html_display;
				$this->load->view(THEME_VIEW_PATH.'modules/excel_layout', $post_data);
				
			}else{
				
				$post_data = $this->post_data;	
				$post_data['add_css_page'] = '
				';
				
				$post_data['add_js_page'] = '		
					<script>window.print();</script>
				';	
				
				$post_data['print_name'] = 'Laporan Tindakan Administrasi Keimigrasian';
				$post_data['html_display'] = $html_display;
				
				$this->load->view(THEME_VIEW_PATH.'modules/print_layout', $post_data);
			}
			
			
		}else{
			
			echo $html_display;
			die();
			
		}
	}
	
}
