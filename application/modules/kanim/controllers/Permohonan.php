<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permohonan extends MY_Controller {
	
	protected $post_data = array();
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('model_kanim', 'kanim');
		$this->table_permohonan = $this->kanim->table_permohonan;
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
		$login_data = get_login_data();
		$this->post_data['login_data'] = $login_data;
		
		
	}
	
	public function index()
	{
		
		$post_data = $this->post_data;	
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			redirect('backend');
		}

		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker3.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/datatables/jquery.dataTables.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.APP_URL.'libs/jquery-validation/jquery.validate.min.js"></script>
			<script src="'.THEME_URL.'modules/kanim/js/permohonan.js"></script>
		';
		
		
		$data_edit = array(
			'is_edit'	=> 0,
			'permohonan_id'	=> 0,
			'id_bebas'	=> 0,
			'tipe_instansi'	=> '',
			'no_permohonan'	=> '',
			'tanggal_permohonan'	=> date("d-m-Y"),
			'produk'	=> 'DEPORTASI',
			'niora'	=> '',
			'catatan'	=> '',
			'nama_wna'	=> '',
			'tempat_lahir'	=> '',
			'tanggal_lahir'	=> date("01-01-1980"),
			'kebangsaan'	=> '',
			'no_paspor'	=> '',
			'jenis_kelamin'	=> 'L',
			'alamat'	=> ''
		);
		
		$post_data['data_edit'] = $data_edit;
		
		$this->load->view(THEME_VIEW_PATH.'modules/kanim/view/permohonan', $post_data);
	}
	
	public function editData($getId = 0)
	{
		$post_data = $this->post_data;	
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Simpan Data Gagal, akses pengguna tidak diijinkan!'
			);
			die(json_encode($data_ret));
		}
		

		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker3.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/datatables/jquery.dataTables.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.APP_URL.'libs/jquery-validation/jquery.validate.min.js"></script>
			<script src="'.THEME_URL.'modules/kanim/js/permohonan.js"></script>
		';
		
		//get id
		$get_xid = $this->input->post_get('xid', true);
		$exp_xid = explode("_", $get_xid);
		$xid = $exp_xid[0];
		
		$get_data = $this->kanim->loadData($xid);
		
		if(!empty($get_data['data'])){
			$data_edit = (array) $get_data['data'];
		
			$data_edit['is_edit'] = 1;
			$data_edit['permohonan_id'] = $xid;
			$post_data['data_edit'] = $data_edit;
			
		}else{
			
			$data_edit = array(
				'is_edit'	=> 1,
				'id_bebas'	=> 0,
				'permohonan_id'	=> 0,
				'tipe_instansi'	=> '',
				'no_permohonan'	=> '',
				'tanggal_permohonan'	=> date("d-m-Y"),
				'produk'	=> 'DEPORTASI',
				'niora'	=> '',
				'catatan'	=> '',
				'nama_wna'	=> '',
				'tempat_lahir'	=> '',
				'tanggal_lahir'	=> date("01-01-1980"),
				'kebangsaan'	=> '',
				'no_paspor'	=> '',
				'jenis_kelamin'	=> 'L',
				'alamat'	=> ''
			);
			
			$post_data['data_edit'] = $data_edit;
			$post_data['is_error'] =  'Data tidak ditemukan!';
		}
		
		$this->load->view(THEME_VIEW_PATH.'modules/kanim/view/permohonan', $post_data);
		
	}
	
	public function detail()
	{
		
		$post_data = $this->post_data;	
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			redirect('backend');
		}

		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker3.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/datatables/jquery.dataTables.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.APP_URL.'libs/jquery-validation/jquery.validate.min.js"></script>
			<script src="'.THEME_URL.'modules/kanim/js/permohonan_detail.js"></script>
		';
		
		//get id
		$get_xid = $this->input->post_get('xid', true);
		$exp_xid = explode("_", $get_xid);
		$xid = $exp_xid[0];
		
		$get_data = $this->kanim->loadDataDetail($xid);
		
		if(!empty($get_data['data'])){
			$data_edit = (array) $get_data['data'];
		
			$data_edit['is_edit'] = 1;
			$data_edit['permohonan_id'] = $xid;
			$post_data['data_edit'] = $data_edit;
			
		}else{
			
			$data_edit = array(
				'is_edit'	=> 1,
				'id_bebas'	=> 0,
				'permohonan_id'	=> 0,
				'tipe_instansi'	=> '',
				'no_permohonan'	=> '',
				'tanggal_permohonan'	=> date("d-m-Y"),
				'produk'	=> 'DEPORTASI',
				'niora'	=> '',
				'nama_wna'	=> '',
				'tempat_lahir'	=> '',
				'tanggal_lahir'	=> date("01-01-1980"),
				'kebangsaan'	=> '',
				'no_paspor'	=> '',
				'jenis_kelamin'	=> 'L',
				'alamat'	=> ''
			);
			
			$post_data['data_edit'] = $data_edit;
		}
		
		
		if(!empty($get_data['data_sponsor'])){
			//data_sponsor
			$data_sponsor = (array) $get_data['data_sponsor'];
			$data_sponsor['permohonan_id'] = $xid;
			$post_data['data_sponsor'] = $data_sponsor;
			
		}else{
			
			$data_sponsor = array(
				'sponsor_id'	=> 0,
				'permohonan_id'	=> $xid,
				'jenis_sponsor'	=> '',
				'status_perusahaan'	=> '',
				'sektor_kegiatan'	=> '',
				'nama_sponsor'	=> '',
				'nama_pimpinan'	=> '',
				'alamat_sponsor'	=> '',
				'kota_sponsor'	=> '',
				'kodepos_sponsor'	=> '',
				'telp_sponsor'	=> '',
			);
			
			$post_data['data_sponsor'] = $data_sponsor;
		}
		
		
		if(!empty($get_data['data_pendaratan'])){
			//data_pendaratan
			$data_pendaratan = (array) $get_data['data_pendaratan'];
			$data_pendaratan['permohonan_id'] = $xid;
			$post_data['data_pendaratan'] = $data_pendaratan;
			
		}else{
			
			$data_pendaratan = array(
				'pendaratan_id'	=> 0,
				'permohonan_id'	=> $xid,
				'no_visa'	=> '',
				'tanggal_pengeluaran'	=> '',
				'tempat_pengeluaran'	=> '',
				'berlaku_sd'	=> '',
				'tempat_masuk'	=> '',
				'tanggal_masuk'	=> ''
			);
			
			$post_data['data_pendaratan'] = $data_pendaratan;
		}
		
		
		if(!empty($get_data['data_izin'])){
			//data_izin
			$data_izin = (array) $get_data['data_izin'];
			$data_izin['permohonan_id'] = $xid;
			$post_data['data_izin'] = $data_izin;
			
		}else{
			
			$data_izin = array(
				'izin_id'	=> 0,
				'permohonan_id'	=> $xid,
				'no_register'	=> '',
				'izin_tinggal_ke'	=> '',
				'tanggal_pengeluaran'	=> '',
				'berlaku_sd'	=> ''
			);
			
			$post_data['data_izin'] = $data_izin;
		}
		
		if(!empty($get_data['data_tindakan'])){
			//data_tindakan
			$data_tindakan = (array) $get_data['data_tindakan'];
			$data_tindakan['permohonan_id'] = $xid;
			$post_data['data_tindakan'] = $data_tindakan;
			
		}else{
			
			$data_tindakan = array(
				'tindakan_id'	=> 0,
				'permohonan_id'	=> $xid,
				'pelanggaran_pasal'	=> '',
				'sumber_kasus'	=> '',
				'tindakan_a'	=> 0,
				'tindakan_b'	=> 0,
				'tindakan_c'	=> 0,
				'tindakan_d'	=> 0,
				'tindakan_e'	=> 0,
				'tindakan_f'	=> 0,
				'tanggal_deportasi'	=> '',
				'sudah_dideportasi'	=> 0,
				'pengajuan_keberatan'	=> ''
			);
			
			$post_data['data_tindakan'] = $data_tindakan;
		}
		
		if(!empty($get_data['data_dokumen'])){
			//data_dokumen
			$data_dokumen = (array) $get_data['data_dokumen'];
			$data_dokumen['permohonan_id'] = $xid;
			$post_data['data_dokumen'] = $data_dokumen;
			
		}else{
			
			$data_dokumen = array(
				'dokumen_id'	=> 0,
				'permohonan_id'	=> $xid,
				'laporan_kejadian'	=> '',
				'berita_acara_instansi_lain'	=> '',
				'berita_acara_pemeriksaan'	=> '',
				'berita_acara_pendapat'	=> '',
				'keputusan_kakanim'	=> '',
				'surat_perintah_pendetensian'	=> '',
				'surat_perintah_pengeluaran_pendetensian'	=> '',
				'berita_acara_pengeluaran_pendetensian'	=> '',
				'surat_perintah_tugas'	=> '',
				'surat_perintah_pengawasan_keberangkatan'	=> '',
				'file_laporan_kejadian'	=> '',
				'file_berita_acara_instansi_lain'	=> '',
				'file_berita_acara_pemeriksaan'	=> '',
				'file_berita_acara_pendapat'	=> '',
				'file_keputusan_kakanim'	=> '',
				'file_surat_perintah_pendetensian'	=> '',
				'file_surat_perintah_pengeluaran_pendetensian'	=> '',
				'file_berita_acara_pengeluaran_pendetensian'	=> '',
				'file_surat_perintah_tugas'	=> '',
				'file_surat_perintah_pengawasan_keberangkatan'	=> '',
			);
			
			$post_data['data_dokumen'] = $data_dokumen;
		}
		
		
		$this->load->view(THEME_VIEW_PATH.'modules/kanim/view/permohonan_detail', $post_data);
	}
		
	
	public function addData()
	{
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Simpan Data Gagal, akses pengguna tidak diijinkan!'
			);
			die(json_encode($data_ret));
		}
		
		$data_ret = $this->kanim->addUpdate();
		echo json_encode($data_ret);
	}
	
	public function addDataSponsor()
	{
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Simpan Data Gagal, akses pengguna tidak diijinkan!'
			);
			die(json_encode($data_ret));
		}
		
		$data_ret = $this->kanim->addDataSponsor();
		echo json_encode($data_ret);
	}
	
	public function addDataPendaratan()
	{
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Simpan Data Gagal, akses pengguna tidak diijinkan!'
			);
			die(json_encode($data_ret));
		}
		
		$data_ret = $this->kanim->addDataPendaratan();
		echo json_encode($data_ret);
	}
		
	public function addDataIzin()
	{
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Simpan Data Gagal, akses pengguna tidak diijinkan!'
			);
			die(json_encode($data_ret));
		}
		
		$data_ret = $this->kanim->addDataIzin();
		echo json_encode($data_ret);
	}
		
	public function addDataTindakan()
	{
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Simpan Data Gagal, akses pengguna tidak diijinkan!'
			);
			die(json_encode($data_ret));
		}
		
		$data_ret = $this->kanim->addDataTindakan();
		echo json_encode($data_ret);
	}
		
	public function addDataDokumen()
	{
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Simpan Data Gagal, akses pengguna tidak diijinkan!'
			);
			die(json_encode($data_ret));
		}
		
		$data_ret = $this->kanim->addDataDokumen();
		echo json_encode($data_ret);
	}
		
	
	public function cekBebas()
	{
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Simpan Data Gagal, akses pengguna tidak diijinkan!',
				'items'	=> array(),
			);
			die(json_encode($data_ret));
		}
		
		$data_ret = $this->kanim->cekBebas();
		echo json_encode($data_ret['items']);
	}
	
	public function upload_dokumen()
	{
		$this->table_dokumen_upload = $this->kanim->table_dokumen_upload;
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Upload Dokumen Gagal!'
		);
		
		$login_data = get_login_data();
		
		$permohonan_id = $this->input->post('permohonan_id', true);
		$jenis_dokumen = $this->input->post('jenis_dokumen', true);
		$nomor_dokumen = $this->input->post('nomor_dokumen', true);
		
		if(empty($permohonan_id)){
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Data Permohonan tidak dikenali!',
				'filename'	=> ''
			);
			die(json_encode($data_ret));
		}
		if(empty($jenis_dokumen)){
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Jenis Dokumen Harus dipilih!',
				'filename'	=> ''
			);
			die(json_encode($data_ret));
		}
		if(empty($nomor_dokumen)){
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Nomor Dokumen Harus diisi!',
				'filename'	=> ''
			);
			die(json_encode($data_ret));
		}
		
				
		$this->file_upload_dokumen = UPLOAD_PATH.'dokumen/';
		
		$r = ''; 
		$is_upload_file = false;	
		
		if(!empty($_FILES['upload_file']['name'])){
			
			$config['upload_path'] = $this->file_upload_dokumen;
			//$config['allowed_types'] = '*';
			$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp|pdf';
			$allowed_types_exp = explode("|",$config['allowed_types']);
			$config['max_size']	= '102400000';

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload("upload_file"))
			{
				$data = $this->upload->display_errors();
				$r = array('success' => false, 'info' => $data );
				die(json_encode($r));
			}
			else
			{
				$is_upload_file = true;
				$data_upload_temp = $this->upload->data();
				
				if(!in_array(str_replace(".","",$data_upload_temp['file_ext']), $allowed_types_exp)){
					$file =  $this->file_upload_dokumen.$data_upload_temp['file_name']."" ;
					unlink($file);
					$r = array('success' => false, 'info' => 'only file image: '.implode(",", $allowed_types_exp).' is allowed');
					die(json_encode($r));
				}
				
				$data_insert = array(
					'permohonan_id'	=> $permohonan_id,
					'jenis_dokumen'	=> $jenis_dokumen,
					'nomor_dokumen'	=> $nomor_dokumen,
					'filename'	=> $data_upload_temp['file_name'],
					'file_ext'	=> $data_upload_temp['file_ext'],
					'createdby'	=> $login_data['user_username'],
					'created'	=> date("Y-m-d H:i:s"),
					'updatedby'	=> $login_data['user_username'],
					'updated'	=> date("Y-m-d H:i:s")
				);
				
				$save = $this->db->insert($this->table_dokumen_upload, $data_insert);
				
				if($save){
					$data_ret = array(
						'success'	=> true,
						'info'	=> 'Dokumen: '.$jenis_dokumen.' sudah diupload!',
						'filename'	=> $data_upload_temp['file_name']
					);
				}
				
			}
		}else{
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Pilih file Upload!',
				'filename'	=> ''
			);
		}
		
		
		die(json_encode($data_ret));
 
	}
	
	public function load_list_upload()
	{
		$permohonan_id = $this->input->post('permohonan_id', true);
		$is_user = $this->input->post_get('is_user', true);
		
		$result_excel = $this->input->post_get('result_excel', true);
		$result_print = $this->input->post_get('result_print', true);
		$show_responsive_mode = true;
		if(!empty($result_excel) OR !empty($result_print)){
			$show_responsive_mode = false;
		}
				
		$params = array(
			'permohonan_id'	=> $permohonan_id,
		);
		$data_dokumen_upload = $this->kanim->data_dokumen_upload($params); 
		
		$data_dokumen_upload_html = '';
		$no = 0;
		if(!empty($data_dokumen_upload)){
			foreach($data_dokumen_upload as $dt){	
				
				$no++;
				$data_dokumen_upload_detail = '<tr>';
				
				if($show_responsive_mode){
					
					$data_dokumen_upload_detail .= '
					<td class="text-center">
						<input type="hidden" id="dokumen_id_'.$dt->id.'" value="'.$dt->id.'">
						<input type="hidden" id="nama_file_'.$dt->id.'" value="'.$dt->filename.'">
						<input type="hidden" id="jenis_dokumen_'.$dt->id.'" value="'.$dt->jenis_dokumen.'">
						<a class="btn btn-xs btn-danger" href="javascript:delete_dokumen('.$dt->id.');"><i class="fa fa-remove"></i></a>
					</td>
					';
				}else{
					$data_dokumen_upload_detail .= '
					<td class="text-center">'.$no.'</td>
					';
				}
				
				
				$data_dokumen_upload_detail .= '
					<td class="hidden-xs"><small>'.$dt->jenis_dokumen.'</small></td>
					<td class="hidden-xs"><small>'.$dt->nomor_dokumen.'</small></td>
					<td class="hidden-xs"><small><a href="'.BASE_URL.'uploads/dokumen/'.$dt->filename.'" class="text-info" target="_blank">'.$dt->filename.'</a></small></td>
					';
					
				if($show_responsive_mode){
					$data_dokumen_upload_detail .= '
					<td class="visible-xs"><small>Dokumen: '.$dt->jenis_dokumen.'
						<br/>Nomor: '.$dt->nomor_dokumen.'
						<br/>Filename: <a href="'.BASE_URL.'uploads/dokumen/'.$dt->filename.'" class="text-info" target="_blank">'.$dt->filename.'</a>
						<br/>Extension: '.$dt->file_ext.'
					</small></td>
					';
				}
				
				$data_dokumen_upload_detail .= '
				</tr>
				';
				
				$data_dokumen_upload_html .= $data_dokumen_upload_detail;
			}
		}
		
		$html_display = '
		<table class="table table-bordered table-striped js-dataTable-full" id="table-dokumen">
			<thead>
				<tr>';
				
			if($show_responsive_mode){
				$html_display .= '
				<th class="text-center" width="40"><small>OPTION</small></th>
				';
			}else{
				$html_display .= '
				<th class="text-center" width="40"><small>No</small></th>
				';
			}
			
			
			$html_display .= '
					<th class="hidden-xs" width="150"><small>JENIS DOKUMEN</small></th>
					<th class="hidden-xs" width="100"><small>NOMOR</small></th>
					<th class="hidden-xs" width="100"><small>FILE</small></th>
			';		
				
			
			if($show_responsive_mode){
				$html_display .= '
				<th class="visible-xs"><small>DATA DOKUMEN</small></th>
				';
			}
			
		$html_display .= '
				</tr>
			</thead>
			<tbody id="dokumen_data">
				'.$data_dokumen_upload_html.'
			</tbody>
		</table>
		';
		
			
		
		if(!empty($result_excel) OR !empty($result_print)){
			
			if(!empty($result_excel)){
				$post_data['print_name'] = 'List Dokumen';
				$post_data['file_name'] = url_title($post_data['print_name']);
				$post_data['html_display'] = $html_display;
				$this->load->view(THEME_VIEW_PATH.'modules/excel_layout', $post_data);
				
			}else{
				
				$post_data = $this->post_data;	
				$post_data['add_css_page'] = '
				';
				
				$post_data['add_js_page'] = '		
					<script>window.print();</script>
				';	
				
				$post_data['print_name'] = 'List Dokumen';
				$post_data['html_display'] = $html_display;
				
				$this->load->view(THEME_VIEW_PATH.'modules/print_layout', $post_data);
			}
			
			
		}else{
			
			echo $html_display;
			die();
			
		}
	}
	
	public function deleteDokumen()
	{
		$data_ret = $this->kanim->deleteDokumen();
		echo json_encode($data_ret);
	}
}
