<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kanim extends MY_Controller {
	
	protected $post_data = array();
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('model_kanim', 'kanim');
		$this->table_permohonan = $this->kanim->table_permohonan;
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
		$login_data = get_login_data();
		$this->post_data['login_data'] = $login_data;
		
	}
	
	public function index()
	{
		
		$this->list_data();
		
	}
	
	public function list_data()
	{
		$post_data = $this->post_data;	

		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker3.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/datatables/jquery.dataTables.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.APP_URL.'libs/datatables/jquery.dataTables.min.js"></script>
			<script src="'.APP_URL.'libs/jquery-validation/jquery.validate.min.js"></script>
			<script src="'.THEME_URL.'modules/kanim/js/kanim.js"></script>
		';
	
		$this->load->view(THEME_VIEW_PATH.'modules/kanim/view/kanim', $post_data);
	}
	
	public function load_list_data()
	{
		$keyword = $this->input->post('keyword_pencarian', true);
		$tipe_tanggal_pencarian = $this->input->post('tipe_tanggal_pencarian', true);
		$tanggal_dari = $this->input->post('tanggal_dari', true);
		$tanggal_sampai = $this->input->post('tanggal_sampai', true);
		$is_user = $this->input->post_get('is_user', true);
		
		$result_excel = $this->input->post_get('result_excel', true);
		$result_print = $this->input->post_get('result_print', true);
		$show_responsive_mode = true;
		if(!empty($result_excel) OR !empty($result_print)){
			$show_responsive_mode = false;
		}
				
		$params = array(
			'keyword'	=> $keyword,
			'tipe_tanggal_pencarian'	=> $tipe_tanggal_pencarian,
			'tanggal_dari'	=> $tanggal_dari,
			'tanggal_sampai'	=> $tanggal_sampai
		);
		$data_kanim = $this->kanim->data_permohonan($params); 
		
		$data_kanim_html = '';
		$no = 0;
		if(!empty($data_kanim)){
			foreach($data_kanim as $dt){	
				
				$no++;
				$data_kanim_html_detail = '<tr>';
				
				if($show_responsive_mode){
					
					$data_kanim_html_detail .= '
					<td class="text-center">
						<input type="hidden" id="kanim_id_'.$dt->id.'" value="'.$dt->id.'">
						<input type="hidden" id="nama_paspor_'.$dt->id.'" value="'.$dt->nama_wna.' '.$dt->no_paspor.'">
						<a class="btn btn-xs btn-success" href="'.BASE_URL.'kanim/permohonan/detail?xid='.$dt->id.'_'.strtotime(date("d-m-Y H:i:s")).'"><i class="fa fa-pencil"></i></a>	
						&nbsp;
						<a class="btn btn-xs btn-danger" href="javascript:delete_load_permohonan('.$dt->id.');"><i class="fa fa-remove"></i></a>
					</td>
					';
				}else{
					$data_kanim_html_detail .= '
					<td class="text-center">'.$no.'</td>
					';
				}
				
				$tanggal_permohonan = date("d-m-Y",strtotime($dt->tanggal_permohonan));
				$tanggal_lahir = date("d-m-Y",strtotime($dt->tanggal_lahir));
				
				$status_deportasi = '-';
				if($dt->sudah_dideportasi == 1){
					$status_deportasi = 'YA';
				}
				
				$data_kanim_html_detail .= '
					<td class="hidden-xs"><small>'.$dt->no_permohonan.'</small></td>
					<td class="hidden-xs"><small>&nbsp;'.$tanggal_permohonan.'</small></td>
					<td class="hidden-xs"><small>'.$dt->niora.'</small></td>
					<td class="hidden-xs"><small>'.$status_deportasi.'</small></td>
					<td class="hidden-xs"><small>'.$dt->nama_wna.'</small></td>
					<td class="hidden-xs"><small>'.$dt->jenis_kelamin.'</small></td>
					<td class="hidden-xs"><small>'.$dt->no_paspor.'</small></td>
					<td class="hidden-xs"><small>'.$dt->kebangsaan.'</small></td>
					';
					
				if($show_responsive_mode){
					$data_kanim_html_detail .= '
					<td class="visible-xs"><small>No.Permohonan: '.$dt->no_permohonan.'
						<br/>Tgl Permohonan: '.$tanggal_permohonan.'
						<br/>Niora: '.$dt->niora.'
						<br/>Deportasi: '.$status_deportasi.'
						<br/>Nama: '.$dt->nama_wna.'
						<br/>No.Paspor: '.$dt->no_paspor.'
						<br/>TTL: '.$dt->tempat_lahir.','.$tanggal_lahir.'
						<br/>Kebangsaan: '.$dt->kebangsaan.'
						<br/>Jenis Kelamin: '.$dt->jenis_kelamin.'
					</small></td>
					';
				}
				
				$data_kanim_html_detail .= '
				</tr>
				';
				
				$data_kanim_html .= $data_kanim_html_detail;
			}
		}
		
		$html_display = '
		<table class="table table-bordered table-striped js-dataTable-full" id="table-kanim">
			<thead>
				<tr>';
				
			if($show_responsive_mode){
				$html_display .= '
				<th class="text-center" width="150"><small>OPTION</small></th>
				';
			}else{
				$html_display .= '
				<th class="text-center" width="40"><small>No</small></th>
				';
			}
			
			
			$html_display .= '
					<th class="hidden-xs" width="150"><small>NO.PERMOHONAN</small></th>
					<th class="hidden-xs" width="120"><small>TGl.PERMOHONAN</small></th>
					<th class="hidden-xs" width="120"><small>NIORA</small></th>
					<th class="hidden-xs" width="120"><small>DEPORTASI</small></th>
					<th class="hidden-xs" width="200"><small>NAMA</small></th>
					<th class="hidden-xs" width="40"><small>L/P</small></th>
					<th class="hidden-xs" width="120"><small>NO.PASPOR</small></th>
					<th class="hidden-xs" width="150"><small>KEBANGSAAN</small></th>
			';		
				
			
			if($show_responsive_mode){
				$html_display .= '
				<th class="visible-xs"><small>DAFTAR PEMBERITAHUAN AKAN BEBAS</small></th>
				';
			}
			
		$html_display .= '
				</tr>
			</thead>
			<tbody id="kanim_data">
				'.$data_kanim_html.'
			</tbody>
		</table>
		';
		
			
		
		if(!empty($result_excel) OR !empty($result_print)){
			
			if(!empty($result_excel)){
				$post_data['print_name'] = 'Daftar Permohonaan';
				$post_data['file_name'] = url_title($post_data['print_name']);
				$post_data['html_display'] = $html_display;
				$this->load->view(THEME_VIEW_PATH.'modules/excel_layout', $post_data);
				
			}else{
				
				$post_data = $this->post_data;	
				$post_data['add_css_page'] = '
				';
				
				$post_data['add_js_page'] = '		
					<script>window.print();</script>
				';	
				
				$post_data['print_name'] = 'Daftar Pemberitahuan Akan Bebas';
				$post_data['html_display'] = $html_display;
				
				$this->load->view(THEME_VIEW_PATH.'modules/print_layout', $post_data);
			}
			
			
		}else{
			
			echo $html_display;
			die();
			
		}
	}
	
	public function notifBebas()
	{
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			$data_ret = array(
				'success'	=> false,
				'info_notif' => '',
				'stop' => 1
			);
		}else{
			$data_ret = $this->kanim->notifBebas();
			
		}
		
		echo json_encode($data_ret);
		
	}
	
	public function hideNotif()
	{
		
		if(!in_array($this->post_data['login_data']['role_id'], array(1,2))){
			$data_ret = array(
				'success'	=> false,
				'info_notif' => ''
			);
		}else{
			$data_ret = $this->kanim->hideNotif();
			
		}
		
		echo json_encode($data_ret);
		
	}
	
	public function addData()
	{
		$data_ret = $this->kanim->addUpdate();
		echo json_encode($data_ret);
	}
	
	public function loadData()
	{
		$data_ret = $this->kanim->loadData();
		echo json_encode($data_ret);
	}
	
	public function kirimNotif()
	{
		$data_ret = $this->kanim->kirimNotif();
		echo json_encode($data_ret);
	}
	
	public function deleteData()
	{
		$data_ret = $this->kanim->deleteData();
		echo json_encode($data_ret);
	}
	
}
