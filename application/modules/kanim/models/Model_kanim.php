<?php

class Model_kanim extends DB_Model {
	
	function __construct()
	{
		parent::__construct();
		
		$this->prefix	= config_item('db_prefix');
		$this->db_prefix2	= config_item('db_prefix2'); 
		$this->table_permohonan = $this->db_prefix2.'permohonan';
		$this->table_bebas = $this->db_prefix2.'bebas';
		$this->table_tindakan = $this->db_prefix2.'tindakan';
		$this->table_dokumen = $this->db_prefix2.'dokumen';
		$this->table_dokumen_upload = $this->db_prefix2.'dokumen_upload';
		$this->table_sponsor = $this->db_prefix2.'sponsor';
		$this->table_pendaratan = $this->db_prefix2.'pendaratan';
		$this->table_izin_tinggal = $this->db_prefix2.'izin_tinggal';
		$this->primary_key = 'id';
		
	}
	
	function data_dokumen_upload($params){
		
		if(!empty($params)){
			extract($params);
		}else{
			
			$dt_return = array();
			
			return $dt_return;
		}
		
		$this->db->select('a.*');
		$this->db->from($this->table_dokumen_upload.' as a');
		
		if(!empty($permohonan_id)){
			$this->db->where("a.permohonan_id = '".$permohonan_id."'");
		}else{
			$this->db->where("a.permohonan_id = -1'");
		}
		
		$this->db->where("a.is_deleted = 0");
		
		
		$this->db->order_by("a.id","DESC");
		
		$get_data = $this->db->get();
		
		$data_upload = array();
		if($get_data->num_rows() > 0){
			foreach($get_data->result() as $dt_upload){
				
				$data_upload[] = $dt_upload;
			}
		}
		
		$dt_return = $data_upload;
		
		return $dt_return;
	}
	
	function data_permohonan($params){
		
		if(!empty($params)){
			extract($params);
		}else{
			
			$dt_return = array();
			
			return $dt_return;
		}
		
		$this->db->select('a.*, b.nama_sponsor, c.sudah_dideportasi');
		$this->db->from($this->table_permohonan.' as a');
		$this->db->join($this->table_sponsor.' as b',"b.permohonan_id = a.id","LEFT");
		$this->db->join($this->table_tindakan.' as c',"c.permohonan_id = a.id","LEFT");
		
		if(!empty($keyword)){
			$this->db->where("(a.nama_wna LIKE '%".$keyword."%' OR a.niora LIKE '%".$keyword."%' OR a.no_paspor LIKE '%".$keyword."%' OR a.kebangsaan LIKE '%".$keyword."%')");
		}
		
		if(!empty($tipe_instansi)){
			$this->db->where("a.tipe_instansi = '".$tipe_instansi."'");
		}
		
		$this->db->where("a.is_deleted = 0");
		
		
		if(!empty($tanggal_dari) AND !empty($tanggal_sampai)){
			$this->db->where("(a.tanggal_permohonan >= '".$tanggal_dari."' AND a.tanggal_permohonan <= '".$tanggal_sampai."')");
			$this->db->order_by("a.tanggal_permohonan","DESC");
		}
		
		if(!empty($tahun_pencarian)){
			$this->db->where("(a.tanggal_permohonan>= '01-01-".$tahun_pencarian."' AND a.tanggal_permohonan <= '31-12-".$tahun_pencarian."')");
			$this->db->order_by("a.tanggal_permohonan","DESC");
		}
		
		$this->db->order_by("a.id","DESC");
		
		$get_permohonan = $this->db->get();
		
		$data_permohonan = array();
		if($get_permohonan->num_rows() > 0){
			foreach($get_permohonan->result() as $dt_permohonan){
				
				$data_permohonan[] = $dt_permohonan;
			}
		}
		
		$dt_return = $data_permohonan;
		
		return $dt_return;
	}
	
	function data_tindakan($params){
		
		if(!empty($params)){
			extract($params);
		}else{
			
			$dt_return = array();
			
			return $dt_return;
		}
		
		$this->db->select('a.*, b.tanggal_permohonan, b.nama_wna, b.tempat_lahir, b.kebangsaan, b.no_paspor, b.tipe_instansi, c.nama_sponsor, d.izin_tinggal_ke, d.berlaku_sd as izin_tinggal_sd');
		$this->db->from($this->table_tindakan.' as a');
		$this->db->join($this->table_permohonan.' as b',"b.id = a.permohonan_id","LEFT");
		$this->db->join($this->table_sponsor.' as c',"c.permohonan_id = b.id","LEFT");
		$this->db->join($this->table_izin_tinggal.' as d',"d.permohonan_id = b.id","LEFT");
		
		if(!empty($keyword)){
			$this->db->where("(b.nama_wna LIKE '%".$keyword."%' OR b.no_paspor LIKE '%".$keyword."%' OR b.kebangsaan LIKE '%".$keyword."%')");
		}
		
		$this->db->where("(a.tindakan_a = 1 OR a.tindakan_b = 1 OR a.tindakan_c = 1 OR a.tindakan_d = 1 OR a.tindakan_e = 1 OR a.tindakan_f = 1)");
		$this->db->where("b.is_deleted = 0");
		
		
		if(!empty($tanggal_dari) AND !empty($tanggal_sampai)){
			$this->db->where("(a.tanggal_deportasi >= '".$tanggal_dari."' AND a.tanggal_deportasi <= '".$tanggal_sampai."')");
			$this->db->order_by("a.tanggal_deportasi","DESC");
		}
		
		if(!empty($tahun_pencarian)){
			$this->db->where("(a.tanggal_deportasi>= '01-01-".$tahun_pencarian."' AND a.tanggal_deportasi <= '31-12-".$tahun_pencarian."')");
			$this->db->order_by("a.tanggal_deportasi","DESC");
		}
		
		$this->db->order_by("a.id","DESC");
		
		$get_tindakan = $this->db->get();
		
		$data_tindakan = array();
		if($get_tindakan->num_rows() > 0){
			foreach($get_tindakan->result() as $dt_tindakan){
				
				$data_tindakan[] = $dt_tindakan;
			}
		}
		
		$dt_return = $data_tindakan;
		
		return $dt_return;
	}
	
	function addUpdate(){
		
		$login_data = get_login_data();
		
		$no_permohonan = $this->input->post('no_permohonan', true);
		$get_tanggal_permohonan = $this->input->post('tanggal_permohonan', true);
		$nama_wna = $this->input->post('nama_wna', true);
		$tempat_lahir = $this->input->post('tempat_lahir', true);
		$get_tanggal_lahir = $this->input->post('tanggal_lahir', true);
		$kebangsaan = $this->input->post('kebangsaan', true);
		$no_paspor = $this->input->post('no_paspor', true);
		$jenis_kelamin = $this->input->post('jenis_kelamin', true);
		$alamat = $this->input->post('alamat', true);
		
		$catatan = $this->input->post('catatan', true);
		$niora = $this->input->post('niora', true);
		$produk = $this->input->post('produk', true);
		$pekerjaan = $this->input->post('pekerjaan', true);
		$status_sipil = $this->input->post('status_sipil', true);
		$paspor_diberikan_tanggal = $this->input->post('paspor_diberikan_tanggal', true);
		$paspor_diberikan_di = $this->input->post('paspor_diberikan_di', true);
		$paspor_berlaku_sampai = $this->input->post('paspor_berlaku_sampai', true);
		$kota = $this->input->post('kota', true);
		$kantor_imigrasi = $this->input->post('kantor_imigrasi', true);
		$kantor_wilayah = $this->input->post('kantor_wilayah', true);
		
		$is_edit = $this->input->post('is_edit', true);
		$id_bebas = $this->input->post('id_bebas', true);
		$permohonan_id = $this->input->post('permohonan_id', true);
		$tipe_instansi = $this->input->post('tipe_instansi', true);
		
		$tanggal_permohonan = date("Y-m-d",strtotime($get_tanggal_permohonan));
		$tanggal_lahir = date("Y-m-d",strtotime($get_tanggal_lahir));
		$paspor_diberikan_tanggal = date("Y-m-d",strtotime($paspor_diberikan_tanggal));
		$paspor_berlaku_sampai = date("Y-m-d",strtotime($paspor_berlaku_sampai));
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Simpan Data Permohonan Gagal!'
		);
		
		if(empty($is_edit)){
			$save_data = array(	
				'id_bebas'=> $id_bebas,
				'no_permohonan'=> $no_permohonan,
				'tanggal_permohonan'=> $tanggal_permohonan,
				'nama_wna'		=> $nama_wna,
				'tempat_lahir'	=> $tempat_lahir,
				'tanggal_lahir'	=> $tanggal_lahir,
				'kebangsaan'	=> $kebangsaan,
				'no_paspor'		=> $no_paspor,
				'jenis_kelamin'	=> $jenis_kelamin,
				'alamat'		=> $alamat,
				'niora'			=> $niora,
				'produk'		=> $produk,
				'catatan'		=> $catatan,
				'tipe_instansi'	=> $tipe_instansi,
				'pekerjaan'		=> $pekerjaan,
				'status_sipil'	=> $status_sipil,
				'paspor_diberikan_tanggal'	=> $paspor_diberikan_tanggal,
				'paspor_diberikan_di'	=> $paspor_diberikan_di,
				'paspor_berlaku_sampai'	=> $paspor_berlaku_sampai,
				'kota'				=> $kota,
				'kantor_imigrasi'	=> $kantor_imigrasi,
				'kantor_wilayah'	=> $kantor_wilayah,
				'createdby'	=> $login_data['user_username'],
				'created'	=> date("Y-m-d H:i:s"),
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			$save = $this->db->insert($this->table_permohonan, $save_data);
			if($save){			
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Data Permohonan Tersimpan!',
					'permohonan_id'	=> $this->db->insert_id()
				);
				
				//update ke bebas is_used = 1
				$update_bebas = array('is_used' => 1);
				$this->db->update($this->table_bebas, $update_bebas, "id = ".$id_bebas." AND tipe_instansi = '".$tipe_instansi."'");
				
			}
		}else{
			$save_data = array(	
				'id_bebas'=> $id_bebas,
				'no_permohonan'=> $no_permohonan,
				'tanggal_permohonan'=> $tanggal_permohonan,
				'nama_wna'		=> $nama_wna,
				'tempat_lahir'	=> $tempat_lahir,
				'tanggal_lahir'	=> $tanggal_lahir,
				'kebangsaan'	=> $kebangsaan,
				'no_paspor'		=> $no_paspor,
				'jenis_kelamin'	=> $jenis_kelamin,
				'alamat'		=> $alamat,
				'niora'			=> $niora,
				'produk'		=> $produk,
				'catatan'		=> $catatan,
				'pekerjaan'		=> $pekerjaan,
				'status_sipil'	=> $status_sipil,
				'paspor_diberikan_tanggal'	=> $paspor_diberikan_tanggal,
				'paspor_diberikan_di'	=> $paspor_diberikan_di,
				'paspor_berlaku_sampai'	=> $paspor_berlaku_sampai,
				'kota'				=> $kota,
				'kantor_imigrasi'	=> $kantor_imigrasi,
				'kantor_wilayah'	=> $kantor_wilayah,
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			
			if(!empty($permohonan_id)){
				$save = $this->db->update($this->table_permohonan, $save_data, "id = ".$permohonan_id." AND tipe_instansi = '".$tipe_instansi."'");
				
				if($save){			
					$data_ret = array(
						'success'	=> true,
						'info'	=> 'Data Permohonan Sudah Diubah!',
						'permohonan_id'	=> $permohonan_id
					);
				}
				
				
				//update ke bebas is_used = 1
				$update_bebas = array('is_used' => 1);
				$this->db->update($this->table_bebas, $update_bebas, "id = ".$id_bebas." AND tipe_instansi = '".$tipe_instansi."'");
				
				
			}else{
				$data_ret = array(
					'success'	=> false,
					'info'	=> 'Ubah Data Permohonan Gagal!'
				);
			}
			
		}
		
		return $data_ret;
	}
	
	function addDataSponsor(){
		
		$login_data = get_login_data();
		
		$jenis_sponsor = $this->input->post('jenis_sponsor', true);
		$status_perusahaan = $this->input->post('status_perusahaan', true);
		$sektor_kegiatan = $this->input->post('sektor_kegiatan', true);
		$nama_sponsor = $this->input->post('nama_sponsor', true);
		$nama_pimpinan = $this->input->post('nama_pimpinan', true);
		$alamat_sponsor = $this->input->post('alamat_sponsor', true);
		$kota_sponsor = $this->input->post('kota_sponsor', true);
		$kodepos_sponsor = $this->input->post('kodepos_sponsor', true);
		$telp_sponsor = $this->input->post('telp_sponsor', true);
		
		$permohonan_id = $this->input->post('permohonan_id', true);
		$sponsor_id = $this->input->post('sponsor_id', true);
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Simpan Data Sponsor Gagal!'
		);
		
		if(empty($sponsor_id)){
			$save_data = array(	
				'permohonan_id'=> $permohonan_id,
				'jenis_sponsor'=> $jenis_sponsor,
				'status_perusahaan'		=> $status_perusahaan,
				'sektor_kegiatan'	=> $sektor_kegiatan,
				'nama_sponsor'	=> $nama_sponsor,
				'nama_pimpinan'	=> $nama_pimpinan,
				'alamat_sponsor'		=> $alamat_sponsor,
				'kota_sponsor'	=> $kota_sponsor,
				'kodepos_sponsor'		=> $kodepos_sponsor,
				'telp_sponsor'			=> $telp_sponsor,
				'createdby'	=> $login_data['user_username'],
				'created'	=> date("Y-m-d H:i:s"),
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			$save = $this->db->insert($this->table_sponsor, $save_data);
			if($save){			
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Data Sponsor Tersimpan!',
					'sponsor_id'	=> $this->db->insert_id()
				);
				
			}
		}else{
			$save_data = array(	
				'jenis_sponsor'=> $jenis_sponsor,
				'status_perusahaan'		=> $status_perusahaan,
				'sektor_kegiatan'	=> $sektor_kegiatan,
				'nama_sponsor'	=> $nama_sponsor,
				'nama_pimpinan'	=> $nama_pimpinan,
				'alamat_sponsor'		=> $alamat_sponsor,
				'kota_sponsor'	=> $kota_sponsor,
				'kodepos_sponsor'		=> $kodepos_sponsor,
				'telp_sponsor'			=> $telp_sponsor,
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			
			if(!empty($sponsor_id)){
				$save = $this->db->update($this->table_sponsor, $save_data, "id = ".$sponsor_id." AND permohonan_id = '".$permohonan_id."'");
				
				if($save){			
					$data_ret = array(
						'success'	=> true,
						'info'	=> 'Data Sponsor Sudah Diubah!',
						'sponsor_id'	=> $sponsor_id
					);
				}
				
			}else{
				$data_ret = array(
					'success'	=> false,
					'info'	=> 'Ubah Data Sponsor Gagal!'
				);
			}
			
		}
		
		return $data_ret;
	}
	
	function addDataPendaratan(){
		
		$login_data = get_login_data();
		
		$no_visa = $this->input->post('no_visa', true);
		$tempat_pengeluaran = $this->input->post('tempat_pengeluaran', true);
		$tanggal_pengeluaran = $this->input->post('tanggal_pengeluaran', true);
		$berlaku_sd = $this->input->post('berlaku_sd', true);
		$tempat_masuk = $this->input->post('tempat_masuk', true);
		$tanggal_masuk = $this->input->post('tanggal_masuk', true);
		
		$permohonan_id = $this->input->post('permohonan_id', true);
		$pendaratan_id = $this->input->post('pendaratan_id', true);
		
		$tanggal_pengeluaran = date("Y-m-d",strtotime($tanggal_pengeluaran));
		$berlaku_sd = date("Y-m-d",strtotime($berlaku_sd));
		$tanggal_masuk = date("Y-m-d",strtotime($tanggal_masuk));
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Simpan Data Pendaratan Gagal!'
		);
		
		if(empty($pendaratan_id)){
			$save_data = array(	
				'permohonan_id'=> $permohonan_id,
				'no_visa'=> $no_visa,
				'tempat_pengeluaran'	=> $tempat_pengeluaran,
				'tanggal_pengeluaran'	=> $tanggal_pengeluaran,
				'berlaku_sd'	=> $berlaku_sd,
				'tempat_masuk'	=> $tempat_masuk,
				'tanggal_masuk'		=> $tanggal_masuk,
				'createdby'	=> $login_data['user_username'],
				'created'	=> date("Y-m-d H:i:s"),
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			$save = $this->db->insert($this->table_pendaratan, $save_data);
			if($save){			
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Data Pendaratan Tersimpan!',
					'pendaratan_id'	=> $this->db->insert_id()
				);
				
			}
		}else{
			$save_data = array(	
				'no_visa'=> $no_visa,
				'tempat_pengeluaran'	=> $tempat_pengeluaran,
				'tanggal_pengeluaran'	=> $tanggal_pengeluaran,
				'berlaku_sd'	=> $berlaku_sd,
				'tempat_masuk'	=> $tempat_masuk,
				'tanggal_masuk'		=> $tanggal_masuk,
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			
			if(!empty($pendaratan_id)){
				$save = $this->db->update($this->table_pendaratan, $save_data, "id = ".$pendaratan_id." AND permohonan_id = '".$permohonan_id."'");
				
				if($save){			
					$data_ret = array(
						'success'	=> true,
						'info'	=> 'Data Pendaratan Sudah Diubah!',
						'pendaratan_id'	=> $pendaratan_id
					);
				}
				
			}else{
				$data_ret = array(
					'success'	=> false,
					'info'	=> 'Ubah Data Pendaratan Gagal!'
				);
			}
			
		}
		
		return $data_ret;
	}
	
	function addDataIzin(){
		
		$login_data = get_login_data();
		
		$no_register = $this->input->post('no_register', true);
		$izin_tinggal_ke = $this->input->post('izin_tinggal_ke', true);
		$tanggal_pengeluaran = $this->input->post('tanggal_pengeluaran', true);
		$berlaku_sd = $this->input->post('berlaku_sd', true);
		
		$permohonan_id = $this->input->post('permohonan_id', true);
		$izin_id = $this->input->post('izin_id', true);
		
		$tanggal_pengeluaran = date("Y-m-d",strtotime($tanggal_pengeluaran));
		$berlaku_sd = date("Y-m-d",strtotime($berlaku_sd));
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Simpan Data Izin Tinggal Gagal!'
		);
		
		if(empty($izin_id)){
			$save_data = array(	
				'permohonan_id'=> $permohonan_id,
				'no_register'=> $no_register,
				'izin_tinggal_ke'	=> $izin_tinggal_ke,
				'tanggal_pengeluaran'	=> $tanggal_pengeluaran,
				'berlaku_sd'	=> $berlaku_sd,
				'createdby'	=> $login_data['user_username'],
				'created'	=> date("Y-m-d H:i:s"),
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			$save = $this->db->insert($this->table_izin_tinggal, $save_data);
			if($save){			
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Data Izin Tinggal Tersimpan!',
					'izin_id'	=> $this->db->insert_id()
				);
				
			}
		}else{
			$save_data = array(	
				'no_register'=> $no_register,
				'izin_tinggal_ke'	=> $izin_tinggal_ke,
				'tanggal_pengeluaran'	=> $tanggal_pengeluaran,
				'berlaku_sd'	=> $berlaku_sd,
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			
			if(!empty($izin_id)){
				$save = $this->db->update($this->table_izin_tinggal, $save_data, "id = ".$izin_id." AND permohonan_id = '".$permohonan_id."'");
				
				if($save){			
					$data_ret = array(
						'success'	=> true,
						'info'	=> 'Data Izin Tinggal Sudah Diubah!',
						'izin_id'	=> $izin_id
					);
				}
				
			}else{
				$data_ret = array(
					'success'	=> false,
					'info'	=> 'Ubah Data Izin Tinggal Gagal!'
				);
			}
			
		}
		
		return $data_ret;
	}
	
	
	function addDataTindakan(){
		
		$login_data = get_login_data();
		
		$pelanggaran_pasal = $this->input->post('pelanggaran_pasal', true);
		$sumber_kasus = $this->input->post('sumber_kasus', true);
		$tindakan_a = $this->input->post('tindakan_a', true);
		$tindakan_b = $this->input->post('tindakan_b', true);
		$tindakan_c = $this->input->post('tindakan_c', true);
		$tindakan_d = $this->input->post('tindakan_d', true);
		$tindakan_e = $this->input->post('tindakan_e', true);
		$tindakan_f = $this->input->post('tindakan_f', true);
		$tanggal_deportasi = $this->input->post('tanggal_deportasi', true);
		$sudah_dideportasi = $this->input->post('sudah_dideportasi', true);
		$pengajuan_keberatan = $this->input->post('pengajuan_keberatan', true);
		
		$permohonan_id = $this->input->post('permohonan_id', true);
		$tindakan_id = $this->input->post('tindakan_id', true);
		
		$tanggal_deportasi = date("Y-m-d",strtotime($tanggal_deportasi));
		
		if(empty($sudah_dideportasi)){
			$sudah_dideportasi = 0;
		}
		if(empty($tindakan_a)){
			$tindakan_a = 0;
		}
		if(empty($tindakan_b)){
			$tindakan_b = 0;
		}
		if(empty($tindakan_c)){
			$tindakan_c = 0;
		}
		if(empty($tindakan_e)){
			$tindakan_e = 0;
		}
		if(empty($tindakan_d)){
			$tindakan_d = 0;
		}
		if(empty($tindakan_f)){
			$tindakan_f = 0;
		}
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Simpan Data Tindakan Gagal!'
		);
		
		if(empty($tindakan_id)){
			$save_data = array(	
				'permohonan_id'=> $permohonan_id,
				'pelanggaran_pasal'=> $pelanggaran_pasal,
				'sumber_kasus'	=> $sumber_kasus,
				'tindakan_a'	=> $tindakan_a,
				'tindakan_b'	=> $tindakan_b,
				'tindakan_c'	=> $tindakan_c,
				'tindakan_d'	=> $tindakan_d,
				'tindakan_e'	=> $tindakan_e,
				'tindakan_f'	=> $tindakan_f,
				'tanggal_deportasi'	=> $tanggal_deportasi,
				'pengajuan_keberatan'	=> $pengajuan_keberatan,
				'sudah_dideportasi'	=> $sudah_dideportasi,
				'createdby'	=> $login_data['user_username'],
				'created'	=> date("Y-m-d H:i:s"),
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			$save = $this->db->insert($this->table_tindakan, $save_data);
			if($save){			
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Data Tindakan Tersimpan!',
					'tindakan_id'	=> $this->db->insert_id()
				);
				
			}
		}else{
			$save_data = array(	
				'pelanggaran_pasal'=> $pelanggaran_pasal,
				'sumber_kasus'	=> $sumber_kasus,
				'tindakan_a'	=> $tindakan_a,
				'tindakan_b'	=> $tindakan_b,
				'tindakan_c'	=> $tindakan_c,
				'tindakan_d'	=> $tindakan_d,
				'tindakan_e'	=> $tindakan_e,
				'tindakan_f'	=> $tindakan_f,
				'tanggal_deportasi'	=> $tanggal_deportasi,
				'pengajuan_keberatan'	=> $pengajuan_keberatan,
				'sudah_dideportasi'	=> $sudah_dideportasi,
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			
			if(!empty($tindakan_id)){
				$save = $this->db->update($this->table_tindakan, $save_data, "id = ".$tindakan_id." AND permohonan_id = '".$permohonan_id."'");
				
				if($save){			
					$data_ret = array(
						'success'	=> true,
						'info'	=> 'Data Tindakan Sudah Diubah!',
						'tindakan_id'	=> $tindakan_id
					);
				}
				
			}else{
				$data_ret = array(
					'success'	=> false,
					'info'	=> 'Ubah Data Tindakan Gagal!'
				);
			}
			
		}
		
		return $data_ret;
	}
	
	function addDataDokumen(){
		
		$login_data = get_login_data();
		
		$laporan_kejadian = $this->input->post('laporan_kejadian', true);
		$berita_acara_instansi_lain = $this->input->post('berita_acara_instansi_lain', true);
		$berita_acara_pemeriksaan = $this->input->post('berita_acara_pemeriksaan', true);
		$berita_acara_pendapat = $this->input->post('berita_acara_pendapat', true);
		$keputusan_kakanim = $this->input->post('keputusan_kakanim', true);
		$surat_perintah_pendetensian = $this->input->post('surat_perintah_pendetensian', true);
		$surat_perintah_pengeluaran_pendetensian = $this->input->post('surat_perintah_pengeluaran_pendetensian', true);
		$berita_acara_pengeluaran_pendetensian = $this->input->post('berita_acara_pengeluaran_pendetensian', true);
		$surat_perintah_tugas = $this->input->post('surat_perintah_tugas', true);
		$surat_perintah_pengawasan_keberangkatan = $this->input->post('surat_perintah_pengawasan_keberangkatan', true);
		
		$permohonan_id = $this->input->post('permohonan_id', true);
		$dokumen_id = $this->input->post('dokumen_id', true);
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Simpan Data Kelengkapan Dokumen Gagal!'
		);
		
		if(empty($dokumen_id)){
			$save_data = array(	
				'permohonan_id'=> $permohonan_id,
				'laporan_kejadian'=> $laporan_kejadian,
				'berita_acara_instansi_lain'	=> $berita_acara_instansi_lain,
				'berita_acara_pemeriksaan'	=> $berita_acara_pemeriksaan,
				'berita_acara_pendapat'	=> $berita_acara_pendapat,
				'keputusan_kakanim'	=> $keputusan_kakanim,
				'surat_perintah_pendetensian'	=> $surat_perintah_pendetensian,
				'surat_perintah_pengeluaran_pendetensian'	=> $surat_perintah_pengeluaran_pendetensian,
				'berita_acara_pengeluaran_pendetensian'	=> $berita_acara_pengeluaran_pendetensian,
				'surat_perintah_tugas'	=> $surat_perintah_tugas,
				'surat_perintah_pengawasan_keberangkatan'	=> $surat_perintah_pengawasan_keberangkatan,
				'createdby'	=> $login_data['user_username'],
				'created'	=> date("Y-m-d H:i:s"),
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			$save = $this->db->insert($this->table_dokumen, $save_data);
			if($save){			
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Data Kelengkapan Dokumen Tersimpan!',
					'dokumen_id'	=> $this->db->insert_id()
				);
				
			}
		}else{
			$save_data = array(	
				'laporan_kejadian'=> $laporan_kejadian,
				'berita_acara_instansi_lain'	=> $berita_acara_instansi_lain,
				'berita_acara_pemeriksaan'	=> $berita_acara_pemeriksaan,
				'berita_acara_pendapat'	=> $berita_acara_pendapat,
				'keputusan_kakanim'	=> $keputusan_kakanim,
				'surat_perintah_pendetensian'	=> $surat_perintah_pendetensian,
				'surat_perintah_pengeluaran_pendetensian'	=> $surat_perintah_pengeluaran_pendetensian,
				'berita_acara_pengeluaran_pendetensian'	=> $berita_acara_pengeluaran_pendetensian,
				'surat_perintah_tugas'	=> $surat_perintah_tugas,
				'surat_perintah_pengawasan_keberangkatan'	=> $surat_perintah_pengawasan_keberangkatan,
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			
			if(!empty($dokumen_id)){
				$save = $this->db->update($this->table_dokumen, $save_data, "id = ".$dokumen_id." AND permohonan_id = '".$permohonan_id."'");
				
				if($save){			
					$data_ret = array(
						'success'	=> true,
						'info'	=> 'Data Kelengkapan Dokumen Sudah Diubah!',
						'dokumen_id'	=> $dokumen_id
					);
				}
				
			}else{
				$data_ret = array(
					'success'	=> false,
					'info'	=> 'Ubah Data Kelengkapan Dokumen Gagal!'
				);
			}
			
		}
		
		return $data_ret;
	}
	
	function cekBebas(){
		
		$data_ret = array(
			'success'	=> false,
			'items' => array()
		);
		
		$login_data = get_login_data();
		
		$cari = $this->input->post_get('q', true);
		
		$this->db->select('a.*');
		$this->db->from($this->table_bebas.' as a');
		
		if(!empty($cari)){
			$this->db->where("a.nama_wna LIKE '%".$cari."%' OR a.no_paspor LIKE '%".$cari."%'");
		}
		
		$this->db->where("a.is_deleted = 0");
		$this->db->where("a.is_used = 0");
		
		
		$load = $this->db->get();
		
		$all_item = array();
		if($load->num_rows() > 0){	

			foreach($load->result() as $dt){
				
				$dt->text = $dt->nama_wna.' / '.$dt->no_paspor;
				
				$dt->tanggal_lahir = date("d-m-Y",strtotime($dt->tanggal_lahir));
			
				$all_item[] = $dt;
			}
			
			
			
			$data_ret = array(
				'success'	=> true,
				'items'	=> $all_item
			);
		}
		
		return $data_ret;
		
	}
	
	function notifBebas(){
		
		$data_ret = array(
			'success'	=> false,
			'info_notif' => '',
			'stop' => 0
		);
		
		$login_data = get_login_data();
		
		$this->db->select('a.*');
		$this->db->from($this->table_bebas.' as a');
		$this->db->where("a.is_deleted = 0");
		$this->db->where("a.is_used = 0");
		$this->db->where("a.is_show = 0");
		$this->db->order_by("a.id","ASC");
		
		$load = $this->db->get();
		
		$all_item = array();
		if($load->num_rows() > 0){	
			
			$max_data = 10;
			$no = 0;
			$all_data = array();
			foreach($load->result() as $dt){
				$no++;
				$all_data[$no] = $dt;
			}
			
			//random
			$get_id = rand(1, $no);
			$get_data = $all_data[$get_id];
			
			if(!empty($get_data->id)){
				
				//$update_show = array("is_show" => 1);
				//$this->db->update($this->table_bebas, $update_show, "id = ".$get_data->id);
				
				$info_notif = '<b>'.$get_data->tipe_instansi.': '.$get_data->no_berita_bebas.'</b><br/>';
				$info_notif .= 'Nama WNA: '.$get_data->nama_wna.'<br/>';
				$info_notif .= 'No.Paspor: '.$get_data->no_paspor.'<br/>';
				$info_notif .= 'Data sudah bisa dibuat permohonan<br/><br/>';
				$info_notif .= '<a class="btn btn-sm btn-success" href="javascript:hideNotif('.$get_data->id.')"> <i class="fa fa-check"></i> OK - Konfirmasi Notifikasi Sudah Dibaca</a><br/>';
				$info_notif .= '<a class="close-notif-'.$get_data->id.'" data-dismiss="alert" aria-hidden="true"></a>';
				
				$data_ret = array(
					'success'	=> true,
					'info_notif'	=> $info_notif,
					'stop' => 0
				);
				
			}
		}
		
		return $data_ret;
		
	}
	
	function hideNotif($xid = ''){
		
		$bebas_id = $this->input->post_get('bebas_id', true);
		
		if(!empty($bebas_id)){
			$update_show = array("is_show" => 1);
			$this->db->update($this->table_bebas, $update_show, "id = ".$bebas_id);
		}
		
		$data_ret = array(
			'success'	=> true
		);
		
		return $data_ret;
	}
	
	function loadData($xid = ''){
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Load Data Gagal!'
		);
		
		$login_data = get_login_data();
		
		$permohonan_id = $this->input->post('permohonan_id', true);
		
		if(!empty($xid)){
			$permohonan_id = $xid;
		}
		
		if(!empty($permohonan_id)){
			
			$this->db->select('a.*');
			$this->db->from($this->table_permohonan.' as a');
			$this->db->where("a.id = ".$permohonan_id);
			$this->db->where("a.is_deleted = 0");
			
			$load = $this->db->get();
			
			if($load->num_rows() > 0){	
	
				$dt_permohonan = $load->row();
				
				$dt_permohonan->tanggal_permohonan = date("d-m-Y",strtotime($dt_permohonan->tanggal_permohonan));
				$dt_permohonan->tanggal_lahir = date("d-m-Y",strtotime($dt_permohonan->tanggal_lahir));
				
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Load Data Selesai!',
					'data'	=> $dt_permohonan
				);
			}
			
		}else{
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Load Data Gagal!',
				'data'	=> array()
			);
		}
		
		return $data_ret;
		
	}
	
	function loadDataDetail($xid = ''){
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Load Data Gagal!'
		);
		
		$login_data = get_login_data();
		
		$permohonan_id = $this->input->post('permohonan_id', true);
		
		if(!empty($xid)){
			$permohonan_id = $xid;
		}
		
		if(!empty($permohonan_id)){
			
			$this->db->select('a.*');
			$this->db->from($this->table_permohonan.' as a');
			$this->db->where("a.id = ".$permohonan_id);
			$this->db->where("a.is_deleted = 0");
			
			$load = $this->db->get();
			
			$dt_permohonan = array();
			if($load->num_rows() > 0){	
	
				$dt_permohonan = $load->row();
				
				$dt_permohonan->tanggal_permohonan = date("d-m-Y",strtotime($dt_permohonan->tanggal_permohonan));
				$dt_permohonan->tanggal_lahir = date("d-m-Y",strtotime($dt_permohonan->tanggal_lahir));
				
				if(!empty($dt_permohonan->paspor_diberikan_tanggal) AND $dt_permohonan->paspor_diberikan_tanggal != '1970-01-01'){
					$dt_permohonan->paspor_diberikan_tanggal = date("d-m-Y",strtotime($dt_permohonan->paspor_diberikan_tanggal));
				}else{
					$dt_permohonan->paspor_diberikan_tanggal = '';
				}
				
				if(!empty($dt_permohonan->paspor_berlaku_sampai) AND $dt_permohonan->paspor_berlaku_sampai != '1970-01-01'){
					$dt_permohonan->paspor_berlaku_sampai = date("d-m-Y",strtotime($dt_permohonan->paspor_berlaku_sampai));
				}else{
					$dt_permohonan->paspor_berlaku_sampai = '';
				}
			}
			
			$this->db->select('a.*');
			$this->db->from($this->table_sponsor.' as a');
			$this->db->where("a.permohonan_id = ".$permohonan_id);
			$this->db->where("a.is_deleted = 0");
			$load_sponsor = $this->db->get();
			
			$dt_sponsor = array();
			if($load_sponsor->num_rows() > 0){
				$dt_sponsor = $load_sponsor->row();
				$dt_sponsor->sponsor_id = $dt_sponsor->id;
			}
				
			$this->db->select('a.*');
			$this->db->from($this->table_pendaratan.' as a');
			$this->db->where("a.permohonan_id = ".$permohonan_id);
			$this->db->where("a.is_deleted = 0");
			$load_pendaratan = $this->db->get();
			
			$dt_pendaratan = array();
			if($load_pendaratan->num_rows() > 0){
				$dt_pendaratan = $load_pendaratan->row();
				$dt_pendaratan->pendaratan_id = $dt_pendaratan->id;
				
				if(!empty($dt_pendaratan->tanggal_pengeluaran)){
					$dt_pendaratan->tanggal_pengeluaran = date("d-m-Y",strtotime($dt_pendaratan->tanggal_pengeluaran));
				}
				
				if(!empty($dt_pendaratan->berlaku_sd)){
					$dt_pendaratan->berlaku_sd = date("d-m-Y",strtotime($dt_pendaratan->berlaku_sd));
				}
				
				if(!empty($dt_pendaratan->tanggal_masuk)){
					$dt_pendaratan->tanggal_masuk = date("d-m-Y",strtotime($dt_pendaratan->tanggal_masuk));
				}
				
			}
				
				
			$this->db->select('a.*');
			$this->db->from($this->table_izin_tinggal.' as a');
			$this->db->where("a.permohonan_id = ".$permohonan_id);
			$this->db->where("a.is_deleted = 0");
			$load_izin_tinggal = $this->db->get();
			
			$dt_izin_tinggal = array();
			if($load_izin_tinggal->num_rows() > 0){
				$dt_izin_tinggal = $load_izin_tinggal->row();
				$dt_izin_tinggal->izin_id = $dt_izin_tinggal->id;
				
				
				if(!empty($dt_izin_tinggal->tanggal_pengeluaran)){
					$dt_izin_tinggal->tanggal_pengeluaran = date("d-m-Y",strtotime($dt_izin_tinggal->tanggal_pengeluaran));
				}
				
				if(!empty($dt_izin_tinggal->berlaku_sd)){
					$dt_izin_tinggal->berlaku_sd = date("d-m-Y",strtotime($dt_izin_tinggal->berlaku_sd));
				}
			}
				
			//TINDAKAN
			$this->db->select('a.*');
			$this->db->from($this->table_tindakan.' as a');
			$this->db->where("a.permohonan_id = ".$permohonan_id);
			$this->db->where("a.is_deleted = 0");
			$load_tindakan = $this->db->get();
			
			$dt_tindakan = array();
			if($load_tindakan->num_rows() > 0){
				$dt_tindakan = $load_tindakan->row();
				$dt_tindakan->tindakan_id = $dt_tindakan->id;
				
				
				if(!empty($dt_tindakan->tanggal_deportasi)){
					$dt_tindakan->tanggal_deportasi = date("d-m-Y",strtotime($dt_tindakan->tanggal_deportasi));
				}
				
			}
				
				
			//DOKUMEN
			$this->db->select('a.*');
			$this->db->from($this->table_dokumen.' as a');
			$this->db->where("a.permohonan_id = ".$permohonan_id);
			$this->db->where("a.is_deleted = 0");
			$load_dokumen = $this->db->get();
			
			$data_dokumen = array();
			if($load_dokumen->num_rows() > 0){
				$data_dokumen = $load_dokumen->row();
				$data_dokumen->dokumen_id = $data_dokumen->id;
				
			}
				
			$data_ret = array(
				'success'	=> true,
				'info'	=> 'Load Data Selesai!',
				'data'	=> $dt_permohonan,
				'data_sponsor'	=> $dt_sponsor,
				'data_pendaratan'	=> $dt_pendaratan,
				'data_izin'	=> $dt_izin_tinggal,
				'data_tindakan'	=> $dt_tindakan,
				'data_dokumen'	=> $data_dokumen
			);
			
		}else{
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Load Data Gagal!',
				'data'	=> array()
			);
		}
		
		return $data_ret;
		
	}
	
	function deleteData(){
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'hapus Data Gagal!'
		);
		
		$login_data = get_login_data();
		
		$permohonan_id = $this->input->post('permohonan_id', true);
		
		if(!empty($permohonan_id)){
			
			$update_lapas = array('is_deleted' => 1);
			$save = $this->db->update($this->table_permohonan, $update_lapas, "id = ".$permohonan_id);
			
			if($save){			
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Data sudah di hapus!'
				);
			}
			
		}else{
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'hapus Data Gagal!'
			);
		}
		
		return $data_ret;
		
	}
	
	function deleteDokumen(){
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'hapus Data Gagal!'
		);
		
		$login_data = get_login_data();
		
		$dokumen_id = $this->input->post('dokumen_id', true);
		
		if(!empty($dokumen_id)){
			
			
			$this->db->from($this->table_dokumen_upload);
			$this->db->where("id",$dokumen_id);
			$get_dokumen = $this->db->get();
			
			if($get_dokumen->num_rows() > 0){
				$data_dokumen = $get_dokumen->row();
				
				$delete = $this->db->delete($this->table_dokumen_upload, "id = ".$dokumen_id);
				
				if($delete){	
					
					@unlink(UPLOAD_PATH.'dokumen/'.$data_dokumen->filename);
				
					$data_ret = array(
						'success'	=> true,
						'info'	=> 'File Dokumen di hapus!'
					);
				}
			}
			
		}else{
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'hapus Dokumen Gagal!'
			);
		}
		
		return $data_ret;
		
	}
	
}
