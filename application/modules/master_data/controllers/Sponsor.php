<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sponsor extends MY_Controller {
	
	protected $post_data = array();
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('model_sponsor', 'sponsor');
		$this->table_sponsor = $this->sponsor->table_sponsor;
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
		$login_data = get_login_data();
		$this->post_data['login_data'] = $login_data;
		
	}
	
	public function index()
	{
		
		$this->list_data();
		
	}
	
	public function list_data()
	{
		$post_data = $this->post_data;	

		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker3.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/datatables/jquery.dataTables.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.APP_URL.'libs/datatables/jquery.dataTables.min.js"></script>
			<script src="'.THEME_URL.'modules/master_data/js/sponsor.js"></script>
		';
	
		$this->load->view(THEME_VIEW_PATH.'modules/master_data/view/sponsor', $post_data);
	}
	
	public function load_list_data()
	{
		$keyword = $this->input->post('keyword', true);
		$is_user = $this->input->post_get('is_user', true);
		
		$result_excel = $this->input->post_get('result_excel', true);
		$result_print = $this->input->post_get('result_print', true);
		$show_responsive_mode = true;
		if(!empty($result_excel) OR !empty($result_print)){
			$show_responsive_mode = false;
		}
				
		$params = array(
			'keyword'	=> $keyword
		);
		$data_sponsor = $this->sponsor->data_sponsor($params); 
		
		$data_sponsor_html = '';
		$no = 0;
		if(!empty($data_sponsor)){
			foreach($data_sponsor as $dt){	
				
				$no++;
				$data_sponsor_html_detail = '<tr>';
				
				if($show_responsive_mode){
					$data_sponsor_html_detail .= '
					<td class="text-center">
						<input type="hidden" id="sponsor_id_'.$dt->id.'" value="'.$dt->id.'">
						<input type="hidden" id="sponsor_data_'.$dt->id.'" value="'.$dt->sponsor_data.'">
						<a class="btn btn-xs btn-success" href="javascript:update_sponsor('.$dt->id.');"><i class="fa fa-pencil"></i></a>	
						&nbsp;
						<a class="btn btn-xs btn-danger" href="javascript:delete_sponsor('.$dt->id.');"><i class="fa fa-remove"></i></a>
					</td>
					';
				}else{
					$data_sponsor_html_detail .= '
					<td class="text-center">'.$no.'</td>
					';
				}
				
				$data_sponsor_html_detail .= '
					<td class="hidden-xs"><small>'.$dt->sponsor_data.'</small></td>
					<td class="hidden-xs"><small>'.$dt->contact_person.'</small></td>
					<td class="hidden-xs"><small>'.$dt->mobile_phone.'</small></td>
					<td class="hidden-xs"><small>'.$dt->email.'</small></td>
					';
					
				if($show_responsive_mode){
					$data_sponsor_html_detail .= '
					<td class="visible-xs"><small>Sponsor: '.$dt->sponsor_data.'
						<br/>Kontak: '.$dt->contact_person.'
						<br/>Telp: '.$dt->mobile_phone.'
						<br/>Email: '.$dt->email.'
					</small></td>
					';
				}
				
				$data_sponsor_html_detail .= '
				</tr>
				';
				
				$data_sponsor_html .= $data_sponsor_html_detail;
			}
		}
		
		$html_display = '
		<table class="table table-bordered table-striped js-dataTable-full" id="table-sponsor">
			<thead>
				<tr>';
				
			if($show_responsive_mode){
				$html_display .= '
				<th class="hidden-xs text-center" width="90"><small>OPTION</small></th>
				';
			}else{
				$html_display .= '
				<th class="hidden-xs text-center" width="30"><small>NO</small></th>
				';
			}
			
			$html_display .= '
					<th class="hidden-xs" width="350"><small>SPONSOR</small></th>
					<th class="hidden-xs" width="120"><small>CONTACT PERSON</small></th>
					<th class="hidden-xs" width="120"><small>MOBILE PHONE</small></th>
					<th class="hidden-xs" width="150"><small>EMAIL</small></th>
			';		
				
			
			if($show_responsive_mode){
				$html_display .= '
				<th class="visible-xs"><small>DATA SPONSOR</small></th>
				';
			}
			
		$html_display .= '
				</tr>
			</thead>
			<tbody id="sponsor_data">
				'.$data_sponsor_html.'
			</tbody>
		</table>
		';
		
			
		
		if(!empty($result_excel) OR !empty($result_print)){
			
			if(!empty($result_excel)){
				$post_data['print_name'] = 'Data Sponsor';
				$post_data['file_name'] = url_title($post_data['print_name']);
				$post_data['html_display'] = $html_display;
				$this->load->view(THEME_VIEW_PATH.'modules/excel_layout', $post_data);
				
			}else{
				
				$post_data = $this->post_data;	
				$post_data['add_css_page'] = '
				';
				
				$post_data['add_js_page'] = '		
					<script>window.print();</script>
				';	
				
				$post_data['print_name'] = 'Data Sponsor';
				$post_data['html_display'] = $html_display;
				
				$this->load->view(THEME_VIEW_PATH.'modules/print_layout', $post_data);
			}
			
			
		}else{
			
			echo $html_display;
			die();
			
		}
	}
	
	public function add()
	{
		$data_ret = $this->sponsor->addUpdate();
		echo json_encode($data_ret);
	}
	
	public function loadSponsor()
	{
		$data_ret = $this->sponsor->loadSponsor();
		echo json_encode($data_ret);
	}
	
	public function deleteSponsor()
	{
		$data_ret = $this->sponsor->deleteSponsor();
		echo json_encode($data_ret);
	}
	
	public function import_data()
	{
		$post_data = $this->post_data;	

		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker3.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/datatables/jquery.dataTables.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.APP_URL.'libs/datatables/jquery.dataTables.min.js"></script>
			<script src="'.THEME_URL.'modules/master_data/js/sponsor_import_data.js"></script>
		';
	
		$this->load->view(THEME_VIEW_PATH.'modules/master_data/view/sponsor_import_data', $post_data);
	}
	
	public function upload_excel()
	{
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Import Excel Gagal!'
		);
		
		$login_data = get_login_data();
				
		$this->file_import_excel = UPLOAD_PATH.'import_excel/';
		
		$r = ''; 
		$is_upload_file = false;	
		
		if(!empty($_FILES['upload_file']['name'])){
			
			$config['upload_path'] = $this->file_import_excel;
			//$config['allowed_types'] = '*';
			$config['allowed_types'] = 'xls';
			$config['max_size']	= '102400000';

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload("upload_file"))
			{
				$data = $this->upload->display_errors();
				$r = array('success' => false, 'info' => $data );
				die(json_encode($r));
			}
			else
			{
				$is_upload_file = true;
				$data_upload_temp = $this->upload->data();
				
				if($data_upload_temp['file_ext'] != '.xls'){
					$file =  $this->file_import_excel.$data_upload_temp['file_name']."" ;
					unlink($file);
					$r = array('success' => false, 'info' => 'only file excel (.xls) is allowed');
					die(json_encode($r));
				}
				
				// Load the spreadsheet reader library
				$this->load->library('spreadsheet_excel_reader');
				$xls = new Spreadsheet_Excel_Reader();
				$xls->setOutputEncoding('CP1251'); 
				$file =  $this->file_import_excel.$data_upload_temp['file_name']."" ;
				$xls->read($file);
				
				if(!empty($file)){
					unlink($file);
				}
				
				//echo '<pre>';
				//print_r($xls->sheets);die();
				
				error_reporting(E_ALL ^ E_NOTICE);
				
				$nr_sheets = count($xls->sheets);    
				
				$total_cek_data = 0;
				$total_valid_data = 0;
				
				$detail_sponsor = array();
				$detail_sponsor_available = array();
				$detail_sponsor_not_available = array();
				$all_sponsor = array();
				$all_sponsor_valid = array();
				
				for($i=0; $i<$nr_sheets; $i++) {
					//echo $xls->boundsheets[$i]['name'];
					//print_r($xls->sheets[$i]);
					
					$total_sheet++;
					$NO_ID = '';
					$SPONSOR_ID = '';
					$CONTACT_PERSON_ID = '';
					$MOBILE_ID = '';
					$EMAIL_ID = '';
					
					for ($row_num = 1; $row_num <= $xls->sheets[$i]['numRows']; $row_num++) {	
						
						
						
						if(!empty($xls->sheets[$i]['cells'][$row_num]) AND $has_find_header == false){
							foreach($xls->sheets[$i]['cells'][$row_num] as $key => $val){
							
								//echo '<pre>';
								//print_r($xls->sheets[$i]['cells'][$row_num]);
								//die();
								
								switch($val){
									case 'NO.': $NO_ID = $key; $has_find_header = true;
										break;
									case 'SPONSOR': $SPONSOR_ID = $key; $has_find_header = true;
										break;
									case 'NO.TLP': $MOBILE_ID = $key; $has_find_header = true;
										break;
									case 'MOBILE PHONE': $MOBILE_ID = $key; $has_find_header2 = true;
										break;
									case 'NAMA': $CONTACT_PERSON_ID = $key; $has_find_header = true;
										break;
									case 'CONTACT_PERSON': $CONTACT_PERSON_ID = $key; $has_find_header2 = true;
										break;
									case 'EMAIL': $EMAIL_ID = $key; $has_find_header = true;
										break;
									
										
								}
								
							}
							
						}else
						if($has_find_header == true){
							
							if(empty($has_find_header2)){
								foreach($xls->sheets[$i]['cells'][$row_num] as $key => $val){
								
									switch($val){
										case 'NO.TLP': $MOBILE_ID = $key; $has_find_header2 = true;
											break;
										case 'MOBILE PHONE': $MOBILE_ID = $key; $has_find_header2 = true;
											break;
										case 'SPONSOR': $SPONSOR_ID = $key; $has_find_header = true;
											break;
										case 'NAMA': $CONTACT_PERSON_ID = $key; $has_find_header2 = true;
											break;
										case 'CONTACT_PERSON': $CONTACT_PERSON_ID = $key; $has_find_header2 = true;
											break;
										case 'EMAIL': $EMAIL_ID = $key; $has_find_header2 = true;
											break;
									}
									
								}
								
							}else{
								
								
							}
							
							//echo '<pre>';
							//print_r($xls->sheets[$i]['cells'][$row_num]);
							//die();
							
							$NO = trim($xls->sheets[$i]['cells'][$row_num][$NO_ID]);
							$SPONSOR = trim($xls->sheets[$i]['cells'][$row_num][$SPONSOR_ID]);
							$MOBILE = trim($xls->sheets[$i]['cells'][$row_num][$MOBILE_ID]);
							$CONTACT_PERSON = trim($xls->sheets[$i]['cells'][$row_num][$CONTACT_PERSON_ID]);
							$EMAIL = trim($xls->sheets[$i]['cells'][$row_num][$EMAIL_ID]);
							
										
							if(!empty($SPONSOR)){
								
								
								$proses_data = true;
								
								if($proses_data){
									
									$total_cek_data++;
									
									
									//spit sponsor
									
									if(!in_array($SPONSOR, $all_sponsor)){
										
										$total_valid_data++;
										
										$detail_sponsor[$total_valid_data] = array(
											'sponsor_data'		=> $SPONSOR,
											'mobile_phone'		=> $MOBILE,
											'contact_person'	=> $CONTACT_PERSON,
											'email'		=> $EMAIL,
											'created'	=> date("Y-m-d H:i:s"),
											'createdby'	=> $login_data['user_username'],
											'updated'	=> date("Y-m-d H:i:s"),
											'updatedby'	=> $login_data['user_username']
										);
										
										$all_sponsor[] = $SPONSOR;
										$all_sponsor_valid[$total_valid_data] = $SPONSOR;
										
									}else{
										$detail_sponsor_available[] = $SPONSOR;
									}
									
								}
								
							}
							
							
							//echo '<pre>';
							//print_r($xls->sheets[$i]['cells'][$row_num]);
							
						}
						
						
					}
					
				}    
				
				
				//echo 'TOTAL DATA = '.$total_valid_data.'<pre>';
				//print_r($detail_sponsor);
				//die();
				
				
				//CEK DATA ON table_sponsor DATA
				//where = ponsor_data
				$this->db->select('*');
				$this->db->from($this->table_sponsor);
				
				if(!empty($all_sponsor)){
					//$all_sponsor_txt = implode("','", $all_sponsor);
					//$this->db->where("sponsor_data IN ('".$all_sponsor_txt."')");
				}
				
				$get_data = $this->db->get();
				
				$all_update = array();
				$all_update_id = array();
				$all_update_var = array();
				if($get_data->num_rows() > 0){
					foreach($get_data->result() as $dt){
						
						$sponsor_data = $dt->sponsor_data;
						
						//cek array value
						$get_key = array_keys($all_sponsor_valid, $sponsor_data);
						
						if(!empty($get_key)){
							foreach($get_key as $nomor){
								if(!empty($detail_sponsor[$nomor])){
									
									if(!in_array($dt->id, $all_update_id)){
										
										$all_update_id[] = $dt->id;
										$all_update_var[] = $sponsor_data;
										
										$dt_detail_sponsor = $detail_sponsor[$nomor];
										$dt_detail_sponsor['id'] = $dt->id;
										
										unset($dt_detail_sponsor['created']);
										unset($dt_detail_sponsor['createdby']);
										
										$all_update[] = $dt_detail_sponsor;
										
									}
									
								}
							}
						}
						
						
						
					}
				}
				
				/*echo 'TOTAL DATA GET = '.$get_data->num_rows().'<pre>';
				echo 'TOTAL DATA = '.count($all_update).'<pre>';
				print_r($all_update);
				die();*/
				
				$all_insert = array();
				if(!empty($detail_sponsor)){
					foreach($detail_sponsor as $dt_det){
						
						$get_var = $dt_det['sponsor_data'];
						
						if(!in_array($get_var, $all_update_var)){
							//input new
							$all_insert[] = $dt_det;
						}else{
							//update
						}
					}
				}
				
				//echo 'TOTAL DATA = '.count($all_insert).'<pre>';
				//print_r($all_insert);
				//die();
				
				$q = true;
				$this->lib_trans->begin();
					
					
					if(!empty($all_update)){
						$q2 = $this->db->update_batch($this->table_sponsor, $all_update, "id");
					}
					
					if(!empty($all_insert)){
						$q1 = $this->db->insert_batch($this->table_sponsor, $all_insert);
						if($q1){
							//$q = true;
						}else{
							$q = false;
						}
					}
					
				$this->lib_trans->commit();	
				
				if($q)
				{ 
					$r = array(
						'success' => true, 
						'total_cek_data' => $total_cek_data, 
						'total_valid_data' => $total_valid_data, 
						'all_update' => count($all_update), 
						'all_insert' => count($all_insert), 
						'available' => count($detail_sponsor_available), 
						'dt_available' => $detail_sponsor_available, 
						'not_available' => count($detail_sponsor_not_available), 
						'dt_not_available' => $detail_sponsor_not_available
					); 				
				}  
				else
				{  				
					$r = array('success' => false);
				}
				
				
			}
		}
		
		
		
		die(json_encode(($r==null or $r=='')? array('success'=>false) : $r));	
 
	}
	
}
