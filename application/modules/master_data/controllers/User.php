<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller {
	
	protected $post_data = array();
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('model_user', 'user');
		$this->table_user = $this->user->table_user;
		
		$apps_env = apps_environtment();		
		$this->post_data = array_merge($this->post_data, $apps_env);
		
		$login_data = get_login_data();
		$this->post_data['login_data'] = $login_data;
		
	}

	public function index($is_load = '')
	{
		
		$post_data = $this->post_data;	

		$post_data['add_css_page'] = '
			<link rel="stylesheet" href="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker3.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2.min.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/select2/select2-bootstrap.css">
			<link rel="stylesheet" href="'.APP_URL.'libs/datatables/jquery.dataTables.min.css">
		';
		
		$post_data['add_js_page'] = '		
			<script src="'.APP_URL.'libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
			<script src="'.APP_URL.'libs/select2/select2.full.min.js"></script>
			<script src="'.APP_URL.'libs/masked-inputs/jquery.maskedinput.min.js"></script>
			<script src="'.APP_URL.'libs/datatables/jquery.dataTables.min.js"></script>
			<script src="'.THEME_URL.'modules/master_data/js/user.js"></script>
		';	
		
		if(!empty($is_load)){
			if($is_load == 'load'){
			
				$keyword = $this->input->post('keyword', true);
				
				$params = array(
					'keyword'	=> $keyword
				);
				$data_user = $this->user->data_user($params); 
				
				$data_user_html = '';
				if(!empty($data_user)){
					foreach($data_user as $dt){	
						
						$data_user_html .= '
						<tr>
							<td class="text-center">
								
								<input type="hidden" id="user_id_'.$dt->id.'" value="'.$dt->id.'">
								<input type="hidden" id="user_username_'.$dt->id.'" value="'.$dt->user_fullname.'">
								<a class="btn btn-xs btn-success" href="javascript:update_user('.$dt->id.');"><i class="fa fa-pencil"></i></a>	
								';
						
						if($dt->id != 1){
							
							$data_user_html .= '
							&nbsp;
							<a class="btn btn-xs btn-danger" href="javascript:delete_user('.$dt->id.');"><i class="fa fa-remove"></i></a>';
							
						}
								
							
						$data_user_html .= '	
							</td>
							<td class="hidden-xs"><small>'.$dt->user_username.'</small></td>
							<td class="hidden-xs"><small>'.$dt->role_name.'</small></td>
							<td class="hidden-xs"><small>'.$dt->user_fullname.'</small></td>
							<td class="hidden-xs"><small>'.$dt->user_email.'</small></td>
							<td class="hidden-xs"><small>'.$dt->user_mobile.'</small></td>
							<td class="visible-xs"><small>Username: '.$dt->user_username.'
								<br/>Role: '.$dt->role_name.'
								<br/>Nama: '.$dt->user_fullname.'
								<br/>HP: '.$dt->user_mobile.'
								<br/>Email: '.$dt->user_email.'
							</small></td>
						</tr>
						';
					}
				}
				
				echo '
				<table class="table table-bordered table-striped js-dataTable-full" id="table-user">
					<thead>
						<tr>
							<th class="hidden-xs text-center"><small>Option</small></th>
							<th class="hidden-xs"><small>Username</small></th>
							<th class="hidden-xs"><small>Role</small></th>
							<th class="hidden-xs"><small>Nama</small></th>
							<th class="hidden-xs"><small>Email</small></th>
							<th class="hidden-xs"><small>HP</small></th>
							<th class="visible-xs"><small>Data User</small></th>
						</tr>
					</thead>
					<tbody id="user_data">
						'.$data_user_html.'
					</tbody>
				</table>
				';
				
				die();
			}
		}
		
		$params = array(
			'keyword'	=> ''
		);
		$data_roles = $this->user->data_roles($params);
		$post_data['data_roles'] = array();
		if(!empty($data_roles)){
			$post_data['data_roles'] = $data_roles;
		}
		
		//echo '<pre>';
		//print_r($data_user);
		//die();
		
		$this->load->view(THEME_VIEW_PATH.'modules/master_data/view/user', $post_data);
		
	}
	
	public function load()
	{
		$this->index('load');
	}
	
	public function add()
	{
		$data_ret = $this->user->addUpdate();
		echo json_encode($data_ret);
	}
	
	public function loadUser()
	{
		$data_ret = $this->user->loadUser();
		echo json_encode($data_ret);
	}
	
	public function deleteUser()
	{
		$data_ret = $this->user->deleteUser();
		echo json_encode($data_ret);
	}
	
}
