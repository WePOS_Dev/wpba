<?php

class Model_sponsor extends DB_Model {
	
	function __construct()
	{
		parent::__construct();
		
		$this->prefix	= config_item('db_prefix');
		$this->prefix1	= config_item('db_prefix1'); 
		$this->table_sponsor 	= $this->prefix1.'sponsor';
		$this->primary_key = 'id';
		
	}
	
	function data_sponsor($params){
		
		if(!empty($params)){
			extract($params);
		}else{
			
			$dt_return = array();
			
			return $dt_return;
		}
		
		$this->db->select('a.*');
		$this->db->from($this->table_sponsor.' as a');
		
		if(!empty($keyword)){
			$this->db->where("a.sponsor_data LIKE '%".$keyword."%' OR a.contact_person LIKE '%".$keyword."%' OR a.mobile_phone LIKE '%".$keyword."%' OR a.email LIKE '%".$keyword."%'");
		}
		
		$this->db->where("a.is_deleted = 0");
		
		$get_sponsor = $this->db->get();
		
		$data_sponsor = array();
		if($get_sponsor->num_rows() > 0){
			foreach($get_sponsor->result() as $dt_sponsor){
				
				$data_sponsor[] = $dt_sponsor;
			}
		}
		
		$dt_return = $data_sponsor;
		
		return $dt_return;
	}
	
	function addUpdate(){
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Tambah Data Gagal!'
		);
		
		$login_data = get_login_data();
		
		$sponsor_data = $this->input->post('sponsor_data', true);
		$contact_person = $this->input->post('contact_person', true);
		$mobile_phone = $this->input->post('mobile_phone', true);
		$email = $this->input->post('email', true);
		$is_edit = $this->input->post('is_edit', true);
		$sponsor_id = $this->input->post('sponsor_id', true);
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Tambah Data Gagal!'
		);
		
		if(empty($is_edit)){
			$save_data = array(	
				'sponsor_data'		=> $sponsor_data,
				'contact_person'	=> $contact_person,
				'mobile_phone'		=> $mobile_phone,
				'email'		=> $email,
				'createdby'	=> $login_data['user_username'],
				'created'	=> date("Y-m-d H:i:s"),
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			$save = $this->db->insert($this->table_sponsor, $save_data);
			if($save){			
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Tambah Data Selesai!'
				);
			}
		}else{
			$save_data = array(	
				'sponsor_data'		=> $sponsor_data,
				'contact_person'	=> $contact_person,
				'mobile_phone'		=> $mobile_phone,
				'email'		=> $email,
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			
			if(!empty($sponsor_id)){
				$save = $this->db->update($this->table_sponsor, $save_data, "id = ".$sponsor_id);
				
				if($save){			
					$data_ret = array(
						'success'	=> true,
						'info'	=> 'Update Data Selesai!'
					);
				}
				
			}else{
				$data_ret = array(
					'success'	=> false,
					'info'	=> 'Update Data Gagal!'
				);
			}
			
		}
		
		return $data_ret;
	}
	
	function loadSponsor(){
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Load Data Gagal!'
		);
		
		$login_data = get_login_data();
		
		$sponsor_data = $this->input->post('sponsor_data', true);
		$sponsor_id = $this->input->post('sponsor_id', true);
		
		if(!empty($sponsor_id)){
			
			$this->db->select('a.*');
			$this->db->from($this->table_sponsor.' as a');
			$this->db->where("a.id = ".$sponsor_id);
			
			$load = $this->db->get();
			
			if($load->num_rows() > 0){	
	
				$dt_sponsor = $load->row();
				
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Load Data Selesai!',
					'data'	=> $dt_sponsor
				);
			}
			
		}else{
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Load Data Gagal!'
			);
		}
		
		return $data_ret;
		
	}
	
	function deleteSponsor(){
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Delete Data Gagal!'
		);
		
		$login_data = get_login_data();
		
		$sponsor_data = $this->input->post('sponsor_data', true);
		$sponsor_id = $this->input->post('sponsor_id', true);
		
		if(!empty($sponsor_id)){
			
			$update_sponsor = array('is_deleted' => 1);
			$save = $this->db->update($this->table_sponsor, $update_sponsor, "id = ".$sponsor_id);
			
			if($save){			
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Delete Data Selesai!'
				);
			}
			
		}else{
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Delete Data Gagal!'
			);
		}
		
		return $data_ret;
		
	}
	
}
