<?php

class Model_user extends DB_Model {
	
	function __construct()
	{
		parent::__construct();
		
		$this->prefix	= config_item('db_prefix');
		$this->table_user 	= $this->prefix.'users';
		$this->table_roles 	= $this->prefix.'roles';
		$this->primary_key = 'id';
		
	}
	
	function data_roles($params){
		if(!empty($params)){
			extract($params);
		}else{
			
			$dt_return = array();
			
			return $dt_return;
		}
		
		$this->db->select('a.*');
		$this->db->from($this->table_roles.' as a');
		
		$this->db->where("a.is_deleted = 0");
		
		if(!empty($keyword)){
			$this->db->where("a.role_name LIKE '%".$keyword."%'");
		}
		
		$get_roles = $this->db->get();
		
		$data_roles = array();
		if($get_roles->num_rows() > 0){
			foreach($get_roles->result() as $dt_roles){
				
				$data_roles[] = $dt_roles;
			}
		}
		
		$dt_return = $data_roles;
		
		return $dt_return;
	}
	
	function data_user($params){
		
		if(!empty($params)){
			extract($params);
		}else{
			
			$dt_return = array();
			
			return $dt_return;
		}
		
		$this->db->select('a.*, b.role_name');
		$this->db->from($this->table_user.' as a');
		$this->db->join($this->table_roles.' as b', "b.id = a.role_id", "LEFT");
		
		$this->db->where("a.is_deleted = 0");
		
		if(!empty($keyword)){
			$this->db->where("a.user_username LIKE '%".$keyword."%' OR a.user_fullname LIKE '%".$keyword."%'");
		}
		
		$get_user = $this->db->get();
		
		$data_user = array();
		if($get_user->num_rows() > 0){
			foreach($get_user->result() as $dt_user){
				
				$data_user[] = $dt_user;
			}
		}
		
		$dt_return = $data_user;
		
		return $dt_return;
	}
	
	function addUpdate(){
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Tambah Data Gagal!'
		);
		
		$login_data = get_login_data();
		
		$user_username = $this->input->post('user_username', true);
		$user_password = $this->input->post('user_password', true);
		$role_id = $this->input->post('role_id', true);
		$user_fullname = $this->input->post('user_fullname', true);
		$user_email = $this->input->post('user_email', true);
		$user_phone = $this->input->post('user_phone', true);
		$user_mobile = $this->input->post('user_mobile', true);
		$user_address = $this->input->post('user_address', true);
		$is_edit = $this->input->post('is_edit', true);
		$user_id = $this->input->post('user_id', true);
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Tambah Data Gagal!'
		);
		
		if(empty($is_edit)){
			
			if(empty($user_password)){
				$user_password = md5(123);
			}else{
				$user_password = md5($user_password);
			}
			
			$save_data = array(	
				'user_username'	=> $user_username,
				'user_password'	=> $user_password,
				'role_id'		=> $role_id,
				'user_fullname'	=> $user_fullname,
				'user_email'	=> $user_email,
				'user_phone'	=> $user_phone,
				'user_mobile'	=> $user_mobile,
				'user_address'	=> $user_address,
				'createdby'	=> $login_data['user_username'],
				'created'	=> date("Y-m-d H:i:s"),
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			$save = $this->db->insert($this->table_user, $save_data);
			if($save){			
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Tambah Data Selesai!'
				);
			}
		}else{
			$save_data = array(	
				'user_username'	=> $user_username,
				'role_id'		=> $role_id,
				'user_fullname'	=> $user_fullname,
				'user_email'	=> $user_email,
				'user_phone'	=> $user_phone,
				'user_mobile'	=> $user_mobile,
				'user_address'	=> $user_address,
				'updatedby'	=> $login_data['user_username'],
				'updated'	=> date("Y-m-d H:i:s")
			);
			
			if(!empty($user_password)){
				$save_data['user_password'] = md5($user_password);
			}
			
			if(!empty($user_id)){
				$save = $this->db->update($this->table_user, $save_data, "id = ".$user_id);
				
				if($save){			
					$data_ret = array(
						'success'	=> true,
						'info'	=> 'Update Data Selesai!'
					);
				}
				
			}else{
				$data_ret = array(
					'success'	=> false,
					'info'	=> 'Update Data Gagal!'
				);
			}
			
		}
		
		return $data_ret;
	}
	
	function loadUser(){
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Load Data Gagal!'
		);
		
		$login_data = get_login_data();
		
		$username = $this->input->post('username', true);
		$user_id = $this->input->post('user_id', true);
		
		if(!empty($user_id)){
			
			$this->db->select('a.*, b.role_name');
			$this->db->from($this->table_user.' as a');
			$this->db->join($this->table_roles.' as b', "b.id = a.role_id", "LEFT");
			$this->db->where("a.id = ".$user_id);
			
			$load = $this->db->get();
			
			if($load->num_rows() > 0){	
	
				$dt_user = $load->row();
				
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Load Data Selesai!',
					'data'	=> $dt_user
				);
			}
			
		}else{
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Load Data Gagal!'
			);
		}
		
		return $data_ret;
		
	}
	
	function deleteUser(){
		
		$data_ret = array(
			'success'	=> false,
			'info'	=> 'Delete Data Gagal!'
		);
		
		$login_data = get_login_data();
		
		$username = $this->input->post('username', true);
		$user_id = $this->input->post('user_id', true);
		
		if(!empty($user_id)){
			$save = $this->db->delete($this->table_user, "id = ".$user_id);
			
			if($save){			
				$data_ret = array(
					'success'	=> true,
					'info'	=> 'Delete Data Selesai!'
				);
			}
			
		}else{
			$data_ret = array(
				'success'	=> false,
				'info'	=> 'Delete Data Gagal!'
			);
		}
		
		return $data_ret;
		
	}
	
}
