<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends MX_Controller{
	
	public $prefix;
	private $parameters;
	private $rest_parameters;
	protected $use_session_check = TRUE;
	
	protected $id_user;
	
	function MY_Controller(){
		parent::__construct();
		
		$this->prefix = config_item('db_prefix');
		
		if($this->use_session_check == TRUE){
			$this->session_check();		
			$this->id_user	= $this->session->userdata('id_user');
		}		
		
		$this->default_value();
	}
	
	function default_value()
	{
		$this->set_value('base_url', base_url());
	}
	
	function set_value($key,$value)
	{
		$this->parameters[$key] = $value;
	}
	
	function get_parameters($key = '')
	{
		if(!empty($key)){
			if(empty($this->parameters[$key])){
				$this->parameters[$key] = 0;
			}
			return $this->parameters[$key];
		}else
		{
			return $this->parameters;
		}
		
	}

	function session_check()
	{		
		if($this->session->userdata('id_user') != '')
		{
			return true;
		}
		else 
		{
			redirect(BASE_URL.'login', 'refresh');
		}
	}
	
}
